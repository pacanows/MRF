#########################################################################################
#
# Author:  Arthur Dufay            Copyright INRIA : 2018
# Author:  Romain Pacanowski       Copyright CNRS  : 2018
#
#########################################################################################

###########################################MRF OPTIX BACKEND TEST SUITE#################
#
#
#  Render a set of scenes from MRF/assets/scenes to benchmark the optix backend.
#  This test suite is also helpfull to assert that no regression have been introduced
#  by a new feature on the optix backend.
#
#
#  All results  are written in first arg of the script
#   or in $MRF_DIR/benchmark/optix_backend  if no arg is given
#
#
#########################################################################################


render_on_plafrim = False
wavelength_range = "380:830:10"
scenes = []
scenes_spectral = []

scenes.append(["cornellbox","1000",False,"halton"]) # lambert, area light
scenes.append(["color_checker","100",False,"halton"]) # directionnal light
#scenes.append(["luxrender_prism","10000",False,"halton"]) # fresnel glass
#scenes.append(["luxrender_prism","10000",False,"sobol"]) # fresnel glass
#scenes.append(["ring","10000",False,"halton"]) #point light
scenes.append(["material_probe/Ag_Babar","1000",False,"halton"]) #ggx, checkerboard
scenes.append(["material_probe/Au_Babar","10000",False,"halton"])
scenes.append(["material_probe/Au_Babar","10000",False,"sobol"])
scenes.append(["material_probe/Au_McPeak","1000",False,"halton"])
scenes.append(["material_probe/Au_McPeakFresnelMirror","1000",False,"halton"]) #fresnel mirror
scenes.append(["material_probe/fresnel_glass","1000",False,"halton"]) #fresnel glass
scenes.append(["material_probe/fresnel_mirror","1000",False,"halton"]) #fresnel mirror
scenes.append(["material_probe/lambert","1000",False,"halton"])
scenes.append(["material_probe/perfect_mirror","1000",False,"halton"])
scenes.append(["material_probe/perfect_mirror","1000",False,"sobol"])
scenes.append(["sphere_on_plane/sphere_ggx0.02_on_plane","1000",False,"halton"])
scenes.append(["sphere_on_plane/sphere_ggx0.5_on_plane","1000",False,"halton"])
scenes.append(["sphere_on_plane/sphere_on_plane","1000",False,"halton"])
scenes.append(["fiat500/fiat500","1000",False,"halton"])

#perfect mirror
#phong
#phong textured

#scenes.append(["cornellbox","100",True,"halton"])
#scenes.append(["color_checker","100",True,"halton"])
#scenes.append(["luxrender_prism","10000",True,"halton"])
#scenes.append(["luxrender_prism","10000",True,"sobol"])
#scenes.append(["ring","10000",True,"halton"])
# scenes.append(["material_probe/Ag_Babar","1000",True,"halton"])
# scenes.append(["material_probe/Au_Babar","10000",True,"halton"])
# scenes.append(["material_probe/Au_Babar","10000",True,"sobol"])
# scenes.append(["material_probe/Au_McPeak","1000",True,"halton"])
# scenes.append(["material_probe/Au_McPeakFresnelMirror","1000",True,"halton"])
# scenes.append(["material_probe/fresnel_glass","1000",True,"halton"])
# scenes.append(["cube_glass","1000",True,"halton"])
# scenes.append(["material_probe/fresnel_mirror","1000",True,"halton"])
# scenes.append(["material_probe/lambert","1000",True,"halton"])
# scenes.append(["material_probe/perfect_mirror","1000",True,"halton"])
# scenes.append(["material_probe/perfect_mirror","1000",True,"sobol"])
# scenes.append(["sphere_on_plane/sphere_ggx0.02_on_plane","1000",True,"halton"])
# scenes.append(["sphere_on_plane/sphere_ggx0.5_on_plane","1000",True,"halton"])
# scenes.append(["sphere_on_plane/sphere_on_plane","1000",True,"halton"])
#scenes.append(["material_probe/effective_thin_film_Ag_R90nm_d0nm_glass_dens2.50_PURE_DIRAC","1000","halton"])
#measured isotropic dirac only
#measured isotropic
#scenes.append(["fiat500/fiat500","1000",True,"halton"])


#SPECTRAL TESTS
#scenes_spectral.append(["cornellbox","100",False,"halton"]) # lambert, area light
#scenes_spectral.append(["material_probe/effective_thin_film_Ag_R90nm_d0nm_glass_dens2.50_PURE_DIRAC","100",False,"halton"])

#scenes_spectral.append(["cornellbox","100",True,"halton"]) # lambert, area light
#scenes_spectral.append(["material_probe/effective_thin_film_Ag_R90nm_d0nm_glass_dens2.50_PURE_DIRAC","100",True,"halton"])
#scenes_spectral.append(["material_probe/effective_thin_film_Ag_R90nm_d0nm_glass_dens2.50_PURE_DIRAC","100",False,"halton"])


import platform
import shutil
import os,sys
import re
import subprocess
import time
import xml.etree.ElementTree as ET
from shutil import copyfile
from shutil import move
import xml.etree.ElementTree as ET
import glob

##########################################################################################
# CHECK THAT MANDATORY ENV VARIABLES are set
if os.environ.get("MRF_DIR") is None:
    print("[ERROR] THE MRF environment variable is not set. FIX THAT!!!")
    exit(-1)

MRF_DIR = os.environ.get("MRF_DIR")
##########################################################################################


def render_scene(scene, renderer, output_folder, render_on_plafrim, wavelength_range):
    scene_file_fullpath = MRF_DIR + "/assets/scenes/" + scene[0] + ".msf"
    print("Now renders "+scene_file_fullpath)

    next_event = scene[2]

    #Render the scene
    output_image = output_folder + scene[0].replace("/","_") + "_";

    output_image = output_image + scene[3] + "_" +  str(scene[1]);
    if next_event:
         output_image = output_image + "spp_NE.exr"
    else:
        output_image = output_image + "spp_NO_NE.exr"


    renderer_args = []

    if render_on_plafrim:    
        renderer_args.append("srun")
    
    renderer_args.append(renderer)
    renderer_args.append("-scene")
    renderer_args.append(scene_file_fullpath)
    renderer_args.append("-wr")
    renderer_args.append(wavelength_range)
    renderer_args.append("-samples")
    renderer_args.append(scene[1])
    renderer_args.append("-o")
    renderer_args.append(output_image)
    renderer_args.append("-next_event")
    
    renderer_args.append("-logging")
    renderer_args.append("-Trace")

    if next_event:
      renderer_args.append("one")
    else:
      renderer_args.append("none")

    renderer_args.append("-rng")
    renderer_args.append(scene[3])
    

    #if spectral_rendering:
    #    output_image = output_image[:-4] + ".hdr"

    process_renderer = subprocess.Popen(renderer_args)

    while process_renderer.poll() is None:
        time.sleep( 10 )

    print("END RENDER Exited with returncode %d" % process_renderer.returncode)


    if render_on_plafrim:
        for f in os.listdir("."):
            if re.search("^core\\.[0-9]+$", f):
                os.remove(os.path.join(".",f))






####render using subprocess.Popen
renderer = ""
renderer_spectral = MRF_DIR+"/bin/OPTIX_RENDERER_SPECTRAL"
renderer = MRF_DIR+"/bin/OPTIX_RENDERER"

if platform.system()=="Windows":
    renderer_spectral = renderer_spectral + ".exe"
    renderer = renderer + ".exe"

##########################################################################################
# CHECK THAT THE RENDERER (Rendering engine does exist)
##########################################################################################

if (os.path.isfile(renderer) == False) or (os.path.isfile(renderer_spectral) == False):
    print("[ERROR] Rendering engine could NOT be found. Check your PATH !")
    print("[ERROR] Current path for the renderer is:"+renderer)
    exit(-1)


output_folder = MRF_DIR + "/benchmarks/optix_backend/"    

if len(sys.argv)>=2 :
    output_folder = sys.argv[1]

if(output_folder[-1:]!="/" and output_folder[-1:]!="\\"):
    output_folder = output_folder + "/"


#loop on scenes
for scene in scenes:
    render_scene(scene, renderer, output_folder, render_on_plafrim, wavelength_range)
#loop on scenes
for scene in scenes_spectral:
    render_scene(scene, renderer_spectral, output_folder, render_on_plafrim, wavelength_range)



# end_if_merge_images
print("END OF SCRIPT REACHED")
