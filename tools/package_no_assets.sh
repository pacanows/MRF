################################################
## Author # Arthur Dufay
##
## Create an archive to deploy MRF
################################################

#!bin/sh

tar czvf MRF.tar.gz cmake/* externals/* mdl/* mrf/* tools/* apps/iray\ renderer/CMakeLists.txt apps/iray\ renderer/main.cpp
