#########################################################################################
#
# Author:  Arthur Dufay            Copyright INRIA : 2018
#
#########################################################################################

#########################################################################################
#
#
#  Multiply all the values of a ENVI file by another: for each pixel, for each wavelength
#  the output image will contain file_1 * file_2
#  
#
#  Script input parameters are: file_1.hdr file_2.hdr file_out.hdr
#
#  The ENVI files should have the same pixel resolution.
#    
#  The wavelengths of each file must be the same.
#
#########################################################################################

import sys
import os

import envi_parsing as ev

import struct
import copy


def merge_envi_header(path_1,path_2,path_3):
  header_1 = ev.parse_envi_header(path_1)
  header_2 = ev.parse_envi_header(path_2)

  #header = header_1 + header_2
  header = copy.copy(header_1)
  header.path = path_3

  if header is not None:
    print(header.waves)

    header.write_file()
    multiply_data(header_1, header_2, header)

  else:
    print("ERROR CAN'T MERGE HEADERS")


def multiply_data(header_1,header_2,header_3):
  path_1 = os.path.splitext(header_1.path)[0] + ".raw"
  path_2 = os.path.splitext(header_2.path)[0] + ".raw"
  path_3 = os.path.splitext(header_3.path)[0] + ".raw"

  f1 = open(path_1, 'rb')
  f2 = open(path_2, 'rb')
  f3 = open(path_3, "wb") # open in `w` mode to write


  
  while True:
    b = f1.read(4)
    c = f2.read(4)
    if not b or not c:
      # eof
      break
    else:
      (float_val,) = struct.unpack('f',b)
      (float_val_2,) = struct.unpack('f',c)
      float_val = float_val * float_val_2
      #print(float_val)
      f3.write(struct.pack('f',float_val))

  f3.close()
  f1.close()
  f2.close()




def help():
  print("Usage: "+sys.argv[0]+" file_1.hdr file_2.hdr file_out.hdr")
  print("File_out.hdr will contain the multiplication of file_1 by file_2")


if(len(sys.argv)!=4):
  help()
else:
  merge_envi_header(sys.argv[1],sys.argv[2],sys.argv[3])

