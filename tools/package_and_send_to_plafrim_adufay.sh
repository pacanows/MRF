############################################################
## Author # Arthur Dufay
##
## Create an archive to deploy MRF and send it to plafrim
############################################################

#!bin/sh

tar czf MRF.tar.gz cmake/* externals/* mdl/* mrf/* tools/* apps/iray\ renderer/CMakeLists.txt apps/iray\ renderer/main.cpp
scp MRF.tar.gz adufay@plafrim:/home/adufay/
ssh plafrim tar xzf MRF.tar.gz