@echo off

if not exist "C:\Dev\Musaq\reconstruction\direct\" mkdir C:\Dev\Musaq\reconstruction\direct
if not exist "C:\Dev\Musaq\reconstruction\ndotl\" mkdir C:\Dev\Musaq\reconstruction\ndotl

for %%a in ("C:\Dev\Musaq\mapping\*.exr") do (
echo Processing %%a
start /wait image_laplacian_filling.exe %%a 10000 0 1 0.3 14
echo Finished direct diffusion for %%a
start /wait image_laplacian_filling.exe %%a 10000 1 1 0.3 14
echo Finished bdrf x ndotl diffusion for %%a
)
move /Y "C:\Dev\Musaq\mapping\*_direct*.exr" "C:\Dev\Musaq\reconstruction\direct"
move /Y "C:\Dev\Musaq\mapping\*_ndotl*.exr" "C:\Dev\Musaq\reconstruction\ndotl"

echo Generated unnormalized slices, normalizing...

if not exist "C:\Dev\Musaq\reconstruction\direct_norm\" mkdir C:\Dev\Musaq\reconstruction\direct_norm

echo Processing direct data.
for %%a in ("C:\Dev\Musaq\reconstruction\direct\*.exr") do (
echo Processing %%a
start /wait brdf_slice_normalization.exe %%a
echo Normalized %%a
)
move /Y "C:\Dev\Musaq\reconstruction\direct\*_norm.exr" "C:\Dev\Musaq\reconstruction\direct_norm"

if not exist "C:\Dev\Musaq\reconstruction\ndotl_norm\" mkdir C:\Dev\Musaq\reconstruction\ndotl_norm
for %%a in ("C:\Dev\Musaq\reconstruction\ndotl\*.exr") do (
echo Processing %%a
start /wait brdf_slice_normalization.exe %%a
echo Normalized %%a
)
move /Y "C:\Dev\Musaq\reconstruction\ndotl\*_norm.exr" "C:\Dev\Musaq\reconstruction\ndotl_norm"


REM for %%a in ("C:\Dev\Musaq\reconstruction\*_norm\*.exr") do (
REM echo Processing %%a
REM start /wait SpectralViewer.exe %%a -o "C:\Dev\Musaq\reconstruction\%%~na.png"
REM echo Normalized %%a
REM )

pause
