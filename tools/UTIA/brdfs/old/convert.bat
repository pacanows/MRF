@echo off

start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\fast_gold_7_9_filled_10000_0_0_1.exr" -o "C:\Dev\Data\measured\png\7_9_direct.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\fast_gold_7_9_filled_10000_0_1_1.exr" -o "C:\Dev\Data\measured\png\7_9_ndotl.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\fast_gold_7_9_filled_10000_1_1_1.exr" -o "C:\Dev\Data\measured\png\7_9_full.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\fast_gold_7_9_filled_10000_1_0_1.exr" -o "C:\Dev\Data\measured\png\7_9_prefilter.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0

start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\robust_leader_gold_7_9_filled_10000_0_0_1.exr" -o "C:\Dev\Data\measured\png\7_9_robust_direct.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\robust_leader_gold_7_9_filled_10000_0_1_1.exr" -o "C:\Dev\Data\measured\png\7_9_robust_ndotl.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\robust_leader_gold_7_9_filled_10000_1_1_1.exr" -o "C:\Dev\Data\measured\png\7_9_robust_full.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\robust_leader_gold_7_9_filled_10000_1_0_1.exr" -o "C:\Dev\Data\measured\png\7_9_robust_prefilter.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0

start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_4_LED1_2_3_4_c5_dir_0_filled_10000_0_0_1.exr" -o "C:\Dev\Data\measured\png\1_4_direct.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_4_LED1_2_3_4_c5_dir_0_filled_10000_0_1_1.exr" -o "C:\Dev\Data\measured\png\1_4_ndotl.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_4_LED1_2_3_4_c5_dir_0_filled_10000_1_1_1.exr" -o "C:\Dev\Data\measured\png\1_4_full.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_4_LED1_2_3_4_c5_dir_0_filled_10000_1_0_1.exr" -o "C:\Dev\Data\measured\png\1_4_prefilter.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0

start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_5_LED1_2_3_4_c5_dir_0_filled_10000_0_0_1.exr" -o "C:\Dev\Data\measured\png\1_5_direct.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_5_LED1_2_3_4_c5_dir_0_filled_10000_0_1_1.exr" -o "C:\Dev\Data\measured\png\1_5_ndotl.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_5_LED1_2_3_4_c5_dir_0_filled_10000_1_1_1.exr" -o "C:\Dev\Data\measured\png\1_5_full.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_5_LED1_2_3_4_c5_dir_0_filled_10000_1_0_1.exr" -o "C:\Dev\Data\measured\png\1_5_prefilter.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0

start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_6_LED1_2_3_4_c5_dir_0_filled_10000_0_0_1.exr" -o "C:\Dev\Data\measured\png\1_6_direct.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_6_LED1_2_3_4_c5_dir_0_filled_10000_0_1_1.exr" -o "C:\Dev\Data\measured\png\1_6_ndotl.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_6_LED1_2_3_4_c5_dir_0_filled_10000_1_1_1.exr" -o "C:\Dev\Data\measured\png\1_6_full.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_1_6_LED1_2_3_4_c5_dir_0_filled_10000_1_0_1.exr" -o "C:\Dev\Data\measured\png\1_6_prefilter.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0

start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_5_6_LED1_2_3_4_c5_dir_0_filled_10000_0_0_1.exr" -o "C:\Dev\Data\measured\png\5_6_direct.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_5_6_LED1_2_3_4_c5_dir_0_filled_10000_0_1_1.exr" -o "C:\Dev\Data\measured\png\5_6_ndotl.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_5_6_LED1_2_3_4_c5_dir_0_filled_10000_1_1_1.exr" -o "C:\Dev\Data\measured\png\5_6_full.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_5_6_LED1_2_3_4_c5_dir_0_filled_10000_1_0_1.exr" -o "C:\Dev\Data\measured\png\5_6_prefilter.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0

start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_7_4_LED1_2_3_4_c5_dir_0_filled_10000_0_0_1.exr" -o "C:\Dev\Data\measured\png\7_4_direct.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_7_4_LED1_2_3_4_c5_dir_0_filled_10000_0_1_1.exr" -o "C:\Dev\Data\measured\png\7_4_ndotl.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_7_4_LED1_2_3_4_c5_dir_0_filled_10000_1_1_1.exr" -o "C:\Dev\Data\measured\png\7_4_full.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_7_4_LED1_2_3_4_c5_dir_0_filled_10000_1_0_1.exr" -o "C:\Dev\Data\measured\png\7_4_prefilter.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0

start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_7_5_c5_LED4_fast_filled_10000_0_0_1.exr" -o "C:\Dev\Data\measured\png\7_5_direct.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_7_5_c5_LED4_fast_filled_10000_0_1_1.exr" -o "C:\Dev\Data\measured\png\7_5_ndotl.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_7_5_c5_LED4_fast_filled_10000_1_1_1.exr" -o "C:\Dev\Data\measured\png\7_5_full.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\Data\measured\filled\selection_7_5_c5_LED4_fast_filled_10000_1_0_1.exr" -o "C:\Dev\Data\measured\png\7_5_prefilter.png" --contrast 0.8 --intensity 10.0 --chromatic_adaptation 0.5 --light_adaptation 0.0

pause
