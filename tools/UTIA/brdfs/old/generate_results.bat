@echo off

REM start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\7_9.exr" 10000 0 0.75 10 0
REM start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\7_9.exr" 10000 1 0.75 10 0

REM start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\7_9_robust.exr" 10000 0 0.75 10 0
REM start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\7_9_robust.exr" 10000 1 0.75 10 0

start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\1_4.exr" 10000 0 0.75 10 0
start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\1_4.exr" 10000 1 0.75 10 0

start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\1_5.exr" 10000 0 0.75 10 0
start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\1_5.exr" 10000 1 0.75 10 0

start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\1_6.exr" 10000 0 0.75 10 0
start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\1_6.exr" 10000 1 0.75 10 0

start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\5_6.exr" 10000 0 0.75 10 0
start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\5_6.exr" 10000 1 0.75 10 0

start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\7_4.exr" 10000 0 0.75 10 0
start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\7_4.exr" 10000 1 0.75 10 0

start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\7_5.exr" 10000 0 0.75 10 0
start /wait image_laplacian_filling.exe "C:\Dev\DATA\brdfs\7_5.exr" 10000 1 0.75 10 0
