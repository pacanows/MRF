@echo off

start /wait cmd /c generate_results.bat
start /wait cmd /c normalize.bat
start /wait cmd /c convert_norm.bat

if not exist "C:\Dev\DATA\brdfs\filled\" mkdir C:\Dev\DATA\brdfs\filled
move /Y "C:\Dev\DATA\brdfs\*_ndotl.exr" "C:\Dev\DATA\brdfs\filled"
move /Y "C:\Dev\DATA\brdfs\*_direct.exr" "C:\Dev\DATA\brdfs\filled"

if not exist "C:\Dev\DATA\brdfs\norm\" mkdir C:\Dev\DATA\brdfs\norm
move /Y "C:\Dev\DATA\brdfs\*_norm.exr" "C:\Dev\DATA\brdfs\norm"

if not exist "C:\Dev\DATA\brdfs\png\" mkdir C:\Dev\DATA\brdfs\png
move /Y "C:\Dev\DATA\brdfs\*.png" "C:\Dev\DATA\brdfs\png"

pause
