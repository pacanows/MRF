@echo off

REM start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\7_9_direct_norm.exr" -o "C:\Dev\DATA\brdfs\7_9_direct.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
REM start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\7_9_ndotl_norm.exr" -o "C:\Dev\DATA\brdfs\7_9_ndotl.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0


REM start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\7_9_robust_direct_norm.exr" -o "C:\Dev\DATA\brdfs\7_9_robust_direct.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
REM start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\7_9_robust_ndotl_norm.exr" -o "C:\Dev\DATA\brdfs\7_9_robust_ndotl.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0


start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\1_4_direct_norm.exr" -o "C:\Dev\DATA\brdfs\1_4_direct.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\1_4_ndotl_norm.exr" -o "C:\Dev\DATA\brdfs\1_4_ndotl.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0


start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\1_5_direct_norm.exr" -o "C:\Dev\DATA\brdfs\1_5_direct.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\1_5_ndotl_norm.exr" -o "C:\Dev\DATA\brdfs\1_5_ndotl.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0


start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\1_6_direct_norm.exr" -o "C:\Dev\DATA\brdfs\1_6_direct.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\1_6_ndotl_norm.exr" -o "C:\Dev\DATA\brdfs\1_6_ndotl.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0


start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\5_6_direct_norm.exr" -o "C:\Dev\DATA\brdfs\5_6_direct.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\5_6_ndotl_norm.exr" -o "C:\Dev\DATA\brdfs\5_6_ndotl.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0


start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\7_4_direct_norm.exr" -o "C:\Dev\DATA\brdfs\7_4_direct.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\7_4_ndotl_norm.exr" -o "C:\Dev\DATA\brdfs\7_4_ndotl.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0


start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\7_5_direct_norm.exr" -o "C:\Dev\DATA\brdfs\7_5_direct.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
start /wait SpectralViewer.exe "C:\Dev\DATA\brdfs\7_5_ndotl_norm.exr" -o "C:\Dev\DATA\brdfs\7_5_ndotl.png" --contrast 0.5 --intensity 1.0 --chromatic_adaptation 0.5 --light_adaptation 0.0
