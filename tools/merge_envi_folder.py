###########################################MERGE ENVI FOLDER#################
#
#
#  Merge all the ENVI files (couple f .hdr and .raw with the same name)
#  present in a directory.
#
#  Script input parameters are: path_to_folder image_output_name
#
#  The ouput image is save in the path_to_folder
#  
#  The script merges the files wavelengths data, not the pixel data.
#  For instance two files with 500spp and with the wavelengths
#  (500,510,520) and (530,540,550) will output an ENVI file
#  at 500spp with wavelengths (500,510,520,530,540,550).
#
#  All ENVI files should have the same pixel resolution.
#    
#  The number of wavelengths sampled in each file can vary.
#
#  The ENVI files should not have any wavelength overlap
#  for instance merging two files with the wavelengths
#  (500,550,600) and (510,520,530) will NOT work.
#
#
#########################################################################################


import sys
import os
import platform

import envi_parsing as ev



def help():
  print("Usage: "+sys.argv[0]+" path_to_folder path_to_output_image")



image_files = []

if(len(sys.argv)<3):
  help()
  exit()

if(os.path.isdir(sys.argv[1])==False):
  print("Specified path is not a directory or does not exists")
  exit()
else:
  print("[INFO]  The dir where the images to merge are is set to:" + sys.argv[1])

current_dir = os.getcwd()
os.chdir(sys.argv[1])

output = sys.argv[2]

delete_temp_images = False
if(len(sys.argv)>3):
  if(sys.argv[3]=="True"):
    delete_temp_images = True

#if(os.path.exists(output))

#adds .hdr extension if not present
if( len(output)<5 or output[-4:]!=".hdr"):
  output = output + ".hdr"




#First found the image files in the directory
for dirname, dirnames, filenames in os.walk('.'):
    for filename in filenames:
        if(filename.endswith(".hdr")):
            image_files.append(filename)
    #avoid recursivity
    break


if(len(image_files)==0):
  print("Cannot find any .hdr files in the directory")
  exit()


header_files_list = []

#first construct a list over the header files
for img_path in image_files:
  header_1 = ev.parse_envi_header(img_path)
  header_files_list.append(header_1)



#sort the list based on the first wavelength present in the file
header_files_list = sorted(header_files_list, key=lambda header: header.waves[0])

print("The following files will be merged together")
for header in header_files_list:
  #print(header)
  print(header.path)
  print("     waves = "+str(header.waves))
  print("")
  
print("into "+output)
print("")


output_header = ev.Header()

output_data_path = output[:-3] + "raw"

output_data = open(output_data_path, "wb") # open in `w` mode to write

img_num = 0
for input_header in header_files_list:

  #first file merged, assign header instead of sum
  if img_num==0:
    output_header = input_header
  else:
    output_header = output_header + input_header


  #merge pixels data
  input_data_path = input_header.path[:-3] + "raw"

  f1 = open(input_data_path,"rb")
  f1_contents = f1.read()
  output_data.write(f1_contents) 
  f1.close()

  img_num = img_num + 1


output_data.close()
output_header.path = output
output_header.write_file()

if delete_temp_images:
  for input_header in header_files_list:
    os.remove(input_header.path)
    input_data_path = input_header.path[:-3] + "raw"
    os.remove(input_data_path)


print("Merge SUCCEED, output wavelengths are:")
print(output_header.waves)

