#########################################################################################
#
# Author:  Arthur Dufay            Copyright INRIA : 2018
# Author:  Romain Pacanowski       Copyright CNRS  : 2018
#
#########################################################################################

###########################################RENDERED MEASURED BRDF FOLDER#################
#
#
#  Render a scene for each  Spectral BRDF (or Material) inside the current folder.
#
#  The spectral BRDF is assumed to be a data-driven BRDF (either coming from
#  a simulation or a measurement process)
#
#
#  The script and the scene must be in the same folder.
#
#  If specified, the first argument of the script specifies the name of the material in the
#  msf file that will be changed.
#
#
#  The folder should not contain any other file with a name ending
#  with .msf (reserved for the scene file) or .bin(reserved for the measured materials).
#
#  The camera file should have the same name than the msf scene file.
#
#  All the measured materials should be in the same folder and have filename
#  ending with .bin.
#
#  Number of samples, wavelength can be change with the variables below
#########################################################################################


import platform

RENDERER_NAME = "malia"

spectral_rendering = True
remove_temp_scene = True
wavelength_range = "380:830:10"
scene_file = ""
brdf_file = []
#choose if camera name must appeared on prefix (True) or suffix (False)
#of the final image name (the merged one)
final_image_name_camera_name_on_prefix = False

# This executable must be in the Path
SPECTRAL_CONVERTER = "SPECTRAL_IMAGE_CONVERTER"
if platform.system()=="Windows":
    SPECTRAL_CONVERTER = SPECTRAL_CONVERTER + ".exe"


# Variable below can be overriden by using the  command line
# First Option
material_name_to_modify = "measured_material"
# Second Option
nb_samples = "1"
#Third Option
export_to_EXR = False
#Fourth Option
export_to_PNG = False
#Fifth Option
render_on_plafrim = True
#Sith Option
exposition_value   = "1.0"
#Seventh option
gamma_value        = "1.0"

import shutil
import os,sys
import re
import subprocess
import time
import xml.etree.ElementTree as ET
from shutil import copyfile
from shutil import move
import xml.etree.ElementTree as ET
import glob


##########################################################################################
######################################## START ############################################
##########################################################################################
if len(sys.argv)>=2 :
    material_name_to_modify = sys.argv[1]

print("\n[INFO]  Split Material name is: " + material_name_to_modify +"\n")

if len(sys.argv) >=3 :
    nb_samples = sys.argv[2]

print("\n[INFO] Number of Rays/Samples per pixel: " + nb_samples +"\n")

if len(sys.argv) >=4 :
    export_to_EXR = bool( "True" in sys.argv[3] )

print("\n[INFO] Exporting to EXR: " + str(export_to_EXR) +"\n")

if len(sys.argv) >=5 :
    export_to_PNG = bool( "True" in sys.argv[4] )

print("\n[INFO] Exporting to PNG: " + str(export_to_PNG) +"\n")

if len(sys.argv) >= 6 :
    render_on_plafrim = bool( "True" in sys.argv[5] )

print( "[INFO] Rendering on Plafrim: " + str(render_on_plafrim)+"\n")

if len(sys.argv) >= 7:
    exposition_value = sys.argv[6]

print("\n[INFO] Exposition value : " + exposition_value +"\n")

if len(sys.argv) >= 8:
    gamma_value = sys.argv[7]

print("\n[INFO] Gamma value : " + gamma_value +"\n")


if export_to_EXR or export_to_PNG:
    print("[DEBUG] Checking if the spectral converter is in Path")
    # CHecking with which
    if shutil.which(SPECTRAL_CONVERTER) is None :
        print("[ERROR] The application to convert from Spectral to EXR or PNG was not found. CHECK YOUR PATH")
        exit(-1)

#end_if


##########################################################################################
# CHECK THAT MANDATORY ENV VARIABLES are set
if os.environ.get("MRF_DIR") is None:
    print("[ERROR] THE MRF environment variable is not set. FIX THAT!!!")
    exit(-1)

MRF_DIR = os.environ.get("MRF_DIR")
RENDERER_PATH = MRF_DIR +"bin/"
##########################################################################################


#First found the brdf files and the scene file (if not specified) in the current directory
for dirname, dirnames, filenames in os.walk('.'):
    for filename in filenames:
        if(filename.endswith(".bin") or filename.endswith(".alta")):
            #print(os.path.join(dirname, filename))
            #brdf_file.append(os.path.join(dirname, filename))
            brdf_file.append(filename)

        #if no scene file have been specified, set it to the first .msf file
        #found in the current directory
        if(filename.endswith(".msf") and len(scene_file)==0):
            scene_file = filename
    #avoid recursivity
    break

brdf_file.sort()
print("Render the scene " + scene_file + " using the brdfs files: " + str(brdf_file) )

#ext_index = brdf_file[0].rfind(".")
#tmp_material_filename = brdf_file[0][0:ext_index]
#tmp_material_filename = tmp_material_filename.replace(".","_")

#print("\n###############################################################################################")
#print("[INFO] OUTPUT IMAGE name is:  " + tmp_material_filename )
#print("##################################################################################################")


current_directory = os.getcwd()
scene_file_fullpath = os.path.join(current_directory,"./"+scene_file)
scene_name = scene_file[0:scene_file.rfind(".")]

#
camera_file = scene_file[0:scene_file.rfind(".")] + ".mcf"
print("camera_file=" + str(camera_file ) )
print("Scene Name is=" + str(scene_name) )     


####render using subprocess.Popen
if spectral_rendering:
    renderer = RENDERER_PATH + RENDERER_NAME
else:
	renderer = RENDERER_PATH + RENDERER_NAME + "_rgb"

if platform.system()=="Windows":
    renderer = renderer + ".exe"

##########################################################################################
# CHECK THAT THE RENDERER (Rendering engine does exist)
##########################################################################################

if os.path.isfile(renderer) == False:
    print("[ERROR] Rendering engine could NOT be found. Check your PATH !")
    print("[ERROR] Current path for the renderer is:"+renderer)
    exit(-1)


#loop on measured material
for measured_material_file in brdf_file:
    src = scene_file_fullpath
    #dst = scene_file_fullpath[:-4] + "_" + measured_material_file[:-4] + ".msf";
    
    # RP: Hard-coding "brdf_" is a bad idea. Bug ahead when the Material file does not star with brdf_ 
    #index_in_material_name = measured_material_file.find("brdf_")+len("brdf_")
    #current_scene_filename = str("./" + measured_material_file[index_in_material_name:-(4+len("_Spectral"))] + ".msf")
    
    ext_index = measured_material_file.rfind(".")
    current_scene_filename = str("./" + scene_name +"_" + measured_material_file[0:ext_index].replace(".","_") +".msf")
    
    dst = os.path.join(current_directory, current_scene_filename )

    print("######################################### CURRENT dst= " + dst + "\n")

    copyfile(src, dst)
    #copy camera file
    src_cam = src[:-4]+".mcf"
    dst_cam = dst[:-4]+".mcf"
    copyfile(src_cam, dst_cam)

    tree = ET.parse(dst)
    root_node = tree.getroot()

    materials = root_node.find('materials')
    for material in materials:
        current_material_name = material.get('name')
        #print(material.get('name'))
        if(current_material_name==material_name_to_modify):
            material.set("file","./"+measured_material_file)

    #write the new scene with the new material (msf file)
    tree.write(dst)


    #Render the scene
    output_image = dst[:-4] + ".exr"

    # print("WAVES ARE:")
    # print(wavelength)


    renderer_args = []

    if render_on_plafrim:    
        renderer_args.append("srun")
    
    renderer_args.append(renderer)
    renderer_args.append("-scene")
    renderer_args.append(dst)
    renderer_args.append("-wr")
    renderer_args.append(wavelength_range)
    renderer_args.append("-samples")
    renderer_args.append(nb_samples)
    renderer_args.append("-o")
    renderer_args.append(output_image)
    renderer_args.append("-next_event")
    renderer_args.append("none")

    if spectral_rendering:
        output_image = output_image[:-4] + ".hdr"

    tmp_output_image = output_image[:-4].replace(".", "_")
    output_image = tmp_output_image + output_image[-3:]
    
    process_renderer = subprocess.Popen(renderer_args)

    while process_renderer.poll() is None:
      #print('Still rendering')
        #time.sleep( 1*int(nb_samples) )
        time.sleep( 15 )


    print("END RENDER Exited with returncode %d" % process_renderer.returncode)


    if render_on_plafrim:
        for f in os.listdir("."):
            if re.search("^core\\.[0-9]+$", f):
                os.remove(os.path.join(".",f))


if remove_temp_scene:
    all_msf_files = glob.glob("*.msf")
    all_mcf_files = glob.glob("*.mcf")

    # Avoid removing the orignal scene and camera files
    all_msf_files.remove(scene_file)
    all_mcf_files.remove(camera_file)

    print("\n#######################################################################")
    print("[INFO] The following temporary scene and camera files are gonna be remove:\n")
    print(str(all_msf_files))
    print(str(all_mcf_files))
    print("#########################################################################")

    [os.remove(x) for x in all_msf_files]
    [os.remove(x) for x in all_mcf_files]

#end_if_remove_temp_scene


outputs = glob.glob("*.hdr")

for output_image in outputs:
    print("\n#############################################################")
    print("[INFO] OUTPUT IMAGE name is:  " + output_image)
    print("#############################################################")
       
    subprocess_convert_list = []

    # IF EXPORT EXR
    if export_to_EXR:
        process_conv_exr = subprocess.Popen( (SPECTRAL_CONVERTER, "-in", output_image, "-out",  output_image[:-4] + ".exr") )
        subprocess_convert_list.append(process_conv_exr)
    #end_if_export_to_exr


    # IF EXPORT PNG
    if export_to_PNG:
        process_conv_png = subprocess.Popen( (SPECTRAL_CONVERTER, "-in", output_image , "-out",  output_image[:-4] + ".png", "-exposition", exposition_value, "-gamma", gamma_value) )
        subprocess_convert_list.append(process_conv_png)
    #end_if_export_to_PNG

while len(subprocess_convert_list)>0:
    for subprocess_convert in subprocess_convert_list:
        if subprocess_convert.poll() is not None:
            print("Conversion exited with returncode %d" % subprocess_convert.returncode)
            subprocess_convert_list.remove(subprocess_convert)	
    time.sleep(5)

# end_if_merge_images
print("END OF SCRIPT REACHED")
