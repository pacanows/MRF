import os

def writeSpd(fileName,spd_array):
  if len(spd_array) <= 0 :
    return
    
  with open(fileName, 'w', newline='') as fileOut:
    for x in spd_array:
      fileOut.write(str(x[0]))
      fileOut.write(":")
      fileOut.write(str(x[1]))
      fileOut.write(", ")

#remove last 2 characters (, )
  with open(fileName, 'rb+') as filehandle:
    filehandle.seek(-2, os.SEEK_END)
    filehandle.truncate()



def readSpd(fileName,spd_array):    
  with open(fileName, 'r') as fileIn:
    for line in fileIn.readlines():
      line_splitted = line.split(",")
      #print(line)
      for val in line_splitted:
        sub_val = val.split(":")
        if len(sub_val) == 2:
          #print("wave "+ str(sub_val[0]) + " val " + str(sub_val[1]))
          temp = [sub_val[0],sub_val[1]]
          spd_array.append(temp)

#Read the input file, add value to all wavelengths and save spd
def addValueToSpd(fileNameIn,fileNameOut,value):
  spd_array = []
  readSpd(fileNameIn,spd_array)
  for s in spd_array:
    s[1] = float(s[1]) + float(value)

  writeSpd(fileNameOut,spd_array)

#Read the input file, multiply value of all wavelengths by value and save spd
def multiplyValueOfSpd(fileNameIn,fileNameOut,value):
  spd_array = []
  readSpd(fileNameIn,spd_array)
  for s in spd_array:
    s[1] = float(s[1]) * float(value)

  writeSpd(fileNameOut,spd_array)