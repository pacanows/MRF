import shutil
import os,sys
import re
import subprocess
import time
from shutil import copyfile
from shutil import move
import glob
import platform

SPECTRAL_CONVERTER = "mic"

if platform.system()=="Windows":
    SPECTRAL_CONVERTER = SPECTRAL_CONVERTER + ".exe"

if len(sys.argv) < 2:
	print("[ERROR] You must specified a path")
	exit(-1)

folder_path = sys.argv[1]

if "MRF_DIR" in os.environ:

	if os.environ["MRF_DIR"] == "":
		print("[WARNING] MRF_DIR is emtpy")
		path_to_bin = os.path.dirname( os.path.dirname( os.path.realpath(__file__) ) + "/../bin/" )
		print(  path_to_bin )
		print("[INFO] Trying to add ../bin/ in the Path to find the Malia Image Converter of Spectral Image Converter")
		os.environ["PATH"] += ":"+path_to_bin

	else:

		current_path = os.environ["PATH"]
		each_path = current_path.split(":")

		MRF_BIN_DIR = os.environ["MRF_DIR"] + "bin/"

		if MRF_BIN_DIR not in each_path :
			os.environ["PATH"] += ":"+MRF_BIN_DIR
			print("[WARNING] MRF_DIR is set but MRF_DIR/bin/ is not in PATH. Adding it temporarily!")
		#endif
	#endif_if_else

else:
	print("[WARNING] MRF_DIR environment variable is not set. Checking if "  + SPECTRAL_CONVERTER + " (spectral image converter) can be found relatively ")
	path_to_bin = os.path.dirname( os.path.dirname( os.path.realpath(__file__) ) + "/../bin/" )
	#print(  path_to_bin )
	print("[INFO] Trying to add ../bin/ in the Path to find the Malia Image Converter of Spectral Image Converter")
	os.environ["PATH"] += ":"+path_to_bin


#end_if_else

print("[INFO] Checking if the spectral converter utility can be found")

# CHecking with which
if shutil.which(SPECTRAL_CONVERTER) is None :
    print("[ERROR] The application to convert from Spectral image (.raw + .hdr ) to EXR (.exr) was not found. CHECK YOUR PATH AND YOUR MRF_DIR environment variables")
    exit(-1)


files = glob.glob(folder_path+"/*.raw")

print("[INFO] Processing "  + str(len(files)) + " files " )
if  len(files) > 0 :
	print("[INFO] Processing the following files: "  + str(files) )
#end_if

pid_of_suprocesses = []
for file in files:
	process_conv_exr = subprocess.Popen( (SPECTRAL_CONVERTER, "-in", file[:-4]+".hdr" , "-out", file[:-4] + ".exr") )
	pid_of_suprocesses.append( process_conv_exr )
#end_for

# Always a good idea to wait for the end of the subprocesses
exit_code = [ p.wait() for p in  pid_of_suprocesses ]
print("[INFO] Done!")
