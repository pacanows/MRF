#
# Copyright : Romain Pacanowski  (CNRS 2019)
# Contact : romain dot pacanowski @ institutoptique  dot fr
#


# nom du materiau dans la scene  .msf qui va etre modifie 
# nom du materiau qui va etre rendu :  exemple my_material.bin

# nom de la scene
# nombre de samples 
#  Racine repertoire racine à prtir du quel la recursivie va s appliquer



# ONLY RGB fronow
RENDERER="malia_rgb"
SIC="mic"



# Variable below can be overriden by using the  command line
# First Option




import shutil
import os,sys
import re
import subprocess
import time
import xml.etree.ElementTree as ET
from shutil import copyfile
from shutil import move
import xml.etree.ElementTree as ET
import glob
import argparse
import fnmatch

def modify_scene_for_material( original_scene_full_path, material_name_to_modify, full_path_to_material, tmp_scene_full_path ):

	tree = ET.parse(original_scene_full_path)
	root_node = tree.getroot()

	modified = False

	materials = root_node.find('materials')

	for material in materials:
		current_material_name = material.get('name')
		if current_material_name == material_name_to_modify:
			modified=True
			#print(material.attrib)
			material.attrib['file'] = full_path_to_material


	#end_for

	print("tmp_scene_full_path = "+ tmp_scene_full_path )
	tree.write(tmp_scene_full_path)

	return modified 	

#end_def





##########################################################################################
######################################## START ############################################
##########################################################################################

##########################################################################################
# CHECK THAT MANDATORY ENV VARIABLES are set
if "MRF_DIR" in os.environ:
	current_path = os.environ["PATH"]
	
	each_path = current_path.split(":")
	
	MRF_BIN_DIR = os.environ["MRF_DIR"] + "bin/"

	if MRF_BIN_DIR not in each_path :
		os.environ["PATH"] += ":"+MRF_BIN_DIR
		print("[WARNING] MRF_DIR is set but MRF_DIR/bin/ is not in PATH. Adding it temporarily!")
	#endif
else:	
	print("[WARNING] MRF_DIR environment variable is not set. Checking if "  + RENDERER + " is in your current PATH ")
#end_if_else

print("[INFO] Checking if " + RENDERER + "  can be found")

# CHecking with which
if shutil.which(RENDERER) is None :
	print("[ERROR] The application " + RENDERER +" to render the scenes was not found. CHECK YOUR PATH AND YOUR MRF_DIR environment variables")
	exit(-1)
else:
	print("[INFO] " + RENDERER + " Found !!!")

if shutil.which(SIC) is None :
	print("[ERROR] The application " + SIC +" to render the scenes was not found. CHECK YOUR PATH AND YOUR MRF_DIR environment variables")
	exit(-1)
else:
	print("[INFO] " + SIC + " Found !!!")



##########################################################################################


parser = argparse.ArgumentParser( "Rendering of all materials recursively " )
parser.add_argument("-s", "--scene",dest='scene_full_path', type=str, help="Specify the input scene (.msf) file", required="True")
parser.add_argument("--spp",type=int, dest='samples_per_pixel', help="Number of samples per pixel", default=100)
parser.add_argument("--starting_path",dest='starting_path', type=str, help="Path from which all materials will be rendered", default=".")
parser.add_argument("--material_file",dest='material_file', type=str, help="Name of the file for the material to be rendered", default="my_material.bin")
parser.add_argument("--material_name", dest='material_name', type=str, help="Name of the material inside the .msf file for which the target file will be changed", default="layered_material")
parser.add_argument("--exposition", dest="exposition", type=float, help="Exposition value used for tone-mapping", default=1.0)
parser.add_argument("--gamma", dest="gamma", type=float, help="Gamma value used for tone-mapping", default=2.2)
parser.add_argument("--disable-tone-mapping",help="Disable the Tone-mapping step", action="store_true")
parser.add_argument("--force_rendering",help="Force the script to render a new image not matter if an image file already exists", action="store_true")

args = parser.parse_args()


#print( args.scene_full_path)
print("[INFO] Using " + args.scene_full_path+ " and processing "  +  args.material_name + " from directory " + args.starting_path)
print("[INFO] Using Material_file = " + args.material_file )

current_directory = os.getcwd()
#scene_file_fullpath = os.path.join(current_directory,"./"+scene_file)

scene_full_path_wo_ext = args.scene_full_path[:-4]
#print(scene_full_path_wo_ext)
dst = scene_full_path_wo_ext + "_tmp.msf" 
copyfile(args.scene_full_path, dst)

scene_name = os.path.splitext( os.path.basename(args.scene_full_path) )[0]

renderer_args = []
renderer_args.append(RENDERER)
renderer_args.append("-scene")
renderer_args.append(dst)
renderer_args.append("-samples")
renderer_args.append(str(args.samples_per_pixel))
renderer_args.append("-o")

sic_args = []
sic_args.append(SIC)
sic_args.append("-exposition")
sic_args.append(str(args.exposition))
sic_args.append("-gamma")
sic_args.append(str(args.gamma))
sic_args.append("-in")



# FULL RECURSIVITY
# AND LAUNCH THE RENDERING and Tone-mapping with SIC
#material_file_regexp = re.compile(args.material_file)

for root, dirs, files in os.walk(args.starting_path):
	print("dirs="+str(dirs))
	for file in files:
		if file in fnmatch.filter(files, args.material_file):
		#if material_file_regexp.match(file):
			print("[INFO] processing "+file + " from " + root)
			full_path_to_material = os.path.join(root, file)
			#print("Full path to material is "+ full_path_to_material)
			full_path_to_output_image = os.path.join(root, scene_name + "_" + file[:-4] + ".exr")
			#print(full_path_to_output_image)

			modify_scene_for_material( args.scene_full_path, args.material_name, full_path_to_material, dst )

			renderer_args.append(full_path_to_output_image)
			#print(renderer_args)


			# IF the file doest not exists launch rendering
			if not( os.path.exists(full_path_to_output_image)):
				process_renderer = subprocess.Popen(renderer_args)
				while process_renderer.poll() is None:
					time.sleep( 5 )
				#end_while
				print("END RENDER Exited with returncode %d" % process_renderer.returncode)
			else:
				print("[INFO] File: "+full_path_to_output_image+ " does exist.")
				if args.force_rendering:
					print("[INFO] File exists rendering enforced")
					process_renderer = subprocess.Popen(renderer_args)
					while process_renderer.poll() is None:
						time.sleep( 5 )
					#end_while

					print("END RENDER Exited with returncode %d" % process_renderer.returncode)
				else:
					print("[INFO] SKIPPING IT !!!")
				#end_if_else
			#endif_else


			#FLUSh argument for the renderer, this is the output image
			renderer_args.pop()

			if args.disable_tone_mapping == None:
				print("CONVERTING TO PNG")
				sic_args.append(full_path_to_output_image)


				sic_args.append("-out")
				full_path_to_output_PNG_image = os.path.join(root, scene_name + "_" + file[:-4] + ".png")
				sic_args.append(full_path_to_output_PNG_image)

				process_sic  = subprocess.Popen(sic_args)
				while process_sic.poll() is None:
					time.sleep( 1 )

				# FLUSH arguments for SIC
				sic_args.pop()
				sic_args.pop()
				sic_args.pop()
			#endif

			#exit(-1)

		#end_if

	#end_for //files
#end_for directories

print("[INFO] Script terminated")
