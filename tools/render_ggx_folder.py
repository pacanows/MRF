#########################################################################################
#
# Author:  Arthur Dufay            Copyright INRIA : 2018
# Author:  Romain Pacanowski       Copyright CNRS  : 2019
#
#########################################################################################

###########################################RENDERED MEASURED BRDF FOLDER#################
#
#
#  Render a scene with a set of ggx materials
#  
#  The script and the scene must be in the same folder
#    
#  The folder should not contain any other file with a name ending
#  with .msf (reserved for the scene file) or .spd(reserved for the ggx materials)
#   
#  The camera file should have the same name than the msf scene file
#
#  All the measured materials should be in the same folder and have filename
#  ending with .bin
#
#  Number of samples, wavelength and the material name (in the msf scene) that
#  received the measured material can be change with the variables below
#########################################################################################

import sys

RENDERER_NAME = "malia"

nb_samples = sys.argv[1]
wavelength = "380:830:10"
spectral_rendering = True
remove_temp_scene = True
render_on_plafrim = True
convert_to_exr = True
material_name_to_modify = "ggx_material"
scene_file = ""
brdf_file = []

output_img_folder = "./"

if( len(sys.argv)>=3 ):
    output_img_folder = sys.argv[2]

if( output_img_folder[len(output_img_folder)-1] != '/'):
    output_img_folder = output_img_folder + "/"

#TODO: THIS OPTION SEEMS WEIRD and only supported for iRay Backend
caustic_sampler = False
if( len(sys.argv)>= 4 and int(sys.argv[3])>0 ):
    caustic_sampler = True
    print("caustic_sampler ON")
else:
    print("caustic_sampler OFF")

import os
import re
import subprocess
import time
import platform
import xml.etree.ElementTree as ET
from shutil import copyfile
from shutil import move


if os.environ.get("MRF_DIR") is None:
    print("[ERROR] THE MRF environment variable is not set. FIX THAT!!!")
    exit(-1)

MRF_DIR = os.environ.get('MRF_DIR')
RENDERER_PATH = MRF_DIR +"bin/"



#First found the brdf files and the scene file (if not specified) in the current directory
for dirname, dirnames, filenames in os.walk('.'):
    for filename in filenames:
        if(filename.endswith(".spd")):
            #print(os.path.join(dirname, filename))
            #brdf_file.append(os.path.join(dirname, filename))
            
            temp_brdf_file_name = filename
            if(dirname!="."):
                temp_brdf_file_name = dirname[2:]+"/"+temp_brdf_file_name

            brdf_file.append(temp_brdf_file_name)

        #if no scene file have been specified, set it to the first .msf file
        #found in the current directory
        if(filename.endswith(".msf") and len(scene_file)==0):
            scene_file = filename
    #uncomment if want to avoid recursivity
    #break

brdf_file.sort()

eta_file = []
kappa_file = []

#loop on measured material
for brdf in brdf_file:
    if(brdf.endswith("eta.spd")):
        eta_file.append(brdf)
    elif(brdf.endswith("kappa.spd")):
        kappa_file.append(brdf)


print("Render the scene " + scene_file + " using the brdfs files: " + str(eta_file) + " " + str(kappa_file) )

current_directory = os.getcwd()
scene_file_fullpath = os.path.join(current_directory,"./"+scene_file)


####render using subprocess.Popen
if spectral_rendering:
    renderer = RENDERER_PATH + RENDERER_NAME
else:
	renderer = RENDERER_PATH + RENDERER_NAME +"_rgb"

if platform.system()=="Windows":
    renderer = renderer + ".exe"


num_brdf = 0

#loop on measured material
for brdf_eta in eta_file:

    brdf_kappa = kappa_file[num_brdf]

    print(brdf_eta)
    print(brdf_kappa)

    if brdf_kappa[:-len("_kappa.sdp")] != brdf_eta[:-len("_eta.spd")]:
        print("eta kappa mismatch, skipping one material")
        continue
        	

    num_brdf = num_brdf + 1    

    src = scene_file_fullpath

    brdf_name_for_scene_file = brdf_eta[:-len("_eta.spd")]
    brdf_name_for_scene_file = brdf_name_for_scene_file.replace("/","_")
    brdf_name_for_scene_file = brdf_name_for_scene_file.replace("\\","_")

    dst = os.path.join(current_directory, str("./"+brdf_name_for_scene_file + ".msf") )

    
    copyfile(src, dst)
    #copy camera file
    src_cam = src[:-4]+".mcf"
    dst_cam = dst[:-4]+".mcf"
    copyfile(src_cam, dst_cam)
    
    tree = ET.parse(dst)
    root_node = tree.getroot()
    
    materials = root_node.find('materials')
    for material in materials:
        current_material_name = material.get('name')
        #print(material.get('name'))
        if(current_material_name==material_name_to_modify):
            fresnel = material.find("fresnel")
            eta = fresnel.find('eta')
            kappa = fresnel.find('kappa')
            spectrum_e = eta.find('spectrum')
            spectrum_k = kappa.find('spectrum')
            spectrum_e.set("file","./"+brdf_eta)
            spectrum_k.set("file","./"+brdf_kappa)
    
    #write the new scene with the new material (msf file)
    tree.write(dst)


    #Render the scene
    output_image = output_img_folder + os.path.basename(dst)[:-4] + ".exr"
        
    
        
    renderer_args = []

    if render_on_plafrim:    
        renderer_args.append("srun")
    
    renderer_args.append(renderer)
    renderer_args.append("-scene")
    renderer_args.append(dst)
    renderer_args.append("-wr")
    renderer_args.append(wavelength)
    renderer_args.append("-samples")
    renderer_args.append(nb_samples)
    renderer_args.append("-o")
    renderer_args.append(output_image)

    if spectral_rendering:
        renderer_args.append("-save_as_spectral")
    if caustic_sampler:
        renderer_args.append("-caustic_sampler")

    process_renderer = subprocess.Popen(renderer_args)
        

    while process_renderer.poll() is None:
      #print('Still rendering')
        time.sleep(5)
    print("END RENDER Exited with returncode %d" % process_renderer.returncode)

    if remove_temp_scene:
        os.remove(dst)
        os.remove(dst_cam)

    if render_on_plafrim:
        for f in os.listdir("."):
            if re.search("core*", f):
                os.remove(os.path.join(".",f))

    if convert_to_exr and spectral_rendering:
        converter = MRF_DIR+"/bin/SPECTRAL_IMAGE_CONVERTER"
        if platform.system()=="Windows":
            converter = converter + ".exe"


        output_image = output_img_folder + os.path.basename(output_image)[:-4] + ".hdr"

        process_converter = subprocess.Popen((converter,"-in",output_image))

        while process_converter.poll() is None:
            time.sleep(5)
        print("END CONVERTER Exited with returncode %d" % process_converter.returncode)
        

   


