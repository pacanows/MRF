#########################################################################################
#
# Author:  Arthur Dufay            Copyright INRIA : 2018
#
#########################################################################################

#########################################################################################
#
#
#  This script contains functions to parse and write ENVI header (.hdr) files
#
# parse a envi header and creae a Header object from  a .hdr file
# def parse_envi_header(header_filepath):
#
# A header can be write to a file using the Header.write_file() method
# You can set the Header.path attribute to change the destination of the .hdr file
#
#
#
#########################################################################################

import sys
import os

from copy import copy, deepcopy

class Header(object):
  def __init__(self):
    self.desc = ""
    self.width = 0
    self.height = 0
    self.bands = 0
    self.interleave = ""
    self.data_type = 0
    self.byte_order = ""
    self.waves = []
    self.path = ""



  def __copy__(self):
    cls = self.__class__
    result = cls.__new__(cls)
    result.__dict__.update(self.__dict__)
    return result

  def __deepcopy__(self, memo):
    cls = self.__class__
    result = cls.__new__(cls)
    memo[id(self)] = result
    for k, v in self.__dict__.items():
        setattr(result, k, deepcopy(v, memo))
    return result

  def __add__(self,other):
    if self.width != other.width:
      return None
    if self.height != other.height:
      return None
    if self.interleave != other.interleave:
      return None
    if self.data_type != other.data_type:
      return None
    if self.byte_order != other.byte_order:
      return None
    
    header = Header()
    header.desc = self.desc+"\n"+other.desc
    header.width = self.width
    header.height = self.height
    header.interleave = self.interleave
    header.data_type = self.data_type
    header.byte_order = self.byte_order
    header.waves = self.waves + other.waves
    header.bands = self.bands + other.bands
    return header

  def write_file(self):
    #print(os.getcwd())
    #print("Try to rite header to"+self.path)
    file = open(self.path,"w")

    file.write("ENVI\n")
    file.write("description = {\n"+self.desc+"}\n")
    file.write("file type = ENVI" +"\n")
    file.write("sensor type = " +"\n")
    file.write("interleave = "+self.interleave + "\n")
    file.write("samples = " + str(self.width) +"\n")
    file.write("lines = " + str(self.height) +"\n")
    file.write("bands = " + str(self.bands) +"\n")
    file.write("data type = " + str(self.data_type) +"\n")
    file.write("byte order = " + self.byte_order +"\n")
    file.write("header offset = 0" +"\n")

    file.write("x start = 0\n")
    file.write("y start = 0\n")
    file.write("errors = {none}\n")
    file.write("Wavelength = {\n")

    for w in self.waves:
      file.write(str(w)+",\n")
    file.write("}\n")

    file.close()



def check_param(line,param):
  return line[:len(param)]==param

def get_param_value(line):
  equal_pos = line.find("=")
  if equal_pos>0:
    return line[equal_pos + 2:]
  return ""



# parse a envi header and creae a Header object from  a .hdr file
def parse_envi_header(header_filepath):
  file = open(header_filepath,"r")
  contents = file.readlines()
  file.close()

  header = Header()
  header.path = header_filepath

  for i in range(0, len(contents)):
    line = contents[i]
    #print(line)
    line = line.lstrip()

    if check_param(line, "description"):
      if line.find("{")>=0:
        header.desc = line[line.find("{")+1:]
        
        while line.find("}")<0 and i < len(contents):
          i = i + 1
          line = contents[i]
          #print(line)
          header.desc = header.desc + contents[i]

        header.desc = header.desc.replace('}','')



    if check_param(line, "file type"):
      param_value = get_param_value(line);
      if check_param(param_value,"ENVI") == False:
        return None
    
    if check_param(line, "lines"):
      header.height = int(get_param_value(line));
    
    if check_param(line, "samples"):
      header.width = int(get_param_value(line));

    if check_param(line, "bands"):
      header.bands = int(get_param_value(line));


    if check_param(line, "Wavelength"):
      if line.find("{")>=0:
        i = i + 1
        line = contents[i]
        while line.find("}")<0 and i < len(contents):

          line = line.strip()
          line = line.rstrip('\n')
          line = line.rstrip(',')


          header.waves.append(int(line))
          i = i + 1
          line = contents[i]
        

    if check_param(line, "interleave"):
      header.interleave = get_param_value(line);

    if check_param(line, "data type"):
      header.data_type = int(get_param_value(line));

    if check_param(line, "byte order"):
      header.byte_order = get_param_value(line);
    

  return header

