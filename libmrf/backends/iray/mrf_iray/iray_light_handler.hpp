/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * backend iray renderer
 * iray light handling
 *
 **/
#pragma once

#include <mrf/mrf_dll.hpp>
#include <mrf/mrf_types.hpp>

#ifdef IRAY_INSTALL_DIR

#  include <mi/neuraylib.h>

#  ifdef MI_PLATFORM_WINDOWS
#    include <mi/base/miwindows.h>
#  else
#    include <dlfcn.h>
#    include <unistd.h>
#  endif

#  include <vector>
#  include "lighting/light.hpp"
#  include "gui/feedback/loger.hpp"

namespace mrf
{
namespace iray
{
MRF_EXPORT class IrayLightHandler
{
public:
  IrayLightHandler(std::vector<mrf::lighting::Light *> const &mrf_lights, mrf::gui::fb::Loger const &loger);
  void createLights(
      mi::base::Handle<mi::neuraylib::IGroup> &      root_group,
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      bool                                           lights_visible_in_scene);

private:
  void createPointLight(
      mi::base::Handle<mi::neuraylib::IGroup> &      root_group,
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      mrf::lighting::Light *                         mrf_light,
      uint                                           light_num,
      bool                                           visible = false);
  void createSurfacicLight(
      mi::base::Handle<mi::neuraylib::IGroup> &      root_group,
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      mrf::lighting::Light *                         mrf_light,
      uint                                           light_num,
      bool                                           visible = false);
  void createDirectionnalLight(
      mi::base::Handle<mi::neuraylib::IGroup> &      root_group,
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      mrf::lighting::Light *                         mrf_light,
      uint                                           light_num,
      bool                                           visible = false);
  void createMeshLight(
      mi::base::Handle<mi::neuraylib::IGroup> &      root_group,
      mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
      mrf::lighting::Light *                         mrf_light,
      uint                                           light_num,
      bool                                           visible = false);


private:
  std::vector<mrf::lighting::Light *> const &_mrf_lights;
  mrf::gui::fb::Loger const &                _loger;
  //std::vector<std::string> _iray_light_names;
};


}   // namespace iray
}   // namespace mrf

#endif