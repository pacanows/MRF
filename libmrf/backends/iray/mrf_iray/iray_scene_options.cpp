/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/
#ifdef IRAY_INSTALL_DIR

#  include "iray/iray_scene_options.hpp"
#  include <iostream>

namespace mrf
{
namespace iray
{
void IraySceneOptions::setMaxRenderingTime(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name,
    int                                            value)
{
  getSceneOption<mi::ISint32>(transaction, options_db_name, "progressive_rendering_max_time")->set_value(value);
}

void IraySceneOptions::setMaxPathLength(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name,
    uint                                           value)
{
  getSceneOption<mi::ISint32>(transaction, options_db_name, "iray_max_path_length")->set_value(value);
}

void IraySceneOptions::setFireflyFilter(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name,
    bool                                           value)
{
  getSceneOption<mi::IBoolean>(transaction, options_db_name, "iray_firefly_filter")->set_value(value);
}
void IraySceneOptions::setCausticSampler(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name,
    bool                                           value)
{
  getSceneOption<mi::IBoolean>(transaction, options_db_name, "iray_caustic_sampler")->set_value(value);
}


void IraySceneOptions::setRenderingQualityCriterion(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name,
    bool                                           value)
{
  getSceneOption<mi::IBoolean>(transaction, options_db_name, "progressive_rendering_quality_enabled")->set_value(value);
}

void IraySceneOptions::setRenderingMinSamples(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name,
    uint                                           value)
{
  getSceneOption<mi::ISint32>(transaction, options_db_name, "progressive_rendering_min_samples")->set_value(value);
}

void IraySceneOptions::setRenderingMaxSamples(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name,
    int                                            value)
{
  getSceneOption<mi::ISint32>(transaction, options_db_name, "progressive_rendering_max_samples")->set_value(value);
}

void IraySceneOptions::setInstancing(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name,
    bool                                           value)
{
  if (value)
    getSceneOption<mi::IString>(transaction, options_db_name, "iray_instancing")->set_c_str("on");
  else
    getSceneOption<mi::IString>(transaction, options_db_name, "iray_instancing")->set_c_str("off");
}

void IraySceneOptions::printSceneOptions(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name)
{
  std::cout << getSceneOptions(transaction, options_db_name);
}

std::string IraySceneOptions::getSceneOptions(
    mi::base::Handle<mi::neuraylib::ITransaction> &transaction,
    const char *                                   options_db_name)
{
  std::string option_string;
  option_string = "IRAY Scene Options: " + std::string(options_db_name) + "\n";

  mi::base::Handle<const mi::neuraylib::IOptions> scene_options(
      transaction->access<const mi::neuraylib::IOptions>(options_db_name));

  if (!scene_options.is_valid_interface())
  {
    option_string += "CANNOT RETRIEVE SCENE OPTIONS\n";
    return option_string;
  }

  const char *name;
  int         index = 0;
  while (name = scene_options->enumerate_attributes(index++))
  {
    option_string = option_string + name + " " + scene_options->get_attribute_type_name(name);

    if (strcmp(scene_options->get_attribute_type_name(name), "Boolean") == 0)
    {
      mi::base::Handle<const mi::IBoolean> value_handle(scene_options->access_attribute<mi::IBoolean>(name));
      if (value_handle.get()->get_value<bool>())
        option_string = option_string + " = true\n";
      else
        option_string = option_string + " = false\n";
    }

    else if (strcmp(scene_options->get_attribute_type_name(name), "Uint32") == 0)
    {
      mi::base::Handle<const mi::IUint32> value_handle(scene_options->access_attribute<mi::IUint32>(name));
      option_string = option_string + " = " + std::to_string(value_handle.get()->get_value<mi::Uint32>()) + "\n";
    }
    else if (strcmp(scene_options->get_attribute_type_name(name), "Sint32") == 0)
    {
      mi::base::Handle<const mi::ISint32> value_handle(scene_options->access_attribute<mi::ISint32>(name));
      option_string = option_string + " = " + std::to_string(value_handle.get()->get_value<mi::Sint32>()) + "\n";
    }
    else if (strcmp(scene_options->get_attribute_type_name(name), "Float32") == 0)
    {
      mi::base::Handle<const mi::IFloat32> value_handle(scene_options->access_attribute<mi::IFloat32>(name));
      option_string = option_string + " = " + std::to_string(value_handle.get()->get_value<mi::Float32>()) + "\n";
    }
    else if (strcmp(scene_options->get_attribute_type_name(name), "String") == 0)
    {
      //std::cout << " = " << scene_options->access_attribute<mi::IString>(name)->get_c_str() << std::endl;

      mi::base::Handle<const mi::IString> value_handle(scene_options->access_attribute<mi::IString>(name));
      option_string = option_string + " = " + value_handle.get()->get_c_str() + "\n";
      //std::cout << " = " << value_handle.get()->get_value<mi::String>() << std::endl;
    }
    else if (strcmp(scene_options->get_attribute_type_name(name), "Ref") == 0)
    {
      //std::cout << " = " << scene_options->access_attribute<mi::IRef>(name)->get_reference_name() << std::endl;

      mi::base::Handle<const mi::IRef> value_handle(scene_options->access_attribute<mi::IRef>(name));
      option_string = option_string + " = " + value_handle.get()->get_reference_name() + "\n";
    }

    else
    {
      option_string = option_string + "\n";
    }
  }
  return option_string + "\n";
}
}   // namespace iray
}   // namespace mrf
#endif
