/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * backend iray renderer
 *
 **/
#pragma once

#include <mrf/mrf_dll.hpp>
#include <mrf/mrf_types.hpp>

#ifdef IRAY_INSTALL_DIR

#  include <mi/neuraylib.h>

#  ifdef MI_PLATFORM_WINDOWS
#    include <mi/base/miwindows.h>
#  else
#    include <dlfcn.h>
#    include <unistd.h>
#  endif

#  include <authentication.h>

#  include <mrf/math/mat4.hpp>
#  include <mrf/math/vec3.hpp>
#  include <mrf/math/math.hpp>

namespace mrf
{
namespace iray
{
void check_start_success(mi::Sint32 result);

// Loads the neuray library and calls the main factory function.
//
// This convenience function loads the neuray DSO, locates and calls the #mi_neuray_factory()
// function. It returns an instance of the main #mi::neuraylib::INeuray interface. It also
// supplies a authentication key (only needed by some variants of the neuray library).
// The function may be called only once.
//
// \param filename    The file name of the DSO. It is feasible to pass \c NULL, which uses a
//                    built-in default value.
// \return            A pointer to an instance of the main #mi::neuraylib::INeuray interface
mi::neuraylib::INeuray *load_and_get_ineuray(const char *filename = 0);

// Unloads the neuray library.
bool unload();

void mrfMatrixToIrayMatrix(mrf::math::Mat4f const &mat_in, mi::Float64_4_4 &mat_out);

void mrfLightDirectionToIrayMatrix(
    mrf::math::Vec3f const &mrf_light_direction,
    mrf::math::Vec3f const &iray_light_direction,
    mi::Float64_4_4 &       mat_out);

// Creates an attribute "name" of type "Boolean" on "attribute_set" and sets its value to "value".
void createFlag(mi::neuraylib::IAttribute_set *attribute_set, const char *name, bool value);

}   // namespace iray
}   // namespace mrf


// Helper macro. Checks whether the expression is true and if not prints a message and exits.
#  define check_success(expr)                                                                                          \
    do                                                                                                                 \
    {                                                                                                                  \
      if (!(expr))                                                                                                     \
      {                                                                                                                \
        fprintf(stderr, "Error in file %s, line %u: \"%s\".\n", __FILE__, __LINE__, #expr);                            \
        exit(EXIT_FAILURE);                                                                                            \
      }                                                                                                                \
    } while (false)

#endif