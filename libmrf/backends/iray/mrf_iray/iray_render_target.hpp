/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * iray render target
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#ifdef IRAY_INSTALL_DIR

#  include <mi/neuraylib.h>

namespace mrf
{
namespace rendering
{
// A simple implementation of the IRender_target interface using default implementations
// for the abstract ICanvas and ITile interfaces.
class MRF_CORE_EXPORT IRayRenderTarget: public mi::base::Interface_implement<mi::neuraylib::IRender_target>
{
public:
  // Constructor.
  //
  // Creates a render target with a single canvas of the given pixel type, width, and height.
  // This variant uses the default implementation for canvases and tiles available via
  // mi::neuraylib::IImage_api.
  IRayRenderTarget(mi::neuraylib::IImage_api *image_api, const char *pixel_type, mi::Uint32 width, mi::Uint32 height)
  {
    _canvas = image_api->create_canvas(pixel_type, width, height);
  }

  // Implement the interface of mi::neuraylib::IRender_target
  mi::Uint32                    get_canvas_count() const { return 1; }
  const char *                  get_canvas_name(mi::Uint32 index) const { return index == 0 ? "result" : 0; }
  const mi::neuraylib::ICanvas *get_canvas(mi::Uint32 index) const
  {
    if (index > 0) return 0;
    _canvas->retain();
    return _canvas.get();
  }
  mi::neuraylib::ICanvas *get_canvas(mi::Uint32 index)
  {
    if (index > 0) return 0;
    _canvas->retain();
    return _canvas.get();
  }

private:
  // The only canvas of this render target
  mi::base::Handle<mi::neuraylib::ICanvas> _canvas;
};
};   // namespace rendering
};   // namespace mrf

#endif