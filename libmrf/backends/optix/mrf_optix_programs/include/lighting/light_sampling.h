bool ray_quad_intersection(float3 o, float3 d, float3 corner, float3 v1, float3 v2, float3 n, float3 &hitpoint)
{
  float ndotdR = -dot(n, d);

  if (ndotdR < 1e-6f || dot(n, normalize(o - corner)) < 0.0)   // Choose your tolerance
  {
    return false;
  }

  float t  = dot(n, o - corner) / ndotdR;
  hitpoint = o + t * d;

  float3 dMS1 = hitpoint - corner;
  float  u    = dot(dMS1, v1);
  float  v    = dot(dMS1, v2);

  return (u >= 0.0f && u <= dot(v1, v1) && v >= 0.0f && v <= dot(v2, v2));
}

//x = hitpoint
float area_quad_pdf(int num_light, float3 x, float3 n, float3 eye, float3 light)
{
  //TODO handle refraction here ?

  float3 hit_on_light;
  if (!ray_quad_intersection(
          x,
          light,
          lights[num_light].corner,
          lights[num_light].v1,
          lights[num_light].v2,
          lights[num_light].normal,
          hit_on_light))
  {
    return 0;
  }

  float Ldist = length(hit_on_light - x);
  float area  = length(cross(lights[num_light].v1, lights[num_light].v2));
  float pdf   = Ldist * Ldist / (dot(lights[num_light].normal, -light) * area);
  return pdf;
}

//x = hitpoint
float area_quad_pdf(int num_light, float3 x, float3 n, float3 eye, float3 light, float &Ldist)
{
  //TODO handle refraction here ?
  float3 hit_on_light;
  if (!ray_quad_intersection(
          x,
          light,
          lights[num_light].corner,
          lights[num_light].v1,
          lights[num_light].v2,
          lights[num_light].normal,
          hit_on_light))
  {
    return 0;
  }

  Ldist      = length(hit_on_light - x);
  float area = length(cross(lights[num_light].v1, lights[num_light].v2));
  float pdf  = Ldist * Ldist / (dot(lights[num_light].normal, -light) * area);
  return pdf;
}

/**
 * from pbrt
 */
float uniform_cone_pdf(float cosThetaMax)
{
  return 1.f / (2.f * M_PIf * (1.f - cosThetaMax));
}

float3 uniform_sample_cone(float r1, float r2, float cosThetaMax)
{
  float cosTheta = (1.f - r1) + r1 * cosThetaMax;
  float sinTheta = sqrtf(1.f - cosTheta * cosTheta);
  float phi      = r2 * 2.f * M_PIf;
  return make_float3(cos(phi) * sinTheta, sin(phi) * sinTheta, cosTheta);
}

bool ray_sphere_intersection(float3 o, float3 d, float3 center, float radius, float3 &hitpoint, float3 &hit_normal)
{
  float3 oc  = center - o;
  float  tca = dot(d, oc);
  if (tca > 0)
  {
    float3 oh       = d * tca;
    float  d_square = dot(oc, oc) - tca * tca;
    if (d_square < radius * radius)
    {
      float thc = sqrt(radius * radius - d_square);
      float t0  = tca - thc;
      float t1  = tca + thc;

      //swap value if needed
      if (t0 > t1)
      {
        float temp = t0;
        t0         = t1;
        t1         = temp;
      }

      if (t0 < 0)
      {
        t0 = t1;                    // if t0 is negative, let's use t1 instead
        if (t0 < 0) return false;   // both t0 and t1 are negative NO INTERSECTION
      }

      float t = t0;

      if (t > 0)   //ray.tmin && t < ray.tmax)
      {
        hitpoint   = o + d * t;
        hit_normal = normalize(hitpoint - center);
        return true;
      }
    }
  }
  return false;
}

float sphere_light_pdf(int num_light, float3 x, float3 n, float3 out_dir, float3 in_dir, float &Ldist)
{
  //TODO handle refraction here
  float3 hit_on_light;
  float3 hit_normal;
  if (!ray_sphere_intersection(
          x,
          in_dir,
          sphere_lights[num_light].position,
          sphere_lights[num_light].radius,
          hit_on_light,
          hit_normal))
  {
    return 0;
  }

  Ldist = length(hit_on_light - x);

  //pdf for point inside sphere is 0
  if (Ldist < sphere_lights[num_light].radius) return 0;

  float sinThetaMax = sphere_lights[num_light].radius / Ldist;
  float cosThetaMax = sqrtf(max(0.f, 1.f - sinThetaMax * sinThetaMax));
  return uniform_cone_pdf(cosThetaMax);
}

unsigned int sample_light(
    unsigned int num_lights,
    float3       hitpoint,
    float3       normal,
    COLOR_B &    light_emission,
    float3 &     L,
    float &      light_pdf,
    float &      Ldist)
{
  if (num_lights > 0)
  {
    //RNG for light sampling
    float2 rnd2d = sampling2D(
        current_prd.num_sample,
        current_prd.depth * PRNG_BOUNCE_NUM + PRNG_LIGHT_U,
        current_prd.offset_sampling,
        current_prd.seed);
    float z3 = rnd2d.x;
    float z4 = rnd2d.y;

    //Choose random light
    //TODO CDF based on POWER or energy of each light sources
    float z5 = sampling1D(
                   current_prd.num_sample,
                   current_prd.depth * PRNG_BOUNCE_NUM + PRNG_LIGHT,
                   current_prd.offset_sampling.x,
                   current_prd.seed)
               * num_lights;
    const int idx_light = min(num_lights - 1, int(z5));

    light_pdf = 0.f;

    //pick a quad light
    if (idx_light < lights.size())
    {
      // Choose random point on light
      QuadLight light     = lights[idx_light];
      float3    light_pos = light.corner + light.v1 * z3 + light.v2 * z4;

      // Calculate properties of light sample (for area based pdf)
      Ldist      = length(light_pos - hitpoint);
      L          = normalize(light_pos - hitpoint);
      float Area = length(cross(light.v1, light.v2));

      // convert area based pdf to solid angle
      if (dot(light.normal, -L) > 0.f)
        light_pdf = Ldist * Ldist / (dot(light.normal, -L) * Area);
      else
        light_pdf = 0.f;


#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
      for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
        light_emission.value[i] = emi_spd_quad_lights[i + idx_light * (NB_MAX_WAVES_MULTIPLEXED + 1)];
#else
      light_emission = light.emission;
#endif
      return idx_light;
    }
    //otherwise pick a sphere light
    else if (idx_light < lights.size() + sphere_lights.size())
    {
      SphereLight light        = sphere_lights[idx_light - lights.size()];
      float3      light_center = light.position;

      L     = light_center - hitpoint;
      Ldist = length(L);
      L     = L / Ldist;

      float sinThetaMax = light.radius / Ldist;
      float cosThetaMax = sqrtf(max(0.f, 1.f - sinThetaMax * sinThetaMax));

      //generate a sample point on a cone towards nx(0,0,1)
      //see Global illumination compendium
      //page 19 eq(34)
      float3 p = uniform_sample_cone(z3, z4, cosThetaMax);

      //turn the cone towards the sphere light center
      optix::Onb onb(L);
      onb.inverse_transform(p);
      L = p;

      float3 ps;   //point on sphere
      float3 pn;   //normal at pn
      //use ray sphere intersection to be sure the sample is valid
      //and compute ps and pn
      if (ray_sphere_intersection(hitpoint, L, light_center, light.radius, ps, pn))
      {
        //recompute Ldist from ps, used to cast shadow ray
        Ldist = length(ps - hitpoint) - scene_epsilon;
        if (dot(pn, -L) > 0.f)
          light_pdf = uniform_cone_pdf(cosThetaMax);
        else
          light_pdf = 0.f;

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
        int sph_light_id = idx_light - lights.size();
        for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED + 1; i++)
        {
          light_emission.value[i] = emi_spd_sphere_lights[i + sph_light_id * (NB_MAX_WAVES_MULTIPLEXED + 1)];
        }
#else
        light_emission = light.emission;
#endif
      }
      else
      {
        //Might need to do something here
        //if we miss the light due to float erros
        //see PBRT 14.6.3 Sampling light sources, Sphere lights
        light_pdf = 0.f;
      }

      return idx_light;
    }
    else if (use_envmap)   //last case, choose to sample the envmap
    {
      sampleEnvmap(normal, z3, z4, Ldist, L, light_emission, light_pdf);
      return idx_light;
    }
    else
    {
      return 0;
    }
  }
  else
    return 0;
}

COLOR compute_directional_light(float3 hitpoint, float3 n, float3 eye)
{
  COLOR result(0.f);

  // Accumulate all DIRECTIONAL LIGHT SOURCES
  for (int i = 0; i < directionnal_lights.size(); i++)
  {
    const float dot_NL = dot(n, -directionnal_lights[i].direction);
    if (dot_NL > 0.0f)
    {
      PerRayData_pathtrace_shadow shadow_prd;
      shadow_prd.inShadow = false;

      Ray shadow_ray = make_Ray(
          hitpoint,
          -directionnal_lights[i].direction,
          pathtrace_shadow_ray_type,
          scene_epsilon,
          RT_DEFAULT_MAX);
      rtTrace(top_object, shadow_ray, shadow_prd);

      if (!shadow_prd.inShadow)
      {
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED

        COLOR light_emission(0.f);

        for (int k = 0; k < NB_MAX_WAVES_MULTIPLEXED; k++)
        {
          light_emission.value[k] = lerp(
              emi_spd_dir_lights[k + i * (NB_MAX_WAVES_MULTIPLEXED + 1)],
              emi_spd_dir_lights[k + 1 + i * (NB_MAX_WAVES_MULTIPLEXED + 1)],
              current_prd.wavelength_offset);
        }

        COLOR tmp = bsdf_eval(n, -directionnal_lights[i].direction, -eye, ANY_REFL);

        tmp *= light_emission * current_prd.attenuation * dot_NL;

        result += tmp;

#else   // WE Are in color mode or One wavelength

        result += bsdf_eval(n, -directionnal_lights[i].direction, -eye, ANY_REFL) * directionnal_lights[i].emission
                  * current_prd.attenuation * dot_NL;

#endif

      }   // end not in shadow

    }   // This directional light can shade the point
  }     //end for-loop on directional lights
  return result;
}