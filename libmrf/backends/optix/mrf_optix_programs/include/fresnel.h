#include "complex.h"

//fresnel for dielectric/dielectric
//eat_i == eta incident
//eta_t = eta transmitted

__host__ __device__ float fresnelDielectricDielectric(float eta_i, float cos_theta_i, float eta_t)
{
  if (cos_theta_i <= 0.f)
  {
    return 1.f;
  }

  float sin_theta_i = sqrtf(1 - cos_theta_i * cos_theta_i);
  float sin_theta_t = sin_theta_i * eta_i / eta_t;

  if (sin_theta_t > 1.f)   //TIR
  {
    return 1.f;
  }

  float cos_theta_t = sqrtf(1 - sin_theta_t * sin_theta_t);

  float Fs = (eta_i * cos_theta_i - eta_t * cos_theta_t) / (eta_i * cos_theta_i + eta_t * cos_theta_t);
  float Fp = (eta_t * cos_theta_i - eta_i * cos_theta_t) / (eta_t * cos_theta_i + eta_i * cos_theta_t);

  return (Fs * Fs + Fp * Fp) * 0.5;
}

//fresnel for dielectric/dielectric
//eat_i == eta incident
//eta_t = eta transmitted

__host__ __device__ float fresnelDielectricDielectric(float eta_i, float cos_theta_i, float eta_t, float cos_theta_t)
{
  if (cos_theta_i <= 0.f)
  {
    return 1.f;
  }

  if ((1 - cos_theta_i * cos_theta_i) * (eta_i / eta_t) * (eta_i / eta_t) > 1.f)   //TIR
  {
    return 1.f;
  }

  float Fs = (eta_i * cos_theta_i - eta_t * cos_theta_t) / (eta_i * cos_theta_i + eta_t * cos_theta_t);
  float Fp = (eta_t * cos_theta_i - eta_i * cos_theta_t) / (eta_t * cos_theta_i + eta_i * cos_theta_t);

  return (Fs * Fs + Fp * Fp) * 0.5;
}

#ifndef MRF_RENDERING_MODE_SPECTRAL   //Function is never used in spectral due to decimation (multiplexing) or COLOR = float
__host__ __device__ COLOR fresnelDielectricDielectric(COLOR eta_i, float cos_theta_i, COLOR eta_t, float cos_theta_t)
{
  if (cos_theta_i <= 0.f)
  {
    return COLOR(1.f);
  }

  if ((1 - cos_theta_i * cos_theta_i) * (eta_i[0] / eta_t[0]) * (eta_i[0] / eta_t[0]) > 1.f)   //TIR
  {
    return 1.f;
  }

  COLOR Fs = (eta_i * cos_theta_i - eta_t * cos_theta_t) / (eta_i * cos_theta_i + eta_t * cos_theta_t);
  COLOR Fp = (eta_t * cos_theta_i - eta_i * cos_theta_t) / (eta_t * cos_theta_i + eta_i * cos_theta_t);

  return (Fs * Fs + Fp * Fp) * 0.5;
}

// fresnel for dielectric/dielectric
// eat_i == eta incident
// eta_t = eta transmitted

__host__ __device__ COLOR fresnelDielectricDielectric(COLOR eta_i, float cos_theta_i, COLOR eta_t)
{
  if (cos_theta_i <= 0.f)
  {
    return COLOR(1.f);
  }

  float sin_theta_i = sqrtf(1 - cos_theta_i * cos_theta_i);

  //TODO use average for eta
  if (sin_theta_i * eta_i[0] / eta_t[0] > 1.f)   //TIR
  {
    return 1.f;
  }

  COLOR sin_theta_t = eta_i / eta_t * sin_theta_i;
  COLOR cos_theta_t = sqrt(COLOR(1.f) - sin_theta_t * sin_theta_t);

  COLOR Fs = (eta_i * cos_theta_i - eta_t * cos_theta_t) / (eta_i * cos_theta_i + eta_t * cos_theta_t);
  COLOR Fp = (eta_t * cos_theta_i - eta_i * cos_theta_t) / (eta_t * cos_theta_i + eta_i * cos_theta_t);

  return (Fs * Fs + Fp * Fp) * 0.5;
}
#endif

// __host__ __device__ float
// fresnelConductorDielectric(float eta_i, float cos_theta_i, float eta_t, float kappa_t, float cos_theta_t)
// {
//   //TODO: does this make sense ? Apart for very (very (very)) thin slab of conductors, no energy crosses through a conductor...
//   if (cos_theta_i <= 0.f)
//   {
//     return 1.f;
//   }
//
//   //  Complex incident(eta_i,0);
//   mrf::math::Complex transmitted(eta_t, -kappa_t);
//
//   mrf::math::Complex Fs;
//   //= (eta_i * cos_theta_i - transmitted * cos_theta_t) / (eta_i * cos_theta_i + transmitted * cos_theta_t);
//   mrf::math::Complex Fp;
//   //= (transmitted * cos_theta_i - eta_i * cos_theta_t) / (transmitted * cos_theta_i + eta_i * cos_theta_t);
//
//   return 0.5f * (norm(Fs) + norm(Fp));
// }

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
__host__ __device__ float fresnelDieletricConductor(float Eta, float Kappa, float CosTheta)
{
  float CosTheta2 = CosTheta * CosTheta;
  float SinTheta2 = 1 - CosTheta2;
  float Eta2      = Eta * Eta;
  float Kappa2    = Kappa * Kappa;

  float t0       = Eta2 - Kappa2 - SinTheta2;
  float a2plusb2 = sqrt(t0 * t0 + 4 * Eta2 * Kappa2);
  float t1       = a2plusb2 + CosTheta2;
  float a        = sqrt(0.5f * (a2plusb2 + t0));
  float t2       = 2 * a * CosTheta;
  float Rs       = (t1 - t2) / (t1 + t2);

  float t3 = CosTheta2 * a2plusb2 + SinTheta2 * SinTheta2;
  float t4 = t2 * SinTheta2;
  float Rp = Rs * (t3 - t4) / (t3 + t4);

  return 0.5 * (Rp + Rs);
}
#endif


__host__ __device__ COLOR fresnelDieletricConductor(COLOR Eta, COLOR Kappa, float CosTheta)
{
  float CosTheta2 = CosTheta * CosTheta;
  float SinTheta2 = 1 - CosTheta2;
  COLOR Eta2      = Eta * Eta;
  COLOR Kappa2    = Kappa * Kappa;

  COLOR t0       = Eta2 - Kappa2 - SinTheta2;
  COLOR a2plusb2 = sqrt(t0 * t0 + COLOR(4.f) * Eta2 * Kappa2);
  COLOR t1       = a2plusb2 + CosTheta2;
  COLOR t2       = COLOR(2.f) * sqrt((a2plusb2 + t0) * 0.5f) * CosTheta;
  COLOR Rs       = (t1 - t2) / (t1 + t2);

  COLOR t3 = a2plusb2 * CosTheta2 + SinTheta2 * SinTheta2;
  COLOR t4 = t2 * SinTheta2;
  COLOR Rp = Rs * (t3 - t4) / (t3 + t4);

  return (Rp + Rs) * 0.5f;
}
