//Use define macro here for conditionnal inclusion to avoid some ifs ?
#include "include/distribution/ggx.h"
// #include "include/distribution/beckmann.h"
// #include "include/distribution/cosine.h"

#define DISTRIB_GGX      0
#define DISTRIB_BECKMANN 1
#define DISTRIB_COSINE   2

bool bsdf_is_dirac()
{
  return (_alpha_g == 0.f);
}

inline float geometricTerm(float3 h, float3 n, float3 v, float3 l, float2 roughness)
{
  //TODO handles other than GGX
  return ggx_geometricTerm(h, n, v, l, roughness);
  // if(_distribution == DISTRIB_GGX)
  // return ggx_geometricTerm(dot_HV, dot_NV, dot_HL, dot_NL, sqr_alpha_g);
  // else if(_distribution == DISTRIB_BECKMANN) return 0.f; //TEMP
  // else if(_distribution == DISTRIB_COSINE) return 0.f;
  // else return 0.f;
}

inline float v_geometricTerm(float dot_HV, float dot_NV, float dot_HL, float dot_NL, float sqr_alpha_g)
{
  // if(_distribution == DISTRIB_GGX)
  return vnggx_geometricTerm(dot_HV, dot_NV, sqr_alpha_g);
  // else if(_distribution == DISTRIB_BECKMANN) return 0.f; //TEMP
  // else if(_distribution == DISTRIB_COSINE) return 0.f;
  // else return 0.f;
}

inline float distribution(float sqr_alpha_g, float cos_theta_h)
{
  // if(_distribution == DISTRIB_GGX)
  return ggx_distribution(sqr_alpha_g, cos_theta_h);
  // else if(_distribution == DISTRIB_BECKMANN) return 0.f; //TEMP
  // else if(_distribution == DISTRIB_COSINE) return 0.f;
  // else return 0.f;
}

inline float v_distribution(float sqr_alpha_g, float cos_theta_h, float dot_HV, float dot_NV)
{
  // if(_distribution == DISTRIB_GGX)
  float g_v = v_geometricTerm(0.f, 0.f, dot_HV, dot_NV, _sqr_alpha_g);
  return (g_v > 0.f) ? g_v * ggx_distribution(sqr_alpha_g, cos_theta_h) : 0.f;
  // else if(_distribution == DISTRIB_BECKMANN) return 0.f; //TEMP
  // else if(_distribution == DISTRIB_COSINE) return 0.f;
  // else return 0.f;
}

float3 generate_half_vector(float3 n, float3 in_dir)
{
  if (_alpha_g > 0.f)
  {
    float2 rnd2d = sampling2D(
        current_prd.num_sample,
        current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
        current_prd.offset_sampling,
        current_prd.seed);
    float z1 = rnd2d.x;
    float z2 = rnd2d.y;

    // if(_distribution == DISTRIB_GGX)
    float3 h = ggx_sample(z1, z2, in_dir, n, make_float2(_alpha_g));
    // float3 h = vnggx_sample(z1, z2, in_dir, n);
    // else if(_distribution == DISTRIB_BECKMANN) beckmann_sample(z1, z2, in_dir, n); //TEMP
    // else if(_distribution == DISTRIB_COSINE) cosine_sample(z1, z2, in_dir, n);
    // else return 0.f;
    return h;
  }
  else
  {
    return n;
  }
}

/**
 * @brief      Generates a sample according to the chosen microfacet distribution, in the case of a reflection.
 *
 * @param      n        the normal vector.
 * @param      in_dir          the incoming direction, pointing toward the hitpoint.
 *
 * @return     The sample direction (xyz component) and the sampled lobe (w component).
 */
float4 generate_reflective_sample(float3 n, float3 in_dir)
{
  return make_float4(optix::reflect(in_dir, n), SPECULAR_REFL);
}