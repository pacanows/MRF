#include "random.h"
#include <optixu/optixu_math_namespace.h>

#include "include/path_tracer_definitions.h"

using namespace optix;

//TEMP ?
using namespace mrf::spectrum;
using mrf::spectrum::COLOR;
using mrf::spectrum::COLOR_B;

#include "include/normal_definitions.h"

rtDeclareVariable(float, scene_epsilon, , );
rtDeclareVariable(rtObject, top_object, , );
rtDeclareVariable(unsigned int, pathtrace_ray_type, , );
rtDeclareVariable(unsigned int, pathtrace_shadow_ray_type, , );

rtDeclareVariable(float3, tangent, attribute tangent, );
rtDeclareVariable(float3, bitangent, attribute bitangent, );
rtDeclareVariable(float3, texcoord, attribute texcoord, );
rtDeclareVariable(float3, geometric_normal, attribute geometric_normal, );
rtDeclareVariable(float3, shading_normal, attribute shading_normal, );

rtDeclareVariable(float3, back_hit_point, attribute back_hit_point, );
rtDeclareVariable(float3, front_hit_point, attribute front_hit_point, );

rtDeclareVariable(PerRayData_pathtrace, current_prd, rtPayload, );
rtDeclareVariable(optix::Ray, ray, rtCurrentRay, );
rtDeclareVariable(float, t_hit, rtIntersectionDistance, );


// rtDeclareVariable(uint, mat_index, attribute mat_index, );

#include "include/lighting/light_definitions.h"

rtBuffer<QuadLight>         lights;
rtBuffer<SphereLight>       sphere_lights;
rtBuffer<DirectionnalLight> directionnal_lights;

rtDeclareVariable(unsigned int, next_event, , );   //0 = none, 1 = all

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> _wavelengths;
rtDeclareVariable(unsigned int, first_wave_index, , );
rtDeclareVariable(unsigned int, last_wave_index, , );
rtDeclareVariable(unsigned int, nb_wavelengths, , );
#  else
rtDeclareVariable(float, _wavelengths, , );
#  endif
#else
rtDeclareVariable(float3, _wavelengths, , );
#endif


rtDeclareVariable(float, _thickness, , );   //0 = none, 1 = all

//List of defined to be used as sampled lobe (int parameter) in sampling function.
#define ABSORPTION     0
#define DIFFUSE_REFL   1
#define SPECULAR_REFL  2
#define CLEARCOAT_REFL 3   // For Principled brdf
/* This means a reflection occured but it can be diffuse or specular */
#define ANY_REFL 4

//#PLUGIN#


#include "include/lighting/envmap_definitions.h"
#include "include/lighting/light_sampling.h"
#include "include/lighting/next_event.h"

float3 get_normal(float3 shading_normal, bool face_forward)
{
  //TODO: handle normal_map here
  float3 world_shading_normal = normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, shading_normal));
  if (face_forward)
  {
    float3 world_geometric_normal = normalize(rtTransformNormal(RT_OBJECT_TO_WORLD, geometric_normal));
    return normalize(faceforward(world_shading_normal, -ray.direction, world_geometric_normal));
  }
  else
  {
    return world_shading_normal;
  }
}

RT_PROGRAM void material_eval()
{
  if (current_prd.shadow_catcher > 0)
  {
    current_prd.done = true;
    return;
  }

  float3 hitpoint = ray.origin + t_hit * ray.direction;
  float3 ffnormal = get_normal(shading_normal, true);

  //We need both -> faceforward for easier evaluation, real_normal for fresnel and refraction stuff.
  float3 real_normal = get_normal(shading_normal, false);

  //Sample a half-vector. For material without this definition -> generate_half_vector return ffnormal.
  float3 h = generate_half_vector(ffnormal, -ray.direction);

  //backface culling
  /*
  if(dot(ray.direction,world_geometric_normal)>0)
  {
      current_prd.backface = true;
      return;
  }
  current_prd.backface = false;
  */

  current_prd.origin       = front_hit_point;
  current_prd.countEmitted = true;

  COLOR bsdf_dot_nl_div_by_pdf(0.f);

  //generate_reflective_sample returns a float4: sampled direction (xyz) and sampled lobe (w).
  float4 sample         = generate_reflective_sample(h, ray.direction);
  current_prd.direction = make_float3(sample);
  int sampled_lobe      = int(sample.w);   //-1 if BRDF has one lobe.

  //This can not happen for a reflection.
  if (dot(ffnormal, current_prd.direction) <= 0.f || sampled_lobe == ABSORPTION)
  {
    current_prd.done = true;
    return;
  }

  //brdf_eval_optim return the BRDF, already multiplied by n dot l and divided by sample pdf.
  //Warning, the division by the reflection pdf MUST be hidden in brdf_eval_optim.
  bsdf_dot_nl_div_by_pdf = bsdf_eval_optim(h, ffnormal, -ray.direction, current_prd.direction, sampled_lobe, 1.f);

  //TODO: try to sample only ONE directional light + stop ray if sampled ?
  current_prd.result += compute_directional_light(hitpoint, ffnormal, ray.direction);

  //Is dirac ? no need to compute next event -> add contribution and return.
  if (bsdf_is_dirac())
  {
    current_prd.attenuation *= bsdf_dot_nl_div_by_pdf;
    return;
  }

  //
  // Next event estimation (== compute direct lighting).
  //
  if (next_event == true && !current_prd.is_inside)   //Dont evaluate for glass TIR
  {
    current_prd.countEmitted = false;   //Avoid double contribution of the source

    COLOR   result(0.f);
    COLOR_B light_emission(0.f);

    float3 L         = make_float3(0.f);
    float  light_pdf = 0.f;
    float  Ldist     = 0.f;

    //TODO: use rtVariable(s) (possibly one "int3" ?), can be computed only once
    //+1 for 1 chance to choose to sample the envmap
    unsigned int num_lights
        = (use_envmap == 1) ? lights.size() + sphere_lights.size() + 1 : lights.size() + sphere_lights.size();
    unsigned int idx_light = sample_light(num_lights, hitpoint, ffnormal, light_emission, L, light_pdf, Ldist);

    //TODO: use rtVariable instead of hardcoded, to make it controllable ?
    bool use_mis = true;

    //
    // cast shadow rays and add MIS
    //
    // Add contribution from light sampling.
    if (dot(ffnormal, L) > 0.f)
    {
      COLOR light = eval_direct_light(hitpoint, ffnormal, -ray.direction, L, sampled_lobe, light_pdf, Ldist, use_mis);
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
      light *= colorFromAsset(light_emission, current_prd.wavelength_offset, current_prd.id_wavelength);
#else
      light *= light_emission;
#endif

      current_prd.result += light * num_lights;
    }

    //add contribution from brdf sampling if MIS
    if (use_mis)
    {
      //First compute light pdf and update Ldist for BRDF sampled direction
      float Ldist, light_pdf;
      if (idx_light < lights.size())
      {
        light_pdf = area_quad_pdf(idx_light, hitpoint, ffnormal, -ray.direction, current_prd.direction, Ldist);
      }
      else if (idx_light < lights.size() + sphere_lights.size())
      {
        light_pdf = sphere_light_pdf(
            idx_light - lights.size(),
            hitpoint,
            ffnormal,
            -ray.direction,
            current_prd.direction,
            Ldist);
      }
      else
      {
        envmap_eval_and_pdf(current_prd.direction, Ldist, light_emission, light_pdf);
      }

      COLOR bsdf = eval_direct_bsdf(
          hitpoint,
          ffnormal,
          -ray.direction,
          current_prd.direction,
          sampled_lobe,
          light_pdf,
          Ldist,
          bsdf_dot_nl_div_by_pdf);
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
      for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; i++)
      {
        bsdf.value[i] *= lerp(light_emission.value[i], light_emission.value[i + 1], current_prd.wavelength_offset);
      }
#else
      bsdf *= light_emission;
#endif

      current_prd.result += bsdf * num_lights;
    }   //endif use_mis
  }

  //update energy along the ray relative to brdf importance sampling
  current_prd.attenuation *= bsdf_dot_nl_div_by_pdf;
}


//-----------------------------------------------------------------------------
//
//  Shadow any-hit
//
//-----------------------------------------------------------------------------

rtDeclareVariable(PerRayData_pathtrace_shadow, current_prd_shadow, rtPayload, );

RT_PROGRAM void shadow()
{
  current_prd_shadow.inShadow = true;
  rtTerminateRay();
}
