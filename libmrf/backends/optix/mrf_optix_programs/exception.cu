/*
 * Copyright (c) 2018 NVIDIA CORPORATION. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions
 * are met:
 *  * Redistributions of source code must retain the above copyright
 *    notice, this list of conditions and the following disclaimer.
 *  * Redistributions in binary form must reproduce the above copyright
 *    notice, this list of conditions and the following disclaimer in the
 *    documentation and/or other materials provided with the distribution.
 *  * Neither the name of NVIDIA CORPORATION nor the names of its
 *    contributors may be used to endorse or promote products derived
 *    from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
 * EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
 * PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
 * OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
 * (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <optixu/optixu_math_namespace.h>
#include "include/path_tracer_definitions.h"

using namespace optix;

rtDeclareVariable(uint2, launch_index, rtLaunchIndex, );

#ifdef MRF_RENDERING_MODE_SPECTRAL
rtBuffer<float, 3> output_buffer;
rtDeclareVariable(float, bad_color, , );
rtDeclareVariable(unsigned int, first_wave_index, , );
rtDeclareVariable(unsigned int, last_wave_index, , );
rtDeclareVariable(unsigned int, nb_wavelengths, , );
#else    //MRF_RENDERING_MODE_SPECTRAL
rtBuffer<float3, 3> output_buffer;
rtDeclareVariable(float3, bad_color, , );
#endif   //MRF_RENDERING_MODE_SPECTRAL

rtDeclareVariable(unsigned int, frame_number, , );
rtDeclareVariable(unsigned int, effective_frame_number, , );
rtDeclareVariable(unsigned int, _spatial_factor, , );

optix::uint2 effective_index(optix::uint2 index, optix::uint frame_number, optix::uint factor)
{
  if (factor == 1) return index;
  //Returns a value between 0 and factor^2 - 1 to access the offset buffer.
  optix::uint offset = (frame_number - 1) % (factor * factor);

  optix::uint2 pixel_offset;
  pixel_offset.x = offset % factor;
  pixel_offset.y = offset / factor;

  //Remap to buffer size.
  optix::uint2 full_index = index * factor;

  return full_index + pixel_offset;
}

//-----------------------------------------------------------------------------
//
//  Exception program
//
//-----------------------------------------------------------------------------

RT_PROGRAM void exception()
{
  uint2 real_index = effective_index(launch_index, frame_number, _spatial_factor);
  uint3 pixel_id;
  pixel_id.x = real_index.x;
  pixel_id.y = real_index.y;
  pixel_id.z = 0;


#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  for (uint num_wave = first_wave_index; num_wave <= last_wave_index; num_wave++)
  {
    pixel_id.z              = num_wave;
    output_buffer[pixel_id] = bad_color;
  }
#else
#  ifdef MRF_RENDERING_MODE_SPECTRAL
  pixel_id.z              = first_wave_index;
#  endif
  output_buffer[pixel_id] = bad_color;
#endif
}
