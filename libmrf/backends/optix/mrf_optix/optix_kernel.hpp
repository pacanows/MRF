/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 * backend optix kernel impl
 *
 **/
#pragma once

#include <mrf_optix_dll.hpp>

#include <mrf_core/mrf_types.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_matrix_namespace.h>


namespace mrf
{
namespace optix_backend
{
struct PTXConfig
{
  bool runtime_compilation;
  bool overwrite_ptx;
  bool mode_sensitive;
  PTXConfig(): runtime_compilation(false), overwrite_ptx(false), mode_sensitive(false) {}

  PTXConfig(bool rt, bool over): runtime_compilation(rt), overwrite_ptx(over), mode_sensitive(false) {}

  PTXConfig toMatConfig()
  {
    mode_sensitive = true;
    return *this;
  }
};

class MRF_OPTIX_EXPORT OptixKernel
{
public:
  OptixKernel();
  OptixKernel(std::string filename);
  OptixKernel(std::string filename, std::vector<std::string> const &cuda_compile_options);
  OptixKernel(std::string filename, std::vector<std::string> const &cuda_compile_options, PTXConfig ptx_cfg);


  optix::Program createProgram(optix::Context &context, std::string fct_name = "");

  optix::Program createClosestHitProgram(optix::Context &context) { return createProgram(context, "material_eval"); }
  optix::Program createAnyHitProgram(optix::Context &context) { return createProgram(context, "shadow"); }
  optix::Program createBBoxProgram(optix::Context &context) { return createProgram(context, "bounds"); }
  optix::Program createIntersectionProgram(optix::Context &context) { return createProgram(context, "intersect"); }

  void load(optix::Context &context);
  void load(optix::Context &context, std::string filename);
  void compile(optix::Context &context, const char **log = NULL);
  void compile(optix::Context &context, std::string ptx_name, const char **log = NULL);
  void compile(
      optix::Context &                context,
      std::vector<std::string> const &cuda_compile_options,
      std::string                     ptx_name,
      const char **                   log = NULL);

  bool addInclude(std::string incl_path, std::string clue = "#PLUGIN#");

private:
#if CUDA_NVRTC_ENABLED
  void getCuStringFromFile(std::string &cu, std::string &location, const char *filename);

  void getPtxFromCuString(
      std::string &                   ptx,
      const char *                    cu_source,
      const char *                    name,
      std::vector<std::string> const &cuda_compile_options,
      const char **                   log_string);

#endif

  bool getPtxStringFromFile(std::string &ptx, const char *filename);

  bool readSourceFile(std::string &str, const std::string &filename);

  bool exportPTX(const char *filename);

  std::string getPTXpath(const char *filename);

private:
  std::string _prg_name;
  std::string _prg_path;

  std::string _prg_cu_srcs;
  std::string _prg_ptx;
  std::string _prg_ptx_sub_path;

  std::vector<std::string> _prg_compile_options;

  PTXConfig _config;
};

}   // namespace optix_backend
}   // namespace mrf
