/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * backend optix
 * Optix geometry handling
 *
 **/
#pragma once

#include <mrf_optix_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/feedback/loger.hpp>
#include <mrf_core/rendering/scene.hpp>
#include <mrf_core/geometry/sphere.hpp>

#include <mrf_optix/optix_material_handler.hpp>
#include <mrf_optix/optix_light_handler.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_stream_namespace.h>

#include <vector>

namespace mrf
{
namespace optix_backend
{
struct MRF_OPTIX_EXPORT OptixMeshBuffers
{
  optix::Buffer tri_indices;
  optix::Buffer mat_indices;
  optix::Buffer positions;
  optix::Buffer normals;
  optix::Buffer texcoords;
  optix::Buffer tangents;
};


//------------------------------------------------------------------------------
//
// OptiX mesh consisting of a single geometry instance with one or more
// materials.
//
// Mesh buffer variables are set on Geometry node:
//   vertex_buffer  : float3 vertex positions
//   normal_buffer  : float3 per vertex normals, may be zero length
//   texcoord_buffer: float2 vertex texture coordinates, may be zero length
//   index_buffer   : int3 indices shared by vertex, normal, texcoord buffers
//   material_buffer: int indices into material list
//
//------------------------------------------------------------------------------
struct MRF_OPTIX_EXPORT OptiXMesh
{
  // Input
  optix::Context  context;    // required
  optix::Material material;   // optional single matl override

  optix::Program intersection;   // optional
  optix::Program bounds;         // optional

  optix::Program attributes;   //optional

  optix::Program closest_hit;   // optional multi matl override
  optix::Program any_hit;       // optional

  // Output
  optix::GeometryTriangles geom_tri;
  optix::Acceleration      accel;
  optix::float3            bbox_min;
  optix::float3            bbox_max;

  int  num_triangles;
  bool has_texcoords;
};

//------------------------------------------------------------------------------
//
// Mesh data structure
//
//------------------------------------------------------------------------------
struct MRF_OPTIX_EXPORT Mesh
{
  int32_t num_vertices;   // Number of triangle vertices
  float * positions;      // Triangle vertex positions (len num_vertices)

  bool   has_normals;   //
  float *normals;       // Triangle normals (len 0 or num_vertices)

  bool   has_texcoords;   //
  float *texcoords;       // Triangle UVs (len 0 or num_vertices)

  bool   has_tangents;
  float *tangents;

  int32_t   num_triangles;   // Number of triangles
  int32_t * tri_indices;     // Indices into positions, normals, texcoords
  uint32_t *mat_indices;     // Indices into mat_params (len num_triangles)

  float bbox_min[3];   // Scene BBox
  float bbox_max[3];   //

  int32_t num_materials;
  //MaterialParams*     mat_params;     // Material params
};


void applyLoadXForm(mrf::optix_backend::Mesh &mesh, const float *load_xform);

class MRF_OPTIX_EXPORT OptixGeometryHandler
{
public:
  OptixGeometryHandler(optix::Context &context);
  ~OptixGeometryHandler();
  void importMrfScene(
      mrf::rendering::Scene const &             scene,
      mrf::optix_backend::OptixMaterialHandler &materialHandler,
      mrf::optix_backend::OptixLightHandler &   light_handler,
      std::vector<std::string> const &          cuda_compile_options,
      PTXConfig                                 ptx_config);

  optix::GeometryInstance
                          createParallelogram(const optix::float3 &anchor, const optix::float3 &offset1, const optix::float3 &offset2);
  optix::GeometryInstance createSphere(const optix::float3 &center, const float &radius);

  std::vector<std::pair<optix::GeometryInstance, mrf::geom::Shape *>> &geometryInstances();

  inline std::vector<std::pair<optix::GeometryInstance, unsigned int>> const &analyticalShapes() const;
  inline std::vector<std::pair<optix::GeometryInstance, unsigned int>> &      analyticalShapes();


  void loadMeshFromMRF(mrf::geom::Mesh const &mrf_mesh, OptiXMesh &optix_mesh, const float *transform = nullptr);

  void setupMeshLoaderInputs(optix::Context context, OptixMeshBuffers &buffers, Mesh &mesh);

private:
  optix::Context &_context; /**< Need a context to create optix acceleration data structure*/


  static constexpr char const *const SPATIAL_ACCELERATION_DATA_STRUCTURE = "Trbvh";

  optix::Program                                                      _pgram_intersection;
  optix::Program                                                      _pgram_bounding_box;
  optix::Program                                                      _pgram_attributes;
  optix::Program                                                      _pgram_intersection_sphere;
  optix::Program                                                      _pgram_bounding_box_sphere;
  optix::Program                                                      _pgram_intersection_triangle;
  optix::Program                                                      _pgram_bounding_box_triangle;
  std::vector<std::pair<optix::GeometryInstance, mrf::geom::Shape *>> _optix_geometry_instances;

  // This stores analytical shapes of the scene such as sphere
  // but not spherical light source!
  std::vector<std::pair<optix::GeometryInstance, unsigned int>> _optix_analytical_shapes;
};

inline std::vector<std::pair<optix::GeometryInstance, unsigned int>> const &
OptixGeometryHandler::analyticalShapes() const
{
  return _optix_analytical_shapes;
}


inline std::vector<std::pair<optix::GeometryInstance, unsigned int>> &OptixGeometryHandler::analyticalShapes()
{
  return _optix_analytical_shapes;
}


}   // namespace optix_backend
}   // namespace mrf
