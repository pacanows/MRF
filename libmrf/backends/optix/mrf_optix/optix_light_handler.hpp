/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * backend optix
 * Optix light handling
 *
 **/
#pragma once

#include <mrf_optix_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/lighting/light.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_optix/optix_material_handler.hpp>

#include <mrf_optix/optix_light_plugin.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <mrf_optix_programs/include/lighting/light_definitions.h>

#include <vector>


//#define MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED

namespace mrf
{
namespace optix_backend
{
class MRF_OPTIX_EXPORT OptixLightHandler
{
public:
  OptixLightHandler(optix::Context &context, std::vector<mrf::lighting::Light *> const &mrf_lights);
  ~OptixLightHandler();
  void releaseMemory();
  void createLights();

  void loadPlugins(OptixLightPlugins &plugins) { _plugins = plugins; }

  std::vector<OptixBaseLight *> &getLights() { return _lights; }

  //void addQuadLight(optix::GeometryInstance gi);
  //void addSphereLight(optix::GeometryInstance gi);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  void updateLights(OptixMaterialHandler &material_handler, std::vector<uint> const &wavelengths);
#else
  void updateLights(OptixMaterialHandler &material_handler);
#endif

  //std::vector<QuadLight> const &  optixQuadLights() const;
  //std::vector<SphereLight> const &optixSphereLights() const;

  //mrf::materials::UMat *getUMatPointerQuadLight(int index) { return _optix_quad_light_materials[index]; }
  //mrf::materials::UMat *getUMatPointerSpereLight(int index) { return _optix_sphere_light_materials[index]; }

private:
  optix::Context &                           _context;    /**< Need a context to create optix buffers*/
  std::vector<mrf::lighting::Light *> const &_mrf_lights; /**< Light sources from the mrf scene*/
  /**
   * Stores the geometry instances associated to the lights
   * note that the directionnal lights doesn't have this structure since
   * they do not have any physical geometry that might be intersected by a ray
   */
  std::vector<optix::GeometryInstance> _optix_quad_light_geometries;
  std::vector<optix::GeometryInstance> _optix_sphere_light_geometries;


  /**Optix buffers for the GPU Path tracer*/
  optix::Buffer _optix_quad_light_buffer;
  optix::Buffer _optix_sphere_light_buffer;
  optix::Buffer _optix_directionnal_light_buffer;


  std::vector<BaseLight *> _optix_quad_lights;
  std::vector<BaseLight *> _optix_sphere_lights;
  std::vector<BaseLight *> _optix_directionnal_lights;

  std::vector<OptixBaseLight *> _lights;

  //std::map<mrf::materials::UMat*, optix::Buffer> _emittanceMat_to_optix_buffer_quad_lights;

  bool _emi_spd_buffer_for_spherical_lights_allocated;
  bool _emi_spd_buffer_for_quad_lights_allocated;
  bool _emi_spd_buffer_for_dir_lights_allocated;

  OptixLightPlugins _plugins;
};


}   // namespace optix_backend
}   // namespace mrf
