/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * modifications : David Murray @ inria.fr
 * Copyright INRIA 2019,2020
 *
 * modifications : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 * backend optix renderer
 *
 **/
#pragma once

#include <mrf_optix_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/rendering/renderer.hpp>
#include <mrf_core/rendering/camera.hpp>

#include <mrf_core/math/vec3.hpp>

#include <mrf_core/lighting/envmap.hpp>
#include <mrf_core/lighting/panoramic_envmap.hpp>
#include <mrf_core/lighting/spherical_envmap.hpp>

#include <mrf_optix/optix_util.hpp>
#include <mrf_optix/optix_light_handler.hpp>
#include <mrf_optix/optix_geometry_handler.hpp>
#include <mrf_optix/optix_material_handler.hpp>
#include <mrf_optix/optix_camera_handler.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>
#include <optixu/optixu_math_stream_namespace.h>

#include <cstdio>
#include <cstdlib>
#include <vector>


namespace mrf
{
namespace optix_backend
{
class MRF_OPTIX_EXPORT OptixRenderer: public rendering::Renderer
{
public:   //METHODS
  OptixRenderer(OptixMaterialPlugins &pluginsMat, OptixLightPlugins &pluginsLight);
  OptixRenderer(OptixMaterialPlugins &pluginsMat, OptixLightPlugins &pluginsLight, bool rt_ptx, bool overwrite_ptx);
  virtual ~OptixRenderer();
#ifdef MRF_RENDERING_MODE_SPECTRAL
  /*!
   *
   *
   * \param interactive
   * \param nb_wavelengths_per_pass
   */
  virtual void init(bool interactive, uint nb_wavelengths_per_pass);
#else
  /*!
   * Function to initialize the rendering backend, depending if the rendeirng is interactive or not.
   * \param interactive
   */
  virtual void init(bool interactive = false);
#endif

  /*!
   * Free the backend resources.
   */
  virtual void shutdown();

  /*!
   * Function to display information about the backend.
   * \return
   */
  virtual std::string getInfos() const;


  virtual void setRotationPhi(float phi);
  virtual void setRotationTheta(float theta);

  /*!
   * Renders a (sub)frame (one sample) at given position for given size, to be used for interactive renderer. To be implemented in specific rendering backend.
   * \param width : the rendering width.
   * \param height : the rendering height.
   * \param posX : the rendering X starting position.
   * \param posY : the rendering Y starting position.
   * \return the rendering time of the frame/the sample.
   */
  virtual float renderFrame(size_t width, size_t height, int posX = 0, int posY = 0);

  /*!
   * Renders a given number of samples at given position for given size. To be implemented in specific rendering backend.
   * \param samples : the number of samples to render. Default is 0, in that case, the attribute _max_samples is used.
   * \param width : the rendering width.
   * \param height : the rendering height.
   * \param posX : the rendering X starting position.
   * \param posY : the rendering Y starting position.
   * \return the rendering time of the desired number of samples.
   */
  virtual float renderSamples(int samples, size_t width, size_t height, int posX = 0, int posY = 0);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  /*!
   *
   * \param interactive_mode
   * \param nb_wavelengths_per_pass
   */
  void createRenderContext(bool interactive_mode, uint nb_wavelengths_per_pass);
#else
  /*!
   *
   * \param interactive_mode
   */
  void createRenderContext(bool interactive_mode);
#endif

  /*!
   * Creates the backend render target.
   */
  virtual void createRenderTarget();

  /*!
   * Resizes the render target.
   * \param width
   * \param height
   */
  virtual void resizeRenderTarget(size_t width, size_t height);

  /*!
   * Updates the Cranley Patterson RNG buffer if necessary.
   */
  void updateCranleyPattersonBuffer();

  /*!
   * Updates the Sobol RNG buffer if necessary.
   */
  void updateSobolRandomNumbers();

  /**
   * Destroy the Optix context.
   */
  void destroyRenderContext();

  /*!
   * Imports the MRF scene and camera into Optix specific buffers and handlers.
   * \param scene_loader : the scene parser to load the .mrf file.
   * \param camera_file : the full path to the .mcf file. The file can contain multiple cameras.
   * \param start_cam : the camera to be used as first rendering camera.
   * \param num_cam : the number of camera to be loaded. If 0 (or in interactive mode), all cameras in camera_file are loaded.
   */
  virtual void importMrfScene(
      mrf::io::SceneParser *scene_loader,
      std::string           camera_file,
      size_t                starting_cam = 0,
      size_t                num_cam      = 0);

  /*!
   * Imports the MRF scene and camera into Optix specific buffers and handlers.
   * \param scene_file : the scene path to load the .mrf file.
   * \param camera_file : the full path to the .mcf file. The file can contain multiple cameras.
   * \param start_cam : the camera to be used as first rendering camera.
   * \param num_cam : the number of camera to be loaded. If 0 (or in interactive mode), all cameras in camera_file are loaded.
   */
  virtual void
  importMrfScene(std::string scene_file, std::string camera_file, size_t starting_cam = 0, size_t num_cam = 0);

  /*!
   * Frees or clears the backend resources (Optix Buffers, kernels and handlers).
   */
  virtual void freeSceneResources();

  /*!
   * Update the camera of the backend
   */
  virtual void updateBackendCamera();

  /*!
   * Updates the camera parameters of the backend and updates kernel parameters.
   */
  virtual void updateCameraParameters();

#if defined(MRF_RENDERING_MODE_SPECTRAL) && defined(RENDERER_INTERACTIVE)
  void convertOutputSpectralImageToRGBGPUBuffer(optix::Buffer buffer, bool log_time = false);
#endif

  void updateOptixContextForEnvmapRotation();

#ifdef RENDERER_INTERACTIVE
  /*!
   * Create an OptiX buffer based on an OpenGL buffer for interoperability purpose.
   * \param GLBufferID
   * \param bufferName
   * \param nbComp
   * \return
   */
  virtual bool
  createBufferFromGLBuffer(int GLBufferID, std::string bufferName, size_t frame_width, size_t frame_height, int nbComp);

  /*!
   * Ensures that OptiX does not prevent the GUI from modifying the buffer.
   * \param buffer_name : the name of the buffer in the backend, if any.
   * \return true if the buffer is ready to be updated by the GUI, false otherwise.
   */
  virtual bool readyBufferForUpdate(int GLBufferID, std::string bufferName);

  /*!
   * Ensures that OptiX gets access to the buffer for modifications.
   * \param buffer_name : the name of the buffer in the backend, if any.
   * \return true if the buffer is ready to be modified by the backend, false otherwise.
   */
  virtual bool readyBufferForRendering(int GLBufferID, std::string bufferName);
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
  /*!
   *
   *
   * \param prepend_to_filename
   * \param wavelength_pass_number
   * \param frame_number
   */
  void debugSpectralOptixBuffer(
      std::string const &prepend_to_filename,
      mrf::uint          wavelength_pass_number,
      mrf::uint          frame_number);
#endif

  /*!
   * Simple setter for the next event estimation (NEE).
   * \param type : NONE for no NEE, ONE to enable NEE, ALL = ONE.
   */
  virtual void setNextEvent(mrf::rendering::NEXT_EVENT type)
  {
    _next_event_type = type;
    if (_context) _context["next_event"]->setUint(_next_event_type);
  }

  /*!
   * Simple setter for the RNG method to use.
   * \param seed
   */
  virtual void setRNGSeed(mrf::rendering::RENDERER_RNG seed);

  /*!
   * Function to add a backend-specific file like an integrator. See setCudaRendererFilename().
   * \param file
   */
  virtual void setExternalFile(std::string const &file) { setCudaRendererFilename(file); }

  /*!
   * Reset the frame counter.
   */
  virtual void resetRendering(bool clear = false);

  /*!
   * Reset the frame counter.
   */
  virtual void resetNumFrame(int value = 1)
  {
    _num_frame = value;
    if (_context) _context["frame_number"]->setUint(_num_frame);
  }

  /*!
   * Simple setter.
   * \param factor
   */
  virtual void setSpatialFactor(uint factor)
  {
    _spatial_subdivision_factor = factor;
    if (_context) _context["_spatial_factor"]->setUint(_spatial_subdivision_factor);
  }

  /*!
   * Simple setter.
   * \param gamma
   */
  virtual void setMaxPathLength(int max_path_length)
  {
    _max_path_length = max_path_length;
    if (_context) _context["max_path_length"]->setUint(_max_path_length);
  }

  /*!
   * Simple setter.
   * \param gamma
   */
  virtual void setGamma(float gamma)
  {
    _gamma = gamma;
    if (_context) _context["gamma"]->setFloat(_gamma);
  }

  /*!
   * Simple setter.
   * \param exposure
   */
  virtual void setExposure(float exposure)
  {
    _exposure = exposure;
    if (_context) _context["exposure"]->setFloat(_exposure);
  }

  /*!
   * Increases the theta rotation angle of the envmap, if any, of the given amount.
   * \param delta
   * \return new value of theta
   */
  virtual float increaseRotationTheta(float delta);

  /*!
   * Increases the phi rotation angle of the envmap, if any, of the given amount.
   * \param delta
   * \return new value of phi
   */
  virtual float increaseRotationPhi(float delta);

private:   //METHODS
  /*!
   * Propagate an update order to the material handler.
   */
  void updateMaterials();

  /*!
   * Update the wavelengths buffer for current pass.
   */
  void updateWavelengths();

  /*!
   * Propagate an update order to the material handler.
   */
  void applyMaterials();

  /*!
   * Propagate an update order to the light handler.
   */
  void updateLights();

  /*!
   * In spectral mode, updates the background according to the wavelenghts being processed. This funciton does nothing in RGB mode.
   */
  void updateBackground();

  /*!
   *
   * \param file
   */
  void setCudaRendererFilename(std::string const &file) { _cuda_renderer_filename = file; };

  /*!
   * Gets the image from the GPU rendering buffer back to CPU in the image container.
   */
  virtual void fetchImage();

private:   //MEMBERS
  optix::Context                            _context;
  mrf::optix_backend::OptixLightHandler *   _light_handler;
  mrf::optix_backend::OptixMaterialHandler *_material_handler;
  mrf::optix_backend::OptixGeometryHandler *_geometry_handler;
  int                                       _rr_begin_depth;

  std::vector<std::string> _cuda_compile_options;
  std::string              _cuda_renderer_filename;

  /**
   *  Store the envmap buffer
   *  The buffer is created once in the first call to updateBackground()
   *  Its size is equal to the image contain in the Envmap of the mrf scene
   *  We never resize this buffer, will be problematic in the future if we change
   *  the envmap after loading the scene.
   **/
  optix::Buffer _envmap_buffer;

  //optix::Buffer _envmap_cdf_inv_rows_buffer;
  optix::Buffer _envmap_cdf_rows_buffer;
  optix::Buffer _envmap_pdf_rows_buffer;
  optix::Buffer _envmap_pdf_cols_buffer;
  //optix::Buffer _envmap_cdf_inv_cols_buffer;
  optix::Buffer _envmap_cdf_cols_buffer;

  optix::Buffer _envmap_cdf_inv_cols_buffer;

#ifdef MRF_RENDERING_MODE_SPECTRAL
  //Used only in multiplexing to store spectrum of uniform background color
  optix::Buffer _background_color_buffer;
#endif

  OptixMaterialPlugins _mat_plugins;
  OptixLightPlugins    _light_plugins;

  PTXConfig _ptx_config;
};

}   // namespace optix_backend
}   // namespace mrf
