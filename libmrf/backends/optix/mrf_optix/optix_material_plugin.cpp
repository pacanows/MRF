/*
 *
 * author : David Murray @institutoptique.fr
 * Copyright INRIA 2021
 *
 *
 **/

#include <mrf_optix/optix_material_plugin.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_util.hpp>
#include <mrf_optix/optix_renderer.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
OptixBaseMaterial::OptixBaseMaterial(optix::Material mat): _optix_mat(mat), _mrf_material(nullptr), _program(nullptr) {}

OptixBaseMaterial::OptixBaseMaterial(std::vector<std::string> const &cuda_compile_options)
  : _cuda_compile_options(cuda_compile_options)
  , _mrf_material(nullptr)
  , _program(nullptr)
{}

OptixBaseMaterial::OptixBaseMaterial(optix::Material mat, std::vector<std::string> const &cuda_compile_options)
  : _optix_mat(mat)
  , _cuda_compile_options(cuda_compile_options)
  , _mrf_material(nullptr)
  , _program(nullptr)
{}

OptixBaseMaterial::OptixBaseMaterial(
    mrf::materials::UMat *          mrf_mat,
    optix::Material                 mat,
    std::vector<std::string> const &cuda_compile_options)
  : _optix_mat(mat)
  , _cuda_compile_options(cuda_compile_options)
  , _mrf_material(mrf_mat)
  , _program(nullptr)
{}

OptixBaseMaterial::~OptixBaseMaterial()
{
  _optix_mat->destroy();
  for (auto tex : _textures)
    tex.second->destroy();

  for (auto buf : _buffers)
    buf.second->destroy();

  if (_program) delete _program;
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixBaseMaterial::updateRadianceGPUBuffer(
    optix::Context &                                  context,
    std::vector<mrf::materials::RADIANCE_TYPE> const &radiances,
    std::vector<std::string> const &                  variable_names,
    std::vector<uint> const &                         wavelengths)
{
  assert(radiances.size() == variable_names.size());
  //if multiplexing
  if (wavelengths.size() > 1)
  {
    //Create one Optix Buffer per Spectral Distribution that is stored in the vector radiances
    //if buffer was never created
    for (auto variable : variable_names)
    {
      //Check if variable buffer need to be created, or already in map.
      if (_buffers.find(variable) == _buffers.end())
      {
        auto buffer = context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_USER);
        buffer->setElementSize(sizeof(float));
        buffer->setSize(wavelengths.size());

        _buffers[variable] = buffer;
      }
    }

    //Now update buffer value
    for (uint id_rad = 0; id_rad < radiances.size(); id_rad++)
    {
      auto & buffer     = _buffers[variable_names[id_rad]];
      void * data       = buffer->map();
      float *float_data = (float *)data;

      for (uint wave_id = 0; wave_id < wavelengths.size(); wave_id++)
      {
        uint wave           = wavelengths[wave_id];
        float_data[wave_id] = radiances[id_rad].findLinearyInterpolatedValue(static_cast<float>(wave));
      }
      buffer->unmap();
    }
  }
  else
  {}
}
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixBaseMaterial::setCustomRadianceVariables(
    optix::Context &                                  context,
    std::vector<mrf::materials::RADIANCE_TYPE> const &radiance_values,
    std::vector<std::string> const &                  variable_names,
    std::vector<uint> const &                         wavelengths)
#else
void OptixBaseMaterial::setCustomRadianceVariables(
    std::vector<mrf::materials::RADIANCE_TYPE> const &radiance_values,
    std::vector<std::string> const &                  variable_names)
#endif
{
  //Sanity check
  if (radiance_values.size() != variable_names.size())
  {
    mrf::gui::fb::Loger::getInstance()->fatal(
        "Radiance values and Variables Names doesn't have the same size setCustomRadianceVariable() !");
    mrf::gui::fb::Loger::getInstance()->fatal("Abort SetCustomVariable !");
    return;
  }

  if (_variable_values.size() == 0) _variable_values = radiance_values;
  if (_variable_names.size() == 0) _variable_names = variable_names;

  if (_variable_values.size() == 0) return;

#ifdef MRF_RENDERING_MODE_SPECTRAL
  //If multiplexing
  if (wavelengths.size() > 1)
  {
    updateRadianceGPUBuffer(context, radiance_values, variable_names, wavelengths);
    //if (_custom_radiance_buffers.find(material) == _custom_radiance_buffers.end())
    //{
    //  mrf::gui::fb::Loger::getInstance()->fatal("Cannot find material buffer in setCustomRadianceVariable() !");
    //  mrf::gui::fb::Loger::getInstance()->fatal("Abort SetCustomVariable !");
    //  return;
    //}

    for (auto variable : variable_names)
    {
      _optix_mat[variable]->set(_buffers[variable]);
    }
  }
  else   //Only one wavelength at each pass
  {
    for (uint id_variable = 0; id_variable < variable_names.size(); id_variable++)
    {
      auto interpolated_val = radiance_values[id_variable].findLinearyInterpolatedValue(float(wavelengths[0]));
      _optix_mat[variable_names[id_variable]]->setFloat(interpolated_val);
    }
  }

#else   //RGB MODE
  for (uint id_variable = 0; id_variable < variable_names.size(); id_variable++)
  {
    _optix_mat[variable_names[id_variable]]->setFloat(mrfToOptix(radiance_values[id_variable]));
  }
#endif
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixBaseMaterial::update(optix::Context &context, std::vector<uint> wavelengths)
{
  setCustomRadianceVariables(context, _variable_values, _variable_names, wavelengths);
}
#else
void OptixBaseMaterial::update(optix::Context &context)
{
  setCustomRadianceVariables(_variable_values, _variable_names);
}
#endif

void OptixBaseMaterial::compile(optix::Context &context)
{
  _program->load(context);
  if (_kernel_path.size())
  {
    _program->addInclude(_kernel_path);
    _program->compile(context, _kernel_path);
  }
  else
    _program->compile(context);
}

void OptixBRDFMaterial::compile(optix::Context &context)
{
  OptixBaseMaterial::compile(context);

  //const char *   ptx         = getPtxString("BRDF_kernel.cu", _cuda_compile_options);
  //optix::Program closest_hit = context->createProgramFromPTXString(ptx, "material_eval");
  //optix::Program any_hit     = context->createProgramFromPTXString(ptx, "shadow");

  optix::Program closest_hit = _program->createClosestHitProgram(context);
  optix::Program any_hit     = _program->createAnyHitProgram(context);

  _optix_mat->setClosestHitProgram(0, closest_hit);
  _optix_mat->setAnyHitProgram(1, any_hit);
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixBRDFMaterial::update(optix::Context &context, std::vector<uint> wavelengths)
{
  OptixBaseMaterial::update(context, wavelengths);
}
#else
void OptixBRDFMaterial::update(optix::Context &context)
{
  OptixBaseMaterial::update(context);
}
#endif

void OptixBSDFMaterial::compile(optix::Context &context)
{
  OptixBaseMaterial::compile(context);

  //const char *   ptx         = getPtxString("BSDF_kernel.cu", _cuda_compile_options);
  //optix::Program closest_hit = context->createProgramFromPTXString(ptx, "material_eval");
  //optix::Program any_hit     = context->createProgramFromPTXString(ptx, "shadow");

  optix::Program closest_hit = _program->createClosestHitProgram(context);
  optix::Program any_hit     = _program->createAnyHitProgram(context);

  _optix_mat->setClosestHitProgram(0, closest_hit);
  _optix_mat->setAnyHitProgram(1, any_hit);
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixBSDFMaterial::update(optix::Context &context, std::vector<uint> wavelengths)
{
  OptixBaseMaterial::update(context, wavelengths);
}
#else
void OptixBSDFMaterial::update(optix::Context &context)
{
  OptixBaseMaterial::update(context);
}
#endif

void OptixEmittanceMaterial::compile(optix::Context &context)
{
  OptixBaseMaterial::compile(context);

  //const char *   ptx_diffuse_em = getPtxString("diffuse_emitter.cu", _cuda_compile_options);
  //optix::Program diffuse_em     = context->createProgramFromPTXString(ptx_diffuse_em, "diffuseEmitter");
  //_optix_mat->setClosestHitProgram(0, diffuse_em);

  optix::Program diffuse_em = _program->createProgram(context, "diffuseEmitter");
  _optix_mat->setClosestHitProgram(0, diffuse_em);
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixEmittanceMaterial::update(optix::Context &context, std::vector<uint> wavelengths)
{
  OptixBaseMaterial::update(context, wavelengths);
}
#else
void OptixEmittanceMaterial::update(optix::Context &context)
{
  OptixBaseMaterial::update(context);
}
#endif

}   // namespace optix_backend
}   // namespace mrf
