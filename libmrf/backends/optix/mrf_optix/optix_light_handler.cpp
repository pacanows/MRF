/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/
#include <mrf_optix/optix_light_handler.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/sphere.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_util.hpp>
#include <mrf_optix/optix_renderer.hpp>

#include <iostream>

using mrf::optix_backend::mrfToOptix;

namespace mrf
{
namespace optix_backend
{
OptixLightHandler::OptixLightHandler(optix::Context &context, std::vector<mrf::lighting::Light *> const &mrf_lights)
  : _context(context)
  , _mrf_lights(mrf_lights)
  , _emi_spd_buffer_for_spherical_lights_allocated(false)
  , _emi_spd_buffer_for_quad_lights_allocated(false)
  , _emi_spd_buffer_for_dir_lights_allocated(false)
{}

OptixLightHandler::~OptixLightHandler()
{
  /*
  **destroy render context already destroy everything

  releaseMemory();
  */

  for (auto light : _lights)
    delete light;
}

void OptixLightHandler::releaseMemory()
{
  for (auto light : _lights)
    delete light;
  _optix_quad_light_buffer->destroy();
  _optix_sphere_light_buffer->destroy();
  _optix_directionnal_light_buffer->destroy();
}

void OptixLightHandler::createLights()
{
  //createSurfacicLight(0,0);

  for (uint light_num = 0; light_num < _mrf_lights.size(); light_num++)
  {
    mrf::lighting::Light *mrf_light = (_mrf_lights[light_num]);
    if (!mrf_light) continue;

    if (mrf_light->type() == mrf::lighting::Light::POINT_LIGHT)
    {
      mrf::gui::fb::Loger::getInstance()->warn(
          " POINT LIGHT SOURCES ARE NOT SUPPORTED. USE SPHERICAL LIGHT SOURCES with a small radius.");
      continue;
    }

    OptixBaseLight *light = _plugins.fcts[mrf_light->type()]->createFromMRF(mrf_light);
    _lights.push_back(light);
    if (mrf_light->type() == mrf::lighting::Light::QUAD_LIGHT)
    {
      _optix_quad_lights.push_back(light->getAsStruct());
    }
    else if (mrf_light->type() == mrf::lighting::Light::DIR_LIGHT)
    {
      _optix_directionnal_lights.push_back(light->getAsStruct());
    }
    else if (mrf_light->type() == mrf::lighting::Light::SPHERE_LIGHT)
    {
      _optix_sphere_lights.push_back(light->getAsStruct());
    }
  }

  _optix_quad_light_buffer = _context->createBuffer(RT_BUFFER_INPUT);
  _optix_quad_light_buffer->setFormat(RT_FORMAT_USER);
  _optix_quad_light_buffer->setElementSize(sizeof(QuadLight));

#ifdef MRF_RENDERING_DEBUG
  std::cout << " TO DEBUG sizeof Quadlight size = " << sizeof(QuadLight) << std::endl;
  std::cout << " TO DEBUG SIZEOF optix::Handle<optix::BufferObj> = " << sizeof(optix::Handle<optix::BufferObj>)
            << std::endl;
#endif

  _optix_quad_light_buffer->setSize(static_cast<unsigned int>(_optix_quad_lights.size()));

  if (_optix_quad_lights.size() > 0)
  {
    QuadLight *qLs = new QuadLight[_optix_quad_lights.size()];
    for (int i = 0; i < _optix_quad_lights.size(); ++i)
    {
      qLs[i].emission = ((QuadLight *)_optix_quad_lights[i])->emission;
      qLs[i].corner   = ((QuadLight *)_optix_quad_lights[i])->corner;
      qLs[i].v1       = ((QuadLight *)_optix_quad_lights[i])->v1;
      qLs[i].v2       = ((QuadLight *)_optix_quad_lights[i])->v2;
      qLs[i].normal   = ((QuadLight *)_optix_quad_lights[i])->normal;
    }
    memcpy(_optix_quad_light_buffer->map(), qLs, _optix_quad_lights.size() * sizeof(QuadLight));
    _optix_quad_light_buffer->unmap();
    delete qLs;
  }
  _context["lights"]->setBuffer(_optix_quad_light_buffer);


  _optix_sphere_light_buffer = _context->createBuffer(RT_BUFFER_INPUT);
  _optix_sphere_light_buffer->setFormat(RT_FORMAT_USER);
  _optix_sphere_light_buffer->setElementSize(sizeof(SphereLight));
  _optix_sphere_light_buffer->setSize(static_cast<unsigned int>(_optix_sphere_lights.size()));

  if (_optix_sphere_lights.size() > 0)
  {
    SphereLight *sLs = new SphereLight[_optix_sphere_lights.size()];
    for (int i = 0; i < _optix_sphere_lights.size(); ++i)
    {
      sLs[i].emission = ((SphereLight *)_optix_sphere_lights[i])->emission;
      sLs[i].position = ((SphereLight *)_optix_sphere_lights[i])->position;
      sLs[i].radius   = ((SphereLight *)_optix_sphere_lights[i])->radius;
    }
    memcpy(_optix_sphere_light_buffer->map(), sLs, _optix_sphere_lights.size() * sizeof(SphereLight));
    _optix_sphere_light_buffer->unmap();
    delete sLs;
  }
  _context["sphere_lights"]->setBuffer(_optix_sphere_light_buffer);


  _optix_directionnal_light_buffer = _context->createBuffer(RT_BUFFER_INPUT);
  _optix_directionnal_light_buffer->setFormat(RT_FORMAT_USER);
  _optix_directionnal_light_buffer->setElementSize(sizeof(DirectionnalLight));
  _optix_directionnal_light_buffer->setSize(static_cast<unsigned int>(_optix_directionnal_lights.size()));

  if (_optix_directionnal_lights.size() > 0)
  {
    DirectionnalLight *dLs = new DirectionnalLight[_optix_directionnal_lights.size()];
    for (int i = 0; i < _optix_directionnal_lights.size(); ++i)
    {
      dLs[i].emission  = ((DirectionnalLight *)_optix_directionnal_lights[i])->emission;
      dLs[i].direction = ((DirectionnalLight *)_optix_directionnal_lights[i])->direction;
    }
    memcpy(_optix_directionnal_light_buffer->map(), dLs, _optix_directionnal_lights.size() * sizeof(DirectionnalLight));
    _optix_directionnal_light_buffer->unmap();
    delete dLs;
  }
  _context["directionnal_lights"]->setBuffer(_optix_directionnal_light_buffer);
}

//void OptixLightHandler::addQuadLight(optix::GeometryInstance gi)
//{
//  _optix_quad_light_geometries.push_back(gi);
//}
//
//void OptixLightHandler::addSphereLight(optix::GeometryInstance gi)
//{
//  _optix_sphere_light_geometries.push_back(gi);
//}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixLightHandler::updateLights(OptixMaterialHandler &, std::vector<uint> const &wavelengths)
#else
void OptixLightHandler::updateLights(OptixMaterialHandler &)
#endif
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  mrf::gui::fb::Loger::getInstance()->trace("OptixLightHandler: ENTERING updateLights");

#  ifdef MRF_RENDERING_DEBUG
  std::cout << " Updating Light assets for wavelengths ";
  for (size_t i = 0; i < wavelengths.size(); i++)
  {
    std::cout << wavelengths[i] << " ";
  }
  std::cout << std::endl;
#  endif

#endif



  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Updating Quad Lights
  //------------------------------------------------------------------------------------------------------------------------------------------------------------

#ifdef MRF_RENDERING_MODE_SPECTRAL
  // ALLOCATE THE BUFFER EVEN IF THERE IS NO QUAD LIGHT SOURCES ?
  if (!_emi_spd_buffer_for_quad_lights_allocated)
  {
#  ifdef MRF_RENDERING_DEBUG
    std::cout << " FIRST TIME ALLOCATION for emi_spd_buffer_for_quad_light_sources. wavelengths.size() = "
              << wavelengths.size() << std::endl;
#  endif

    auto buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_USER);
    buffer->setElementSize(sizeof(float));

    // Stores all SPD for emissive materials in a global buffer wavelengths.size() is equal to (NB_WAVELENGHT_MULTIPLEXED + 1)
    buffer->setSize(std::max(_optix_quad_lights.size(), (size_t)1) * wavelengths.size());

    _context["emi_spd_quad_lights"]->setBuffer(buffer);

    _emi_spd_buffer_for_quad_lights_allocated = true;
  }

  auto   buffer            = _context["emi_spd_quad_lights"]->getBuffer();
  void * buffer_data       = buffer->map();
  float *buffer_float_data = (float *)buffer_data;
#endif

  for (auto light : _lights)
#ifdef MRF_RENDERING_MODE_SPECTRAL
    light->updateLight(wavelengths);
#else
    light->updateLight();
#endif



  if (_optix_quad_lights.size() >= 1)
  {
    // HERE WE allocate the main buffer for SPD on quad lights.
    // This is done ONCE
    //
    // This buffer is declared in material_eval.cu  under the name rtbuffer<float,1> emi_spd_quad_lights

    int count = 0;
    for (size_t i = 0; i < _mrf_lights.size(); ++i)
    {
      auto light = _mrf_lights[i];
      if (!(light->type() == mrf::lighting::Light::QUAD_LIGHT))
      {
        continue;
      }
      //if (diffuse_emittance)
      {
        float                    radiance       = light->emit_pointer()->getRadiance();
        std::vector<std::string> variable_names = {"emission_color"};

        std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
        variable_values.push_back(light->emit_pointer()->getColor() * radiance);

#ifdef MRF_RENDERING_MODE_SPECTRAL
        auto light_spectrum = light->emit_pointer()->getColor();

        // Construct a spectral distribution at requested Wavelength
        for (uint wave_id = 0; wave_id < wavelengths.size(); wave_id++)
        {
          uint const wave = wavelengths[wave_id];

          buffer_float_data[wave_id + count * wavelengths.size()]
              = light_spectrum.findLinearyInterpolatedValue(static_cast<float>(wave)) * radiance;
        }

        auto light_color = light_spectrum.findLinearyInterpolatedValue(float(wavelengths[0])) * radiance;

        //Putting a value here for spectral rendering with one wavelength (no multiplexing)
        _optix_quad_lights[count]->emission = light_color;

        //material_handler.setCustomRadianceVariables(
        //    _optix_quad_light_geometries[i],
        //    _optix_quad_light_materials[i],
        //    variable_values,
        //    variable_names,
        //    wavelengths);
#else
        auto light_color = light->emit_pointer()->getColor() * radiance;
        /*       material_handler.setCustomRadianceVariables(
                   _optix_quad_light_geometries[i]),
                   variable_values,
                   variable_names);*/
        _optix_quad_lights[count]->emission = mrfToOptix(light_color);
#endif
        count++;
      }

    }   //enf of for-loop on quad lights

    /*Update optix GPU Buffer from CPU Buffer*/
    // REMARK ONLy useful for RGB or one wavelength per pass

    QuadLight *qLs = new QuadLight[_optix_quad_lights.size()];
    for (int i = 0; i < _optix_quad_lights.size(); ++i)
    {
      qLs[i].emission = ((QuadLight *)_optix_quad_lights[i])->emission;
      qLs[i].corner   = ((QuadLight *)_optix_quad_lights[i])->corner;
      qLs[i].v1       = ((QuadLight *)_optix_quad_lights[i])->v1;
      qLs[i].v2       = ((QuadLight *)_optix_quad_lights[i])->v2;
      qLs[i].normal   = ((QuadLight *)_optix_quad_lights[i])->normal;
    }
    memcpy(_optix_quad_light_buffer->map(), qLs, _optix_quad_lights.size() * sizeof(QuadLight));
    _optix_quad_light_buffer->unmap();
    delete qLs;
  }

// Un-mapping buffer in any case
#ifdef MRF_RENDERING_MODE_SPECTRAL
  //  std::cout << " ENTERING " << __FILE__ << " " << __LINE__ << std::endl;
  buffer->unmap();
#endif

  //  std::cout << " ENTERING " << __FILE__ << " " << __LINE__ << std::endl;

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Updating Spherical Lights
  //------------------------------------------------------------------------------------------------------------------------------------------------------------


#ifdef MRF_RENDERING_MODE_SPECTRAL
  // HERE WE allocate the main buffer for SPD on SPHERICAL light sources.
  // This is done ONCE
  //
  // This buffer is declared in material_eval.cu  under the name rtbuffer<float,1> emi_spd_spherelights
  if (!_emi_spd_buffer_for_spherical_lights_allocated)
  {
    //    std::cout << " FIRST TIME ALLOCATION for emi_spd_buffer_for SPHERICAL light_sources" << std::endl;

    auto emi_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_USER);
    emi_buffer->setElementSize(sizeof(float));

    // Stores all SPD for emissive materials in a global buffer wavelengths.size() is equal to (NB_WAVELENGHT_MULTIPLEXED + 1)
    emi_buffer->setSize(_optix_sphere_lights.size() * wavelengths.size());

    _context["emi_spd_sphere_lights"]->setBuffer(emi_buffer);

    _emi_spd_buffer_for_spherical_lights_allocated = true;
  }

  buffer            = _context["emi_spd_sphere_lights"]->getBuffer();
  buffer_data       = buffer->map();
  buffer_float_data = (float *)buffer_data;
#endif

  if (_optix_sphere_lights.size() >= 1)
  {
    int count = 0;
    for (size_t i = 0; i < _mrf_lights.size(); ++i)
    {
      auto light = _mrf_lights[i];
      if (!(light->type() == mrf::lighting::Light::SPHERE_LIGHT))
      {
        continue;
      }

      //auto diffuse_emittance = dynamic_cast<mrf::materials::DiffuseEmittance *>(_optix_sphere_light_materials[i]);
      //if (diffuse_emittance)
      {
        float                    radiance       = light->emit_pointer()->getRadiance();
        std::vector<std::string> variable_names = {"emission_color"};

        std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
        variable_values.push_back(light->emit_pointer()->getColor() * radiance);

#ifdef MRF_RENDERING_MODE_SPECTRAL
        auto light_spectrum = light->emit_pointer()->getColor();

        // Construct a spectral distribution at requested Wavelength
        for (uint wave_id = 0; wave_id < wavelengths.size(); wave_id++)
        {
          uint const wave = wavelengths[wave_id];
          buffer_float_data[wave_id + count * wavelengths.size()]
              = light_spectrum.findLinearyInterpolatedValue(static_cast<float>(wave)) * radiance;
        }

        auto light_color = light_spectrum.findLinearyInterpolatedValue(float(wavelengths[0])) * radiance;

        //Putting a value here for spectral rendering with one wavelength (no multiplexing)
        _optix_sphere_lights[count]->emission = light_color;

        //material_handler.setCustomRadianceVariables(
        //    _optix_sphere_light_geometries[i],
        //    _optix_sphere_light_materials[i],
        //    variable_values,
        //    variable_names,
        //    wavelengths);
#else
        auto light_color                    = light->emit_pointer()->getColor() * radiance;
        //material_handler.setCustomRadianceVariables(_optix_sphere_light_geometries[i], variable_values, variable_names);
        _optix_sphere_lights[count]->emission = mrfToOptix(light_color);
#endif
        count++;
      }
    }   //end of for loop for spherical lights

    /*Update optix GPU Buffer from CPU Buffer*/
    // REMARK ONLy useful for RGB or one wavelength per pass
    SphereLight *sLs = new SphereLight[_optix_sphere_lights.size()];
    for (int i = 0; i < _optix_sphere_lights.size(); ++i)
    {
      sLs[i].emission = ((SphereLight *)_optix_sphere_lights[i])->emission;
      sLs[i].position = ((SphereLight *)_optix_sphere_lights[i])->position;
      sLs[i].radius   = ((SphereLight *)_optix_sphere_lights[i])->radius;
    }
    memcpy(_optix_sphere_light_buffer->map(), sLs, _optix_sphere_lights.size() * sizeof(SphereLight));
    _optix_sphere_light_buffer->unmap();
    delete sLs;
  }

// Unmap the spherical light buffer in any case
#ifdef MRF_RENDERING_MODE_SPECTRAL
  //std::cout << " ENTERING " << __FILE__ << " " << __LINE__ << std::endl;
  buffer->unmap();
#endif

  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  // Updating Directional Lights
  //------------------------------------------------------------------------------------------------------------------------------------------------------------
  //std::cout << __FILE__ << " " << __LINE__ << std::endl;

#ifdef MRF_RENDERING_MODE_SPECTRAL
  if (!_emi_spd_buffer_for_dir_lights_allocated)
  {
    // std::cout << " FIRST TIME ALLOCATION for emi_spd_buffer_for DIRECTIONAL  light_sources" << std::endl;

    auto emi_buffer = _context->createBuffer(RT_BUFFER_INPUT, RT_FORMAT_USER);
    emi_buffer->setElementSize(sizeof(float));

    // Stores all SPD for emissive materials in a global buffer wavelengths.size() is equal to (NB_WAVELENGHT_MULTIPLEXED + 1)
    emi_buffer->setSize(_optix_directionnal_lights.size() * wavelengths.size());

    _context["emi_spd_dir_lights"]->setBuffer(emi_buffer);

    _emi_spd_buffer_for_dir_lights_allocated = true;
  }

  buffer            = _context["emi_spd_dir_lights"]->getBuffer();
  buffer_data       = buffer->map();
  buffer_float_data = (float *)buffer_data;

#endif
  if (_optix_directionnal_lights.size() >= 1)
  {
    int count = 0;
    for (size_t i = 0; i < _mrf_lights.size(); ++i)
    {
      auto light = _mrf_lights[i];
      if (!(light->type() == mrf::lighting::Light::DIR_LIGHT))
      {
        continue;
      }

      {
        float                    radiance       = light->emit_pointer()->getRadiance();
        std::vector<std::string> variable_names = {"emission_color"};

        std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
        variable_values.push_back(light->emit_pointer()->getColor() * radiance);

#ifdef MRF_RENDERING_MODE_SPECTRAL
        auto light_spectrum = light->emit_pointer()->getColor();

        // Construct a spectral distribution at requested Wavelength
        for (uint wave_id = 0; wave_id < wavelengths.size(); wave_id++)
        {
          uint const wave = wavelengths[wave_id];
          buffer_float_data[wave_id + count * wavelengths.size()]
              = light_spectrum.findLinearyInterpolatedValue(static_cast<float>(wave)) * radiance;
        }

        auto light_color = light_spectrum.findLinearyInterpolatedValue(float(wavelengths[0])) * radiance;

        //Putting a value here for spectral rendering with one wavelength (no multiplexing)
        _optix_directionnal_lights[count]->emission = light_color;

#else
        auto light_color                      = light->emit_pointer()->getColor() * radiance;
        //material_handler.setCustomRadianceVariables(_optix_sphere_light_geometries[i], variable_values, variable_names);
        _optix_directionnal_lights[count]->emission = mrfToOptix(light_color);
#endif
        count++;
      }
    }   //end of for loop for spherical lights

    /*Update optix GPU Buffer from CPU Buffer*/
    // REMARK ONLy useful for RGB or one wavelength per pass
    DirectionnalLight *dLs = new DirectionnalLight[_optix_directionnal_lights.size()];
    for (int i = 0; i < _optix_directionnal_lights.size(); ++i)
    {
      dLs[i].emission  = ((DirectionnalLight *)_optix_directionnal_lights[i])->emission;
      dLs[i].direction = ((DirectionnalLight *)_optix_directionnal_lights[i])->direction;
    }
    memcpy(_optix_directionnal_light_buffer->map(), dLs, _optix_directionnal_lights.size() * sizeof(DirectionnalLight));
    _optix_directionnal_light_buffer->unmap();
    delete dLs;
  }
//  if (_optix_directionnal_lights.size() >= 1)
//  {
//    for (size_t i = 0; i < _optix_directionnal_lights.size(); ++i)
//    {
//      auto emit = dynamic_cast<mrf::materials::Emittance *>(_optix_directionnal_light_materials[i]);
//      if (emit)
//      {
//        float radiance = emit->getRadiance();
//#ifdef MRF_RENDERING_MODE_SPECTRAL
//        auto light_spectrum = emit->getColor();
//
//        int spectrum_interval = (wavelengths.size() > 1) ? wavelengths[1] - wavelengths[0] : 0;
//        // Construct a spectral distribution at requested Wavelength
//        for (uint wave_id = 0; wave_id < wavelengths.size(); wave_id++)
//        {
//          uint const wave = wavelengths[wave_id];
//          buffer_float_data[wave_id + i * wavelengths.size()]
//              = light_spectrum.findLinearyInterpolatedValue(static_cast<float>(wave)) * radiance;
//          //= light_spectrum.findIntegratedValue(static_cast<float>(wave), spectrum_interval) * radiance;
//        }
//
//        auto light_color = light_spectrum.findLinearyInterpolatedValue(float(wavelengths[0])) * radiance;
//        //auto light_color = light_spectrum.findIntegratedValue(float(wavelengths[0]), spectrum_interval) * radiance;
//
//        _optix_directionnal_lights[i].emission = light_color;
//#else
//        auto light_color                 = emit->getColor() * radiance;
//
//        _optix_directionnal_lights[i].emission = mrfToOptix(light_color);
//#endif
//      }
//    }   //end of for-loop  for directional light sources
//
//
//    /*Update optix GPU Buffer from CPU Buffer*/
//    // REMARK ONLy useful for RGB or one wavelength per pass
//    memcpy(
//        _optix_directionnal_light_buffer->map(),
//        &_optix_directionnal_lights[0],
//        _optix_directionnal_lights.size() * sizeof(DirectionnalLight));
//    _optix_directionnal_light_buffer->unmap();
//  }

//Unmap the directional light buffer no matter what
#ifdef MRF_RENDERING_MODE_SPECTRAL
  buffer->unmap();
#endif

  /*Update optix GPU Buffer from CPU Buffer*/
  //  if (_optix_quad_lights.size() > 0)
  // {
  //  memcpy(_optix_quad_light_buffer->map(), &_optix_quad_lights[0], _optix_quad_lights.size() * sizeof(QuadLight));
  // _optix_quad_light_buffer->unmap();
  //}

  // if (_optix_sphere_lights.size() > 0)
  // {
  //   memcpy(_optix_sphere_light_buffer->map(), &_optix_sphere_lights[0], _optix_sphere_lights.size() * sizeof(SphereLight));
  //   _optix_sphere_light_buffer->unmap();
  // }

  // if (_optix_directionnal_lights.size() > 0)
  // {
  //   memcpy(_optix_directionnal_light_buffer->map(), &_optix_directionnal_lights[0], _optix_directionnal_lights.size() * sizeof(DirectionnalLight));
  //   _optix_directionnal_light_buffer->unmap();
  // }



  // TO DEBUG AND TRACE
  // std::cout << __FILE__ << " " << __LINE__ << std::endl;
}

//std::vector<QuadLight> const &OptixLightHandler::optixQuadLights() const
//{
//  return _optix_quad_lights;
//}
//std::vector<SphereLight> const &OptixLightHandler::optixSphereLights() const
//{
//  return _optix_sphere_lights;
//}

//void OptixLightHandler::createQuadLight(mrf::lighting::Light *mrf_light)
//{
//  auto pos = mrf_light->center();
//
//  mrf::materials::UMat *umat = mrf_light->umat_pointer();
//
//  auto emittance = dynamic_cast<mrf::materials::DiffuseEmittance *>(umat);
//  if (emittance)
//  {
//    mrf::geom::RQuad *rquad = dynamic_cast<mrf::geom::RQuad *>(mrf_light->geometry());
//    if (!rquad)
//    {
//      mrf::gui::fb::Loger::getInstance()->fatal("Unsupported geometry in create surfacic light Optix Backend");
//      return;
//    }
//
//    //auto reff = rquad->localRef();
//    //auto mrf_matrix = reff.getMatrixTo();
//
//    // Light buffer
//    QuadLight light;
//    //light.corner = optix::make_float3(343.0f, 548.6f, 227.0f);
//    //light.v1 = optix::make_float3(-130.0f, 0.0f, 0.0f);
//    //light.v2 = optix::make_float3(0.0f, 0.0f, 105.0f);
//
//    auto vertices = rquad->getVertices();
//
//    light.corner = mrfToOptix(vertices[0]);
//    light.v1     = mrfToOptix(vertices[3] - vertices[0]);
//    light.v2     = mrfToOptix(vertices[1] - vertices[0]);
//    light.normal = optix::normalize(optix::cross(light.v1, light.v2));
//
//
//    //std::cout << "light.corner " << light.corner.x << " " << light.corner.y << " " << light.corner.z << std::endl;
//    //std::cout << "light.v1 " << light.v1.x << " " << light.v1.y << " " << light.v1.z << std::endl;
//    //std::cout << "light.v2 " << light.v2.x << " " << light.v2.y << " " << light.v2.z << std::endl;
//    //std::cout << "light.normal " << light.normal.x << " " << light.normal.y << " " << light.normal.z << std::endl;
//
//#ifdef MRF_RENDERING_MODE_SPECTRAL
//    // #ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
//    //     std::cout << light.emission.size() << std::endl;
//    //     assert(0);
//    // #else
//    //will be updtated in update lights
//    //    light.emission = 1.0;
//    // #endif
//
//#else
//    light.emission = mrfToOptix(emittance->getColor() * emittance->getRadiance());
//#endif
//
//    _optix_quad_lights.push_back(light);
//    _optix_quad_light_materials.push_back(umat);
//  }
//  else
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal("Unsupported emittance type in create surfacic light Optix Backend");
//  }
//}

//void OptixLightHandler::createSphereLight(mrf::lighting::Light *mrf_light)
//{
//  mrf::materials::UMat *umat = mrf_light->umat_pointer();
//
//  auto emittance = dynamic_cast<mrf::materials::DiffuseEmittance *>(umat);
//  if (emittance)
//  {
//    mrf::geom::Sphere *sphere = dynamic_cast<mrf::geom::Sphere *>(mrf_light->geometry());
//    if (!sphere)
//    {
//      mrf::gui::fb::Loger::getInstance()->fatal("Unsupported geometry in create sphere light Optix Backend");
//      return;
//    }
//
//    SphereLight light;
//    light.position = mrfToOptix(sphere->position());
//    light.radius   = sphere->radius();
//
//#ifdef MRF_RENDERING_MODE_SPECTRAL
//    //will be updtated in update lights
//    light.emission = 1.0;
//#else
//    light.emission = mrfToOptix(emittance->getColor() * emittance->getRadiance());
//#endif
//
//    _optix_sphere_lights.push_back(light);
//    _optix_sphere_light_materials.push_back(umat);
//  }
//  else
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal("Unsupported emittance type in create sphere light Optix Backend");
//  }
//}

//void OptixLightHandler::createDirectionnalLight(mrf::lighting::Light *mrf_light)
//{
//  mrf::materials::UMat *umat = mrf_light->umat_pointer();
//
//  auto emittance = dynamic_cast<mrf::materials::DiracEmittance *>(umat);
//  if (emittance)
//  {
//    DirectionnalLight dl;
//    dl.direction = mrfToOptix(emittance->getDirection());
//
//#ifdef MRF_RENDERING_MODE_SPECTRAL
//    dl.emission = 1.0;
//#else
//    dl.emission    = mrfToOptix(emittance->getColor() * emittance->getRadiance());
//#endif
//    _optix_directionnal_lights.push_back(dl);
//    _optix_directionnal_light_materials.push_back(umat);
//  }
//  else
//  {
//    mrf::gui::fb::Loger::getInstance()->fatal("Wrong emittance type in a directionnal light for optix backend, can't add the light !");
//  }
//}

}   // namespace optix_backend
}   // namespace mrf
