/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/
#include <mrf_optix/optix_light_plugin.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/sphere.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_util.hpp>
#include <mrf_optix/optix_renderer.hpp>

#include <iostream>

using mrf::optix_backend::mrfToOptix;

namespace mrf
{
namespace optix_backend
{
OptixBaseLight::OptixBaseLight(mrf::lighting::Light *light)
  : _light(light)
  , _has_geometry(false)
  , _spd_buffer_allocated(false)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  //TODO some things may be missing here ?
#else
  mrf::materials::Emittance *emittance = light->emit_pointer();
  _emission                            = mrfToOptix(emittance->getColor() * emittance->getRadiance());
#endif
}

}   // namespace optix_backend
}   // namespace mrf
