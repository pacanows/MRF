#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_plugins/materials/optix_mat_plugins.hpp>
#include <mrf_plugins/lights/optix_light_plugins.hpp>

namespace mrf
{
namespace optix_backend
{
template<typename O>
static void register_plugin(std::string id, std::map<std::string, OptixMaterialPlugin *> &parse_fct_map)
{
  parse_fct_map[id] = O::create();
}

//Static function: vector to list all registered material ID
static void register_all_mat(std::map<std::string, OptixMaterialPlugin *> &fcts)
{
  register_plugin<mrf::optix_backend::LambertPlugin>(mrf::materials::BRDFTypes::LAMBERT, fcts);
  register_plugin<mrf::optix_backend::CheckerboardPlugin>(mrf::materials::BRDFTypes::CHECKERBOARD, fcts);
  register_plugin<mrf::optix_backend::PhongPlugin>(mrf::materials::BRDFTypes::PHONG_NORMALIZED, fcts);
  register_plugin<mrf::optix_backend::PhongPlugin>(mrf::materials::BRDFTypes::PHONG_NORMALIZED_TEXTURED, fcts);
  register_plugin<mrf::optix_backend::PrincipledPlugin>(mrf::materials::BRDFTypes::PRINCIPLED, fcts);
  register_plugin<mrf::optix_backend::MicrofacetConductorPlugin>(mrf::materials::BRDFTypes::GGX, fcts);
  register_plugin<mrf::optix_backend::MicrofacetConductorPlugin>(mrf::materials::BRDFTypes::PERFECT_MIRROR, fcts);
  register_plugin<mrf::optix_backend::MicrofacetConductorPlugin>(mrf::materials::BRDFTypes::FRESNEL_MIRROR, fcts);
  register_plugin<mrf::optix_backend::MicrofacetDielectricPlugin>(mrf::materials::BRDFTypes::FRESNEL_GLASS, fcts);
  register_plugin<mrf::optix_backend::MicrofacetDielectricPlugin>(mrf::materials::BRDFTypes::WALTER_BSDF, fcts);
  register_plugin<mrf::optix_backend::UTIAPlugin>(mrf::materials::BRDFTypes::UTIA, fcts);
  register_plugin<mrf::optix_backend::UTIAPlugin>(mrf::materials::BRDFTypes::UTIA_INDEXED, fcts);
  register_plugin<mrf::optix_backend::MeasuredPlugin>(mrf::materials::BRDFTypes::MEASURED_ISOTROPIC, fcts);
  register_plugin<mrf::optix_backend::DebugPlugin>(mrf::materials::BRDFTypes::FLAT, fcts);
  register_plugin<mrf::optix_backend::DebugPlugin>(mrf::materials::BRDFTypes::NORMAL, fcts);
  register_plugin<mrf::optix_backend::DebugPlugin>(mrf::materials::BRDFTypes::TANGENT, fcts);
  register_plugin<mrf::optix_backend::DebugPlugin>(mrf::materials::BRDFTypes::UV, fcts);
  register_plugin<mrf::optix_backend::ShadowCatcherPlugin>(mrf::materials::BRDFTypes::SHADOW_CATCHER, fcts);
  register_plugin<mrf::optix_backend::DiffuseEmittancePlugin>(mrf::materials::EmittanceTypes::DIFFUSE, fcts);
  register_plugin<mrf::optix_backend::DiracEmittancePlugin>(mrf::materials::EmittanceTypes::DIRAC, fcts);

  //TODO: uncomment and adjust template according to your material.
  // register_plugin<mrf::optix_backend::TemplateMatOptixPlugin>(mrf::materials::BRDFTypes::TEMPLATE_MAT_XML_NAME, fcts);
}

//Static function: vector to list all registered material ID
static void register_all_mat(OptixMaterialPlugins &all_fcts)
{
  register_all_mat(all_fcts.fcts);
}


static void register_all_lights(std::map<int, OptixLightPlugin *> &fcts)
{
  register_plugin<mrf::optix_backend::DirectionnalLightPlugin>(mrf::lighting::Light::DIR_LIGHT, fcts);
  register_plugin<mrf::optix_backend::QuadLightPlugin>(mrf::lighting::Light::QUAD_LIGHT, fcts);
  register_plugin<mrf::optix_backend::SphereLightPlugin>(mrf::lighting::Light::SPHERE_LIGHT, fcts);
}

//Static function: vector to list all registered material ID
static void register_all_lights(OptixLightPlugins &all_fcts)
{
  register_all_lights(all_fcts.fcts);
}

}   // namespace optix_backend
}   // namespace mrf
