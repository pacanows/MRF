/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2018
 *
 *  This class represents a Shadow catcher, see MRF online documentation
 * for more explanation
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//MRF
#include <mrf_core/math/math.hpp>
#include <mrf_core/materials/brdf.hpp>
#include <mrf_core/materials/bsdf.hpp>

#include <mrf_core/geometry/local_frame.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *SHADOW_CATCHER = "ShadowCatcher";
};   // namespace BRDFTypes

class MRF_PLUGIN_EXPORT ShadowCatcher: public BRDF
{
private:
public:
  ShadowCatcher(std::string const &name);
  virtual ~ShadowCatcher();



  virtual float
  brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;

  virtual float diffuseAlbedo() const;
  virtual float diffuseAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  diffuseComponent() const;

  virtual float specularAlbedo() const;
  virtual float specularAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  specularComponent() const;

  virtual float transmissionAlbedo() const;
  virtual float transmissionAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  transmissionComponent() const;

  virtual float nonDiffuseAlbedo() const;
  virtual bool  nonDiffuseComponent() const;
  virtual float totalAlbedo() const;


  //-------------------------------------------------------------------------
  virtual float
  diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;
};



}   // namespace materials
}   // namespace mrf