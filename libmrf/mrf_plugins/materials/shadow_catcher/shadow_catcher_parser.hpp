#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <mrf_core/materials/materials.hpp>

#include <mrf_plugins/materials/shadow_catcher/shadow_catcher.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
class ShadowCatcherParser: public BaseMaterialParser<mrf::materials::ShadowCatcher>
{
public:
  ShadowCatcherParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new ShadowCatcherParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::SHADOW_CATCHER; }

  virtual bool parse(tinyxml2::XMLElement *, mrf::materials::UMat *a_mat, ParserHelper &)
  {
    mrf::materials::ShadowCatcher *shadowcatcher = dynamic_cast<mrf::materials::ShadowCatcher *>(a_mat);

    return shadowcatcher != nullptr;
  }

protected:
private:
};
}   // namespace io
}   // namespace mrf
