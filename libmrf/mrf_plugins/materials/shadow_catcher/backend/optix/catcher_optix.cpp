/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/shadow_catcher/backend/optix/catcher_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *ShadowCatcherPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *ShadowCatcherPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  //if (!material) return;
  optix::Material optix_mat       = context->createMaterial();
  auto            compile_options = cuda_compile_options;

  auto shadow_catcher_mat = dynamic_cast<mrf::materials::ShadowCatcher *>(material);
  if (shadow_catcher_mat)
  {
    compile_options.push_back("-DSHADOW_CATCHER");
    //compileBRDF(optix_mat, compile_options);
    //return optix_mat;
    return new OptixShadowCatcher(shadow_catcher_mat, optix_mat, compile_options, ptx_cfg);
  }

  return nullptr;
}


}   // namespace optix_backend
}   // namespace mrf
