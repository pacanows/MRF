To register a plugin (replace the template name by yours):

  1 - Add :
~~~
#include <mrf_plugins/materials/material_template/material_template.hpp>
~~~

At the bottom of the header "mrf_plugins/materials/all_material_plugins.hpp".


  2 - Add :
~~~
#include <mrf_plugins/materials/material_template/backend/optix/template_optix.hpp>
~~~

At the bottom of the header "mrf_plugins/materials/optix_all_plugins.hpp".


  3 - Add :
~~~  
register_plugin<mrf::materials::UMat, mrf::io::TemplateMatParser>(ids, fcts);
~~~

At the end of the function 
~~~
static void
register_all_mat(std::vector<std::string> &ids, std::map<std::string, BasePluginParser<mrf::materials::UMat> *> &fcts)
~~~

In the header "mrf_plugins/register_mrf_plugins.hpp".


  4 - Add :
~~~
register_plugin<mrf::optix_backend::TemplateMatOptixPlugin>(mrf::materials::BRDFTypes::TEMPLATE_MAT_XML_NAME, fcts);
~~~
/!\ Replace mrf::materials::BRDFTypes if necessary

At the end of the function 
~~~
static void
register_all_mat(std::map<std::string, OptixMaterialPlugin *> &fcts)
~~~

In the header "mrf_plugins/register_optix_plugins.hpp".

