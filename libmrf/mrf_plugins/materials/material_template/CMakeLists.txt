#Replace template by your material name

set(SOURCES 
  ${SOURCES}
    ${CMAKE_CURRENT_LIST_DIR}/material_template.cpp
  PARENT_SCOPE)

set(HEADERS 
  ${HEADERS}
    ${CMAKE_CURRENT_LIST_DIR}/material_template.hpp
    ${CMAKE_CURRENT_LIST_DIR}/template_parser.hpp
  PARENT_SCOPE
  )

add_subdirectory(backend)

set(BCK_SOURCES ${BCK_SOURCES} PARENT_SCOPE)
set(BCK_HEADERS ${BCK_HEADERS} PARENT_SCOPE)
set(BCK_CU_SOURCES ${BCK_CU_SOURCES} PARENT_SCOPE)