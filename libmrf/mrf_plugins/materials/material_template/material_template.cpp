/*
 * Template author: David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 * author : Your Name @ your.institute
 * Copyright XXXX 20XX
 *
 **/

#include <mrf_plugins/materials/material_template/material_template.hpp>

#include <mrf_core/sampling/random_generator.hpp>

namespace mrf
{
namespace materials
{
TemplateMat::TemplateMat(std::string const &name): BRDF(name, BRDFTypes::TEMPLATE_MAT_XML_NAME) {}

TemplateMat::~TemplateMat() {}


//-------------------------------------------------------------------
float TemplateMat::brdfValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.f;
}


RADIANCE_TYPE
TemplateMat::coloredBRDFValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return RADIANCE_TYPE(0.f);
}


//-------------------------------------------------------------------
float TemplateMat::diffPDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const & /*out_dir*/) const

{
  return 0.f;
}

float TemplateMat::specPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

float TemplateMat::transPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

float TemplateMat::nonDiffusePDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}



//-------------------------------------------------------------------
ScatterEvent::SCATTER_EVT TemplateMat::generateScatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    RADIANCE_TYPE &   incident_radiance,
    mrf::math::Vec3f &outgoing_dir) const
{
  return ScatterEvent::ABSORPTION;
}

ScatterEvent::SCATTER_EVT TemplateMat::scatteringDirection(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f &outgoing_dir,
    float &           proba_outgoing_dir,
    float             e1,
    float             e2) const
{
  return ScatterEvent::ABSORPTION;
}

ScatterEvent::SCATTER_EVT TemplateMat::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    float             e1,
    float             e2,
    mrf::math::Vec3f &outgoing_dir,
    RADIANCE_TYPE &   brdf_corrected_by_pdf) const
{
  return ScatterEvent::ABSORPTION;
}

}   // namespace materials
}   // namespace mrf
