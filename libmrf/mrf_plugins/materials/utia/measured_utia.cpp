/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include <mrf_plugins/materials/utia/measured_utia.hpp>



#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/sampling/random_generator.hpp>

namespace mrf
{
namespace materials
{
using namespace mrf::sampling;
using namespace math;


UTIA::UTIA(std::string const &name): MeasuredMaterial(name, BRDFTypes::UTIA)
//, _parameterization(STANDARD)
//, _interpolate(false)
//, _thetaVdiv(1)
//, _phiVdiv(1)
//, _thetaIdiv(1)
//, _phiIdiv(1)
{}

UTIA::~UTIA() {}

mrf::image::IMAGE_LOAD_SAVE_FLAGS UTIA::loadUTIAData(std::string const &filename)
{
  _file = filename;
  try
  {
#ifdef MRF_RENDERING_MODE_SPECTRAL
    //_data = mrf::image::loadSpectralImage(filename, true);
#else
    _utia_data      = mrf::image::loadColorImage(filename, true);
#endif
  }
  catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS &error)
  {
    //TODO use loger
    printf("\n***IMAGE NOT LOADED***\n");
    return error;
  }
  return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}



IndexedUTIA::IndexedUTIA(std::string const &name): BRDF(name, BRDFTypes::UTIA_INDEXED)
//, _parameterization(STANDARD)
//, _interpolate(false)
//, _thetaVdiv(1)
//, _phiVdiv(1)
//, _thetaIdiv(1)
//, _phiIdiv(1)
{}

IndexedUTIA::~IndexedUTIA()
{
  //for (auto it = _utia_materials.begin(); it != _utia_materials.end(); ++it)
  //{
  //  auto mat = it->second;
  //  mat->destroy();
  //}
}

mrf::image::IMAGE_LOAD_SAVE_FLAGS IndexedUTIA::loadUTIAData(std::string const &filename, const int index)
{
  if (_utia_materials.count(index) == 0)
  {
    _utia_materials[index] = new UTIA("");
  }
  return _utia_materials.at(index)->loadUTIAData(filename);
}

mrf::image::IMAGE_LOAD_SAVE_FLAGS IndexedUTIA::loadUTIAMap(std::string const &filename)
{
  //_file[index] = filename;
  try
  {
#ifdef MRF_RENDERING_MODE_SPECTRAL
    //_data = mrf::image::loadSpectralImage(filename, true);
#else
    _utia_index_map = mrf::image::loadColorImage(filename, true);
#endif
  }
  catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS &error)
  {
    //TODO use loger
    printf("\n***IMAGE NOT LOADED***\n");
    return error;
  }
  return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}

void IndexedUTIA::normalizeMapIndices(std::string exportPath)
{
  std::map<unsigned int, int> matToIndex;
  for (int i = 0; i < _utia_index_list.size(); ++i)
  {
    unsigned int mat = getMatIndex(i);
    matToIndex[mat]  = i;
  }

  for (int i = 0; i < _utia_index_map.get()->width() * _utia_index_map.get()->height(); ++i)
  {
    mrf::color::Color pixel = _utia_index_map.get()->getPixel(i);

    int mat;
    if (matToIndex.count((unsigned int)pixel.x() * 255) > 0)
    {
      mat = matToIndex.at((unsigned int)pixel.x() * 255);
    }
    else
    {
      //material is not among the provided UTIA files, disable it.
      mat = -1;
    }

    pixel.r() = mat / 255.f;

    _utia_index_map.get()->setPixel(i, pixel);
  }
}

}   // namespace materials
}   // namespace mrf
