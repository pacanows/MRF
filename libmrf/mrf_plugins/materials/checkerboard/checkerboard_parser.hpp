#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <mrf_core/materials/materials.hpp>
#include <mrf_plugins/materials/checkerboard/checkerboard.hpp>

#include <mrf_core/io/base_plugin_parser.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
class CheckerboardParser: public BaseMaterialParser<mrf::materials::CheckerBoard>
{
public:
  CheckerboardParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new CheckerboardParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::CHECKERBOARD; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::CheckerBoard *checkerboard = dynamic_cast<mrf::materials::CheckerBoard *>(a_mat);

    //PARSE COLOR AND REFLECTIVITY
    std::vector<mrf::materials::COLOR> colors;
    if (parser.retrieveColors(a_mat_element, colors))
    {
      if (colors.size() > 0) checkerboard->setColor1(colors[0]);

      if (colors.size() > 1) checkerboard->setColor2(colors[1]);
    }
    else
    {
      a_mat = nullptr;
      return false;
    }

    std::string repeat_name;
    if (a_mat_element->Attribute(SceneLexer::CHECKERBOARD_FREQUENCY_AT))
    {
      repeat_name = SceneLexer::CHECKERBOARD_FREQUENCY_AT;
    }
    else if (a_mat_element->Attribute(SceneLexer::CHECKERBOARD_REPEAT_AT))
    {
      repeat_name = SceneLexer::CHECKERBOARD_REPEAT_AT;
    }
    else if (a_mat_element->Attribute(SceneLexer::CHECKERBOARD_REPETITION_AT))
    {
      repeat_name = SceneLexer::CHECKERBOARD_REPETITION_AT;
    }
    else
    {
      a_mat = nullptr;
      return false;
    }


    uint repeat  = 1;
    int  valueOK = a_mat_element->QueryUnsignedAttribute(repeat_name.c_str(), &repeat);
    if (valueOK != tinyxml2::XML_SUCCESS)
    {
      a_mat = nullptr;
      return false;
    }
    checkerboard->setRepetition(repeat);
    return true;
  }

protected:
private:
};


}   // namespace io
}   // namespace mrf
