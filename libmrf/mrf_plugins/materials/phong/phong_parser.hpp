#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <mrf_core/materials/materials.hpp>

#include <mrf_plugins/materials/phong/l_phong.hpp>
#include <mrf_plugins/materials/phong/l_phong_textured.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>

//TODO TEXTURE

namespace mrf
{
namespace io
{
class PhongParser: public BaseMaterialParser<mrf::materials::LPhysicalPhong>
{
public:
  PhongParser() {}

  static BasePluginParser<mrf::materials::UMat> *create() { return new PhongParser; }

  static std::string getID() { return mrf::materials::BRDFTypes::PHONG_NORMALIZED; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::LPhysicalPhong *phong_mat = dynamic_cast<mrf::materials::LPhysicalPhong *>(a_mat);

    if (!phong_mat) return false;

    if (a_mat_element->FirstChildElement(SceneLexer::DIFFUSE_CMP_MK))
    {
      tinyxml2::XMLElement *diffuse_elem = a_mat_element->FirstChildElement(SceneLexer::DIFFUSE_CMP_MK);

      mrf::materials::COLOR diffuse_color = parser.retrieveColor(diffuse_elem);

      float diffuse_reflectivity;
      if (!parser.retrieveReflectivity(diffuse_elem, diffuse_reflectivity))
      {
        a_mat = nullptr;
        return false;
      }

      phong_mat->setDiffuseReflectivity(diffuse_reflectivity);
      phong_mat->setDiffuseColor(diffuse_color);
    }

    if (a_mat_element->FirstChildElement(SceneLexer::SPECULAR_CMP_MK))
    {
      tinyxml2::XMLElement *specular_elem = a_mat_element->FirstChildElement(SceneLexer::SPECULAR_CMP_MK);

      int r_exponent = -1;
      int valueOK    = specular_elem->QueryIntAttribute(SceneLexer::EXPONENT_AT, &r_exponent);

      if (valueOK == tinyxml2::XML_SUCCESS)
      {
        phong_mat->setExponent(math::MathI::absVal(r_exponent));
      }

      mrf::materials::COLOR specular_color = parser.retrieveColor(specular_elem);

      float specular_reflectivity;
      if (!parser.retrieveReflectivity(specular_elem, specular_reflectivity))
      {
        a_mat = nullptr;
        return false;
      }

      phong_mat->setSpecularReflectivity(specular_reflectivity);
      phong_mat->setSpecularColor(specular_color);
    }
    return true;
  }

protected:
private:
};
}   // namespace io
}   // namespace mrf
