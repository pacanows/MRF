/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/brdf.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *PHONG_NORMALIZED = "PhongNormalized";
};   // namespace BRDFTypes

//-------------------------------------------------------------------//
//---------------        Physical Phong Model by Lafortune CLASS   --//
//-------------------------------------------------------------------//
class MRF_PLUGIN_EXPORT LPhysicalPhong: public BRDF
{
protected:
  float        _dalbedo;
  COLOR        _dcolor;
  float        _salbedo;
  COLOR        _scolor;
  unsigned int _exp;


public:
  LPhysicalPhong(
      std::string const &name,
      COLOR const &      dcolor  = COLOR(0.0f),
      float const        dalbedo = 0.1f,
      COLOR const &      scolor  = COLOR(0.5f),
      float const        salbedo = 0.7f,
      unsigned int const exp     = 10);
  virtual ~LPhysicalPhong();

  //-------------------------------------------------------------------------
  // Some Setters
  //-------------------------------------------------------------------------
  inline void setDiffuseReflectivity(float a_reflectivity);
  inline void setDiffuseColor(COLOR const &a_color);
  //-------------------------------------------------------------------------
  inline void setSpecularReflectivity(float a_reflectivity);
  inline void setSpecularColor(COLOR const &a_color);

  inline void setExponent(unsigned int exponent);

  inline COLOR const &getDiffuseColor() const;
  inline COLOR const &getSpecularColor() const;
  inline unsigned int getExponent() const;


  //-------------------------------------------------------------------------
  // UMat Interface Methods Implementation
  //-------------------------------------------------------------------------
  virtual float
  brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;


  virtual float diffuseAlbedo() const;
  virtual float diffuseAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  diffuseComponent() const;

  virtual float specularAlbedo() const;
  virtual float specularAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  specularComponent() const;

  virtual float transmissionAlbedo() const;
  virtual float transmissionAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  transmissionComponent() const;

  virtual float nonDiffuseAlbedo() const;
  virtual bool  nonDiffuseComponent() const;


  virtual float totalAlbedo() const;

  //-------------------------------------------------------------------------
  virtual float
  diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;
};


//-------------------------------------------------------------------------
// Some Accessors
//-------------------------------------------------------------------------

inline void LPhysicalPhong::setDiffuseReflectivity(float a_reflectivity)
{
  _dalbedo = a_reflectivity;
}

inline void LPhysicalPhong::setDiffuseColor(COLOR const &a_color)
{
  _dcolor = a_color;
}

inline void LPhysicalPhong::setSpecularReflectivity(float a_reflectivity)
{
  _salbedo = a_reflectivity;
}

inline void LPhysicalPhong::setSpecularColor(COLOR const &a_color)
{
  _scolor = a_color;
}

inline void LPhysicalPhong::setExponent(unsigned int exponent)
{
  _exp = exponent;
}

inline COLOR const &LPhysicalPhong::getDiffuseColor() const
{
  return _dcolor;
}

inline COLOR const &LPhysicalPhong::getSpecularColor() const
{
  return _scolor;
}

inline unsigned int LPhysicalPhong::getExponent() const
{
  return _exp;
}


}   // namespace materials
}   // namespace mrf
