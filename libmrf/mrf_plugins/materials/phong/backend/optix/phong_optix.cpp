/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/phong/backend/optix/phong_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *PhongPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *PhongPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  optix::Material optix_mat       = context->createMaterial();
  auto            compile_options = cuda_compile_options;

  auto phong_textured = dynamic_cast<mrf::materials::LPhysicalPhongTextured *>(material);
  if (phong_textured)
  {
    optix_mat["_exponent"]->setFloat(float(phong_textured->getExponent()));
    optix_mat["_kd"]->setFloat(phong_textured->diffuseAlbedo());
    optix_mat["_ks"]->setFloat(phong_textured->specularAlbedo());
    optix_mat["_texture_repeat"]->setFloat(float(phong_textured->textureRepeat()));

    compile_options.push_back("-DTEXTURED");

    OptixPhong *ret_mat = new OptixPhong(phong_textured, optix_mat, compile_options, ptx_cfg);

    std::vector<std::string> variable_names = {"_diffuse_color", "_specular_color"};

    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(phong_textured->getDiffuseColor());
    variable_values.push_back(phong_textured->getSpecularColor());

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->updateTextures(context, wavelengths);
#else
    ret_mat->updateTextures(context);
#endif

    if (ret_mat->hasTexture("diffuse"))
    {
      optix_mat["_d_tex_id"]->setInt(ret_mat->getTexture("diffuse")->getId());
    }
    else
    {
      optix_mat["_d_tex_id"]->setInt(-1);
    }

    if (ret_mat->hasTexture("specular"))
    {
      optix_mat["_s_tex_id"]->setInt(ret_mat->getTexture("specular")->getId());
    }
    else
    {
      optix_mat["_s_tex_id"]->setInt(-1);
    }

    if (ret_mat->hasTexture("normal"))
    {
      optix_mat["_normal_map_id"]->setInt(ret_mat->getTexture("normal")->getId());
    }
    else
    {
      optix_mat["_normal_map_id"]->setInt(-1);
    }

    if (ret_mat->hasTexture("exponent"))
    {
      optix_mat["_exponent_map_id"]->setInt(ret_mat->getTexture("exponent")->getId());
    }
    else
    {
      optix_mat["_exponent_map_id"]->setInt(-1);
    }


    return ret_mat;
  }

  auto phong = dynamic_cast<mrf::materials::LPhysicalPhong *>(material);
  if (phong)
  {
    optix_mat["_exponent"]->setFloat(float(phong->getExponent()));
    optix_mat["_kd"]->setFloat(phong->diffuseAlbedo());
    optix_mat["_ks"]->setFloat(phong->specularAlbedo());

    optix_mat["_d_tex_id"]->setInt(-1);
    optix_mat["_s_tex_id"]->setInt(-1);
    optix_mat["_exponent_map_id"]->setInt(-1);
    optix_mat["_normal_map_id"]->setInt(-1);

    compile_options.push_back("-DPHONG_NOTEX");

    OptixPhong *ret_mat = new OptixPhong(phong, optix_mat, compile_options, ptx_cfg);

    std::vector<std::string> variable_names = {"_diffuse_color", "_specular_color"};

    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(phong->getDiffuseColor());
    variable_values.push_back(phong->getSpecularColor());

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif

    return ret_mat;
  }

  optix_mat->destroy();
  return nullptr;
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixPhong::updateTextures(optix::Context &context, std::vector<uint> wavelengths)
#else
void OptixPhong::updateTextures(optix::Context &context)
#endif
{
  RTformat tex_format;
#ifdef MRF_RENDERING_MODE_SPECTRAL
  tex_format = RT_FORMAT_FLOAT;
#else
  tex_format = RT_FORMAT_FLOAT4;
#endif

  //_loger.trace("UPDATE PHONG TEXTURES");
  //mrf::util::PrecisionTimer timer;
  //timer.start();
  optix::TextureSampler tex_sampler;

  auto phong_textured = dynamic_cast<mrf::materials::LPhysicalPhongTextured *>(_mrf_material);
  if (!phong_textured) return;   //Current phong is not textured

  if (phong_textured->hasDiffuseTexture())
  {
    if (phong_textured->getDiffuseTexture().width() > 0 && phong_textured->getDiffuseTexture().height() > 0)
    {
      //Check if diffuse tex and its sampler need to be created
      if (_textures.find("diffuse") == _textures.end())
      {
#ifdef MRF_RENDERING_MODE_SPECTRAL
        tex_sampler = mrf::optix_backend::createRepeatedOptixTexture(
            context,
            tex_format,
            phong_textured->getDiffuseTexture().width(),
            phong_textured->getDiffuseTexture().height(),
            wavelengths.size());
#else
        tex_sampler = mrf::optix_backend::createRepeatedOptixTexture(
            context,
            tex_format,
            phong_textured->getDiffuseTexture().width(),
            phong_textured->getDiffuseTexture().height());
#endif
        _textures["diffuse"] = tex_sampler;
      }
      else   //Otherwise, map the existing one to the tex sampler
      {
        tex_sampler = _textures["diffuse"];
      }
#ifdef MRF_RENDERING_MODE_SPECTRAL
      mrf::optix_backend::updateTextureFromSpectralImage(phong_textured->getDiffuseTexture(), tex_sampler, wavelengths);
#else
      mrf::optix_backend::updateTextureFromImage(phong_textured->getDiffuseTexture(), tex_sampler);
#endif
    }
  }

  if (phong_textured->hasSpecularTexture())
  {
    if (phong_textured->getSpecularTexture().width() > 0 && phong_textured->getSpecularTexture().height() > 0)
    {
      //Check if specular tex and its sampler need to be created
      if (_textures.find("specular") == _textures.end())
      {
#ifdef MRF_RENDERING_MODE_SPECTRAL
        tex_sampler = mrf::optix_backend::createRepeatedOptixTexture(
            context,
            tex_format,
            phong_textured->getSpecularTexture().width(),
            phong_textured->getSpecularTexture().height(),
            wavelengths.size());
#else
        tex_sampler = mrf::optix_backend::createRepeatedOptixTexture(
            context,
            tex_format,
            phong_textured->getSpecularTexture().width(),
            phong_textured->getSpecularTexture().height());
#endif
        _textures["specular"] = tex_sampler;
      }
      else   //Otherwise, map the existing one to the tex sampler
      {
        tex_sampler = _textures["specular"];
      }
#ifdef MRF_RENDERING_MODE_SPECTRAL
      mrf::optix_backend::updateTextureFromSpectralImage(
          phong_textured->getSpecularTexture(),
          tex_sampler,
          wavelengths);
#else
      mrf::optix_backend::updateTextureFromImage(phong_textured->getSpecularTexture(), tex_sampler);
#endif
    }
  }

  if (phong_textured->hasNormalTexture())
  {
    if (phong_textured->getNormalMap().width() > 0 && phong_textured->getNormalMap().height() > 0)
    {
      if (_textures.find("normal") == _textures.end())
      {
        tex_sampler = mrf::optix_backend::createRepeatedOptixTexture(
            context,
            RT_FORMAT_FLOAT4,
            phong_textured->getNormalMap().width(),
            phong_textured->getNormalMap().height());
        _textures["normal"] = tex_sampler;

        mrf::optix_backend::updateTextureFromImage(phong_textured->getNormalMap(), tex_sampler);
      }
      else
      {
        tex_sampler = _textures["normal"];
      }
    }
  }

  if (phong_textured->hasExponentTexture())
  {
    if (phong_textured->getExponentMap().width() > 0 && phong_textured->getExponentMap().height() > 0)
    {
      if (_textures.find("exponent") == _textures.end())
      {
        tex_sampler = mrf::optix_backend::createRepeatedOptixTexture(
            context,
            RT_FORMAT_FLOAT,
            phong_textured->getExponentMap().width(),
            phong_textured->getExponentMap().height());
        _textures["exponent"] = tex_sampler;

        mrf::optix_backend::updateTextureFromImage(phong_textured->getExponentMap(), tex_sampler);
      }
      else
      {
        tex_sampler = _textures["exponent"];
      }
    }
  }

  //float elapsed = timer.elapsed();
  //_loger.trace("Updated Phong textures in: " + std::to_string(elapsed) + "s");
}



}   // namespace optix_backend
}   // namespace mrf
