#include <optixu/optixu_matrix_namespace.h>

#include "include/distribution/cosine.h"

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> _diffuse_color;
rtBuffer<float, 1> _specular_color;
#  else
rtDeclareVariable(float, _diffuse_color, , );
rtDeclareVariable(float, _specular_color, , );
#  endif
#else
rtDeclareVariable(float3, _diffuse_color, , );
rtDeclareVariable(float3, _specular_color, , );
#endif

rtDeclareVariable(int, _d_tex_id, , "Diffuse texture id");
rtDeclareVariable(int, _s_tex_id, , "Specular texture id");
rtDeclareVariable(int, _normal_map_id, , "Normal texture id");
rtDeclareVariable(int, _exponent_map_id, , "Exponent texture id");
rtDeclareVariable(float, _texture_repeat, , );

rtDeclareVariable(float, _exponent, , );
rtDeclareVariable(float, _kd, , );
rtDeclareVariable(float, _ks, , );


float3 bumpedNormal(float3 n)
{
  if (_normal_map_id < 0)
    return n;
  else
  {
    float3 Tangent   = generateOrthogonal(n);
    float3 Bitangent = cross(n, Tangent);
    Tangent          = cross(n, Bitangent);

    float2 uv = _texture_repeat * make_float2(texcoord);
    uv -= make_float2(floorf(uv.x), floorf(uv.y));

    float3 BumpMapNormal = make_float3(optix::rtTex2D<float4>(_normal_map_id, uv.x, 1.f - uv.y));
    BumpMapNormal        = 2.0 * BumpMapNormal - make_float3(1.0, 1.0, 1.0);
    float3       NewNormal;
    Matrix<3, 3> TBN;   // = mat3(Tangent, Bitangent, Normal);
    //TBN.setRow(0,Tangent);
    TBN.setCol(0, Tangent);
    //TBN.setRow(1,Bitangent);
    TBN.setCol(1, Bitangent);
    //TBN.setRow(2,n);
    TBN.setCol(2, n);
    NewNormal = TBN * BumpMapNormal;
    NewNormal = normalize(NewNormal);
    //return n;
    // return BumpMapNormal;
    return NewNormal;
  }
}


COLOR getDiffuseColor()
{
  if (_d_tex_id < 0)
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    return colorFromAsset(_diffuse_color, current_prd.wavelength_offset, current_prd.id_wavelength);
#else
    return _diffuse_color;
#endif
  else
  {
    float2 uv = _texture_repeat * make_float2(texcoord);
    uv -= make_float2(floorf(uv.x), floorf(uv.y));
    uv.y = 1.f - uv.y;
#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    COLOR diffuse = colorFromAsset(_diffuse_color, current_prd.wavelength_offset, current_prd.id_wavelength);

    if (current_prd.id_wavelength == -1)
    {
      for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; ++i)
      {
        //Use built-in texture interpolation for bounds.
        float wave = (i + current_prd.wavelength_offset) / (NB_MAX_WAVES_MULTIPLEXED - 1.f);
        diffuse[i] *= optix::rtTex3D<float>(_d_tex_id, uv.x, uv.y, wave);
      }
    }
    else
    {
      float wave = (current_prd.id_wavelength + current_prd.wavelength_offset) / (NB_MAX_WAVES_MULTIPLEXED - 1.f);
      diffuse[current_prd.id_wavelength] *= optix::rtTex3D<float>(_d_tex_id, uv.x, uv.y, wave);
    }

    return diffuse;
#  else
    return _diffuse_color * optix::rtTex3D<float>(_d_tex_id, uv.x, uv.y, 0.f);
#  endif
#else
    return _diffuse_color * make_float3(optix::rtTex2D<float4>(_d_tex_id, uv.x, uv.y));
#endif
  }
}

COLOR getSpecularColor()
{
  if (_s_tex_id < 0)
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    return colorFromAsset(_specular_color, current_prd.wavelength_offset, current_prd.id_wavelength);
#else
    return _specular_color;
#endif
  else
  {
    float2 uv = _texture_repeat * make_float2(texcoord);
    uv -= make_float2(floorf(uv.x), floorf(uv.y));
    uv.y = 1.f - uv.y;
#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    COLOR diffuse = colorFromAsset(_specular_color, current_prd.wavelength_offset, current_prd.id_wavelength);

    if (current_prd.id_wavelength == -1)
    {
      for (int i = 0; i < NB_MAX_WAVES_MULTIPLEXED; ++i)
      {
        //Use built-in texture interpolation for bounds.
        float wave = (i + current_prd.wavelength_offset) / (NB_MAX_WAVES_MULTIPLEXED - 1.f);
        diffuse[i] *= optix::rtTex3D<float>(_s_tex_id, uv.x, uv.y, wave);
      }
    }
    else
    {
      float wave = (current_prd.id_wavelength + current_prd.wavelength_offset) / (NB_MAX_WAVES_MULTIPLEXED - 1.f);
      diffuse[current_prd.id_wavelength] *= optix::rtTex3D<float>(_s_tex_id, uv.x, uv.y, wave);
    }

    return diffuse;
#  else
    return _specular_color * optix::rtTex3D<float>(_s_tex_id, uv.x, uv.y, 0.f);
#  endif
#else
    return _specular_color * make_float3(optix::rtTex2D<float4>(_s_tex_id, uv.x, uv.y));
#endif
  }
}

float getExponent()
{
  if (_exponent_map_id < 0)
    return _exponent;
  else
  {
    float2 uv = _texture_repeat * make_float2(texcoord);
    uv -= make_float2(floorf(uv.x), floorf(uv.y));
    uv.y = 1.f - uv.y;
    return _exponent * (1.f - optix::rtTex2D<float>(_exponent_map_id, uv.x, uv.y));
  }
}

bool bsdf_is_dirac()
{
  return false;   //Phong is full reflective
}

float3 randomHemiPowerCosDirection(float const exponent, float const random_var_1, float const random_var_2)
{
  //Global illumination compendium formula (36)
  float const phi = 2.f * M_PIf * random_var_2;

  float const z         = powf(random_var_1, 1.f / (exponent + 1.f));
  float const sin_theta = sqrtf((1.f - z * z));
  float const x         = sin_theta * cosf(phi);
  float const y         = sin_theta * sinf(phi);

  return make_float3(x, y, z);
}

float bsdf_pdf(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  float3 normal = bumpedNormal(ffnormal);
  if (dot(ffnormal, light) < 0.f || dot(ffnormal, eye) < 0.f) return 0.f;
  if (lobe == DIFFUSE_REFL)
  {
    float cosTheta = dot(normal, light);
    return cosTheta / M_PIf;
  }
  else if (lobe == SPECULAR_REFL)
  {
    float3 reflected = optix::reflect(-eye, normal);
    if (dot(reflected, light) < 0.f)   //specular lobe under the surface.
      return 0.f;
    return 0.5f * (getExponent() + 1.f) * powf(dot(reflected, light), getExponent()) / M_PIf;
  }
  else   // ANY_REFL
  {
    float  pdf_diffuse_part = _kd * dot(normal, light) / M_PIf;
    float3 reflected        = optix::reflect(-eye, normal);
    if (dot(reflected, light) < 0.f)   //specular lobe under the surface.
      return pdf_diffuse_part / (_kd + _ks);

    float pdf_specular_part = _ks * 0.5f * (getExponent() + 1.f) / M_PIf * powf(dot(reflected, light), getExponent());

    return (pdf_diffuse_part + pdf_specular_part) / (_kd + _ks);
  }
}

COLOR bsdf_eval(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  float3 normal = bumpedNormal(ffnormal);
  if (dot(ffnormal, light) < 0.f || dot(ffnormal, eye) < 0.f) return COLOR(0.f);
  if (lobe == DIFFUSE_REFL)   //DIFFUSE LOBE
  {
    return getDiffuseColor() * _kd / M_PIf;
  }
  else if (lobe == SPECULAR_REFL)
  {
    float3 reflected = optix::reflect(-eye, normal);
    if (dot(reflected, light) < 0.f)   //specular lobe under the surface.
      return COLOR(0.f);
    return getSpecularColor() * _ks * powf(dot(reflected, light), getExponent()) * (getExponent() + 2.f) * 0.5f / M_PIf;
  }
  else   // ANY_REFL
  {
    COLOR  diffuse_part = getDiffuseColor() * _kd / M_PIf;
    float3 reflected    = optix::reflect(-eye, normal);
    if (dot(reflected, light) < 0.f)   //specular lobe under the surface.
      return diffuse_part;
    COLOR specular_part
        = getSpecularColor() * _ks * powf(dot(reflected, light), getExponent()) * (getExponent() + 2.f) * 0.5f / M_PIf;
    return diffuse_part + specular_part;
  }
}

COLOR bsdf_eval_optim(float3 h, float3 ffnormal, float3 eye, float3 light, int lobe, float external_pdf)
{
  float3 normal = bumpedNormal(ffnormal);
  if (dot(ffnormal, light) < 0.f || dot(ffnormal, eye) < 0.f) return COLOR(0.f);
  if (lobe == DIFFUSE_REFL)   //DIFFUSE LOBE
  {
    return getDiffuseColor();
  }
  else if (lobe == SPECULAR_REFL)
  {
    return getSpecularColor() * (_exponent + 2.f) / (_exponent + 1.f) * dot(normal, light);
  }
  else   //SHOULD BE CALLED IF LOBE == ANY_REFL -> no optim possible for full phong
  {
    return COLOR(0.f);
  }
}

float3 generate_half_vector(float3 n, float3 in_dir)
{
  return n;
}

float4 generate_reflective_sample(float3 n, float3 in_dir)
{
  float2 rnd2d = sampling2D(
      current_prd.num_sample,
      current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
      current_prd.offset_sampling,
      current_prd.seed);
  float z1 = rnd2d.x;
  float z2 = rnd2d.y;

  float z3 = sampling1D(
      current_prd.num_sample,
      current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF,
      current_prd.offset_sampling.x,
      current_prd.seed);

  float3 normal = bumpedNormal(n);

  if (z3 <= _kd)
  {
    float3 p;
    cosine_sample_hemisphere(z1, z2, p);
    optix::Onb onb(normal);
    onb.inverse_transform(p);
    return make_float4(p, DIFFUSE_REFL);
  }
  else if (z3 <= _kd + _ks)
  {
    float3 p = randomHemiPowerCosDirection(_exponent, z1, z2);

    float3     reflected = reflect(in_dir, normal);
    optix::Onb onb(reflected);
    onb.inverse_transform(p);
    return make_float4(p, SPECULAR_REFL);
  }
  // else Ray is absorbed, return a 0 vector -> ray will be terminated.
  return make_float4(make_float3(0.f), ABSORPTION);
}
