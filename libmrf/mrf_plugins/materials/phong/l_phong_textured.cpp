#include "l_phong_textured.hpp"
#include <mrf_core/sampling/random_generator.hpp>
#include <mrf_core/image/image_formats.hpp>

namespace mrf
{
namespace materials
{
using namespace mrf::sampling;
using namespace math;
using namespace mrf::image;

//-------------------------------------------------------------------//
//---------------        Physical Phong Model by Lafortune CLASS   --//
//-------------------------------------------------------------------//
LPhysicalPhongTextured::LPhysicalPhongTextured(std::string const & name,
    COLOR const & dcolor, float const  dalbedo,
    COLOR const & scolor,float const  salbedo,
    unsigned int const exp,
  float texture_repeat)
  : LPhysicalPhong( name,dcolor,dalbedo,scolor,salbedo,exp )
{
  _type           = BRDFTypes::PHONG_NORMALIZED_TEXTURED;
  _texture_repeat = texture_repeat;
}


LPhysicalPhongTextured::~LPhysicalPhongTextured()
{
}

IMAGE_LOAD_SAVE_FLAGS LPhysicalPhongTextured::loadDiffuseTexture(std::string const & path)
{
    try {
        _d_texture = mrf::image::load(path);
    } catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS& error) {
        return error;
    }

    return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}

IMAGE_LOAD_SAVE_FLAGS LPhysicalPhongTextured::loadSpecularTexture(std::string const & path)
{
    try {
        _s_texture = mrf::image::load(path);
    } catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS& error) {
        return error;
    }

    return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}

IMAGE_LOAD_SAVE_FLAGS LPhysicalPhongTextured::loadNormalMap(std::string const & path)
{
    try {
        _normal_map = mrf::image::loadColorImage(path, true);
    } catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS& error) {
        return error;
    }

    return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}

IMAGE_LOAD_SAVE_FLAGS LPhysicalPhongTextured::loadExponentMap(std::string const & path)
{
    try {
        _exp_map = mrf::image::loadColorImage(path, true);
    } catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS& error) {
        return error;
    }

    return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}

//-------------------------------------------------------------------
float
LPhysicalPhongTextured::brdfValue( mrf::geom::LocalFrame const & lf,
                                   mrf::math::Vec3f const & in_dir,
                                   mrf::math::Vec3f const & out_dir ) const
{
  //TODO use textures
  mrf::math::Vec3f const reflectedDir = 2.f*(in_dir.dot(lf.normal()))*lf.normal()-in_dir;
  return static_cast<float>(powf(gpos(reflectedDir.dot(out_dir)), static_cast<float>(_exp))*_salbedo*static_cast<float>(_exp+2)/(2.f*Math::PI)+_dalbedo / Math::PI);
}


RADIANCE_TYPE
LPhysicalPhongTextured::coloredBRDFValue( mrf::geom::LocalFrame const & lf,
    mrf::math::Vec3f const & in_dir,
    mrf::math::Vec3f const & out_dir ) const
{
  //TODO use textures
  mrf::math::Vec3f const reflectedDir = 2.f*(in_dir.dot(lf.normal()))*lf.normal()-in_dir;
  return _scolor*(powf(gpos(reflectedDir.dot(out_dir)), static_cast<float>(_exp))*_salbedo*(_exp+2.f)/(2.f*static_cast<float>(Math::PI)))+_dcolor*(_dalbedo / static_cast<float>(Math::PI));
}







}
}
