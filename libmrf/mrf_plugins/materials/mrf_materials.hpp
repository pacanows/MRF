/*
 *
 * author : Arthur Dufay
 * Copyright INRIA 2018
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>

#include <mrf_core/materials/umat.hpp>
#include <mrf_core/materials/brdf.hpp>

#include <mrf_plugins/materials/emittance/dirac_emittance.hpp>
#include <mrf_plugins/materials/emittance/uniform_emittance.hpp>
#include <mrf_plugins/materials/emittance/diffuse_emittance.hpp>
#include <mrf_plugins/materials/emittance/conical_emittance.hpp>

#include <mrf_plugins/materials/lambert/lambert.hpp>

#include <mrf_plugins/materials/phong/l_phong.hpp>
#include <mrf_plugins/materials/phong/l_phong_textured.hpp>

#include <mrf_plugins/materials/shadow_catcher/shadow_catcher.hpp>

#include <mrf_plugins/materials/debug/flat.hpp>
#include <mrf_plugins/materials/debug/uv_shader.hpp>
#include <mrf_plugins/materials/debug/normal_shader.hpp>
#include <mrf_plugins/materials/debug/tangent_shader.hpp>

#include <mrf_plugins/materials/checkerboard/checkerboard.hpp>

#include <mrf_plugins/materials/microfacet/conductor/ggx.hpp>
#include <mrf_plugins/materials/microfacet/conductor/aniso_ggx.hpp>
#include <mrf_plugins/materials/microfacet/conductor/perfect_mirror.hpp>
#include <mrf_plugins/materials/microfacet/conductor/fresnel_mirror.hpp>

#include <mrf_plugins/materials/microfacet/dielectric/fresnel_glass.hpp>
#include <mrf_plugins/materials/microfacet/dielectric/walter_bsdf.hpp>

#include <mrf_plugins/materials/measured/measured_isotropic.hpp>

#include <mrf_plugins/materials/principled/principled.hpp>

#include <mrf_plugins/materials/utia/measured_utia.hpp>

//TODO: uncomment and adjust template according to your material.
// #include <mrf_plugins/materials/material_template/material_template.hpp>