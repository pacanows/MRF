/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_optix/optix_material_plugin.hpp>
#include <mrf_optix/optix_light_plugin.hpp>

#include <mrf_plugins/materials/lambert/backend/optix/lambert_optix.hpp>
#include <mrf_plugins/materials/checkerboard/backend/optix/checkerboard_optix.hpp>
#include <mrf_plugins/materials/phong/backend/optix/phong_optix.hpp>
#include <mrf_plugins/materials/principled/backend/optix/principled_optix.hpp>
#include <mrf_plugins/materials/microfacet/conductor/backend/optix/conductor_optix.hpp>
#include <mrf_plugins/materials/microfacet/dielectric/backend/optix/dielectric_optix.hpp>
#include <mrf_plugins/materials/measured/backend/optix/measured_optix.hpp>
#include <mrf_plugins/materials/utia/backend/optix/utia_optix.hpp>
#include <mrf_plugins/materials/debug/backend/optix/debug_optix.hpp>
#include <mrf_plugins/materials/shadow_catcher/backend/optix/catcher_optix.hpp>
#include <mrf_plugins/materials/emittance/backend/optix/emittance_optix.hpp>

//TODO: uncomment and adjust template according to your material.
// #include <mrf_plugins/materials/material_template/backend/optix/template_optix.hpp>
