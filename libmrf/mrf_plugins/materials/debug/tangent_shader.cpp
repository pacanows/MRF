#include "tangent_shader.hpp"


namespace mrf
{
namespace materials
{
TangentShader::TangentShader(std::string const &name): BRDF(name, BRDFTypes::TANGENT) {}

TangentShader::~TangentShader() {}


RADIANCE_TYPE
TangentShader::coloredBRDFValue(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return RADIANCE_TYPE(lf.tangent().x() * 0.5f + 0.5f, lf.tangent().y() * 0.5f + 0.5f, lf.tangent().z() * 0.5f + 0.5f);
}
}   // namespace materials
}   // namespace mrf
