/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 *  This class represents a constant BRDF represented only by a color
 *
 *  The only purpose of this class is to help debugging and also to provide
 *  a base class with most methods implemented for other specific BRDF class such as UVShader
 *  NormalShader and TangentShader
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/materials/brdf.hpp>
#include <mrf_core/materials/bsdf.hpp>
#include <mrf_core/geometry/local_frame.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *FLAT = "Flat";
};   // namespace BRDFTypes

class MRF_PLUGIN_EXPORT FlatBRDF: public BRDF
{
private:
  COLOR _color;

public:
  FlatBRDF(std::string const &name, COLOR const &color = COLOR());
  FlatBRDF(std::string const &name, std::string const &type, COLOR const &color = COLOR());
  virtual ~FlatBRDF();



  virtual float
  brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;

  virtual void  setColor(COLOR const &color) { _color = color; };
  virtual COLOR getColor() const { return _color; };
  virtual float diffuseAlbedo() const;
  virtual float diffuseAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  diffuseComponent() const;

  virtual float specularAlbedo() const;
  virtual float specularAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  specularComponent() const;

  virtual float transmissionAlbedo() const;
  virtual float transmissionAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  transmissionComponent() const;

  virtual float nonDiffuseAlbedo() const;
  virtual bool  nonDiffuseComponent() const;
  virtual float totalAlbedo() const;


  //-------------------------------------------------------------------------
  virtual float
  diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;
};



}   // namespace materials
}   // namespace mrf