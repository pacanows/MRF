/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/principled/backend/optix/principled_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *PrincipledPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *PrincipledPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  //if (!material) return;
  optix::Material optix_mat       = context->createMaterial();
  auto            compile_options = cuda_compile_options;

  auto principled = dynamic_cast<mrf::materials::PrincipledDisney *>(material);
  if (principled)
  {
    optix_mat["_roughness"]->setFloat(principled->getRoughness());
    optix_mat["_subsurface"]->setFloat(principled->getSubsurface());
    optix_mat["_sheen"]->setFloat(principled->getSheen());
    optix_mat["_sheenTint"]->setFloat(principled->getSheenTint());
    optix_mat["_clearcoat"]->setFloat(principled->getClearcoat());
    optix_mat["_clearcoatGloss"]->setFloat(principled->getClearcoatGloss());
    optix_mat["_specular"]->setFloat(principled->getSpecular());
    optix_mat["_specularTint"]->setFloat(principled->getSpecularTint());
    optix_mat["_anisotropic"]->setFloat(principled->getAnisotropic());
    optix_mat["_metallic"]->setFloat(principled->getMetallic());
    optix_mat["_baseColorTint"]->setFloat(principled->getBaseColorTint());

    compile_options.push_back("-DPRINCIPLED");
    OptixPrincipled *ret_mat = new OptixPrincipled(principled, optix_mat, compile_options, ptx_cfg);

    std::vector<std::string>                   variable_names = {"_base_color"};
    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(principled->getBaseColor());

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif

    //compileBRDF(optix_mat, compile_options);

    //return optix_mat;
    //return new OptixPrincipled(optix_mat, compile_options);
    return ret_mat;
  }

  return nullptr;
}

}   // namespace optix_backend
}   // namespace mrf
