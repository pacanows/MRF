//------------------------------------------------------------------------
//------------------------------------------------------------------------
//
//  Originally designed by Bruce Walter (introduced in SIGGRAPH12)
//   Taken from BRDF Explorer, Disney's open source BRDF renderer
//   Author: Léo Ackermann (L3 Internship, 2020)
//   Modification: David Murray @ institutoptique.fr (2020)
//
//  Notes on the current version :
//   Based on a linear color space (!= Disney)
//   Only available for RGB_Malia
//
//  TODO: transmission (burley2015)
//------------------------------------------------------------------------
//------------------------------------------------------------------------


#include <optixu/optixu_matrix_namespace.h>

#include "include/fresnel.h"
#include "include/distribution/cosine.h"
#include "include/distribution/ggx.h"

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> _base_color;
#  else
rtDeclareVariable(float, _base_color, , );
#  endif
#else
rtDeclareVariable(float3, _base_color, , );
#endif

rtDeclareVariable(float, _baseColorTint, , );

rtDeclareVariable(float, _roughness, , );
rtDeclareVariable(float, _subsurface, , );
rtDeclareVariable(float, _sheen, , );
rtDeclareVariable(float, _sheenTint, , );
rtDeclareVariable(float, _clearcoat, , );
rtDeclareVariable(float, _clearcoatGloss, , );
rtDeclareVariable(float, _metallic, , );
rtDeclareVariable(float, _specular, , );
rtDeclareVariable(float, _specularTint, , );
rtDeclareVariable(float, _anisotropic, , );

/* Square function */
inline float sqr(float a)
{
  return a * a;
}

/* Power 5 function */
inline float pow5(float a)
{
  return a * a * a * a * a;
}

bool bsdf_is_dirac()
{
  return _roughness == 0.f;
}

//------------------------------------------------------------------------------
// Global
//------------------------------------------------------------------------------

float3 generate_half_vector(float3 n, float3 in_dir)
{
  //TODO do all the component choosing here to generate associated half-vector
  return n;
}

/* Main case */
/* The probability to computes a lobe is weighted by its final contribution */
/**
 * @brief      Generates a sample according to the chosen microfacet distribution, in the case of a reflection.
 *
 * @param      n        the normal vector.
 * @param      in_dir          the incoming direction, pointing toward the hitpoint.
 *
 * @return     The sample direction (xyz component) and the sampled lobe (w component).
 */
float4 generate_reflective_sample(float3 n, float3 in_dir)
{
  float4 out_dir = make_float4(0.0f);

  float2 rnd2d = sampling2D(
      current_prd.num_sample,
      current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
      current_prd.offset_sampling,
      current_prd.seed);

  float z1 = rnd2d.x;
  float z2 = rnd2d.y;

  float2 rnd2d2 = sampling2D(
      current_prd.num_sample,
      current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
      current_prd.offset_sampling,
      current_prd.seed);

  float z3 = rnd2d2.x;
  float z4 = rnd2d2.y;

  //Reminder:
  //metallic = 0 for pure dielectric (diffuse+specular+clearcoat)
  //metallic = 1 for pure metal (specular+clearcoat)
  //Weight of specular component is the combination:
  float specularWeight  = _metallic + (1.f - _metallic) * (_specular);
  float clearcoatWeight = _clearcoat;
  float diffuseWeight   = (1.f - _metallic) * (1.f - _specular);

  float norm = specularWeight + clearcoatWeight + diffuseWeight;
  specularWeight /= norm;
  clearcoatWeight /= norm;
  diffuseWeight /= norm;

  //TODO CHECK ME HERE
  if (z3 <= specularWeight / norm)   //Specular component.
  {
    float  aspect  = sqrt(1.0f - _anisotropic * 0.9f);
    float  ax      = max(0.001f, sqr(_roughness) / aspect);
    float  ay      = max(0.001f, sqr(_roughness) * aspect);
    float2 alpha_g = make_float2(ax, ay);
    float3 h       = ggx_sample(z1, z2, in_dir, n, alpha_g);

    out_dir = make_float4(optix::reflect(in_dir, h), SPECULAR_REFL);
  }
  else if (z3 - specularWeight <= clearcoatWeight)
  {
    float2 alpha_g = make_float2(_roughness);
    float3 h       = ggx_sample(z1, z2, in_dir, n, alpha_g);
    out_dir        = make_float4(optix::reflect(in_dir, h), CLEARCOAT_REFL);
  }
  else if (z3 - specularWeight - clearcoatWeight <= diffuseWeight)
  {
    out_dir = make_float4(cosine_sample(z1, z2, in_dir, n), DIFFUSE_REFL);
  }
  else
  {
    //Undefined behavior, should probably be absorption...
    out_dir = make_float4(cosine_sample(z1, z2, in_dir, n), ANY_REFL);
  }

  return out_dir;
}

//------------------------------------------------------------------------------
//  Computes the pdf of the BRDF
//------------------------------------------------------------------------------
/* Clearcoat component */
float bsdf_pdf_clearcoat_cmp(float3 ffnormal, float3 eye, float3 light)
{
  float  dot_NL = dot(light, ffnormal);
  float3 h      = halfVector(eye, light, ffnormal);
  float  dot_NH = dot(h, ffnormal);
  if (dot_NH <= 0.f || dot_NL <= 0.f) return 0.f;

  float distribution_value = ggx_distribution(_roughness * _roughness, dot_NH);
  float temp_float         = 1.f / (4.f * abs(dot_NL));   // 1 / 4 |o dot h| -> jacobien term

  return temp_float * distribution_value * abs(dot_NH);
}

/* Specular component */
float bsdf_pdf_specular_cmp(float3 ffnormal, float3 eye, float3 light)
{
  float  dot_NL = dot(light, ffnormal);
  float3 h      = halfVector(eye, light, ffnormal);
  float  dot_NH = dot(h, ffnormal);
  if (dot_NH <= 0.f || dot_NL <= 0.f) return 0.f;

  float distribution_value;
  if (_anisotropic > 0.f)
  {
    float aspect = sqrt(1.0f - _anisotropic * 0.9f);
    float ax     = max(0.001f, sqr(_roughness) / aspect);
    float ay     = max(0.001f, sqr(_roughness) * aspect);

    float3 local_h     = toLocalFrame(h, ffnormal);
    distribution_value = ggx_distribution(ax, ay, local_h.x, local_h.y, local_h.z);
  }
  else
    distribution_value = ggx_distribution(_roughness * _roughness, dot_NH);

  float temp_float = 1.f / (4.f * abs(dot_NL));   // 1 / 4 |o dot h| -> jacobien term

  return temp_float * distribution_value * abs(dot_NH);
}

/* Main case pdf */
float bsdf_pdf(float3 n, float3 eye, float3 light, int lobe)
{
  //We need the weights to be accounted for in the pdfs.
  float specularWeight  = _metallic + (1.f - _metallic) * (_specular);
  float clearcoatWeight = _clearcoat;
  float diffuseWeight   = (1.f - _metallic) * (1.f - _specular);
  float norm            = specularWeight + clearcoatWeight + diffuseWeight;

  if (lobe == DIFFUSE_REFL)
  {
    diffuseWeight /= norm;
    return cosine_pdf(n, eye, light) * diffuseWeight;
  }
  else if (lobe == CLEARCOAT_REFL)
  {
    clearcoatWeight /= norm;
    return bsdf_pdf_clearcoat_cmp(n, eye, light) * clearcoatWeight;
  }
  else if (lobe == SPECULAR_REFL)
  {
    specularWeight /= norm;
    return bsdf_pdf_specular_cmp(n, eye, light) * specularWeight;
  }
  else   //ANY_REFL -> no importance sampling. Used by light sampling.
  {
    specularWeight /= norm;
    clearcoatWeight /= norm;
    diffuseWeight /= norm;
    float diffuse_pdf_cmp   = cosine_pdf(n, eye, light) * diffuseWeight;
    float clearcoat_pdf_cmp = bsdf_pdf_clearcoat_cmp(n, eye, light) * clearcoatWeight;
    float specular_pdf_cmp  = bsdf_pdf_specular_cmp(n, eye, light) * specularWeight;
    return specular_pdf_cmp + diffuse_pdf_cmp + clearcoat_pdf_cmp;
  }
}

//------------------------------------------------------------------------------
// By component
//------------------------------------------------------------------------------
/* Diffuse component */
COLOR bsdf_diffuse_cmp(float3 n, float3 eye, float3 light)
{
  float3 h     = halfVector(light, eye, n);
  float  NdotL = dot(n, light);
  float  NdotV = dot(n, eye);
  if (NdotL <= 0.f || NdotV <= 0.f) return COLOR(0.f);

  float NdotH  = max(dot(n, h), 0.0f);
  float LdotH  = max(dot(h, light), 0.0f);
  float LdotH2 = LdotH * LdotH;
  float FL     = pow5(1.0f - NdotL);
  float FV     = pow5(1.0f - NdotV);
  float FH     = pow5(1.0f - LdotH);
  float Ctint  = _baseColorTint;

  float F90         = 0.5f + 2.0f * LdotH2 * _roughness;
  float LambertLike = optix::lerp(1.0f, F90, FL) * optix::lerp(1.0f, F90, FV);

  float FSS90 = LdotH2 * _roughness;
  float HanrahanLike
      = (optix::lerp(1.0f, FSS90, FL) * optix::lerp(1.0f, FSS90, FV) * (1.0f / (NdotL + NdotV) - 0.5f) + 0.5f) * 1.25f;
  float subsurfaceTerm = optix::lerp(LambertLike, HanrahanLike, _subsurface) / M_PIf;

  float sheenTerm = optix::lerp(1.0f, Ctint, _sheenTint) * FH;

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  COLOR baseColor = colorFromAsset(_base_color, current_prd.wavelength_offset, current_prd.id_wavelength);
  COLOR C0        = baseColor * subsurfaceTerm;
  return (C0 + sheenTerm * _sheen) * (1.0f - _metallic);
#else
  return (_base_color * subsurfaceTerm + sheenTerm * _sheen) * (1.0f - _metallic);
#endif
}

/* Clearcoat component */
COLOR bsdf_clearcoat_cmp(float3 n, float3 eye, float3 light)
{
  float3 h     = halfVector(light, eye, n);
  float  NdotL = max(dot(n, light), 0.0f);
  float  NdotV = max(dot(n, eye), 0.0f);
  if (NdotL <= 0.f || NdotV <= 0.f) return COLOR(0.f);

  float NdotH = max(dot(n, h), 0.0f);
  float HdotL = max(dot(h, light), 0.0f);
  float HdotV = max(dot(h, eye), 0.0f);
  float LdotH = max(dot(h, light), 0.0f);
  float FH    = pow5(1.0f - LdotH);

  //TODO CHECK GTR1->ggx
  float alpha_gloss = optix::lerp(0.1f, 0.001f, _clearcoatGloss);
  float Dterm       = ggx_distribution(alpha_gloss * alpha_gloss, NdotH);
  float Fterm       = optix::lerp(0.04f, 1.0f, FH);
  float Gterm       = ggx_geometricTerm(h, n, eye, light, make_float2(_roughness));

  float norm = 4.f * NdotL * NdotV;

  return 0.25f * _clearcoat * Gterm * Dterm * Fterm / norm;
}

/* Specular component */
COLOR bsdf_specular_cmp(float3 n, float3 eye, float3 light)
{
  float3 h = halfVector(light, eye, n);

  float NdotL = max(dot(n, light), 0.0f);
  float NdotV = max(dot(n, eye), 0.0f);
  if (NdotL <= 0.f || NdotV <= 0.f) return COLOR(0.f);

  float NdotH = max(dot(n, h), 0.0f);
  float HdotL = max(dot(h, light), 0.0f);
  float HdotV = max(dot(h, eye), 0.0f);
  float LdotH = max(dot(light, h), 0.0f);
  float FH    = pow5(1.0f - LdotH);
  float Ctint = _baseColorTint;

  float Dterm, Gterm;
  if (_anisotropic > 0.f)
  {
    float aspect = sqrt(1.0f - _anisotropic * 0.9f);
    float ax     = max(0.001f, sqr(_roughness) / aspect);
    float ay     = max(0.001f, sqr(_roughness) * aspect);

    float3 local_h = toLocalFrame(h, n);
    Dterm          = ggx_distribution(ax, ay, local_h.x, local_h.y, local_h.z);

    Gterm = ggx_geometricTerm(h, n, eye, light, make_float2(ax, ay));
  }
  else
  {
    Dterm = ggx_distribution(_roughness * _roughness, NdotH);
    Gterm = ggx_geometricTerm(h, n, eye, light, make_float2(_roughness));
  }


  float tintTerm = optix::lerp(1.0f, Ctint, _specularTint) * _specular * .08f;

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  COLOR baseColor = colorFromAsset(_base_color, current_prd.wavelength_offset, current_prd.id_wavelength);
  COLOR Cspec0    = mrf::spectrum::lerp(tintTerm, baseColor, _metallic);
#else
  COLOR Cspec0 = mrf::spectrum::lerp(COLOR(tintTerm), _base_color, _metallic);
#endif

  COLOR Fterm = mrf::spectrum::lerp(Cspec0, COLOR(1.0f), FH);

  float norm = 4.f * NdotL * NdotV;

  return Fterm * Dterm * Gterm / norm;
}

//------------------------------------------------------------------------------
// Global
//------------------------------------------------------------------------------

/**
 * @brief      Compute the BSDF for either a reflection or refraction.
 *
 * @param      ffnormal        the face-forward normal vector.
 * @param      eye             the incoming direction, pointing away from the hitpoint.
 * @param      light           the outgoing direction, pointing away from the hitpoint.
 * @param      lobe            the previously sampled lobe. To use full BRDF, lobe = -1.
 *
 * @return     The value of the BSDF.
 */
COLOR bsdf_eval(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  if (lobe == DIFFUSE_REFL)
    return bsdf_diffuse_cmp(ffnormal, eye, light);
  else if (lobe == CLEARCOAT_REFL)
    return bsdf_clearcoat_cmp(ffnormal, eye, light);
  else if (lobe == SPECULAR_REFL)
    return bsdf_specular_cmp(ffnormal, eye, light);
  else   //ANY_REFL -> no IS, compute "generic" form of the BSDF. Used by light sampling strategy.
  {
    float specularWeight  = _metallic + (1.f - _metallic) * (_specular);
    float clearcoatWeight = _clearcoat;
    float diffuseWeight   = (1.f - _metallic) * (1.f - _specular);
    float norm            = specularWeight + clearcoatWeight + diffuseWeight;
    specularWeight /= norm;
    clearcoatWeight /= norm;
    diffuseWeight /= norm;

    //TODO CHECK ME
    // bool  do_diffuse    = _metallic * _specular < 1.f;
    // bool  do_clearCoat  = _clearcoat > 0.f;
    // COLOR diffuse_cmp   = do_diffuse ? bsdf_diffuse_cmp(ffnormal, eye, light) : COLOR(0.f);
    // COLOR clearcoat_cmp = do_clearCoat ? bsdf_clearcoat_cmp(ffnormal, eye, light) : COLOR(0.f);
    // COLOR specular_cmp  = bsdf_specular_cmp(ffnormal, eye, light);

    COLOR diffuse_cmp   = bsdf_diffuse_cmp(ffnormal, eye, light) * diffuseWeight;
    COLOR clearcoat_cmp = bsdf_clearcoat_cmp(ffnormal, eye, light) * clearcoatWeight;
    COLOR specular_cmp  = bsdf_specular_cmp(ffnormal, eye, light) * specularWeight;

    return diffuse_cmp + clearcoat_cmp + specular_cmp;
  }
}

/**
 * @brief      Compute the optimized term BSDF * n dot l / pdf, for either a reflection or refraction.
 *
 * @param      ffnormal        the face-forward normal vector.
 * @param      eye             the incoming direction, pointing away from the hitpoint.
 * @param      light           the outgoing direction, pointing away from the hitpoint.
 * @param      lobe            the previously sampled lobe. To use full BSDF, lobe = -1.
 * @param      external_pdf    an pdf previously computed if needed.
 *
 * @return     The value of BSDF * n dot l / pdf.
 */
COLOR bsdf_eval_optim(float3 h, float3 ffnormal, float3 eye, float3 light, int lobe, float external_pdf)
{
  float pdf = bsdf_pdf(ffnormal, eye, light, lobe);
  return (pdf > 0.f) ? bsdf_eval(ffnormal, eye, light, lobe) * abs(dot(ffnormal, light)) / pdf : COLOR(0.f);
}
