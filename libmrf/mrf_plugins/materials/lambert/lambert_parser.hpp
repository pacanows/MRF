#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/rendering/scene.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/lexer.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <mrf_core/materials/materials.hpp>

#include <mrf_plugins/materials/lambert/lambert.hpp>

#include <cstdio>
#include <string>
#include <iostream>
#include <ostream>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace io
{
class LambertParser: public BaseMaterialParser<mrf::materials::Lambert>
{
public:
  LambertParser() {}

  static std::string getID() { return mrf::materials::BRDFTypes::LAMBERT; }

  static BasePluginParser<mrf::materials::UMat> *create() { return new LambertParser; }

  virtual bool parse(tinyxml2::XMLElement *a_mat_element, mrf::materials::UMat *a_mat, ParserHelper &parser)
  {
    mrf::materials::Lambert *lambert_mat = dynamic_cast<mrf::materials::Lambert *>(a_mat);

    if (!lambert_mat) return false;

    mrf::materials::COLOR color = parser.retrieveColor(a_mat_element);
    lambert_mat->setColor(color);

    float reflectivity;
    if (!parser.retrieveReflectivity(a_mat_element, reflectivity))
    {
      a_mat = nullptr;
      return false;
    }

    lambert_mat->setReflectivity(reflectivity);

    return true;
  }

protected:
private:
};
}   // namespace io
}   // namespace mrf
