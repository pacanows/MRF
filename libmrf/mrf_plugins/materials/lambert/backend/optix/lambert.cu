//-----------------------------------------------------------------------------
//
//  Lambertian surface closest-hit
//
//-----------------------------------------------------------------------------

#include "include/distribution/cosine.h"

#ifdef MRF_RENDERING_MODE_SPECTRAL

#  ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
rtBuffer<float, 1> diffuse_color;
rtBuffer<float, 1> diffuse_color2;
#  else
rtDeclareVariable(float, diffuse_color, , );
rtDeclareVariable(float, diffuse_color2, , );
#  endif

#else
rtDeclareVariable(float3, diffuse_color, , );
rtDeclareVariable(float3, diffuse_color2, , );
#endif
rtDeclareVariable(float, repeat, , );

bool bsdf_is_dirac()
{
  return false;
}

COLOR fetchColor()
{
  float2 temp_uv;
  temp_uv.x = repeat * texcoord.x;
  temp_uv.y = repeat * texcoord.y;

  int sum = int(temp_uv.x) + int(temp_uv.y);

  if (sum % 2 == 1)
  {
#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
    return colorFromAsset(diffuse_color2, current_prd.wavelength_offset, current_prd.id_wavelength);
#else
    return diffuse_color2;
#endif
  }

#ifdef MRF_RENDERING_MODE_SPECTRAL_MULTIPLEXED
  return colorFromAsset(diffuse_color, current_prd.wavelength_offset, current_prd.id_wavelength);
#else
  return diffuse_color;
#endif
}

COLOR bsdf_eval(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  float NdotL = max(dot(ffnormal, light), 0.0f);
  float NdotV = max(dot(ffnormal, eye), 0.0f);
  if (NdotL <= 0.f || NdotV <= 0.f) return COLOR(0.f);
  return fetchColor() * INV_PIf;
}

COLOR bsdf_eval_optim(float3 h, float3 ffnormal, float3 eye, float3 light, int lobe, float external_pdf)
{
  float NdotL = max(dot(ffnormal, light), 0.0f);
  float NdotV = max(dot(ffnormal, eye), 0.0f);
  if (NdotL <= 0.f || NdotV <= 0.f) return COLOR(0.f);
  return fetchColor();
}

float bsdf_pdf(float3 ffnormal, float3 eye, float3 light, int lobe)
{
  return cosine_pdf(ffnormal, eye, light);
}

float3 generate_half_vector(float3 n, float3 in_dir)
{
  return n;
}

float4 generate_reflective_sample(float3 n, float3 in_dir)
{
  float2 rnd2d = sampling2D(
      current_prd.num_sample,
      current_prd.depth * PRNG_BOUNCE_NUM + PRNG_BSDF_U,
      current_prd.offset_sampling,
      current_prd.seed);
  float z1 = rnd2d.x;
  float z2 = rnd2d.y;

  return make_float4(cosine_sample(z1, z2, in_dir, n), DIFFUSE_REFL);
}
