/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/materials/lambert/backend/optix/lambert_optix.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
OptixBaseMaterial *LambertPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg,
    std::vector<uint>               wavelengths)
#else
OptixBaseMaterial *LambertPlugin::createMaterialFromMRF(
    mrf::materials::UMat *          material,
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
#endif
{
  //if (!material) return;

  auto lambert = dynamic_cast<mrf::materials::Lambert *>(material);
  if (lambert)
  {
    optix::Material optix_mat = context->createMaterial();
    optix_mat["repeat"]->setFloat(0.f);
    auto compile_options = cuda_compile_options;
    compile_options.push_back("-DLAMBERT");

    OptixLambert *ret_mat = new OptixLambert(lambert, optix_mat, compile_options, ptx_cfg);

    float                    albedo         = lambert->diffuseAlbedo();
    std::vector<std::string> variable_names = {"diffuse_color", "diffuse_color2"};

    std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
    variable_values.push_back(lambert->getColor() * albedo);
    variable_values.push_back(mrf::materials::COLOR(0.f));

#ifdef MRF_RENDERING_MODE_SPECTRAL
    ret_mat->setCustomRadianceVariables(context, variable_values, variable_names, wavelengths);
#else
    ret_mat->setCustomRadianceVariables(variable_values, variable_names);
#endif

    return ret_mat;
  }

  return nullptr;
}

}   // namespace optix_backend
}   // namespace mrf
