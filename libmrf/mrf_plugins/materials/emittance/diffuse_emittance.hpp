/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/emittance.hpp>

namespace mrf
{
namespace materials
{
namespace EmittanceTypes
{
static char const *DIFFUSE = "diffuse";
};   // namespace EmittanceTypes

/**
 * Diffuse Emitter emits the same value of radiance
 * in all direction
 *
 **/
class MRF_PLUGIN_EXPORT DiffuseEmittance: public Emittance
{
protected:
public:
  DiffuseEmittance(std::string const &name, float radiance = 1.0f, COLOR const &color = COLOR(1.0f));


  //virtual float radiance( mrf::math::Vec3f const & emit_out_dir ) const;


  //-------------------------------------------------------------------
  // UMat's interface Implementation of
  //-------------------------------------------------------------------
  virtual RADIANCE_TYPE radianceEmitted(mrf::math::Vec3f const &emit_out_dir, mrf::geom::LocalFrame const &lf) const;

  virtual RADIOSITY_TYPE radiosityEmitted() const;


  virtual RADIOSITY_TYPE
  particleEmission(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2, mrf::math::Vec3f &direction)
      const;

protected:
  virtual void computeInternalValues();
};
}   // namespace materials
}   // namespace mrf