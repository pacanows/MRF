#include <mrf_plugins/materials/emittance/uniform_emittance.hpp>
#include <mrf_core/math/math.hpp>

using namespace mrf::math;


namespace mrf
{
namespace materials
{
UniformEmittance::UniformEmittance(std::string const &name, float radiance, COLOR const &color)
  : DiffuseEmittance(name, radiance, color)
{
  _type       = EmittanceTypes::UNIFORM;
  _cRadiosity = _cRadiance * static_cast<float>(Math::FOUR_PI);
}


RADIOSITY_TYPE
UniformEmittance::particleEmission(
    mrf::geom::LocalFrame const &lf,
    float                        random_var_1,
    float                        random_var_2,
    mrf::math::Vec3f &           direction) const
{
  direction = randomSphereDirection(random_var_1, random_var_2);

  // Uniform means that cos(N, w) is always equal to 1
  // p(w) = 1 / (4 * Pi )

  //None spatially varying material
  //We reversed direction because direction is Not pointing
  //toward the material surface
  return radianceEmitted(-direction, lf) * static_cast<float>(Math::FOUR_PI);
}

void UniformEmittance::computeInternalValues()
{
  _cRadiance = _color * _radiance;

  _cRadiosity = _cRadiance * static_cast<float>(Math::FOUR_PI);
}
}   // namespace materials
}   // namespace mrf
