/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/


#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/emittance.hpp>


namespace mrf
{
namespace materials
{
namespace EmittanceTypes
{
//For Directional light sources
//Emission occurs in only ONE direction
static char const *DIRAC = "dirac";
};   // namespace EmittanceTypes

/**
 * A Material representing emission on only one direction
 *
 *
 *
 **/
class MRF_PLUGIN_EXPORT DiracEmittance: public Emittance
{
protected:
public:
  DiracEmittance(
      std::string const &     name,
      mrf::math::Vec3f const &direction = mrf::math::Vec3f(0.9f, -1.0f, 0.f),   //Cone orientation in global frame
      float                   radiance  = 1.0f,                                 //Has to be >0
      COLOR const &           color     = COLOR(1.0f));

  //-------------------------------------------------------------------
  // UMat's interface Implementation of
  //-------------------------------------------------------------------
  virtual RADIANCE_TYPE radianceEmitted(mrf::math::Vec3f const &emit_out_dir, mrf::geom::LocalFrame const &lf) const;


  virtual RADIOSITY_TYPE radiosityEmitted() const;


  virtual RADIOSITY_TYPE
  particleEmission(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2, mrf::math::Vec3f &direction)
      const;

  virtual void computeInternalValues();
};

}   // namespace materials
}   // namespace mrf
