#include <mrf_plugins/materials/emittance/dirac_emittance.hpp>
#include <mrf_core/math/math.hpp>

namespace mrf
{
namespace materials
{
using namespace std;
using namespace mrf::math;
using namespace mrf::geom;


DiracEmittance::DiracEmittance(string const &name, math::Vec3f const &direction, float radiance, COLOR const &color)
  : Emittance(name, EmittanceTypes::DIRAC)
{
  _radiance  = radiance;
  _color     = color;
  _cRadiance = _color * _radiance;

  //Since this is a DIRAC EMISSION ONLY ONE VALUE IS DIFFERENT FROM 0
  _cRadiosity = _cRadiance;

  _direction = direction;
  _direction.normalize();
}


//-------------------------------------------------------------------
RADIANCE_TYPE
DiracEmittance::radianceEmitted(Vec3f const &emit_out_dir, LocalFrame const & /*lf*/) const
{
  float dotDirs = emit_out_dir.dot(_direction);

  if ((dotDirs < (-1 + EPSILON)) && (dotDirs > (-1 - EPSILON)))
  {
    return _cRadiance;
  }
  else
  {
    return RADIANCE_TYPE();
  }
}


RADIOSITY_TYPE
DiracEmittance::radiosityEmitted() const
{
  return _cRadiosity;
}


RADIOSITY_TYPE
DiracEmittance::particleEmission(
    LocalFrame const & /*lf*/,
    float /*random_var_1*/,
    float /*random_var_2*/,
    Vec3f &direction) const
{
  direction = _direction;

  //TODO : CHECK THIS WITH COACH
  return _cRadiosity;
}

void DiracEmittance::computeInternalValues()
{
  _cRadiance = _color * _radiance;

  //Radiosity B for the Diffuse Emitter is B = Pi * L
  //where L is the radiance

  //TODO : CHECK THIS WITH COACH
  _cRadiosity = _cRadiance;
}
}   // namespace materials
}   // namespace mrf
