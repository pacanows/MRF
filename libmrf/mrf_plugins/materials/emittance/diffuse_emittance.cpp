#include <mrf_plugins/materials/emittance/diffuse_emittance.hpp>
#include <mrf_core/math/math.hpp>

using namespace mrf::math;

namespace mrf
{
namespace materials
{
DiffuseEmittance::DiffuseEmittance(std::string const &name, float radiance, COLOR const &color)
  : Emittance(name, EmittanceTypes::DIFFUSE)
{
  _radiance = radiance;
  _color    = color;
  //Normalize the Color to avoid energy Creation
  //TODO : use a better normalization way ?
  //_color.normalize();
  computeInternalValues();
}



//-------------------------------------------------------------------
// UMat's interface Implementation
//-------------------------------------------------------------------
RADIANCE_TYPE
DiffuseEmittance::radianceEmitted(mrf::math::Vec3f const & /*emit_out_dir*/, mrf::geom::LocalFrame const & /*lf*/) const
{
  return _cRadiance;
}

RADIOSITY_TYPE
DiffuseEmittance::radiosityEmitted() const
{
  //Radiosity B for the Diffuse Emitter is B = Pi * L
  //where L is the radiance
  return _cRadiosity;
}


RADIOSITY_TYPE
DiffuseEmittance::particleEmission(
    mrf::geom::LocalFrame const &lf,
    float                        random_var_1,
    float                        random_var_2,
    mrf::math::Vec3f &           direction) const
{
  direction = randomHemiCosDirection(lf, random_var_1, random_var_2);

  //We should return L(x,w) * cos(N,w) / p(w)
  // since p(w) = cos(N,w) / PI
  //

  //Should we directly return _cRadiosity ?
  //None spatially varying material
  return radianceEmitted(direction, lf) * static_cast<float>(Math::PI);
}

void DiffuseEmittance::computeInternalValues()
{
  _cRadiance = _color * _radiance;

  //Radiosity B for the Diffuse Emitter is B = Pi * L
  //where L is the radiance
  _cRadiosity = _cRadiance * static_cast<float>(mrf::math::Math::PI);
}

}   // namespace materials
}   // namespace mrf
