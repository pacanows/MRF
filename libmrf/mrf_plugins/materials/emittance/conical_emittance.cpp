#include <mrf_plugins/materials/emittance/conical_emittance.hpp>
#include <mrf_core/math/math.hpp>

using namespace mrf::math;

namespace mrf
{
namespace materials
{
ConicalEmittance::ConicalEmittance(
    std::string const &     name,
    float                   aperture,
    mrf::math::Vec3f const &direction,
    float                   radiance,
    COLOR const &           color)
  : Emittance(name, EmittanceTypes::CONIC)
  , _cos_aperture(mrf::math::Math::absVal(std::cos(aperture)))
{
  _radiance = radiance;
  _color    = color;
  //_color.normalize();
  _direction = direction;
  _direction.normalize();

  //             #ifdef DEBUG
  std::cout << __FILE__ << " " << __LINE__ << std::endl;
  std::cout << "  _color = " << _color << std::endl;
  std::cout << "  _direction = " << _direction << std::endl;
  //             #endif


  _Xorientation = _direction.generateOrthogonal().normal();
  _Yorientation = _direction.cross(_Xorientation);

  computeInternalValues();
}



void ConicalEmittance::computeInternalValues()
{
  _cRadiance = _color * _radiance;

  //Radiosity B for the Diffuse Emitter is B = Pi * L
  //where L is the radiance
  _cRadiosity = _cRadiance * (static_cast<float>(mrf::math::Math::TWO_PI) * (1.f - _cos_aperture));
}


//-------------------------------------------------------------------
// UMat's interface Implementation of
//-------------------------------------------------------------------
RADIANCE_TYPE
ConicalEmittance::radianceEmitted(mrf::math::Vec3f const &emit_out_dir, mrf::geom::LocalFrame const & /*lf*/) const
{
  if (_direction.dot(-emit_out_dir) > _cos_aperture)
  {
    return _cRadiance;
  }

  return RADIANCE_TYPE();
}



RADIOSITY_TYPE
ConicalEmittance::radiosityEmitted() const
{
  //Radiosity B for the Diffuse Emitter is B = Pi * L
  //where L is the radiance
  return _cRadiosity;
}



RADIOSITY_TYPE
ConicalEmittance::particleEmission(
    mrf::geom::LocalFrame const & /*lf*/,
    float             random_var_1,
    float             random_var_2,
    mrf::math::Vec3f &particle_direction) const
{
  float const cos_theta = 1.f - random_var_1 * (1.f - _cos_aperture);
  float const sin_theta = sqrtf(1.f - cos_theta * cos_theta);
  float const phi       = random_var_2 * static_cast<float>(Math::TWO_PI);
  float const x         = cosf(phi) * sin_theta;
  float const y         = sinf(phi) * sin_theta;

  particle_direction = x * _Xorientation + y * _Yorientation + cos_theta * _direction;

  return _cRadiosity;
}
}   // namespace materials
}   // namespace mrf
