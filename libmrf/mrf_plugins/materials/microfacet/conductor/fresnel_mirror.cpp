/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#include "fresnel_mirror.hpp"

namespace mrf
{
namespace materials
{
FresnelMirror::FresnelMirror(std::string const &name)
  : PerfectMirror(name)
  , _eta(mrf::materials::COLOR(0.f))
  , _kappa(mrf::materials::COLOR(0.f))
{
  _type = BRDFTypes::FRESNEL_MIRROR;
}

FresnelMirror::FresnelMirror(
    std::string const &          name,
    mrf::materials::COLOR const &eta,
    mrf::materials::COLOR const &kappa)
  : PerfectMirror(name)
  , _eta(eta)
  , _kappa(kappa)
{
  _type = BRDFTypes::FRESNEL_MIRROR;
}

FresnelMirror::~FresnelMirror() {}

mrf::materials::COLOR const &FresnelMirror::eta() const
{
  return _eta;
}

mrf::materials::COLOR const &FresnelMirror::kappa() const
{
  return _kappa;
}

// float FresnelMirror::specularAlbedo() const
// {
//   //TODO
//   assert(0);
//   return 1.0f;
// }

// float FresnelMirror::nonDiffuseAlbedo() const
// {
//   //TODO
//   assert(0);
//   return 1.0f;
// }

// RADIANCE_TYPE
// FresnelMirror::coloredBRDFValue(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     mrf::math::Vec3f const &     out_dir) const
// {
//   //TODO
//   assert(0);
//   return mrf::materials::RADIANCE_TYPE();
// }

// ScatterEvent::SCATTER_EVT FresnelMirror::scatteringDirectionWithBRDF(
//     mrf::geom::LocalFrame const &lf,
//     mrf::math::Vec3f const &     in_dir,
//     float                        e1,
//     float                        e2,
//     mrf::math::Vec3f &           outgoing_dir,
//     RADIANCE_TYPE &              brdf_corrected_by_pdf) const
// {
//   RADIANCE_TYPE tmp;
//   generateScatteringDirection(lf, in_dir, tmp, outgoing_dir);

//   //TODO
//   assert(0);
//   brdf_corrected_by_pdf = RADIANCE_TYPE(1.0f);

//   return ScatterEvent::SPECULAR_REFL;
//   ;
// }


}   // namespace materials

}   // namespace mrf
