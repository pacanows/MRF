/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 * GGX Microfacet BRDF
 *
 **/
#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/materials/brdf.hpp>
#include <mrf_core/materials/fresnel.hpp>
#include <mrf_core/geometry/local_frame.hpp>


namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *ANISOGGX = "anisoGGX";   // Anisotropic GGC
};                                          // namespace BRDFTypes

/**
 * @brief      The Anisotropic version of the GGX BRDF
 */
//template<class FRESNEL_EVALUATOR>
class MRF_PLUGIN_EXPORT AnisoGGX: public BRDF
{
private:
  /* Parameter controling the Shape of the Distribution */
  mrf::math::Vec2f  _alpha_xy;
  mrf::math::Vec2f  _sqr_alpha_xy;
  FresnelConductor *_fresnel;

public:
  AnisoGGX(
      std::string const &  name,
      float                alpha_x,
      float                alpha_y,
      RADIANCE_TYPE const &eta = RADIANCE_TYPE(1.0),
      RADIANCE_TYPE const &k   = RADIANCE_TYPE(0.0));

  virtual ~AnisoGGX();

  virtual RADIANCE_TYPE eta() const;
  virtual RADIANCE_TYPE k() const;
  virtual float         alpha_x() const;
  virtual float         alpha_y() const;

  // Microfacet Interface methods
  inline float distribution(float cos_theta_h, float dot_HT, float dot_HB) const;

  inline float lambda(float cos_phi, float sin_phi, float tan_theta) const;

  inline float geometricTerm(
      mrf::geom::LocalFrame const &lf,
      float                        dot_HV, /* cos_theta_d */
      mrf::math::Vec3f const &     view_dir,
      mrf::math::Vec3f const &     light_dir,
      float                        dot_NL,
      float                        dot_NV) const;

  /**
   * @brief      Generate a new Half Vector direction from incoming view direction
   *
   * @param[in]  lf      Local Frame Description in Wordl Coordinates
   * @param[in]  in_dir  The ray direction (view direction = - ray.dir)
   * @param[in]  e1      First random number
   * @param[in]  e2      Second random number
   *
   * According to Walter EGSR2007, the generated half direction pdf is
   *  pdf  = D(h) * (Normal dot H)
   *
   * @return     Sampled Half Direction
   */
  inline mrf::math::Vec3f sampleHalfDir(mrf::geom::LocalFrame const &lf, float e1, float e2) const;



  // BRDF Interface for Path-Tracers
  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;


  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;

};   //end of Class def

}   // namespace materials
}   // namespace mrf
