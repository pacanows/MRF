/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 * GGX Microfacet BRDF
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/brdf.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/geometry/local_frame.hpp>
#include <mrf_core/materials/fresnel.hpp>

//STL
#include <string>
#include <complex>


namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *GGX = "ggx";   // GGX from Walter et al. 2007
};                                // namespace BRDFTypes

/**
 * @brief      The GGX BRDF as proposed by Walter et al. [2007. EGSR]
 */
////template<class FRESNEL_EVALUATOR>
class MRF_PLUGIN_EXPORT GGX: public BRDF
{
private:
  /* Parameter controling the Shape of the Distribution */
  float             _alpha_g;
  float             _sqr_alpha_g;
  FresnelConductor *_fresnel;

public:
  GGX(std::string const &name);

  GGX(std::string const &  name,
      float                alpha_g,
      RADIANCE_TYPE const &eta = RADIANCE_TYPE(1.0),
      RADIANCE_TYPE const &k   = RADIANCE_TYPE(0.0));

  virtual ~GGX();

  virtual mrf::materials::IOR ior() const;
  virtual RADIANCE_TYPE       eta() const;
  virtual RADIANCE_TYPE       k() const;
  virtual float               alpha_g() const;

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;


  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;


  // Microfacet Interface methods
  float distribution(float cos_theta_h) const;
  float distribution(float cos_theta_h, float sqr_sin_phi_h, float sqr_cos_phi_h) const;

  float G1(float dot_NK) const;
  float geometricTerm(float dot_HV, float dot_NV, float dot_NL) const;

  // Full-Interface to be compatible with anisotropic models
  // This functions calls the previous one
  float geometricTerm(
      mrf::geom::LocalFrame const &lf,
      float                        dot_HV,
      mrf::math::Vec3f const &     view_dir,
      mrf::math::Vec3f const &     light_dir,
      float                        dot_NL,
      float                        dot_NV) const;

  RADIANCE_TYPE fresnelTerm(float dot_HV) const;

  /**
   * @brief      Generate a new Half Vector direction from incoming view direction
   *
   * @param[in]  lf      Local Frame Description in Wordl Coordinates
   * @param[in]  in_dir  The ray direction (view direction = - ray.dir)
   * @param[in]  e1      First random number
   * @param[in]  e2      Second random number
   *
   * According to Walter EGSR2007, the generated half direction pdf is
   *  pdf  = D(h) * (Normal dot H)
   *
   * @return     Sampled Half Direction
   */
  mrf::math::Vec3f sampleHalfDir(mrf::geom::LocalFrame const &lf, float e1, float e2) const;

  void setAlpha(float alpha_g);

  void setFresnel(RADIANCE_TYPE const &eta, RADIANCE_TYPE const &k);
};

}   // namespace materials
}   // namespace mrf
