/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 * GGX Microfacet BRDF
 *
 **/

#include <mrf_plugins/materials/microfacet/conductor/aniso_ggx.hpp>


namespace mrf
{
namespace materials
{
//template<class FRESNEL_EVALUATOR>
AnisoGGX::AnisoGGX(
    std::string const &  name,
    float                alpha_x,
    float                alpha_y,
    RADIANCE_TYPE const &eta,
    RADIANCE_TYPE const &k)
  : BRDF(name, BRDFTypes::ANISOGGX)
  , _alpha_xy(alpha_x, alpha_y)
  , _sqr_alpha_xy(alpha_x * alpha_x, alpha_y * alpha_y)
  , _fresnel(new FresnelConductor(eta, k))
{
  /*
  std::cout << __FILE__ << "  " << __LINE__ << std::endl;
  std::cout << " DEBUGING " << std::endl;
  std::cout << "alpha_xy " << _alpha_xy << std::endl;
  std::cout << " Fresnel = " << _fresnel->eta() << " " << _fresnel->kappa() << " _sqr_alpha_xy = " << _sqr_alpha_xy << std::endl;
  */
}

//template<class FRESNEL_EVALUATOR>
AnisoGGX::~AnisoGGX() {}


//template<class FRESNEL_EVALUATOR>
RADIANCE_TYPE AnisoGGX::eta() const
{
  return _fresnel->eta();
}

//template<class FRESNEL_EVALUATOR>
RADIANCE_TYPE AnisoGGX::k() const
{
  return _fresnel->kappa();
}

//template<class FRESNEL_EVALUATOR>
float AnisoGGX::alpha_x() const
{
  return _alpha_xy.x();
}

//template<class FRESNEL_EVALUATOR>
float AnisoGGX::alpha_y() const
{
  return _alpha_xy.y();
}

/**
 * @brief      Evaluate the Anisotropic version of GGX
 *
 * @param[in]  cos_theta_h        The cosine theta h
 * @param[in]  dot_HT             The dot between the half-vector and the surface tangent
 * @param[in]  dot_HB             The dot between the half-vector and the surface bi-tangent
 *
 * @tparam     FRESNEL_EVALUATOR  Functor to evalute the Fresnel Term
 *
 * @return     Return the evluation of the distribution term of Anisotropic GGX
 */
//template<class FRESNEL_EVALUATOR>
float AnisoGGX::distribution(float cos_theta_h, float sqr_cos_phi_h, float sqr_sin_phi_h) const
{
  using namespace mrf::math;

  float const sqr_cos_theta_h = cos_theta_h * cos_theta_h;
  float const sqr_tan_th      = (1.f - sqr_cos_theta_h) / sqr_cos_theta_h;
  assert(std::isfinite(sqr_tan_th));


#ifdef MRF_ANISOTROPICGGX_DEBUG
  if (!(mrf::math::equals(sqr_sin_phi_h + sqr_cos_phi_h, 1.f, 0.1f)))
  {
    std::cout << " cos_theta_h = " << cos_theta_h << " sqr_sin_phi_h = " << sqr_sin_phi_h
              << " sqr_cos_phi_h = " << sqr_cos_phi_h << std::endl;
    std::cout << " sqr_sin_phi_h + sqr_cos_phi_h = " << sqr_sin_phi_h + sqr_cos_phi_h << std::endl;
    assert(0);
  }
#endif


  float const dot_NH_4 = sqr_cos_theta_h * sqr_cos_theta_h;

  float const factor
      = std::pow(1.f + sqr_tan_th * (sqr_cos_phi_h / _sqr_alpha_xy.x() + sqr_sin_phi_h / _sqr_alpha_xy.y()), 2.f);
  assert(std::isfinite(factor));

  float const D = 1.0f / (_alpha_xy.x() * _alpha_xy.y() * static_cast<float>(Math::PI) * dot_NH_4 * factor);

  return D;
}


// Either View or Light directions
//template<class FRESNEL_EVALUATOR>
float AnisoGGX::lambda(float cos_phi, float sin_phi, float tan_theta) const
{
  float const alpha_0 = std::sqrt(cos_phi * cos_phi * _sqr_alpha_xy.x() + sin_phi * sin_phi * _sqr_alpha_xy.y());
  assert(std::isfinite(alpha_0));

  float const sqr_alpha_0   = alpha_0 * alpha_0;
  float const sqr_tan_theta = tan_theta;

  float const one_plus_sqr_a0_tan_theta = std::sqrt(1.f + (sqr_alpha_0 * sqr_tan_theta));

  return (-1.f + one_plus_sqr_a0_tan_theta) * 0.5f;
}


//template<class FRESNEL_EVALUATOR>
float AnisoGGX::geometricTerm(
    mrf::geom::LocalFrame const &lf,
    float                        dot_HV, /* cos_theta_d */
    mrf::math::Vec3f const &     view_dir,
    mrf::math::Vec3f const &     light_dir,
    float                        dot_NL,
    float                        dot_NV) const
{
  using namespace mrf::math;

  /* Checking Heavy-Side function */
  if (dot_HV <= 0.0f)
  {
    return 0.0f;
  }

  Vec3f const local_view  = lf.localDir(view_dir);
  Vec3f const local_light = lf.localDir(light_dir);

  double const phi_light = std::atan2(local_light.z(), local_light.x());
  double const phi_view  = std::atan2(local_view.z(), local_view.x());

  assert(std::isfinite(phi_light));
  assert(std::isfinite(phi_view));

  float const sin_theta_light = std::sqrt(1.f - dot_NL * dot_NL);
  float const sin_theta_view  = std::sqrt(1.f - dot_NV * dot_NV);


  assert(std::isfinite(sin_theta_light));
  assert(std::isfinite(sin_theta_view));

  double const tan_theta_i = sin_theta_light / dot_NL;
  double const tan_theta_o = sin_theta_view / dot_NV;


#ifdef MRF_ANISOTROPICGGX_DEBUG
  if (!std::isfinite(tan_theta_i) || !std::isfinite(tan_theta_o))
  {
    std::cout << " lf = " << lf << std::endl;
    std::cout << " view_dir = " << view_dir << std::endl;
    std::cout << " light_dir = " << light_dir << std::endl;
    std::cout << " local_view = " << local_view << std::endl;
    std::cout << " local_light = " << local_light << std::endl;
    std::cout << "  local_light dot lf.normal() = " << local_light.dot(lf.normal()) << std::endl;
    std::cout << " sin_theta_light =  " << sin_theta_light << " dot_NL " << dot_NL;
    std::cout << " sin_theta_view =  " << sin_theta_view << " dot_NV " << dot_NV;
    assert(0);
    assert(std::isfinite(tan_theta_i));
    assert(std::isfinite(tan_theta_o));
  }
#endif


  float const lambda_light = lambda(
      static_cast<float>(std::cos(phi_light)),
      static_cast<float>(std::sin(phi_light)),
      static_cast<float>(tan_theta_i));
  assert(std::isfinite(lambda_light));

  float const G_light = (1.f / (1.f + lambda_light));
  assert(std::isfinite(G_light));

  float const lambda_view = lambda(
      static_cast<float>(std::cos(phi_view)),
      static_cast<float>(std::sin(phi_view)),
      static_cast<float>(tan_theta_o));
  assert(std::isfinite(lambda_view));

  float const G_view = (1.f / (1.f + lambda_view));
  assert(std::isfinite(G_view));



  return G_view * G_light;

  //
  // Un-correlated version see Heitz 2014.
  //
  //return  1./ ( 1.f + lambda_view + lambda_light);

  // float const test1 = G_view * G_light;


  // if( test1 != test2 )
  // {
  //   std::cout << " test1 = " << test1 << std::endl;
  //   std::cout << " test2 = " << test2 << std::endl;
  //   assert(0);
  // }
}


//template<class FRESNEL_EVALUATOR>
RADIANCE_TYPE AnisoGGX::coloredBRDFValue(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const &     out_dir) const
{
  using namespace mrf::math;

  Vec3f const V = -in_dir;

  float const dot_NV = clamp(lf.normal().dot(V), 0.0f, 1.0f);
  float const dot_NL = clamp(lf.normal().dot(out_dir), 0.0f, 1.0f);

  if (dot_NV <= 0.0 || dot_NL <= 0.0)   // Early finish
  {
    return RADIANCE_TYPE(0.0);
  }

  Vec3f const h           = halfVector(V, out_dir, lf.normal());
  float const cos_theta_d = clamp(V.dot(h), 0.0f, 1.0f);
  float const cos_theta_h = clamp(lf.normal().dot(h), 0.0f, 1.0f);

  if (cos_theta_h <= 0.0)
  {
    return RADIANCE_TYPE(0.0);
  }


  Vec3f const H = lf.localDir(h);

  float const Phi_H     = std::atan2(H.z(), H.x());
  float const cos_phi_H = std::cos(Phi_H);
  float const sin_phi_H = std::sin(Phi_H);

  // #ifdef MRF_ANISOTROPICGGX_DEBUG
  // float const dot_HT = h.dot( lf.tangent()  );
  // float const dot_HB = h.dot( lf.binormal() );
  // std::cout << " cos_phi_H = " << cos_phi_H << " sin_phi_H = " << sin_phi_H << std::endl;
  // std::cout << " cos_phi_H^2 + sin_phi_H^2 = " << cos_phi_H*cos_phi_H + sin_phi_H*sin_phi_H << std::endl;
  // std::cout << "dot_HT = " << dot_HT ;
  // std::cout << "dot_HB = " << dot_HB << std::endl;
  // #endif


  float const D = distribution(cos_theta_h, cos_phi_H * cos_phi_H, sin_phi_H * sin_phi_H);
  float const G = geometricTerm(lf, cos_theta_d, V, out_dir, dot_NL, dot_NV);

  RADIANCE_TYPE const F = _fresnel->evalFromCosThetaDiff(cos_theta_d);

#ifdef MRF_RENDERING_MODE_SPECTRAL
#else
  assert(F > EPSILON);
  assert(F < (1.0f + EPSILON));
#endif

  assert(std::isfinite(D));
  assert(std::isfinite(G));

  assert(G >= 0.0f);
  assert(G < 1.f + EPSILON);


  return F * (G * D / (4.f * dot_NL * dot_NV));
}

//template<class FRESNEL_EVALUATOR>
mrf::math::Vec3f AnisoGGX::sampleHalfDir(mrf::geom::LocalFrame const &lf, float e1, float e2) const
{
  using namespace mrf::math;

  float const u1 = std::sqrt(e1) / std::sqrt(1.f - e1);
  float const u2 = static_cast<float>(Math::TWO_PI) * e2;

  float const x_h = _alpha_xy.x() * u1 * std::cos(u2);
  float const y_h = _alpha_xy.y() * u1 * std::sin(u2);

  Vec3f h(x_h, y_h, 1.f);
  h /= std::sqrt(x_h * x_h + y_h * y_h + 1.f);


  Vec3f const H_global_frame = h.x() * lf.tangent() + h.y() * lf.binormal() + h.z() * lf.normal();

  return H_global_frame;
}



//template<class FRESNEL_EVALUATOR>
ScatterEvent::SCATTER_EVT AnisoGGX::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    float                        e1,
    float                        e2,
    mrf::math::Vec3f &           outgoing_dir,
    RADIANCE_TYPE &              brdf_corrected_by_pdf) const
{
  using namespace mrf::math;

  //TODO:? Check View dir = V dot N
  Vec3f const H_global_frame = sampleHalfDir(lf, e1, e2);

  float const dot_NH = clamp(H_global_frame.dot(lf.normal()), 0.0f, 1.0f);

  if (dot_NH <= 0.0f)
  {
    brdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
    return ScatterEvent::ABSORPTION;
  }

  float const dot_HV = clamp(H_global_frame.dot(-in_dir), 0.0f, 1.0f);   // = cos_theta_d
  outgoing_dir       = in_dir + 2.0f * dot_HV * H_global_frame;



  RADIANCE_TYPE const F = _fresnel->evalFromCosThetaDiff(dot_HV);


  float const dot_NL = clamp(outgoing_dir.dot(lf.normal()), 0.0f, 1.0f);
  float const dot_NV = clamp((-in_dir).dot(lf.normal()), 0.0f, 1.0f);

  if (dot_NL <= 0.0f || dot_NV <= 0.0f)
  {
    brdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
    return ScatterEvent::ABSORPTION;
  }



  float const G = geometricTerm(lf, dot_HV, -in_dir, outgoing_dir, dot_NL, dot_NV);

  brdf_corrected_by_pdf = F * G * dot_HV / (dot_NL * dot_NV * dot_NH);
  return ScatterEvent::SPECULAR_REFL;
}

}   // namespace materials
}   // namespace mrf
