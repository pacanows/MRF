/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 * GGX Microfacet BRDF
 *
 **/

#include <mrf_plugins/materials/microfacet/conductor/ggx.hpp>

//STL
#include <string>
#include <complex>


namespace mrf
{
namespace materials
{
void GGX::setAlpha(float alpha_g)
{
  _alpha_g     = alpha_g;
  _sqr_alpha_g = alpha_g * alpha_g;
}

void GGX::setFresnel(RADIANCE_TYPE const &eta, RADIANCE_TYPE const &k)
{
  if (_fresnel)
  {
    _fresnel->setEta(eta);
    _fresnel->setK(k);
  }
  else
    _fresnel = new FresnelConductor(eta, k);
}

//template<class FRESNEL_EVALUATOR>
GGX::GGX(std::string const &name, float alpha_g, RADIANCE_TYPE const &eta, RADIANCE_TYPE const &k)
  : BRDF(name, BRDFTypes::GGX)
  , _alpha_g(alpha_g)
  , _sqr_alpha_g(_alpha_g * _alpha_g)
  , _fresnel(new FresnelConductor(eta, k))
{}

//template<class FRESNEL_EVALUATOR>
GGX::GGX(std::string const &name)
  : BRDF(name, BRDFTypes::GGX)
  , _alpha_g(0.f)
  , _sqr_alpha_g(0.f)
  , _fresnel(new FresnelConductor(RADIANCE_TYPE(1.f), RADIANCE_TYPE(0.f)))
{}

//template<class FRESNEL_EVALUATOR>
GGX::~GGX()
{
  delete _fresnel;
}


//template<class FRESNEL_EVALUATOR>
mrf::materials::IOR GGX::ior() const
{
  return IOR(_fresnel->eta(), _fresnel->kappa());
}

//template<class FRESNEL_EVALUATOR>
RADIANCE_TYPE GGX::eta() const
{
  return _fresnel->eta();
}

//template<class FRESNEL_EVALUATOR>
RADIANCE_TYPE GGX::k() const
{
  return _fresnel->kappa();
}

//template<class FRESNEL_EVALUATOR>
float GGX::alpha_g() const
{
  return _alpha_g;
}

//template<class FRESNEL_EVALUATOR>
float GGX::distribution(float cos_theta_h) const
{
  using namespace mrf::math;

  float const sqr_cos_theta_h = cos_theta_h * cos_theta_h;
  //float const cos_theta_h_pow_4 = sqr_cos_theta_h * sqr_cos_theta_h;
  //float const sqr_tan_theta_H  = (1. - sqr_cos_theta_h) / sqr_cos_theta_h;
  //float const D =  _sqr_alpha_g / ( Math::PI * cos_theta_h_pow_4 * pow(_sqr_alpha_g + sqr_tan_theta_H, 2) ) ;

  float const D
      = _sqr_alpha_g / (static_cast<float>(Math::PI) * pow(sqr_cos_theta_h * (_sqr_alpha_g - 1.f) + 1.f, 2.f));
  //BRDFEXplorer : return alphaG*alphaG / (PI * sqr(NdotH*NdotH*(alphaG*alphaG-1) + 1));

  return D;
}


//template<class FRESNEL_EVALUATOR>
float GGX::distribution(float cos_theta_h, float /*sqr_sin_phi_h*/, float /*sqr_cos_phi_h*/) const
{
  return GGX::distribution(cos_theta_h);
}


// K is either View or Light directions
//template<class FRESNEL_EVALUATOR>
float GGX::G1(float dot_NK) const
{
  float const sqr_tan_tK = (1.f - dot_NK * dot_NK) / (dot_NK * dot_NK);
  float const G          = 2.f / (1.f + sqrtf(1.f + _sqr_alpha_g * sqr_tan_tK));

  return G;
}


/**
 * @brief      Evaluate the Geometric Term of isotropic Microfacet BRDF
 *
 * @param[in]  dot_HK             The dot product between H and K where K is light or view directions
 * @param[in]  dot_NV             The dot product between normal N and View dir V
 * @param[in]  dot_NL             The dot product between normal N and Light dir L
 *
 * @tparam     FRESNEL_EVALUATOR  functor used to evaluate the Fresnel component for the GGX BRDF
 *
 * @return     the value of the geometric term in [0,1]
 */
//template<class FRESNEL_EVALUATOR>
float GGX::geometricTerm(float dot_HK, float dot_NV, float dot_NL) const
{
  float const geometric_term = (dot_HK <= 0.0f) ? (0.0f) : (G1(dot_NV) * G1(dot_NL));


  assert(geometric_term >= 0.0f);

  if (geometric_term > 1.0f)
  {
    std::cout << " geometric_term = " << geometric_term << std::endl;
    std::cout << " dot_HK = " << dot_HK << " dot-NV = " << dot_NV << "  dot_NL = " << dot_NL << std::endl;
  }

  assert(geometric_term <= 1.0f);

  return geometric_term;
}

//template<class FRESNEL_EVALUATOR>
float GGX::geometricTerm(
    mrf::geom::LocalFrame const & /*lf*/,
    float dot_HV,
    mrf::math::Vec3f const & /*view_dir*/,
    mrf::math::Vec3f const & /*light_dir*/,
    float dot_NL,
    float dot_NV) const
{
  return geometricTerm(dot_HV, dot_NV, dot_NL);
}

//template<class FRESNEL_EVALUATOR>
RADIANCE_TYPE GGX::fresnelTerm(float dot_HV) const
{
  return _fresnel->evalFromCosThetaDiff(dot_HV);
}


//template<class FRESNEL_EVALUATOR>
RADIANCE_TYPE GGX::coloredBRDFValue(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    mrf::math::Vec3f const &     out_dir) const
{
  using namespace mrf::math;

  Vec3f const V = -in_dir;

  float const dot_NV = clamp(lf.normal().dot(V), 0.0f, 1.0f);
  float const dot_NL = clamp(lf.normal().dot(out_dir), 0.0f, 1.0f);

  if (dot_NV <= 0.0 || dot_NL <= 0.0)
  {
    return RADIANCE_TYPE(0.0);
  }

  Vec3f const h           = halfVector(V, out_dir, lf.normal());
  float const cos_theta_d = clamp(V.dot(h), 0.0f, 1.0f);
  float const cos_theta_h = clamp(lf.normal().dot(h), 0.0f, 1.0f);

  if (cos_theta_h <= 0.0)
  {
    return RADIANCE_TYPE(0.0);
  }


  RADIANCE_TYPE const F = _fresnel->evalFromCosThetaDiff(cos_theta_d);

  // assert( F > EPSILON  );
  // assert( F <  (1.0f + EPSILON)  );


  float const D = distribution(cos_theta_h);

  float const G = geometricTerm(cos_theta_d, dot_NV, dot_NL);
  assert(G > -EPSILON);
  assert(G < (1.0f + EPSILON));

  return F * D * G / (4 * dot_NL * dot_NV);
}


//template<class FRESNEL_EVALUATOR>
ScatterEvent::SCATTER_EVT GGX::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const &lf,
    mrf::math::Vec3f const &     in_dir,
    float                        e1,
    float                        e2,
    mrf::math::Vec3f &           outgoing_dir,
    RADIANCE_TYPE &              brdf_corrected_by_pdf) const
{
  using namespace mrf::math;
  using namespace std;

  float const theta_H = std::atan(_alpha_g * sqrt(e1) / sqrt(1 - e1));
  float const phi_H   = static_cast<float>(Math::TWO_PI) * e2;

  //Generate H locally then globally
  float const cos_theta_H = std::cos(theta_H);
  float const sin_theta_H = std::sin(theta_H);

  float const cos_pH = std::cos(phi_H);
  float const sin_pH = std::sin(phi_H);

  //H : Back to World
  Vec3f const h
      = cos_pH * sin_theta_H * lf.tangent() + sin_pH * sin_theta_H * lf.binormal() + cos_theta_H * lf.normal();


  //float const dotinh = std::max( h.dot( - in_dir ), 0.0f) ;
  float const dot_HV = clamp(h.dot(-in_dir), 0.0f, 1.0f);   // = cos_theta_d

  float const dot_NH = clamp(h.dot(lf.normal()), 0.0f, 1.0f);
  if (dot_NH <= 0.0)
  {
    brdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
    return ScatterEvent::ABSORPTION;
  }

  //NEW OUTGOING DIR:  in World coordinates
  outgoing_dir = in_dir + 2.0f * dot_HV * h;



  RADIANCE_TYPE const F = _fresnel->evalFromCosThetaDiff(dot_HV);
  // // WHAT ABOUT ABSORPTION ? if FRESNEL
  // if( e2 > F.avgCmp() )
  // {
  //   return ScatterEvent::ABSORPTION; //SHOULD BE A TRANSMISSION
  // }

  float const dot_NL = clamp(outgoing_dir.dot(lf.normal()), 0.0f, 1.0f);
  float const dot_NV = clamp((-in_dir).dot(lf.normal()), 0.0f, 1.0f);

  if (dot_NL <= 0.0f || dot_NV <= 0.0f)
  {
    brdf_corrected_by_pdf = RADIANCE_TYPE(0.0);
    return ScatterEvent::ABSORPTION;
  }

  //float const dot_HL = clamp( h.dot(outgoing_dir), 0.0f, 1.0f );
  // if( !( std::abs(dot_HV - dot_HL) <= 0.1 ) )
  // {
  //   std::cout << " dot_HL = " << dot_HL << " dot_HV = " << dot_HV << std::endl;
  //   assert(0);
  // }

  float const G = geometricTerm(dot_HV, dot_NV, dot_NL);

  // WHy the 4  ?  Should cancel .... need to check
  // Where is (dot_NL) (dot_NV)
  //brdf_corrected_by_pdf = F * G * 4 * dot_HV / dot_NH;

  brdf_corrected_by_pdf = F * G * dot_HV / (dot_NV * dot_NL * dot_NH);


  return ScatterEvent::SPECULAR_REFL;
}

//template<class FRESNEL_EVALUATOR>
mrf::math::Vec3f GGX::sampleHalfDir(mrf::geom::LocalFrame const &lf, float e1, float e2) const
{
  using namespace mrf::math;

  //float const theta_H =  std::atan( _alpha_g  * sqrt(e1) / sqrt(1. - e1) ) ;
  float const theta_H = std::atan2(_alpha_g * std::sqrt(e1), std::sqrt(1.f - e1));
  float const phi_H   = static_cast<float>(Math::TWO_PI) * e2;

  //Generate H locally then globally
  float const cos_theta_H = clamp(std::cos(theta_H), 0.0f, 1.0f);
  float const sin_theta_H = clamp(std::sin(theta_H), 0.0f, 1.0f);

  float const cos_pH = std::cos(phi_H);
  float const sin_pH = std::sin(phi_H);

  //H : Back to World
  Vec3f const h
      = cos_pH * sin_theta_H * lf.tangent() + sin_pH * sin_theta_H * lf.binormal() + cos_theta_H * lf.normal();

  return h;
}

}   // namespace materials

}   // namespace mrf
