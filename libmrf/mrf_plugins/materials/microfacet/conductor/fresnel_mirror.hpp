/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 * Perfect Mirror BRDF
 *
 **/

#pragma once

#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/brdf.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_plugins/materials/microfacet/conductor/perfect_mirror.hpp>

namespace mrf
{
namespace materials
{
namespace BRDFTypes
{
static char const *FRESNEL_MIRROR = "FresnelMirror";   // Mirror Material but with an absorption coefficient
};                                                     // namespace BRDFTypes


class MRF_PLUGIN_EXPORT FresnelMirror: public PerfectMirror
{
public:
  FresnelMirror(std::string const &name);
  FresnelMirror(std::string const &name, mrf::materials::COLOR const &eta, mrf::materials::COLOR const &kappa);

  virtual ~FresnelMirror();

  mrf::materials::COLOR const &eta() const;
  mrf::materials::COLOR const &kappa() const;

  void setFresnel(mrf::materials::COLOR eta, mrf::materials::COLOR kappa)
  {
    _eta   = eta;
    _kappa = kappa;
  }
  void setEta(mrf::materials::COLOR eta) { _eta = eta; }
  void setKappa(mrf::materials::COLOR kappa) { _kappa = kappa; }

  // virtual float specularAlbedo() const;

  // virtual float nonDiffuseAlbedo() const;

  // virtual RADIANCE_TYPE coloredBRDFValue(
  //     mrf::geom::LocalFrame const &lf,
  //     mrf::math::Vec3f const &     in_dir,
  //     mrf::math::Vec3f const &     out_dir) const;

  // ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
  //     mrf::geom::LocalFrame const &lf,
  //     mrf::math::Vec3f const &     in_dir,
  //     float                        e1,
  //     float                        e2,
  //     mrf::math::Vec3f &           outgoing_dir,
  //     RADIANCE_TYPE &              brdf_corrected_by_pdf) const;

protected:
  mrf::materials::COLOR _eta;
  mrf::materials::COLOR _kappa;
};


}   // namespace materials

}   // namespace mrf