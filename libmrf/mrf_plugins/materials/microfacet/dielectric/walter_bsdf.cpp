/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 * Walter Microfacet BSDF
 *
 **/

#include <mrf_plugins/materials/microfacet/dielectric/walter_bsdf.hpp>

namespace mrf
{
namespace materials
{}   // namespace materials
}   // namespace mrf