/*
 *
 * author : David Murray @ institutoptique.fr
 * Copyright CNRS 2021
 *
 *
 **/

#include <mrf_plugins/lights/optix_light_plugins.hpp>

#include <mrf_core/util/precision_timer.hpp>

#include <mrf_core/geometry/quad.hpp>
#include <mrf_core/geometry/direction.hpp>

#include <mrf_optix/optix_renderer.hpp>
#include <mrf_optix/optix_util.hpp>

#include <optixu/optixu_math_namespace.h>
#include <optixu/optixpp_namespace.h>

#include <iostream>


namespace mrf
{
namespace optix_backend
{
////////////////////////////////////////////////////////////////////////////
/////////////////////LIGHTS/////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////


OptixQuadLight::OptixQuadLight(mrf::lighting::Light *light): OptixBaseLight(light)
{
  mrf::geom::RQuad *rquad = dynamic_cast<mrf::geom::RQuad *>(light->geometry());

  auto vertices = rquad->getVertices();

  _corner = mrfToOptix(vertices[0]);
  _v1     = mrfToOptix(vertices[3] - vertices[0]);
  _v2     = mrfToOptix(vertices[1] - vertices[0]);
  _normal = optix::normalize(optix::cross(_v1, _v2));
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
//TODO we might be missing something here
void OptixQuadLight::updateLight(std::vector<uint> const &)
#else
void OptixQuadLight::updateLight()
#endif
{
  float                    radiance       = _light->emit_pointer()->getRadiance();
  std::vector<std::string> variable_names = {"emission_color"};

  std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
  variable_values.push_back(_light->emit_pointer()->getColor() * radiance);
};

optix::GeometryInstance OptixQuadLight::getGeometryInstance(
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
{
  if (!_has_geometry)
  {
    optix::Geometry parallelogram = context->createGeometry();
    //const char *    ptx           = getPtxString("parallelogram.cu", cuda_compile_options);
    //parallelogram->setIntersectionProgram(context->createProgramFromPTXString(ptx, "intersect"));
    //parallelogram->setBoundingBoxProgram(context->createProgramFromPTXString(ptx, "bounds"));

    OptixKernel prg_parallelogram("parallelogram.cu", cuda_compile_options, ptx_cfg);
    prg_parallelogram.compile(context);
    parallelogram->setBoundingBoxProgram(prg_parallelogram.createProgram(context, "bounds"));
    parallelogram->setIntersectionProgram(prg_parallelogram.createProgram(context, "intersect"));


    parallelogram->setPrimitiveCount(1u);

    optix::float3 normal = optix::normalize(optix::cross(_v1, _v2));
    float         d      = optix::dot(normal, _corner);
    optix::float4 plane  = make_float4(normal, d);

    optix::float3 v1 = _v1 / optix::dot(_v1, _v1);
    optix::float3 v2 = _v2 / optix::dot(_v2, _v2);

    parallelogram["plane"]->setFloat(plane);
    parallelogram["anchor"]->setFloat(_corner);
    parallelogram["v1"]->setFloat(v1);
    parallelogram["v2"]->setFloat(v2);

    _gi = context->createGeometryInstance();
    _gi["registered_as_light"]->setInt(1);
    _gi->setGeometry(parallelogram);
  }
  return _gi;
}

OptixSphereLight::OptixSphereLight(mrf::lighting::Light *light): OptixBaseLight(light)
{
  mrf::geom::Sphere *sphere = dynamic_cast<mrf::geom::Sphere *>(light->geometry());

  _position = mrfToOptix(sphere->position());
  _radius   = sphere->radius();
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixSphereLight::updateLight(std::vector<uint> const &)
#else
void OptixSphereLight::updateLight()
#endif
{
  float                    radiance       = _light->emit_pointer()->getRadiance();
  std::vector<std::string> variable_names = {"emission_color"};

  std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
  variable_values.push_back(_light->emit_pointer()->getColor() * radiance);
};

optix::GeometryInstance OptixSphereLight::getGeometryInstance(
    optix::Context &                context,
    std::vector<std::string> const &cuda_compile_options,
    PTXConfig                       ptx_cfg)
{
  if (!_has_geometry)
  {
    optix::Geometry sphere = context->createGeometry();
    //const char *    ptx    = getPtxString("sphere.cu", cuda_compile_options);
    //sphere->setIntersectionProgram(context->createProgramFromPTXString(ptx, "intersect"));
    //sphere->setBoundingBoxProgram(context->createProgramFromPTXString(ptx, "bounds"));

    OptixKernel prg_sphere("sphere.cu", cuda_compile_options, ptx_cfg);
    prg_sphere.compile(context);
    sphere->setBoundingBoxProgram(prg_sphere.createProgram(context, "bounds"));
    sphere->setIntersectionProgram(prg_sphere.createProgram(context, "intersect"));

    sphere->setPrimitiveCount(1u);

    sphere["center"]->setFloat(_position);
    sphere["radius"]->setFloat(_radius);

    _gi = context->createGeometryInstance();
    _gi["registered_as_light"]->setInt(1);
    _gi->setGeometry(sphere);
    _has_geometry = true;
  }
  return _gi;
}


OptixDirectionnalLight::OptixDirectionnalLight(mrf::lighting::Light *light): OptixBaseLight(light)
{
  _direction = mrfToOptix(light->emit_pointer()->getDirection());
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void OptixDirectionnalLight::updateLight(std::vector<uint> const &)
#else
void OptixDirectionnalLight::updateLight()
#endif
{
  float                    radiance       = _light->emit_pointer()->getRadiance();
  std::vector<std::string> variable_names = {"emission_color"};

  std::vector<mrf::materials::RADIANCE_TYPE> variable_values;
  variable_values.push_back(_light->emit_pointer()->getColor() * radiance);
};

optix::GeometryInstance
OptixDirectionnalLight::getGeometryInstance(optix::Context &, std::vector<std::string> const &, PTXConfig)
{
  _has_geometry = false;
  return optix::GeometryInstance();
}




OptixBaseLight *QuadLightPlugin::createFromMRF(mrf::lighting::Light *mrf_light)
{
  auto pos = mrf_light->center();

  return new OptixQuadLight(mrf_light);
}

OptixBaseLight *SphereLightPlugin::createFromMRF(mrf::lighting::Light *mrf_light)
{
  auto pos = mrf_light->center();

  return new OptixSphereLight(mrf_light);
}

OptixBaseLight *DirectionnalLightPlugin::createFromMRF(mrf::lighting::Light *mrf_light)
{
  auto pos = mrf_light->center();

  return new OptixDirectionnalLight(mrf_light);
}


}   // namespace optix_backend
}   // namespace mrf
