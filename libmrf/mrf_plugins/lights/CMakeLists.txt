  
if(${USE_OPTIX})
  set(BCK_SOURCES ${BCK_SOURCES}  
    ${CMAKE_CURRENT_LIST_DIR}/optix_light_plugins.cpp
    PARENT_SCOPE)
  set(BCK_HEADERS ${BCK_HEADERS}
    ${CMAKE_CURRENT_LIST_DIR}/optix_light_plugins.hpp
    PARENT_SCOPE)
endif()