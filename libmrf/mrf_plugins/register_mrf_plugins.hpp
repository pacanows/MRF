#include <mrf_plugin_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_plugins/materials/mrf_material_parsers.hpp>

namespace mrf
{
namespace io
{
template<typename T, typename O>
static void
register_plugin(std::vector<std::string> &id_fct_map, std::map<std::string, BasePluginParser<T> *> &parse_fct_map)
{
  id_fct_map.push_back(O::getID());
  parse_fct_map[O::getID()] = O::create();
}

//Static function: vector to list all registered material ID
static void
register_all_mat(std::vector<std::string> &ids, std::map<std::string, BasePluginParser<mrf::materials::UMat> *> &fcts)
{
  register_plugin<mrf::materials::UMat, mrf::io::LambertParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::CheckerboardParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::PhongParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::PrincipledParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::GGXParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::WalterBSDFParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::UTIAParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::UTIAIndexedParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::MeasuredIsotropicParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::FlatParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::NormalParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::TangentParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::UVParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::ShadowCatcherParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::PerfectMirrorParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::FresnelMirrorParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::FresnelGlassParser>(ids, fcts);

  register_plugin<mrf::materials::UMat, mrf::io::ConicalEmittanceParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::DiffuseEmittanceParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::DiracEmittanceParser>(ids, fcts);
  register_plugin<mrf::materials::UMat, mrf::io::UniformEmittanceParser>(ids, fcts);

  //TODO: uncomment and adjust template according to your material.
  // register_plugin<mrf::materials::UMat, mrf::io::TemplateMatParser>(ids, fcts);
}

static void register_all_mat(MaterialPlugins &plugins)
{
  register_all_mat(plugins.ids, plugins.fcts);
}

}   // namespace io
}   // namespace mrf
