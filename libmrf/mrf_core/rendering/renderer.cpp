#include <mrf_core/rendering/renderer.hpp>
#include <mrf_core/image/image_formats.hpp>

namespace mrf
{
namespace rendering
{
Renderer::Renderer(mrf::rendering::Scene *mrf_scene, mrf::rendering::RENDERER_RNG rng_type, unsigned long long rng_seed)
  : _scene_imported(false)
  , _mrf_scene(mrf_scene)
  , _assets_update_required(true)
  , _current_camera(0)
  , _previous_camera(0)
  , _main_camera(nullptr)
  , _render_width(0)
  , _render_height(0)
  , _spatial_subdivision_factor(1)
  , _num_frame(1)
  , _spp(0)
  , _elapsed_time(0.f)
  , _save_variance(false)
  , _num_pass(0)
  , _total_num_passes(0)
  , _max_samples(0)
  , _num_samples_per_frame(1)
  , _seconds_between_log_advancement(10)
  , _max_rendering_time_seconds(0)
  , _max_path_length(10)
  , _continuous_sampling(false)
  , _override_width(false)
  , _override_height(false)
  , _override_samples(false)
  , _rng_type(rng_type)
  , _rng_seed(rng_seed)
  , _next_event_type(NONE)
  , _save_temporary_spectral_images(false)
  , _envmap_rotation_theta(0.f)
  , _envmap_rotation_phi(0.f)
  , _envmap_is(true)
  , _envmap(nullptr)
#ifdef MRF_RENDERING_MODE_SPECTRAL
  , _output_buffer_image(new mrf::image::EnviSpectralImage)
  , _variance_buffer_image(new mrf::image::EnviSpectralImage)
  , _nb_max_wavelengths_per_pass(1)
#else
  , _output_buffer_image(new mrf::image::EXRColorImage)
  , _variance_buffer_image(new mrf::image::EXRColorImage)
#endif
  , _is_interactive(false)
  , _exposure(0.f)
  , _gamma(2.2f)
{}


Renderer::~Renderer()
{
  if (_mrf_scene)
  {
    delete _mrf_scene;
    _mrf_scene = nullptr;
    delete _main_camera;
  }
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void Renderer::setSpectralWavelengths(std::vector<uint> const &waves)
{
  _wavelengths = waves;

  const uint delta = _wavelengths[1] - _wavelengths[0];

  // Sanity check: ensure we have equal spacing for each bins
  for (size_t i = 1; i < _wavelengths.size() - 1; i++)
  {
    const size_t currDelta = _wavelengths[i + 1] - _wavelengths[i];

    if (delta != currDelta)
    {
      mrf::gui::fb::Loger::getInstance()->fatal("Specified wavelengths are not equally spaced.");
    }
  }

  // Now, build the bounds for each bin
  // _wavelengths[i] is within [ _wavelengths_bounds[i] ; _wavelengths_bounds[i + 1] ]
  _wavelengths_bounds.resize(_wavelengths.size() + 1);

  for (size_t i = 0; i < _wavelengths.size(); i++)
  {
    _wavelengths_bounds[i] = _wavelengths[i] - delta / 2;
  }

  _wavelengths_bounds.back() = _wavelengths.back() + delta / 2;

#  ifdef MRF_RENDERING_DEBUG
  for (uint i = 1; i < _wavelengths.size(); i++)
  {
    std::cout << _wavelengths_bounds[i] << " ";
    std::cout << _wavelengths[i] << " ";
    std::cout << _wavelengths_bounds[i + 1] << std::endl;
  }
#  endif

  // We need to know which wavelengths have to be uploaded at each pass
  _total_num_passes = uint(ceilf(float(waves.size()) / float(_nb_max_wavelengths_per_pass)));

  _wavelengths_per_passes.resize(_total_num_passes);
  _wavelengths_per_passes_bounds.resize(_total_num_passes);

  for (size_t pass_idx = 0; pass_idx < _total_num_passes; pass_idx++)
  {
    // Determine the number of wavelengths for this specific pass
    size_t curr_pass_n_wl = 0;
    if ((pass_idx + 1) * _nb_max_wavelengths_per_pass < _wavelengths.size())
    {
      curr_pass_n_wl = _nb_max_wavelengths_per_pass;
    }
    else
    {
      curr_pass_n_wl = _wavelengths.size() - pass_idx * _nb_max_wavelengths_per_pass;
    }

    _wavelengths_per_passes[pass_idx].resize(curr_pass_n_wl);
    _wavelengths_per_passes_bounds[pass_idx].resize(curr_pass_n_wl + 1);

    for (size_t i = 0; i < curr_pass_n_wl; i++)
    {
      const size_t wl_idx = pass_idx * _nb_max_wavelengths_per_pass + i;

      _wavelengths_per_passes[pass_idx][i]        = _wavelengths[wl_idx];
      _wavelengths_per_passes_bounds[pass_idx][i] = _wavelengths_bounds[wl_idx];
    }

    _wavelengths_per_passes_bounds[pass_idx][curr_pass_n_wl]
        = _wavelengths_bounds[pass_idx * _nb_max_wavelengths_per_pass + curr_pass_n_wl];
  }
}
#endif   // MRF_RENDERING_MODE_SPECTRAL

std::string Renderer::getInfos() const
{
  std::stringstream ss;

  ss << "Rendered with Malia Rendering Engine" << std::endl;
  ss << "  Next event set to:          ";

  switch (_next_event_type)
  {
    case mrf::rendering::NEXT_EVENT::ONE:
      ss << "ONE";
      break;
    case mrf::rendering::NEXT_EVENT::NONE:
      ss << "NONE";
      break;
  }

  ss << std::endl;
  ss << "  RNG set to:                 ";

  switch (_rng_type)
  {
    case mrf::rendering::RENDERER_RNG::OPTIX:
      ss << "OPTIX";
      break;
    case mrf::rendering::RENDERER_RNG::SOBOL:
      ss << "SOBOL";
      break;
    case mrf::rendering::RENDERER_RNG::HALTON:
      ss << "HALTON";
      break;
  }

  ss << std::endl;
  ss << "  Envmap importance sampling: ";

  if (_envmap_is)
    ss << "ON";
  else
    ss << "OFF";

  ss << std::endl;
  ss << "  Max path length:            " << _max_path_length << std::endl;
  ss << "  Nb samples per frame:       " << _num_samples_per_frame << std::endl;
  ss << "  Nb rays per pixel:          " << _max_samples << std::endl;
#ifdef MRF_RENDERING_MODE_SPECTRAL
  ss << "  Nb wavelengths per pass:    " << _nb_max_wavelengths_per_pass << std::endl;
  ss << "  Nb wavelengths:             " << _wavelengths.size();
#endif

  return ss.str();
}

size_t Renderer::overrideSensorWidth(size_t width)
{
  if (width > 0)
  {
    _override_width        = true;
    const size_t tmp_width = _render_width;
    _render_width          = width;
    return tmp_width;
  }
  return _render_width;
}

size_t Renderer::overrideSensorHeight(size_t height)
{
  if (height > 0)
  {
    _override_height        = true;
    const size_t tmp_height = _render_height;
    _render_height          = height;
    return tmp_height;
  }
  return _render_height;
}

size_t Renderer::overrideSensorSamples(size_t samples)
{
  if (samples > 0)
  {
    _override_samples        = true;
    const size_t tmp_samples = _max_samples;
    _max_samples             = samples;
    return tmp_samples;
  }
  return _max_samples;
}

float Renderer::increaseRotationTheta(float delta)
{
  _envmap_rotation_theta += delta;

  if (_envmap_rotation_theta > 360.f) _envmap_rotation_theta -= 360.f;
  if (_envmap_rotation_theta < 0.f) _envmap_rotation_theta += 360.f;

  return _envmap_rotation_theta;
}

float Renderer::increaseRotationPhi(float delta)
{
  _envmap_rotation_phi += delta;

  if (_envmap_rotation_phi > 180.f) _envmap_rotation_phi -= 360.f;
  if (_envmap_rotation_phi < -180.f) _envmap_rotation_phi += 360.f;

  return _envmap_rotation_phi;
}

bool Renderer::saveImage(std::string const &filename, const std::string &additional_comments)
{
  if (_is_interactive) fetchImage();

  const std::string comments = getInfos() + "\n" + additional_comments;

  std::string extension;
  mrf::util::StringParsing::getFileExtension(filename.c_str(), extension);

  bool success_save = false;

  // 1. Save rendering
  success_save = saveImageBuffer(*_output_buffer_image, filename, comments);

  // 2. Save variance
  if (_save_variance)
  {
    std::string var_name;
    mrf::util::StringParsing::getFileNameWithoutExtension(filename, var_name);
    var_name += "_variance." + extension;

    success_save &= saveVarianceImage(var_name, _spp, _elapsed_time);
  }

  return success_save;
}


void Renderer::setCamera(const size_t &current_camera)
{
  _previous_camera = _current_camera;
  _current_camera  = current_camera;

  if (_main_camera) delete _main_camera;
  _main_camera = _all_cameras[current_camera]->copy();

  if (_all_cameras[_previous_camera]->getType() != _main_camera->getType())
  {
    updateBackendCamera();
  }
  updateCameraParameters();
  if (!_override_width) _render_width = _main_camera->sensor().resolution().x();
  if (!_override_height) _render_height = _main_camera->sensor().resolution().y();
  if (!_override_samples) _max_samples = _main_camera->sensorSampler().sqrtSPP();
  if (!_is_interactive && _scene_imported) resizeRenderTarget(_render_width, _render_height);
}


void Renderer::setCamera()
{
  setCamera(_current_camera);
  if (_scene_imported) resetRendering(true);
}


void Renderer::makeHeadStraight()
{
  _main_camera->up(mrf::math::Vec3f(0, 1, 0));
}

void Renderer::setCurrentCamera(const size_t &index)
{
  assert(index < _all_cameras.size());

  mrf::gui::fb::Loger::getInstance()->trace(" current camera index set to ", index);


  _current_camera = index;
  setCamera();
}

void Renderer::resetCamera()
{
  setCamera(_current_camera);
}


void Renderer::previousCamera()
{
  assert(_all_cameras.size() >= 1);

  size_t new_camera = _current_camera;
  if (_current_camera == 0)
    new_camera = _all_cameras.size() - 1;
  else
    new_camera--;

  mrf::gui::fb::Loger::getInstance()->info(" Camera Updated. Current Camera Number: ", new_camera);
  setCamera(new_camera);
  resetRendering(true);
}

void Renderer::nextCamera()
{
  assert(_all_cameras.size() >= 1);

  size_t new_camera = _current_camera + 1;

  if (new_camera == _all_cameras.size())
  {
    new_camera = 0;
  }

  mrf::gui::fb::Loger::getInstance()->info(" Camera Updated. Current Camera Number: ", new_camera);
  setCamera(new_camera);
  resetRendering(true);
}

/*
void Renderer::copyCamera(mrf::rendering::Camera *&dst_cam, mrf::rendering::Camera *src_cam)
{
  if (src_cam)
  {
    if (dst_cam)
    {
      dst_cam->position(src_cam->position());
      dst_cam->lookAt(src_cam->lookAt());
      dst_cam->up(src_cam->up());

      dst_cam->fovy(src_cam->fovy());
      dst_cam->viewPlaneDistance(src_cam->viewPlaneDistance());
      dst_cam->farPlaneDistance(src_cam->farPlaneDistance());

      dst_cam->setSensorResolution(src_cam->sensor().resolution());

      dst_cam->sensorSampler().setSPP(src_cam->sensorSampler().spp());
      dst_cam->rotMat(mrf::math::Mat3f::identity());
    }
    else
    {
      dst_cam = new mrf::rendering::Camera(
          src_cam->position(),
          src_cam->lookAt(),
          src_cam->up(),
          src_cam->fovy(),
          src_cam->viewPlaneDistance(),
          src_cam->farPlaneDistance(),
          src_cam->sensor().resolution().x(),
          src_cam->sensor().resolution().y(),
          src_cam->sensorSampler().spp());
      dst_cam->rotMat(mrf::math::Mat3f::identity());
    }
  }
}
*/

float Renderer::renderFrame()
{
  return renderFrame(_render_width, _render_height, 0, 0);
}

float Renderer::renderSamples(int samples)
{
  return renderSamples(samples, _render_width, _render_height, 0, 0);
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void Renderer::clearSpectralOutput()
{
  _output_buffer_image->clear();
}

void Renderer::allocateSpectralOutput()
{
  _output_buffer_image->allocateMemory(_render_width, _render_height, _wavelengths);
  if (_save_variance) _variance_buffer_image->allocateMemory(_render_width, _render_height, _wavelengths);

  for (unsigned int i = 0; i < _wavelengths.size(); i++)
  {
    _sortedWavelengths.insert(_wavelengths[i]);
  }
}

void Renderer::resizeSpectralOutput()
{
  clearSpectralOutput();
  allocateSpectralOutput();
}
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
mrf::image::UniformSpectralImage &Renderer::getOutputBuffer() const
#else
mrf::image::ColorImage &Renderer::getOutputBuffer() const
#endif
{
  return *_output_buffer_image.get();
}


bool Renderer::saveVarianceImage(std::string const &filename, int nb_samples, float rendering_time)
{
  std::string comments = "\n samples per pixel = " + std::to_string(nb_samples) + "\n per channel mean variance = ";

  int width  = static_cast<int>(_output_buffer_image->width());
  int height = static_cast<int>(_output_buffer_image->height());

  int n_pixels = width * height;

  float mean_var = 0.f;
#ifdef MRF_RENDERING_MODE_SPECTRAL

  std::vector<float> sum_var(_wavelengths.size());   // Accumulates all pixel variance per wavelength
  memset(sum_var.data(), 0, sum_var.size() * sizeof(float));

#  pragma omp parallel for schedule(static)
  for (int x = 0; x < width; ++x)
  {
    for (int y = 0; y < height; ++y)
    {
      std::vector<float> sum_pix     = _output_buffer_image->getPixel(x, y);
      std::vector<float> sum_sqr_pix = _variance_buffer_image->getPixel(x, y);
      std::vector<float> variance(_wavelengths.size());

      for (size_t k = 0; k < _wavelengths.size(); ++k)
      {
        variance[k] = sum_sqr_pix[k] - sum_pix[k] * sum_pix[k];   //sum(x^2) - (sum(x))^2
        sum_var[k] += variance[k];
      }

      _variance_buffer_image->setPixel(x, y, variance);
    }
  }

  for (size_t i = 0; i < _wavelengths.size(); ++i)
  {
    comments += "[" + std::to_string(_wavelengths[i]) + ":" + std::to_string(sum_var[i] / n_pixels) + "]; ";
    mean_var += sum_var[i];
  }
  mean_var /= (float)(n_pixels * (_wavelengths.size() - 1));


#else   // MRF_RENDERING_MODE_RGB

  mrf::color::Color sum_var = mrf::color::Color(0.f);   // Accumulates all pixel variance per color

#  pragma omp parallel for schedule(static)
  for (int i = 0; i < n_pixels; ++i)
  {
    mrf::color::Color sum_pix     = (*_output_buffer_image)[i];
    mrf::color::Color sum_sqr_pix = (*_variance_buffer_image)[i];
    mrf::color::Color variance    = (sum_sqr_pix - sum_pix * sum_pix);   //sum(x^2) - (sum(x))^2

    sum_var += variance;

    (*_variance_buffer_image)[i] = variance;
  }

  comments += "[r:" + std::to_string(sum_var.r() / n_pixels) + "]; ";
  comments += "[g:" + std::to_string(sum_var.g() / n_pixels) + "]; ";
  comments += "[b:" + std::to_string(sum_var.b() / n_pixels) + "]; ";

  mean_var += sum_var.avgCmp() / n_pixels;

#endif   // MRF_RENDERING_MODE_SPECTRAL

  comments += "\n overall mean variance = " + std::to_string(mean_var);

  comments += "\n total time = " + std::to_string(rendering_time) + " seconds \n";
  comments += "\n efficiency = " + std::to_string(1.f / (rendering_time * mean_var)) + "\n";

  return saveImageBuffer(*_variance_buffer_image, filename, comments, false, true);
}



#ifdef MRF_RENDERING_MODE_SPECTRAL

bool Renderer::saveImageBuffer(
    const mrf::image::UniformSpectralImage &image,
    const std::string &                     filename,
    const std::string &                     comments,
    bool                                    apply_exposure_ldr,
    bool                                    save_linear) const
{
  std::string extension;
  mrf::util::StringParsing::getFileExtension(filename.c_str(), extension);

  mrf::image::IMAGE_LOAD_SAVE_FLAGS save_flags;

  if (mrf::image::isSpectralExtension(extension.c_str()))
  {
    save_flags = mrf::image::save(image, filename, comments);
  }
  else   // We convert the spectral image to RGB
  {
    std::unique_ptr<mrf::image::ColorImage> rgb_image = mrf::image::getColorImageForSave(filename);

    mrf::color::SpectrumConverter spectrum_converter(mrf::color::CIE_1931_2DEG);
    spectrum_converter.emissiveSpectralImageToXYZImage(image, *rgb_image);

    // Apply the exposition if we are not saving in an HDR format
    if (mrf::image::isLDRExtension(extension.c_str()) && apply_exposure_ldr)
    {
#  pragma omp parallel for schedule(static)
      for (int i = 0; i < int(rgb_image->height() * rgb_image->width()); i++)
      {
        (*rgb_image)[i] = (*rgb_image)[i] * exp2(_exposure);
      }
    }

// Apply the XYZ -> RGB conversion
#  pragma omp parallel for schedule(static)
    for (int i = 0; i < int(rgb_image->height() * rgb_image->width()); i++)
    {
      (*rgb_image)[i] = (*rgb_image)[i].XYZtoLinearRGB();   //colorSpace, whitePoint);
    }

    save_flags = mrf::image::save(*rgb_image, filename, comments, save_linear);
  }

  return save_flags == mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}

#else

bool Renderer::saveImageBuffer(
    const mrf::image::ColorImage &image,
    const std::string &           filename,
    const std::string &           comments,
    bool                          apply_exposure_ldr,
    bool                          save_linear) const
{
  std::string extension;
  mrf::util::StringParsing::getFileExtension(filename.c_str(), extension);

  mrf::image::IMAGE_LOAD_SAVE_FLAGS res_saving;

  // Apply the exposition if we are not saving in an HDR format
  if (mrf::image::isLDRExtension(extension.c_str()) && apply_exposure_ldr)
  {
    std::unique_ptr<mrf::image::ColorImage> rgb_image(new mrf::image::EXRColorImage);

#  pragma omp parallel for schedule(static)
    for (int i = 0; i < int(rgb_image->height() * rgb_image->width()); i++)
    {
      (*rgb_image)[i] = image[i] * exp2(_exposure);
    }

    res_saving = mrf::image::save(*rgb_image, filename, comments, save_linear);
  }
  else
  {
    res_saving = mrf::image::save(image, filename, comments, save_linear);
  }

  return res_saving == mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}

#endif


}   // namespace rendering
}   // namespace mrf
