/*
 *
 * author : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 **/

#include <mrf_core/rendering/orthographic_camera.hpp>

namespace mrf
{
namespace rendering
{
using namespace mrf::math;

Orthographic::Orthographic(
    mrf::math::Vec3f const &pos,
    mrf::math::Vec3f const &lookAt,
    mrf::math::Vec3f const &up,
    mrf::math::Vec2f        sensor_size,
    float                   vpd,
    float                   fpd,
    size_t                  image_width_in_pixels,
    size_t                  image_height_in_pixels,
    unsigned int            nb_samples_per_pixels,
    std::string             id_name)
  : Camera(pos, lookAt, up, vpd, fpd, 1., image_width_in_pixels, image_height_in_pixels, nb_samples_per_pixels, id_name)
{
  _sensor.setSize(sensor_size);
}

mrf::lighting::Ray Orthographic::generateCameraRay(unsigned int i, unsigned int j, unsigned int k, unsigned int l) const
{
  return mrf::lighting::Ray();
}


void Orthographic::generateCameraRay(
    unsigned int        i,
    unsigned int        j,
    unsigned int        k,
    unsigned int        l,
    mrf::lighting::Ray &ray) const
{}

mrf::math::Mat4f Orthographic::projMat() const
{
  return mrf::math::Mat4f();
}

mrf::math::Mat4f Orthographic::viewMat() const
{
  return mrf::math::Mat4f();
}

void Orthographic::updateInnerData() {}

}   // namespace rendering

}   // namespace mrf