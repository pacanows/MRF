/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/color/color.hpp>

namespace mrf
{
namespace rendering
{
class MRF_CORE_EXPORT BckgManager
{
public:
  virtual ~BckgManager() {};

  /**
   *
   * Returns a color for the given direction dir
   *
   * Notice that the radiometric unit has no importance here
   * since this color will only be used to fill the background of the scene
   *
   **/
  virtual mrf::materials::COLOR colorFromDirection(mrf::math::Vec3f const &dir) const = 0;
};
}   // namespace rendering
}   // namespace mrf
