/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//STL
#include <vector>
#include <map>
#include <unordered_map>
#include <typeinfo>
#include <memory>

//MRF LIB
//Geometry
#include <mrf_core/geometry/direction.hpp>
#include <mrf_core/geometry/aabbox.hpp>
#include <mrf_core/geometry/intersectable.hpp>
#include <mrf_core/geometry/point.hpp>
#include <mrf_core/geometry/mesh.hpp>
#include <mrf_core/geometry/shape.hpp>
#include <mrf_core/rendering/background_manager.hpp>

#include <mrf_core/materials/umat.hpp>
#include <mrf_core/materials/emittance.hpp>
#include <mrf_core/lighting/light.hpp>

namespace mrf
{
namespace rendering
{
typedef std::unordered_map<std::string, std::shared_ptr<mrf::geom::Intersectable>> mesh_map;

class MRF_CORE_EXPORT Scene
{
private:
  /**
   * The bounding box of the whole scene
   * automatically updated each time an object is added to the scene
   * using the addIntersectable method.
   **/
  mrf::geom::MRF_AABBox _bbox;

  /**
   * Meshes of the scene
   **/
  mesh_map                                               _meshes;
  std::vector<std::shared_ptr<mrf::geom::Intersectable>> _meshes_vector;

  /**
   * vector of shapes
   **/
  std::vector<mrf::geom::Shape *> _shapes;

  /**
   * Link between _meshes_vector and _shapes
   **/
  std::vector<std::vector<uint>> _meshes_instances;

  /**
   *
   **/
  std::vector<mrf::lighting::Light *> _lights;

  /**
   * Materials of the scene
   **/
  std::vector<mrf::materials::UMat *> _umats;

  /**
   * Stores the number of different type of light sources
   * Only used for statistics
   **/
  unsigned int _nbPointLights;
  unsigned int _nbDirectionalLights;
  unsigned int _nbQuadLights;
  unsigned int _nbSphereLights;

  /**
   * Handle the background of the scene rendering
   **/
  BckgManager *_bckgmng;

public:
  Scene();
  ~Scene();

  inline mrf::geom::MRF_AABBox const &bbox() const;
  inline mrf::geom::MRF_AABBox &      bbox();

  //-------------------------------------------------------------------------
  // Background Management
  //-------------------------------------------------------------------------
  inline mrf::materials::COLOR background(mrf::math::Vec3f const &dir) const;
  inline void                  setBckManager(BckgManager *bck_manager);
  inline bool                  hasBackground() const;

  /**
   * call hasBackground() method before this function to avoid seg fault!
   **/
  inline BckgManager *background() const;

  //-------------------------------------------------------------------------
  // Material Management
  //-------------------------------------------------------------------------

  /**
   * Add new Material (BRDF or Emittance) to the scene and returns the index/id of the material
   **/
  inline uint addMaterial(mrf::materials::UMat *a_brdf);

  /**
   * Mark a material as "unused", setting its pointer to nullptr to avoid using it in backends.
   **/
  inline void markMaterialUnused(uint index);

  /**
   * Returns the index-th unified material
   **/
  inline mrf::materials::UMat const &umaterial(uint index) const;

  /**
   * Returns the index-th unified material (non const version)
   **/
  inline mrf::materials::UMat &umaterial(uint index);

  /**
   * Accessor to all materials in the scene
   **/
  inline std::vector<mrf::materials::UMat *> const &getAllMaterials() const { return _umats; }

  /**
   * Returns the number of umaterial currently stored in the Scene
   **/
  inline uint umatSize() const;

  //-------------------------------------------------------------------------
  // Light Management
  //-------------------------------------------------------------------------
  inline std::vector<mrf::lighting::Light *> const &lights() const;
  inline mrf::lighting::Light const &               lightByIndex(unsigned index) const;

  template<class LIGHT_TYPE>
  inline bool getLight(LIGHT_TYPE &a_light, unsigned int index) const;

  template<class LIGHT_TYPE>
  inline bool getAllLightsByType(std::vector<LIGHT_TYPE> &a_light) const;

  // Used to add Lights and keep track of each type of light
  inline void addPointLight(mrf::lighting::Light *a_light);
  inline void addDirLight(mrf::lighting::Light *a_light);
  inline void addQuadLight(mrf::lighting::Light *a_light);
  inline void addSphereLight(mrf::lighting::Light *a_light);

  /**
   * Deletes AND Removes all the lights in the scene.
   * i.e. the memory used by the lights in the scene is released.
   * This method is used by the destructor of the scene.
   **/
  inline void removeAllLights();

  //-------------------------------------------------------------------------
  // Accessors to get some stats
  //-------------------------------------------------------------------------

  /**
   * Returns the number of point light sources
   **/
  inline unsigned int nbPointLights() const;
  /**
   * Returns the number of directional light sources
   **/
  inline unsigned int nbDirLights() const;
  /**
   * Returns the number of quad light sources
   **/
  inline unsigned int nbQuadLights() const;
  /**
   * Returns the number of sphere light sources
   **/
  inline unsigned int nbSphereLights() const;
  /**
   * Returns the number of all light sources
   **/
  inline uint nbLights() const;

  /**
   * Returns the total Ray-Geometry Intersection cost of this scene
   * @see Intersectable
   **/
  inline unsigned int totalIntersectionCost() const;

  /**
   * Returns the total amount (in Mega Bytes) of Computer memory used
   * by all geometric objects in this scene.
   **/
  inline float totalGeometryCost() const;

  /**
   *
   * Returns a string which contains all information about the Scene
   *
   **/
  inline std::string info() const;


  //-------------------------------------------------------------------------
  // Intersectable Objects management
  //-------------------------------------------------------------------------

  /**
   * Always test the return value of this function, if its return value == scene.meshes().end(), the
   * intersectable was not added and must be deleted by the caller of this function,
   * otherwise it was added, the scene will be responsible for the deletion of the intersectable.
   *
   * If the argument mesh_name is "", this function will try to add the intersectable with
   * a default name: "MRF_mesh_" + std::to_string(_meshes.size());
   *
   * This method also update the bbox of the scene if it succeed to add the intersectable
   **/
  mesh_map::iterator
  addIntersectable(std::shared_ptr<mrf::geom::Intersectable> &an_intersectable, std::string &mesh_name);

  /**
   * Add a newly created shape, the scene object
   * will take care of  its deletion
   **/
  inline void addShape(mrf::geom::Shape *a_shape);

  /**
   * Accessor to meshes
   **/
  inline mesh_map &meshes();

  /**
   * Const Accessor to meshes
   **/
  inline mesh_map const &meshes() const;

  /**
   * Retrieve a mesh based on its name
   **/
  inline std::shared_ptr<mrf::geom::Intersectable> mesh(std::string const &name);

  /**
   * Accessor to meshes
   **/
  inline std::vector<std::shared_ptr<mrf::geom::Intersectable>> &meshes_vector();

  /**
   * Const Accessor to meshes
   **/
  inline std::vector<std::shared_ptr<mrf::geom::Intersectable>> const &meshes_vector() const;


  /**
   * Accessor to shapes
   **/
  inline std::vector<mrf::geom::Shape *> &shapes();

  /**
   * const Accessor to shapes
   **/
  inline std::vector<mrf::geom::Shape *> const &shapes() const;

  /**
   * Accessor to shapes
   **/
  inline std::vector<std::vector<uint>> &meshes_instances();

  /**
   * const Accessor to shapes
   **/
  inline std::vector<std::vector<uint>> const &meshes_instances() const;

  /**
   * NOT supported anymore
   **/
  void recomputeAABBox();
};

/**
 *  verify the brdf of the scene
 * not supported in spectral mode
 **/
void verifyBRDF(
    mrf::rendering::Scene const &a_scene,
    unsigned int                 number_of_view_directions             = 10,
    unsigned int                 number_of_samples_to_compute_integral = 100000);

#include "scene.inl"


}   // namespace rendering
}   // namespace mrf
