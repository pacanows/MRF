#include <mrf_core/rendering/color_background_manager.hpp>

using mrf::materials::COLOR;

namespace mrf
{
namespace rendering
{
ColorBckgManager::ColorBckgManager(COLOR const a_color): _background_color(a_color) {}

COLOR
ColorBckgManager::colorFromDirection(mrf::math::Vec3f const & /*dir*/) const
{
  return _background_color;
}

}   // namespace rendering


}   // namespace mrf
