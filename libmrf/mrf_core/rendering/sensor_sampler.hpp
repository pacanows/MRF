/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * SensorSampler class for rendering
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/rendering/sensor.hpp>

namespace mrf
{
namespace rendering
{
/**
 * @brief      This class samples uniformly a given sensor.
 *
 * The number of samples per pixel is controlled with the _sqrtSPP
 * attributes that defines the number of samples for each pixel dimension.
 *
 * The total number of samples per pixel is therefore equal _sqrtSPP*_sqrtSPP
 * Each pixel is subdivided into _sqrtSPP*_sqrtSPP subpixels. Each supixel
 * will be sampled once and at its center
 *
 */
class MRF_CORE_EXPORT SensorSampler
{
private:
  /* A const reference to a sensor that cannot be modified from the SensorSampler */
  Sensor *_sensor;

  /* Square Root number of Samples per Pixel */
  unsigned int _sqrt_spp;


  /* In  X direction vector, which length is a subpixel  */
  mrf::math::Vec3d _x_sub_pixel;
  /* In  Y direction vector, which length is a subpixel  */
  mrf::math::Vec3d _y_sub_pixel;

  /* Center offset of the subpixel i.e.,  (_xSubPixel + _ySubPixel) / 2 */
  mrf::math::Vec3d _middle_sub_pixel;

public:
  /**
   * @brief      Construct a sampler to sampler a sensor uniformly
   *
   * @param      sensor                  { The sensor that needs to be sample }
   * @param[in]  sqrt_samples_per_pixel  { The square root number of samples per pixel }
   */
  inline SensorSampler(Sensor *sensor, unsigned int sqrt_samples_per_pixel);


  /**
   * @brief      Returns the 3D position in World Coordinates of a given pixel and subpixel
   *
   * @param[in]  i     { the row index of the pixel }
   * @param[in]  j     { the column index of the pixel }
   * @param[in]  k     { the row index of the sub-pixel }
   * @param[in]  l     { the column index of the sub-pixel }
   *
   * @return     { Returns the 3D position in World Coordinates of the given (i,j) pixel and (k,l) subpixel
   * whithin the pixel }
   */
  inline mrf::math::Vec3f positionOnSensor(unsigned int i, unsigned int j, unsigned int k, unsigned int l) const;
  inline void
  positionOnSensor(unsigned int i, unsigned int j, unsigned int k, unsigned int l, mrf::math::Vec3f &position) const;


  // Accessors to class attributes
  inline unsigned int            spp() const;
  inline unsigned int            sqrtSPP() const;
  inline mrf::math::Vec3d const &xSubPixel() const;
  inline mrf::math::Vec3d const &ySubPixel() const;
  inline mrf::math::Vec3d const &middleSubPixel() const;

  /**
   * @brief      Sets the number of samples per pixel
   *             Update the inner data accordingly by calling updateSubPixelVectors
   * Note:       The number is truncated to the neares power of two.
   */
  inline void setSPP(unsigned int new_spp);


  // This will recompute the attribute vectors. Use this when the associated
  // sensor has been changed
  inline void updateSubPixelVectors();

private:
  //SensorSampler();
};

//----------------------------------------------------------------------------------------------
//EXTERNAL OPERATOR
//----------------------------------------------------------------------------------------------
inline std::ostream &operator<<(std::ostream &os, SensorSampler const &a_sensor_sampler)
{
  os << " SensorSampler = { Samples per Pixel: " << a_sensor_sampler.spp() << std::endl
     << "                   xSubPixel: " << a_sensor_sampler.xSubPixel() << " "
     << " ySubPixel: " << a_sensor_sampler.ySubPixel() << " " << std::endl
     << "                   Middle Sub Pixel:" << a_sensor_sampler.middleSubPixel()
     << " Position On sensor (0,0) = " << a_sensor_sampler.positionOnSensor(0, 0, 0, 0) << " } " << std::endl;

  return os;
}


//----------------------------------------------------------------------------------------------
// IMPLEMENTATION OF SensorSampler Class
//----------------------------------------------------------------------------------------------

SensorSampler::SensorSampler(Sensor *sensor, unsigned int sqrt_samples_per_pixel)
  : _sensor(sensor)
  , _sqrt_spp(sqrt_samples_per_pixel)
{
  updateSubPixelVectors();
}

mrf::math::Vec3f SensorSampler::positionOnSensor(unsigned int i, unsigned int j, unsigned int k, unsigned int l) const
{
  using namespace mrf::math;

  Vec3f const pixel_upper_left_corner = _sensor->pixelULC(i, j);



  return _sensor->localToWorldMatrix()
         * (pixel_upper_left_corner + _y_sub_pixel * k + _x_sub_pixel * l + _middle_sub_pixel);
}

void SensorSampler::positionOnSensor(
    unsigned int      i,
    unsigned int      j,
    unsigned int      k,
    unsigned int      l,
    mrf::math::Vec3f &position) const
{
  mrf::math::Vec3f const pixel_upper_left_corner = _sensor->pixelULC(i, j);
  position                                       = _sensor->localToWorldMatrix()
             * (pixel_upper_left_corner + _y_sub_pixel * k + _x_sub_pixel * l + _middle_sub_pixel);
}



inline unsigned int SensorSampler::spp() const
{
  return _sqrt_spp * _sqrt_spp;
}

inline unsigned int SensorSampler::sqrtSPP() const
{
  return _sqrt_spp;
}


inline mrf::math::Vec3d const &SensorSampler::xSubPixel() const
{
  return _x_sub_pixel;
}

inline mrf::math::Vec3d const &SensorSampler::ySubPixel() const
{
  return _y_sub_pixel;
}

inline mrf::math::Vec3d const &SensorSampler::middleSubPixel() const
{
  return _middle_sub_pixel;
}


inline void SensorSampler::setSPP(unsigned int new_spp)
{
  _sqrt_spp = static_cast<unsigned int>(std::floor(std::sqrt(new_spp)));
  updateSubPixelVectors();
}

void SensorSampler::updateSubPixelVectors()
{
  float const inv_sqrt_samples_per_pixel = 1.0f / _sqrt_spp;

  _x_sub_pixel = _sensor->pAxisX() * inv_sqrt_samples_per_pixel;
  _y_sub_pixel = _sensor->pAxisY() * inv_sqrt_samples_per_pixel;

  _middle_sub_pixel = (_x_sub_pixel + _y_sub_pixel) * 0.5;
}
}   // namespace rendering
}   // namespace mrf