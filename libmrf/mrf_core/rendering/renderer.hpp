/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * modifications : David Murray @ inria.fr
 * Copyright INRIA 2019,2020
 *
 * modifications : Simon LUCAS @ inria.fr
 * Copyright INRIA 2021
 *
 * interface for renderer
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>
#include <mrf_core/color/spectrum_converter.hpp>

#include <mrf_core/color/color.hpp>

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  include <mrf_core/image/uniform_spectral_image.hpp>
#  include <mrf_core/image/envi_spectral_image.hpp>
#else
#  include <mrf_core/image/color_image.hpp>
#endif

#include <mrf_core/math/mat3.hpp>
#include <mrf_core/rendering/camera.hpp>
#include <mrf_core/rendering/pinhole_camera.hpp>
#include <mrf_core/rendering/thin_lens_camera.hpp>
#include <mrf_core/rendering/orthographic_camera.hpp>
#include <mrf_core/rendering/environment_camera.hpp>
#include <mrf_core/lighting/envmap.hpp>
#include <mrf_core/rendering/scene.hpp>
#include <mrf_core/feedback/loger.hpp>

#include <mrf_core/io/scene_parser.hpp>

#include <string>
#include <vector>
#include <set>

namespace mrf
{
namespace rendering
{
enum RENDERER_RNG
{
  OPTIX  = 0,
  SOBOL  = 1,
  HALTON = 2
};

enum NEXT_EVENT
{
  NONE,
  ONE,
  ALL = ONE
};

class MRF_CORE_EXPORT Renderer
{
public:
  /*!
   * Constructor
   */
  Renderer(
      mrf::rendering::Scene *      mrf_scene = nullptr,
      mrf::rendering::RENDERER_RNG rng_type  = mrf::rendering::RENDERER_RNG::HALTON,
      unsigned long long           rng_seed  = 0);

  virtual ~Renderer();

  /*!
   * Simple setter for the theta rotation angle of the envmap, if any.
   * \param theta
   */
  virtual void setRotationTheta(float theta) { _envmap_rotation_theta = theta; }

  /*!
   * Simple getter.
   * \return the theta rotation angle of the envmap, if any.
   */
  virtual float getRotationTheta() const { return _envmap_rotation_theta; }

  /*!
   * Increases the theta rotation angle of the envmap, if any, of the given amount.
   * \param delta
   * \return new value of theta
   */
  virtual float increaseRotationTheta(float delta);

  /*!
   * Simple setter for the phi rotation angle of the envmap, if any.
   * \param phi
   */
  virtual void setRotationPhi(float phi) { _envmap_rotation_phi = phi; }

  /*!
   * Simple getter.
   * \return the phi rotation angle of the envmap, if any.
   */
  virtual float getRotationPhi() const { return _envmap_rotation_phi; }

  /*!
   * Increases the phi rotation angle of the envmap, if any, of the given amount.
   * \param delta
   * \return new value of phi
   */
  virtual float increaseRotationPhi(float delta);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  /*!
   * Pure virtual function to initialize the rendering backend based on the requested number of wavelength per pass.
   *
   * \param interactive
   * \param nb_wavelengths_per_pass
   */
  virtual void init(bool interactive, uint nb_wavelengths_per_pass) = 0;

  /*!
   * Simple getter.
   *
   * \return the output result of the rendering backend.
   */
  virtual mrf::image::UniformSpectralImage &getOutputBuffer() const;
#else
  /*!
   * Pure virtual function to initialize the rendering backend.
   *
   * \param interactive
   */
  virtual void init(bool interactive = false) = 0;

  /*!
   * Simple getter.
   *
   * \return the output result of the rendering backend.
   */
  virtual mrf::image::ColorImage &getOutputBuffer() const;
#endif

  /*!
   * Pure virtual function to render a frame using _render_width/_render_height, to be used for interactive renderer. To be implemented in specific rendering backend.
   * \return the rendering time of the frame/the sample.
   */
  virtual float renderFrame();

  /*!
   * Pure virtual function to render a (sub)frame (one sample) at given position for given size, to be used for interactive renderer. To be implemented in specific rendering backend.
   * \param width : the rendering width.
   * \param height : the rendering height.
   * \param posX : the rendering X starting position.
   * \param posY : the rendering Y starting position.
   * \return the rendering time of the frame/the sample.
   */
  virtual float renderFrame(size_t width, size_t height, int posX = 0, int posY = 0) = 0;

  /*!
   * Pure virtual function to render a given number of frames using _render_width/_render_height. To be implemented in specific rendering backend.
   * \param samples : the number of frames to render. Default is 0, in that case, the attribute _max_samples is used.
   * \return the rendering time of the desired number of samples.
   */
  virtual float renderSamples(int samples = 0);

  /*!
   * Pure virtual function to render a given number of (sub)frames at given position for given size. To be implemented in specific rendering backend.
   * \param samples : the number of frames to render. Default is 0, in that case, the attribute _max_samples is used.
   * \param width : the rendering width.
   * \param height : the rendering height.
   * \param posX : the rendering X starting position.
   * \param posY : the rendering Y starting position.
   * \return the rendering time of the desired number of samples.
   */
  virtual float renderSamples(int samples, size_t width, size_t height, int posX = 0, int posY = 0) = 0;

  /*!
   * Pure virtual function to free the backend resources.
   */
  virtual void shutdown() = 0;

  /*!
   * Saves the rendering buffer as an image.
   * \param filename : the filename of the image to save.
   * \param additional_comments : additional comments to be written in the image header.
   * \return true if image has been saved, false otherwise.
   */
  virtual bool saveImage(std::string const &filename, const std::string &additional_comments = "");


#ifdef RENDERER_INTERACTIVE
  /*!
   * Pure virtual function to create a backend-specific buffer based on an OpenGL buffer for interoperability purpose. To be implemented in a specific rendering backend.
   * \param GLBufferID : the ID of the OpenGL buffer, as generated by glGenBuffer.
   * \param buffer_name : the name of the buffer in the backend, if any.
   * \param nb_comp : the number of component per buffer element (e.g. the RGB channels).
   * \return true if the buffer has been created, false otherwise.
   */
  virtual bool createBufferFromGLBuffer(
      int         GLBufferID,
      std::string buffer_name,
      size_t      frame_width,
      size_t      frame_height,
      int         nb_comp)
      = 0;

  /*!
   * Pure virtual function to ensure that the backend does not prevent the GUI from modifying the buffer.
   * \param GLBufferID : the ID of the OpenGL buffer requiring update.
   * \param buffer_name : the name of the buffer in the backend, if any.
   * \return true if the buffer is ready to be updated by the GUI, false otherwise.
   */
  virtual bool readyBufferForUpdate(int GLBufferID, std::string bufferName) = 0;

  /*!
   * Pure virtual function to ensure that the backend gets access to the buffer for modifications.
   * \param GLBufferID : the ID of the OpenGL buffer.
   * \param buffer_name : the name of the buffer in the backend, if any.
   * \return true if the buffer is ready to be modified by the backend, false otherwise.
   */
  virtual bool readyBufferForRendering(int GLBufferID, std::string bufferName) = 0;
#endif

#ifdef MRF_RENDERING_MODE_SPECTRAL
  /*!
   * Clears spectral output buffer.
   */
  virtual void clearSpectralOutput();

  /**
   * Allocate memory for spectral output buffer.
   */
  virtual void allocateSpectralOutput();

  /**
   * Resize the spectral output buffer.
   */
  virtual void resizeSpectralOutput();
#endif

  /*!
   * Pure virtual function to add a backend-specific file like an integrator.
   * \param file
   */
  virtual void setExternalFile(std::string const &file) = 0;

  /*!
   * Method to display information about the rendering parameters
   * Backends should extend this method to add additional information.
   * \return
   */
  virtual std::string getInfos() const;

  /*!
   * Override the sensor width with command line or user-specified width. Returns the previous value if caching is necessary.
   * \param width : the desired width.
   * \return the previous width if any.
   */
  size_t overrideSensorWidth(size_t width);

  /*!
   * Override the sensor height with command line or user-specified height. Returns the previous value if caching is necessary.
   * \param width : the desired height.
   * \return the previous height if any.
   */
  size_t overrideSensorHeight(size_t height);

  /*!
   * Override the sensor number of samples with command line or user-specified value. Returns the previous value if caching is necessary.
   * \param width : the desired number of samples.
   * \return the previous number if any.
   */
  size_t overrideSensorSamples(size_t samples);

  /*!
   * Pure virtual function to imports a MRF scene from a specified scene and camera file into the rendering backend. Also initializes the backend resources if necessary.
   * \param scene_loader : the scene parser to load the .mrf file.
   * \param camera_file : the full path to the .mcf file. The file can contain multiple cameras.
   * \param start_cam : the camera to be used as first rendering camera.
   * \param num_cam : the number of camera to be loaded. If 0 (or in interactive mode), all cameras in camera_file are loaded.
   */
  virtual void
  importMrfScene(mrf::io::SceneParser *scene_loader, std::string camera_file, size_t starting_cam, size_t num_cam)
      = 0;

  virtual void importMrfScene(mrf::io::SceneParser *scene_loader, std::string camera_file)
  {
    importMrfScene(scene_loader, camera_file, 0, 0);
  }

  /*!
   * Pure virtual function to imports a MRF scene from a specified scene and camera file into the rendering backend. Also initializes the backend resources if necessary.
   * \param scene_file : the scene path to load the .mrf file.
   * \param camera_file : the full path to the .mcf file. The file can contain multiple cameras.
   * \param start_cam : the camera to be used as first rendering camera.
   * \param num_cam : the number of camera to be loaded. If 0 (or in interactive mode), all cameras in camera_file are loaded.
   */
  virtual void importMrfScene(std::string scene_file, std::string camera_file, size_t starting_cam, size_t num_cam) = 0;

  virtual void importMrfScene(std::string scene_file, std::string camera_file)
  {
    importMrfScene(scene_file, camera_file, 0, 0);
  }

  /*!
   * Simple getter, to be used by the UI when handling the renderer.
   * \return true if a scene has been loaded, false otherwise.
   */
  bool hasScene() const { return _scene_imported; }

  /*!
   * Pure virtual function to free the backend resources.
   */
  virtual void freeSceneResources() = 0;

  /*!
   * Pure virtual function to create the backend render target.
   */
  virtual void createRenderTarget() = 0;

  /*!
   * Pure virtual function to resize the backend render target.
   * \param width
   * \param height
   */
  virtual void resizeRenderTarget(size_t width, size_t height) = 0;

  /*!
   * Simple getter, to be used by the UI to manage envmap parameters.
   * \return true if an envmap has been loaded, false otherwise.
   */
  virtual bool hasEnvmap() const { return _envmap != nullptr; }

  /*!
   * Simple getter.
   * \return the current camera.
   */
  virtual Camera *getMainCamera() { return _main_camera; }

  /*!
   * Simple getter.
   * \return the vector of cameras.
   */
  virtual std::vector<Camera *> getAllCamera() { return _all_cameras; }

  /*!
   * Simple getter.
   * \return the current number of cameras.
   */
  virtual size_t nbCameras() const { return _all_cameras.size(); }

  /*!
   * Simple getter.
   * \return the index of the current camera.
   */
  virtual size_t getCurrentCameraIndex() const { return _current_camera; }

  /*!
   * Update the current camera with Camera stored at the given index.
   *
   * \param index of the new camera
   */
  virtual void setCurrentCamera(const size_t &index);

  /*!
   * Reset the main camera to its default setting.
   */
  virtual void resetCamera();

  /*!
   * Align the current camera on the vertical (0,1,0) axis.
   */
  virtual void makeHeadStraight();

  /*!
   * Make the previous camera the current one.
   */
  virtual void previousCamera();

  /*!
   * Make the next camera the current one.
   */
  virtual void nextCamera();


  /*!
   *  Pure virtual function to update the camera of the backend
   */
  virtual void updateBackendCamera() = 0;

  /*!
   * Pure virtual function to update the camera parameters of the backend.
   */
  virtual void updateCameraParameters() = 0;

  /*!
   * Simple setter for the next event estimation (NEE).
   * \param type : NONE for no NEE, ONE to enable NEE, ALL = ONE.
   */
  virtual void setNextEvent(NEXT_EVENT type) { _next_event_type = type; }

  /*!
   * Simple getter.
   * \return
   */
  NEXT_EVENT getNextEvent() const { return _next_event_type; }

  /*!
   * Simple setter.
   * \param type
   */
  void setRngType(RENDERER_RNG type) { _rng_type = type; }

  /*!
   * Simple getter.
   * \return
   */
  RENDERER_RNG getRngType() const { return _rng_type; }

  /*!
   * Simple setter.
   * \param debug_pixels
   */
  virtual void setDebugPixels(std::vector<mrf::uint> const &debug_pixels) { _debug_pixels = debug_pixels; }

  /*!
   * Simple setter.
   * \param num_pass
   */
  virtual void setNumPass(uint num_pass) { _num_pass = num_pass; }

  /*!
   * Simple getter.
   * \return number of current pass.
   */
  virtual uint getNumPass() const { return _num_pass; }

  /*!
   * Simple setter.
   * \param total
   */
  virtual void setTotalNumPasses(uint total) { _total_num_passes = total; }

  /*!
   * Simple getter.
   * \return
   */
  virtual uint getTotalNumPasses() const { return _total_num_passes; }

  /*!
   * Simple setter.
   * \param factor
   */
  virtual void setSpatialFactor(uint factor) { _spatial_subdivision_factor = factor; }

  /*!
   * Simple getter.
   * \return the spatial subdivision factor
   */
  virtual uint getSpatialFactor() const { return _spatial_subdivision_factor; }

  /*!
   * Simple setter.
   * \param max_samples
   */
  virtual void setMaxSamples(uint max_samples) { _max_samples = max_samples; }

  /*!
   * Simple getter.
   * \param num_samples
   */
  virtual void setNumSamplesPerFrame(uint num_samples) { _num_samples_per_frame = num_samples; }

  /*!
   * Simple getter.
   * \return
   */
  virtual uint numSamplesPerFrame() { return _num_samples_per_frame; }

  /*!
   * Simple getter.
   * \return
   */
  virtual size_t numSamplesPerPixel() const { return _max_samples; }

  /*!
   * Simple setter.
   * \param time
   */
  virtual void setMaxRenderingTimeSeconds(int time)
  {
    if (time > 0) _max_rendering_time_seconds = time;
  };

  /*!
   * Simple getter.
   * \return
   */
  virtual int maxRenderingTimeSeconds() const { return _max_rendering_time_seconds; }

  /*!
   * Simple setter.
   * \param value
   */
  virtual void setSaveTemporarySpectralImages(bool value) { _save_temporary_spectral_images = value; }

  /*!
   * Simple setter.
   * \param activate_envmap_is
   */
  void setEnvmapImportanceSampling(bool activate_envmap_is) { _envmap_is = activate_envmap_is; }

  /*!
   * Simple getter.
   * \return
   */
  bool envmapImportanceSampling() const { return _envmap_is; }

  /*!
   * Simple setter.
   * \param max_path_length
   */
  virtual void setMaxPathLength(int max_path_length) { _max_path_length = max_path_length; }

  /*!
   * Simple getter.
   * \return
   */
  virtual int getMaxPathLength() const { return _max_path_length; }

  /*!
   * Simple setter.
   * \param max_path_length
   */
  virtual void setContinuousSampling(bool continuous) { _continuous_sampling = continuous; }

  /*!
   * Simple getter.
   * \return
   */
  virtual bool setContinuousSampling() const { return _continuous_sampling; }

  /*!
   *
   */
  void disableAssetsUpdate() { _assets_update_required = false; }

  /*!
   *
   */
  void enableAssetsUpdate() { _assets_update_required = true; }

#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual std::vector<uint> const &getSpectralWavelengths() const { return _wavelengths; }

  virtual void setSpectralWavelengths(std::vector<uint> const &waves);

  virtual size_t getNbWavelengths() const { return _wavelengths.size(); }

  virtual std::vector<std::vector<uint>> const &getWavelengthsPerPass() const { return _wavelengths_per_passes; }

  virtual size_t getNbMaxWavelengthsPerPass() const { return _nb_max_wavelengths_per_pass; }

#endif

  /*!
   * Simple setter.
   * \param seed
   */
  virtual void setRNGSeed(mrf::rendering::RENDERER_RNG seed) = 0;

  /*!
   * Simple setter.
   * \param log
   */
  virtual void setVarianceLog(bool log) { _save_variance = log; };

  /*!
   * Simple setter.
   */
  virtual void setWidth(size_t w) { _render_width = w; }

  /*!
   * Simple getter.
   * /return width
   */
  virtual size_t getWidth() { return _render_width; }

  /*!
   * Simple getter.
   * /return height
   */
  virtual void setHeight(size_t h) { _render_height = h; }

  /*!
   * Simple getter.
   * /return height
   */
  virtual size_t getHeight() { return _render_height; }

  /*!
   * Simple getter.
   * /return actual number of samples per pixel.
   */
  virtual int getSpp() { return _spp; }

  /*!
   * Reset the frame counter.
   */
  virtual void resetRendering(bool clear = false) = 0;

  /*!
   * Reset the frame counter.
   */
  virtual void resetNumFrame(int value = 1) { _num_frame = value; }

  /*!
   * Simple getter.
   *
   * \return the current frame number.
   */
  virtual uint getNumFrame() const { return _num_frame; }

  /*!
   * Simple getter.
   *
   * \return gamma.
   */
  virtual float getGamma() const { return _gamma; }

  /*!
   * Simple setter.
   *
   * \param gamma
   */
  virtual void setGamma(float gamma) { _gamma = gamma; }

  /*!
   * Simple getter.
   *
   * \return exposure
   */
  virtual float getExposure() { return _exposure; }

  /*!
   * Simple setter.
   *
   * \param exposure
   */
  virtual void setExposure(float exposure) { _exposure = exposure; }

protected:   //METHODS
  /*!
   * Gets the image from the backend rendering buffer (if any) back to the CPU image container.
   */
  virtual void fetchImage() = 0;


  /*!
   * Convenience method to account for a change in current camera index.
   */
  virtual void setCamera();

  /*!
   * Set a given camera as the current camera.
   *
   * /param camera index
   */
  virtual void setCamera(const size_t &current_camera);


  /*!
   * Convenience method to copy a camera into another.
   *
   * \param dst_cam
   * \param src_cam
   */
  //virtual void copyCamera(mrf::rendering::Camera *&dst_cam, mrf::rendering::Camera *src_cam);

  /*!
   * Save the rendering buffer variance information as an image.
   * \param filename : the filename of the image to save.
   * \param nb_samples : the number of samples per pixel used for the image.
   * \param rendering_time : the total rendering time of the image.
   * \return true if image has been saved, false otherwise.
   */
  virtual bool saveVarianceImage(std::string const &filename, int nb_samples, float rendering_time);


  /*!
   * Convenience method to export a buffer in an image.
   * \param image Image buffer to save
   * \param filename Path to save the image to
   * \param comments Additional comments to add to the file metadata if supported
   * by the requested format
   * \param apply_exposure_ldr Apply the exposure value set in the renderer when
   * the requested format is low dynamic range RGB.
   * \param save_linear Do not apply gamma correction when saving in a low dynamic
   * range RGB image format.
   */
#ifdef MRF_RENDERING_MODE_SPECTRAL
  virtual bool saveImageBuffer(
      const mrf::image::UniformSpectralImage &image,
      const std::string &                     filename,
      const std::string &                     comments,
      bool                                    apply_exposure_ldr = true,
      bool                                    save_linear        = false) const;
#else    // MRF_RENDERING_MODE_RGB

  virtual bool saveImageBuffer(
      const mrf::image::ColorImage &image,
      const std::string &           filename,
      const std::string &           comments,
      bool                          apply_exposure_ldr = true,
      bool                          save_linear        = false) const;
#endif   // MRF_RENDERING_MODE_SPECTRAL

protected:   //MEMBERS
  std::string            _scene_file;
  bool                   _scene_imported;
  mrf::io::SceneParser * _parser;
  mrf::rendering::Scene *_mrf_scene;

  bool _assets_update_required;   // Allows to control if the assets have changed and should be updated
                                  // or not between different cameras for example

  size_t                                _current_camera;
  size_t                                _previous_camera;
  mrf::rendering::Camera *              _main_camera;
  std::vector<mrf::rendering::Camera *> _all_cameras;

  size_t _render_width;
  size_t _render_height;
  uint   _spatial_subdivision_factor;

  uint  _num_frame;
  uint  _spp;   //In case a frame does not correspond to 1 spp.
  float _elapsed_time;
  bool  _save_variance;

  //current num pass ranges in [0,total_num_passes-1]
  uint _num_pass;

  //number of required passes  (SPECTRALLY) to render the full image
  uint   _total_num_passes;
  size_t _max_samples;
  uint   _num_samples_per_frame;
  uint   _seconds_between_log_advancement;
  uint   _max_rendering_time_seconds;
  int    _max_path_length;
  bool   _continuous_sampling;

  bool _override_width;
  bool _override_height;
  bool _override_samples;

  /**
   * Type of rng
   * Cannot be change after initialization is created
   **/
  RENDERER_RNG       _rng_type;
  unsigned long long _rng_seed;

  /**
   * Type of next event
   * Cannot be change after initialization is created
   **/
  NEXT_EVENT _next_event_type;

  /**
   *  True means we save a spectral image at the end of each pass,
   *  Usefull to recover some part of the spectral image when rendering time is too long.
   **/
  bool _save_temporary_spectral_images;

  /**
  ** Envmap rotation angles IN DEGREES
  **/
  float _envmap_rotation_theta;
  float _envmap_rotation_phi;


  /**
   * true if renderer uses importance sampling for the envmap
   * Cannot be change after render context is created
   **/
  bool _envmap_is;

  /*
   * Pointer to envmap contained in scene if present.
   * Usefull for faster retrieve, (avoid multiples dynamic cast on scene->bakground())
   */
  mrf::lighting::EnvMap *_envmap;

  /**
   * Array of id (pixel number) of pixels that must print their values after a call of renderer.render();
   * Usefull for debugging
   **/
  std::vector<mrf::uint> _debug_pixels;

  //for shadow catcher
  mrf::materials::RADIANCE_TYPE _envmap_lum_over_horizon;

#ifdef MRF_RENDERING_MODE_SPECTRAL

  //RP:  this vector should also be available in _outputspectral_image ?!!!
  // TODO allow different formats
  std::unique_ptr<mrf::image::UniformSpectralImage> _output_buffer_image;
  std::unique_ptr<mrf::image::UniformSpectralImage> _variance_buffer_image;
  mrf::color::SpectrumConverter                     _spectrum_converter;
  /**
   * For multiplexed (wavelengths) mode we store the number of wavelengths
   * rendered per pass
   * WARNING NEED TO BE SET BEFORE CREATING RENDER CONTEXT
   **/

  std::vector<uint>              _wavelengths;
  std::vector<uint>              _wavelengths_bounds;
  std::vector<std::vector<uint>> _wavelengths_per_passes;
  std::vector<std::vector<uint>> _wavelengths_per_passes_bounds;
  // std::vector<mrf::uint>         _wavelengths_indices_per_passes; // TODO

  uint _nb_max_wavelengths_per_pass;

  std::set<mrf::uint> _sortedWavelengths;
#else
  std::unique_ptr<mrf::image::ColorImage> _output_buffer_image;
  std::unique_ptr<mrf::image::ColorImage> _variance_buffer_image;
#endif

  /**
   * We need to store if renderer is interactive mode or not
   * so we can  know for instance in resizeRenderTarget if we need to resize
   * a dedicated display buffer.
   */
  bool _is_interactive;

  /**
   * Tonemaping parameters
   **/
  float _exposure;
  float _gamma;
};
}   // namespace rendering
}   // namespace mrf
