/**
 * Author: Romain Pacanowski @ institutoptique . fr
 * Copyright: CNRS 2020
 *
 * Author: Romain Pacanowski @ inria . fr
 * Copyright:  Inria 2022
 *
 * Additions: sMAPE difference
 **/

#pragma once


#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace image
{
/**
 * Compute the Mean Square Error (MSE) between two images
 *
 * For colored (resp. spectral) images the MSE between two pixels is computed by summing over each color (resp. wavelength) channel.
 *
 */
template<class IMG_TYPE>
inline float mse_diff(IMG_TYPE const &img1, IMG_TYPE const &img2)
{
  // PRE-CONDITIIONS
  assert(img1.size() > 0);
  assert(img1.size() == img2.size());
  assert(img1.height() == img2.height());
  assert(img1.width() == img2.width());

  //TODO : Optimize me with OpenMP reduction
  float mse = 0.0f;
  for (mrf::uint i = 0; i < img1.size(); i++)
  {
    mse += mrf::color::Color(img1(i) - img2(i)).sqr().sum();
  }

  return mse / img1.size();
}

/**
 * Compute the Mean Square Error (MSE) between two images, saves the per-pixel diff in another image.
 *
 * For colored (resp. spectral) images the MSE between two pixels is computed by summing over each color (resp. wavelength) channel.
 *
 */
template<class IMG_TYPE>
inline float mse_diff(IMG_TYPE const &img1, IMG_TYPE const &img2, IMG_TYPE &diffImg)
{
  // PRE-CONDITIIONS
  assert(img1.size() > 0);
  assert(img1.size() == img2.size());
  assert(img1.height() == img2.height());
  assert(img1.width() == img2.width());

  //TODO : Optimize me with OpenMP reduction
  float mse = 0.0f;
  for (mrf::uint i = 0; i < img1.size(); i++)
  {
    mrf::color::Color tmp = mrf::color::Color(img1(i) - img2(i)).sqr();
    diffImg.setPixel(i, tmp);
    mse += tmp.sum();
  }

  return mse / img1.size();
}

/**
 * Compute the Root Mean Square Error (MSE) between two images
 *
 * For colored (resp. spectral) images the MSE between two pixels is computed by
 * summing over each color (resp. wavelength) channel.
 *
 */
template<class IMG_TYPE>
inline float rmse_diff(IMG_TYPE const &img1, IMG_TYPE const &img2)
{
  float const mse = mse_diff(img1, img2);

  return std::sqrt(mse);
}

/**
 * Compute the Root Mean Square Error (MSE) between two images, saves the per-pixel diff in another image.
 *
 * For colored (resp. spectral) images the MSE between two pixels is computed by
 * summing over each color (resp. wavelength) channel.
 *
 */
template<class IMG_TYPE>
inline float rmse_diff(IMG_TYPE const &img1, IMG_TYPE const &img2, IMG_TYPE &diffImg)
{
  float const mse = mse_diff(img1, img2, diffImg);

  return std::sqrt(mse);
}

template<class IMG_TYPE = UniformSpectralImage>
inline float mse_diff(UniformSpectralImage const &img1, UniformSpectralImage const &img2)
{
  // Pre-Conditions
  assert(img1.size() > 0);
  assert(img1.size() == img2.size());

  assert(img1.height() == img2.height());
  assert(img1.width() == img2.width());

  assert(img1.wavelengths().size() == img2.wavelengths().size());

  //std::vector<uint> waves = img1.wavelengths();

  std::vector<mrf::data_struct::Array2D> const &image1_data = img1.data();
  std::vector<mrf::data_struct::Array2D> const &image2_data = img2.data();

  float mse = 0.0f;
  for (size_t w = 0; w < image1_data.size(); w++)
  {
    mrf::data_struct::Array2D const &a1 = image1_data[w];
    mrf::data_struct::Array2D const &a2 = image2_data[w];

    mse += (a1 - a2).square().sum();
  }

  return mse / img1.size();
}

template<class IMG_TYPE = UniformSpectralImage>
inline float mse_diff(UniformSpectralImage const &img1, UniformSpectralImage const &img2, UniformSpectralImage &diffImg)
{
  // Pre-Conditions
  assert(img1.size() > 0);
  assert(img1.size() == img2.size());

  assert(img1.height() == img2.height());
  assert(img1.width() == img2.width());

  assert(img1.wavelengths().size() == img2.wavelengths().size());

  //std::vector<uint> waves = img1.wavelengths();

  std::vector<mrf::data_struct::Array2D> const &image1_data = img1.data();
  std::vector<mrf::data_struct::Array2D> const &image2_data = img2.data();

  double mse = 0.0;
  for (size_t w = 0; w < image1_data.size(); w++)
  {
    mrf::data_struct::Array2D const &a1 = image1_data[w];
    mrf::data_struct::Array2D const &a2 = image2_data[w];

    diffImg.data()[w] = (a1 - a2).square();

    mse += (a1 - a2).square().sum();
  }
  //img1.size since we need to divde by widh*height*nb_wavelengths to get a unique metric
  return static_cast<float>(mse / img1.size());
}

template<class IMG_TYPE = UniformSpectralImage>
inline float rmse_diff(UniformSpectralImage const &img1, UniformSpectralImage const &img2)
{
  return std::sqrt(mse_diff(img1, img2));
}

template<class IMG_TYPE = UniformSpectralImage>
inline float
rmse_diff(UniformSpectralImage const &img1, UniformSpectralImage const &img2, UniformSpectralImage &diffImg)
{
  return std::sqrt(mse_diff(img1, img2, diffImg));
}


/**
 * @brief Compute the sMAPE difference image between 2 given images and returns the average sMAPE value.
 *        The returned value is computed by averaging over all wavelengths/colors and pixels.
 *        The returned values are between 0 and 1.
 *
 *        See https://en.wikipedia.org/wiki/Symmetric_mean_absolute_percentage_error
 *        sMAPE for more detail on this metric.
 *
 *
 * @tparam UniformSpectralImage
 * @param img1
 * @param img2
 * @param diffImg
 * @return float
 */
inline float
mean_smape(UniformSpectralImage const &img1, UniformSpectralImage const &img2, UniformSpectralImage &diffImg)
{
  // Pre-Conditions
  assert(img1.size() > 0);
  assert(img1.size() == img2.size());

  assert(img1.height() == img2.height());
  assert(img1.width() == img2.width());

  std::vector<mrf::data_struct::Array2D> const &image1_data = img1.data();
  std::vector<mrf::data_struct::Array2D> const &image2_data = img2.data();

  float mean_smape = 0.0f;
  for (size_t w = 0; w < image1_data.size(); w++)   //for each wavelength
  {
    mrf::data_struct::Array2D const &a1 = image1_data[w];
    mrf::data_struct::Array2D const &a2 = image2_data[w];

    mrf::data_struct::Array2D &diff_img_slice = diffImg.data()[w];

    for (unsigned int i = 0; i < a1.size(); i++)   // over all pixels
    {
      float const num   = std::abs(a1(i) - a2(i));
      float const denom = std::abs(a1(i) + a2(i));

      if (denom != 0.0f)
      {
        diff_img_slice(i) = num / denom;
        mean_smape += diff_img_slice(i);
      }
    }
  }

  //img1.size() == nb_wavelength * nb_pixels
  return mean_smape / img1.size();
}

inline float mean_smape(UniformSpectralImage const &img1, UniformSpectralImage const &img2)
{
  // Pre-Conditions
  assert(img1.size() > 0);
  assert(img1.size() == img2.size());

  assert(img1.height() == img2.height());
  assert(img1.width() == img2.width());

  std::vector<mrf::data_struct::Array2D> const &image1_data = img1.data();
  std::vector<mrf::data_struct::Array2D> const &image2_data = img2.data();

  float mean_smape = 0.0f;
  // TODO: Optimize me with OpenMP
  for (size_t w = 0; w < image1_data.size(); w++)   //for each wavelength
  {
    mrf::data_struct::Array2D const &a1 = image1_data[w];
    mrf::data_struct::Array2D const &a2 = image2_data[w];

    for (unsigned int i = 0; i < a1.size(); i++)
    {
      float const num   = std::abs(a1(i) - a2(i));
      float const denom = std::abs(a1(i) + a2(i));


      if (denom != 0.0f)
      {
        mean_smape += num / denom;
      }
    }
  }

  //img1.size() == nb_wavelength * nb_pixels
  return mean_smape / img1.size();
}


template<class T>
inline float mean_smape(MultiChannelImage<T> const &img1, MultiChannelImage<T> const &img2)
{
  // PRE-CONDITIIONS
  assert(img1.size() > 0);
  assert(img1.size() == img2.size());
  assert(img1.height() == img2.height());
  assert(img1.width() == img2.width());

  float mean_smape = 0.0f;
  //TODO : Optimize me with OpenMP reduction
  for (mrf::uint i = 0; i < img1.size(); i++)
  {
    // T& numerator = ;
    //T denom = ;

    mean_smape += ((img1(i) - img2(i)).abs() / (img1(i) + img2(i)).abs()).sum();
  }

  return mean_smape / img1.size();
}

template<class T>
inline float
mean_smape(MultiChannelImage<T> const &img1, MultiChannelImage<T> const &img2, MultiChannelImage<T> &diffImg)
{
  // PRE-CONDITIIONS
  assert(img1.size() > 0);
  assert(img1.size() == img2.size());
  assert(img1.height() == img2.height());
  assert(img1.width() == img2.width());

  float mean_smape = 0.0f;

  //TODO : Optimize me with OpenMP reduction
  for (mrf::uint i = 0; i < img1.size(); i++)
  {
    //T const & numerator = ;
    //T const & denom = ;

    T const tmp = ((img1(i) - img2(i)).abs() / (img1(i) + img2(i)).abs());
    diffImg.setPixel(i, tmp);

    mean_smape += tmp.sum();
  }

  return mean_smape / img1.size();
}

}   // namespace image
}   // namespace mrf
