/**
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *
 * Alban Fichet @ institutoptique.fr
 * April 2020
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <string>
#include <functional>
#include <map>

namespace mrf
{
namespace image
{
enum IMAGE_LOAD_SAVE_FLAGS
{
  MRF_NO_ERROR                 = 0,
  MRF_ERROR_WRONG_EXTENSION    = 1,
  MRF_ERROR_WRONG_FILE_PATH    = 2,
  MRF_ERROR_LOADING_FILE       = 3,
  MRF_ERROR_DECODING_FILE      = 4,
  MRF_ERROR_SAVING_FILE        = 5,
  MRF_ERROR_WRONG_PIXEL_FORMAT = 6,
  MRF_ERROR_WRONG_IMAGE_SIZE   = 7
};

//template <class T>
class MRF_CORE_EXPORT Image
{
public:
  // TODO:
  //virtual T const& getPixel(size_t x, size_t y) const = 0;

  virtual size_t width() const  = 0;
  virtual size_t height() const = 0;
  virtual size_t size() const   = 0;

  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const = 0;

  /**
   * @brief Image's metadata
   *
   * Returns associative map (key->value) describing the metadata
   * of the image.
   * If not overrided the default implementation returns an empty map
   *
   * @return an associative map
   */
  virtual std::map<std::string, std::string> const &metadata() const { return _metadata; }


protected:
  Image() {};
  virtual ~Image() {};

protected:
  std::map<std::string, std::string> _metadata;
};

}   // namespace image
}   // namespace mrf
