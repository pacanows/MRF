/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#include <mrf_core/image/png_color_image.hpp>
#include <lodepng/lodepng.h>

using mrf::color::Color;

namespace mrf
{
namespace image
{
PNGColorImage::PNGColorImage(size_t width, size_t height): ColorImage(width, height) {}


PNGColorImage::PNGColorImage(const std::string &file_path, std::function<void(int)> progress, bool loadLinear)
  : ColorImage()
{
  std::vector<unsigned char> png;
  std::vector<unsigned char> image;

  unsigned int width, height;

  progress(0);

  unsigned error = lodepng::load_file(png, file_path);

  if (error)
  {
    throw MRF_ERROR_LOADING_FILE;
  }

  error = lodepng::decode(image, width, height, png);

  if (error)
  {
    throw MRF_ERROR_DECODING_FILE;
  }

  MultiChannelImage::init(width, height);

  auto inverseGamma = mrf::color::getInverseGammaFunction();

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < static_cast<int>(width * height); i++)
  {
    const size_t pixel_offset = 4 * static_cast<size_t>(i);
    (*this)[i]                = Color(
                     float(image[pixel_offset + 0]),
                     float(image[pixel_offset + 1]),
                     float(image[pixel_offset + 2]),
                     float(image[pixel_offset + 3]))
                 / 255.0f;

    // reverse sRGB
    if (!loadLinear)
    {
      (*this)[i] = inverseGamma((*this)[i]);
    }
  }

  progress(100);
}

PNGColorImage::PNGColorImage(const ColorImage &img): ColorImage(img) {}


PNGColorImage::~PNGColorImage() {}


IMAGE_LOAD_SAVE_FLAGS PNGColorImage::save(
    const std::string &      file_path,
    const std::string &      additional_comments,
    std::function<void(int)> progress) const
{
  return save(file_path, false, additional_comments, progress);
}

IMAGE_LOAD_SAVE_FLAGS PNGColorImage::save(
    const std::string &file_path,
    bool               saveLinear,
    const std::string & /*additional_comments*/,
    std::function<void(int)> progress) const
{
  std::vector<unsigned char> temp_values;
  temp_values.resize(4 * _width * _height);

  progress(0);

  auto gammaFunction = mrf::color::getGammaFunction();

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < int(_width * _height); i++)
  {
    // Parallel... progress(100.f * static_cast<float>(i) /
    // static_cast<float>(width * height)); if linear RGB writting mode
    // requested, ignore sRGB conversion
    Color px_color = (*this)[i];

    if (!saveLinear)
    {
      px_color = gammaFunction(px_color);
    }

    px_color = 255.f * px_color.clamped(0.f, 1.f);

    const size_t pixel_offset = 4 * static_cast<size_t>(i);

    // Put the color in the uchar buffer for PNG
    for (int c = 0; c < 4; c++)
    {
      temp_values[pixel_offset + c] = static_cast<unsigned char>(px_color[c]);
    }
  }

  uint error = lodepng::encode(
      file_path,
      temp_values,
      static_cast<unsigned int>(width()),
      static_cast<unsigned int>(height()),
      LCT_RGBA,
      8);

  if (error)
  {
    return MRF_ERROR_SAVING_FILE;
  }

  progress(100);

  return MRF_NO_ERROR;
}
}   // namespace image
}   // namespace mrf
