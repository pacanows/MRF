/**
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *
 * Alban Fichet @ institutoptique.fr
 * April 2020
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/radiometry/spectrum.hpp>
#include <mrf_core/color/spectrum_converter.hpp>
#include <mrf_core/image/image.hpp>
#include <mrf_core/image/color_image.hpp>

namespace mrf
{
namespace image
{
class MRF_CORE_EXPORT SpectralImage: public mrf::image::MultiChannelImage<mrf::radiometry::Spectrum_T<mrf::uint, float>>
{
public:
  SpectralImage();
  IMAGE_LOAD_SAVE_FLAGS addValuesFromGrayImage(
      std::string const &file_path,
      uint               wavelength);   //TODO return error flags, create an enum ? use mrf::image enum ?
private:
};
}   // namespace image
}   // namespace mrf
