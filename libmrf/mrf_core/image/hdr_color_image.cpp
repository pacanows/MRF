/**
 *
 * author : Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

// fopen
#define _CRT_SECURE_NO_WARNINGS

#include <mrf_core/image/hdr_color_image.hpp>

#include <rgbe/rgbe.h>

#include <memory>

using mrf::color::Color;

namespace mrf
{
namespace image
{
HDRColorImage::HDRColorImage(size_t width, size_t height): ColorImage(width, height) {}


HDRColorImage::HDRColorImage(const std::string &file_path, std::function<void(int)> progress): ColorImage()
{
  progress(0);
  std::FILE *f = fopen(file_path.c_str(), "rb");

  if (f == nullptr)
  {
    std::perror("File opening failed");
    throw MRF_ERROR_LOADING_FILE;
  }

  int read_width  = 0;
  int read_height = 0;

  if (RGBE_ReadHeader(f, &read_width, &read_height, NULL) != RGBE_RETURN_SUCCESS)
  {
    fclose(f);
    throw MRF_ERROR_LOADING_FILE;
  }
  const size_t width  = static_cast<size_t>(read_width);
  const size_t heigth = static_cast<size_t>(read_height);

  std::unique_ptr<float> image(new float[3 * width * heigth]);

  if (RGBE_ReadPixels_RLE(f, image.get(), read_width, read_height) != RGBE_RETURN_SUCCESS)
  {
    fclose(f);
    throw MRF_ERROR_LOADING_FILE;
  }

  fclose(f);

  MultiChannelImage<mrf::color::Color>::init(width, heigth);

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < static_cast<int>(_width * _height); i++)
  {
    (*this)[i] = mrf::color::Color(image.get()[3 * i], image.get()[3 * i + 1], image.get()[3 * i + 2]);
  }

  progress(100);
}


HDRColorImage::HDRColorImage(const ColorImage &img): ColorImage(img) {}


HDRColorImage::~HDRColorImage() {}

IMAGE_LOAD_SAVE_FLAGS HDRColorImage::save(
    const std::string &file_path,
    const std::string & /*additional_comments*/,
    std::function<void(int)> progress) const
{
  progress(0);
  std::FILE *f = std::fopen(file_path.c_str(), "wb");

  if (f == nullptr)
  {
    return MRF_ERROR_LOADING_FILE;
  }

  const int write_width  = static_cast<int>(_width);
  const int write_heigth = static_cast<int>(_height);

  if (RGBE_WriteHeader(f, write_width, write_heigth, NULL) != RGBE_RETURN_SUCCESS)
  {
    fclose(f);
    return MRF_ERROR_LOADING_FILE;
  }


  std::unique_ptr<float> image(new float[3 * _width * _height]);

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < static_cast<int>(_width * _height); i++)
  {
    for (int c = 0; c < 3; c++)
    {
      image.get()[3 * i + c] = (*this)[i][c];
    }
  }

  if (RGBE_WritePixels_RLE(f, image.get(), write_width, write_heigth) != RGBE_RETURN_SUCCESS)
  {
    fclose(f);
    return MRF_ERROR_LOADING_FILE;
  }


  fclose(f);

  progress(100);
  return MRF_NO_ERROR;
}
}   // namespace image
}   // namespace mrf
