/**
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <mrf_core/image/color_image.hpp>

namespace mrf
{
namespace image
{
class MRF_CORE_EXPORT HDRColorImage: public ColorImage
{
public:
  HDRColorImage(size_t width = 0, size_t height = 0);

  HDRColorImage(
      std::string const &      file_path,
      std::function<void(int)> progress = [](int) {
      });

  HDRColorImage(ColorImage const &img);

  virtual ~HDRColorImage();

  virtual IMAGE_LOAD_SAVE_FLAGS save(
      const std::string &      file_path,
      const std::string &      additional_comments = "",
      std::function<void(int)> progress            = [](int) {
      }) const override;
};
}   // namespace image
}   // namespace mrf
