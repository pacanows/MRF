/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/


inline mrf::math::Vec3f const &ColorImage::redStatistics() const
{
  return _red;
}

inline mrf::math::Vec3f const &ColorImage::greenStatistics() const
{
  return _green;
}

inline mrf::math::Vec3f const &ColorImage::blueStatistics() const
{
  return _blue;
}

inline mrf::math::Vec3f const &ColorImage::lumStatistics() const
{
  return _lum;
}

inline float ColorImage::logMeanLuminance() const
{
  return _l_log_mean;
}
