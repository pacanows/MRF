/**
 *
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Modifications:
 *
 * Romain Pacanowski @ institutoptique.fr
 * March 2020
 * Copyright CNRS 2020
 *
 * Alban Fichet @ institutoptique.fr
 * April 2020
 * Copyright CNRS 2020
 *
 **/


#include <mrf_core/util/precision_timer.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/radiometry/illuminants.hpp>
#include <mrf_core/io/exr_io.hpp>

#include <lodepng/lodepng.h>
#include <tinyexr/tinyexr.h>

#include <fstream>
#include <regex>
#include <functional>
#include <climits>
#include <iostream>
#include <algorithm>
#include <list>
#include <array>

using mrf::data_struct::Array2D;

namespace mrf
{
namespace image
{
UniformSpectralImage::UniformSpectralImage()
  : Image()
  , _data(0)
  , _accumulative_data(0)
  , _wavelengths(0)
  , _accumulative_data_computed(false)
{}


UniformSpectralImage::UniformSpectralImage(
    size_t width,
    size_t height,
    uint   start_wavelength,
    uint   step_wavelength,
    size_t nb_wavelength)
  : Image()
  , _data(nb_wavelength)
  , _accumulative_data(nb_wavelength)
  , _wavelengths(nb_wavelength)
  , _accumulative_data_computed(false)
{
  #pragma omp parallel for schedule(static)
  for (int i = 0; i < static_cast<int>(nb_wavelength); i++)
  {
    _wavelengths[i] = start_wavelength + i * step_wavelength;
    _data[i].resize(height, width);
  }
}


UniformSpectralImage::UniformSpectralImage(size_t width, size_t height, const std::vector<uint> &wavelengths)
  : Image()
  , _data(wavelengths.size())
  , _accumulative_data(wavelengths.size())
  , _wavelengths(wavelengths)
  , _accumulative_data_computed(false)
{
  std::sort(_wavelengths.begin(), _wavelengths.end());

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < static_cast<int>(_wavelengths.size()); i++)
  {
    _data[i].resize(height, width);
  }
}


UniformSpectralImage::UniformSpectralImage(
    size_t                       width,
    size_t                       height,
    std::pair<uint, uint> const &start_end_wavelength,
    size_t                       nb_wavelength)
  : Image()
  , _data(nb_wavelength)
  , _accumulative_data(nb_wavelength)
  , _wavelengths(nb_wavelength)
  , _accumulative_data_computed(false)
{
  const float step_wavelength
      = (start_end_wavelength.second - start_end_wavelength.first) / static_cast<float>(nb_wavelength - 1);

  if (mrf::math::_Math<float>::absVal(step_wavelength - std::floor(step_wavelength))
      > std::numeric_limits<float>::epsilon())
  {
    // TODO LOGGER
    std::cout << "[WARNING] step wavelength is not an integer" << std::endl;
  }

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < static_cast<int>(nb_wavelength); i++)
  {
    _wavelengths[i] = static_cast<uint>(start_end_wavelength.first + i * step_wavelength);
    _data[i].resize(height, width);
  }
}


UniformSpectralImage::~UniformSpectralImage() {}


void UniformSpectralImage::clear()
{
  clearConserveWavelengths();
  _wavelengths.clear();
}


void UniformSpectralImage::clearConserveWavelengths()
{
  _data.clear();
  _accumulative_data.clear();
  _accumulative_data_computed = false;
}


void UniformSpectralImage::zeros()
{
  #pragma omp parallel for schedule(static)
  for (int num_array = 0; num_array < static_cast<int>(_data.size()); num_array++)
  {
    _data[num_array].setZero();
  }

  _accumulative_data_computed = false;
}


void UniformSpectralImage::allocateMemory(size_t width, size_t height, std::vector<uint> wavelengths)
{
  _wavelengths = wavelengths;
  std::sort(_wavelengths.begin(), _wavelengths.end());

  _data.resize(_wavelengths.size());

  for (auto &image_array : _data)
  {
    image_array.resize(height, width);
  }

  _accumulative_data_computed = false;
}


IMAGE_LOAD_SAVE_FLAGS
UniformSpectralImage::addValuesFromGrayImage(std::string const &file_path, uint wavelength, bool loadLinearWhenLDR)
{
  std::unique_ptr<ColorImage> img;

  try
  {
    img = loadColorImage(file_path, loadLinearWhenLDR);
  }
  catch (IMAGE_LOAD_SAVE_FLAGS const &f)
  {
    return f;
  }

  return addValuesFromGrayImage(*img, wavelength);
}


IMAGE_LOAD_SAVE_FLAGS UniformSpectralImage::addValuesFromGrayImage(ColorImage const &image, uint wavelength)
{
  // Check if contains an array and avoid adding arrays of different size
  if (_data.size() > 0 && (image.width() != this->width() || image.height() != this->height()))
  {
    return MRF_ERROR_WRONG_IMAGE_SIZE;
  }

  std::vector<float> v(image.size());

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < static_cast<int>(image.size()); i++)
  {
    v[i] = (image[i].r() + image[i].g() + image[i].b()) / 3.F;
  }

  addValuesFromBuffer(v, image.width(), image.height(), wavelength);

  return MRF_NO_ERROR;
}


IMAGE_LOAD_SAVE_FLAGS UniformSpectralImage::addValuesFromColorImage(
    std::string const &                 file_path,
    std::tuple<uint, uint, uint> const &wavelengths,
    bool                                loadLinearWhenLDR)
{
  std::unique_ptr<ColorImage> img;

  try
  {
    img = loadColorImage(file_path, loadLinearWhenLDR);
  }
  catch (IMAGE_LOAD_SAVE_FLAGS const &f)
  {
    return f;
  }

  return addValuesFromColorImage(*img, wavelengths);
}


IMAGE_LOAD_SAVE_FLAGS
UniformSpectralImage::addValuesFromColorImage(ColorImage const &image, std::tuple<uint, uint, uint> const &wavelengths)
{
  // Check if contains an array and avoid adding arrays of different size
  if (_data.size() > 0 && (image.width() != this->width() || image.height() != this->height()))
  {
    return MRF_ERROR_WRONG_IMAGE_SIZE;
  }

  std::vector<float>  v(image.size());
  std::array<uint, 3> wl;

  //add wavelength
  wl[0] = std::get<0>(wavelengths);
  wl[1] = std::get<1>(wavelengths);
  wl[2] = std::get<2>(wavelengths);

  for (int k = 0; k < 3; k++)
  {
    #pragma omp parallel for schedule(static)
    for (int i = 0; i < static_cast<int>(image.size()); i++)
    {
      v[i] = image[i][k];
    }

    addValuesFromBuffer(v, image.width(), image.height(), wl[k]);
  }

  return MRF_NO_ERROR;
}

void UniformSpectralImage::spectralDownsampling(uint downsampling_factor)
{
  assert(downsampling_factor > 1);

  uint nb_new_waves = static_cast<uint>(std::ceil(static_cast<float>(_wavelengths.size()) / downsampling_factor));
  std::vector<uint> new_waves(nb_new_waves);

  //Compute new wavelength
  uint num_wave = 0;
  for (auto &wave : new_waves)
  {
    wave = _wavelengths[num_wave];
    num_wave += downsampling_factor;
  }

  std::vector<mrf::data_struct::Array2D> new_data(nb_new_waves);

  const uint box_size      = 2 * downsampling_factor - 1;
  const uint half_box_size = box_size / 2;

  for (uint i = 0; i < nb_new_waves; i++)
  {
    new_data[i].setZero(height(), width());

    for (uint j = downsampling_factor * i - half_box_size; j <= downsampling_factor * i + half_box_size; j++)
    {
      new_data[i] += _data[std::min(std::max(j, uint(0)), uint(_data.size()) - 1)];
    }
    new_data[i] /= static_cast<float>(box_size);
  }

  //now move downsample image to the object
  _data.resize(nb_new_waves);

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < static_cast<int>(nb_new_waves); i++)
  {
    _data[i] = new_data[i];
  }

  _wavelengths = new_waves;

  _accumulative_data_computed = false;
}


void UniformSpectralImage::generateAccumulativeData()
{
  //for each wavelength
  for (size_t i = 0; i < _data.size(); i++)
  {
    Array2D new_array(width(), height());

    if (i == 0)
      new_array = _data[0];
    else
      new_array = _data[i] + _accumulative_data[i - 1];

    _accumulative_data.push_back(std::move(new_array));
    _accumulative_data_computed = true;
  }
}


size_t UniformSpectralImage::width() const
{
  if (_data.empty())
  {
    return 0;
  }

  return _data[0].width();
}


size_t UniformSpectralImage::height() const
{
  if (_data.empty())
  {
    return 0;
  }

  return _data[0].height();
}

// std::map<std::string, std::string> UniformSpectralImage::metadata() const
// {
//     return std::map<std::string, std::string>();
// }

//pixel accessor i in [0-width] j in [0-height]
std::vector<float> UniformSpectralImage::getPixel(size_t x, size_t y) const
{
  std::vector<float> res(_data.size());

  for (uint wavelength = 0; wavelength < _data.size(); wavelength++)
  {
    res[wavelength] = _data[wavelength](y, x);
  }

  return res;
}


void UniformSpectralImage::setPixel(size_t x, size_t y, const std::vector<float> &data)
{
  for (uint wavelength = 0; wavelength < _data.size(); wavelength++)
  {
    _data[wavelength](y, x) = data[wavelength];
  }
}


IMAGE_LOAD_SAVE_FLAGS UniformSpectralImage::saveAsGrayPngImages(std::string const &folder_path) const
{
  // Get max length of filename from number of wavelengths
  uint max_string_size = uint(std::to_string(_wavelengths.size()).length());

  uint num_wavelength = 0;
  for (auto it = _wavelengths.cbegin(); it != _wavelengths.cend(); ++it)
  {
    std::string file_number = std::to_string(num_wavelength);
    std::string filename    = file_number;

    // Append 0 at the begining of file to have files ordered by wavelengths
    for (size_t i = 0; i < max_string_size - file_number.length(); i++)
    {
      filename = "0" + filename;
    }

    filename = folder_path + "/" + filename + ".png";

    std::vector<unsigned char> temp_values;
    temp_values.resize(width() * height());

    for (size_t i = 0; i < width() * height(); i++)
    {
      unsigned char value = static_cast<unsigned char>(255 * (_data[num_wavelength])(i));
      temp_values[i]      = value;
    }

    uint error = lodepng::encode(
        filename,
        temp_values,
        static_cast<unsigned int>(width()),
        static_cast<unsigned int>(height()),
        LCT_GREY,
        8);
    if (error)
    {
      return MRF_ERROR_SAVING_FILE;
    }
    num_wavelength++;
  }
  return MRF_NO_ERROR;
}


IMAGE_LOAD_SAVE_FLAGS UniformSpectralImage::savePixelToFile(size_t x, size_t y, std::string const &file_path) const
{
  return savePixelToFile(x, y, 0, file_path);
}


IMAGE_LOAD_SAVE_FLAGS
UniformSpectralImage::savePixelToFile(size_t x, size_t y, uint kernel_size, std::string const &file_path) const
{
  int                nb_spectrums_averaged = 0;
  std::vector<float> avg_spectrum_values;

  for (int i = int(x) - int(kernel_size); i <= int(x) + int(kernel_size); i++)
  {
    for (int j = int(y) - int(kernel_size); j <= int(y) + int(kernel_size); j++)
    {
      if (i < int(width()) && i >= 0 && j < int(height()) && j >= 0)
      {
        //average spectrum values
        if (avg_spectrum_values.size() == 0)
        {
          //spectrum_wavelengths = temp_wavelengths;
          avg_spectrum_values   = getPixel(i, j);
          nb_spectrums_averaged = 1;
        }
        else
        {
          //accumulate color
          const std::vector<float> temp_spectrum = getPixel(i, j);

          for (size_t it_spectrum = 0; it_spectrum < avg_spectrum_values.size(); it_spectrum++)
          {
            avg_spectrum_values[it_spectrum] += temp_spectrum[it_spectrum];
          }
          nb_spectrums_averaged++;
        }
      }
    }
  }

  //average spectrum values
  for (size_t it_spectrum = 0; it_spectrum < avg_spectrum_values.size(); it_spectrum++)
  {
    avg_spectrum_values[it_spectrum] /= float(nb_spectrums_averaged);
  }

  std::ofstream file(file_path);
  if (!file.is_open())
  {
    return MRF_ERROR_SAVING_FILE;
  }

  for (uint num_wavelength = 0; num_wavelength < avg_spectrum_values.size(); num_wavelength++)
  {
    //accessor for mrf::Array2D and Eigen::Array are (row,column)
    file << _wavelengths[num_wavelength] << ":" << avg_spectrum_values[num_wavelength] << "," << std::endl;
  }

  file.close();

  return MRF_NO_ERROR;
}


double UniformSpectralImage::memoryOccupancy() const
{
  double size = 0;
  for (size_t i = 0; i < _data.size(); i++)
    size += _data[i].size();

  for (size_t i = 0; i < _accumulative_data.size(); i++)
    size += _accumulative_data[i].size();

  size *= double(sizeof(float)) / (1000.0 * 1000.0);
  size += double(sizeof(uint) * _wavelengths.size()) / (1000.0 * 1000.0);

  return size;
}


double UniformSpectralImage::expectedMemoryOccupancy(int image_width, int image_height, int image_bands)
{
  double size = double(image_bands) * double(image_width * image_height);   //_data[i].size();

  size *= double(sizeof(float)) / (1000.0 * 1000.0);
  size += double(sizeof(uint) * image_bands) / (1000.0 * 1000.0);

  return size;
}

}   // namespace image
}   // namespace mrf
