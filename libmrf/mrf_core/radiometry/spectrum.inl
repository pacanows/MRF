/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 **/



template<class T_wave, class T_val>
Spectrum_T<T_wave, T_val>::Spectrum_T()
{}

template<class T_wave, class T_val>
Spectrum_T<T_wave, T_val>::Spectrum_T(T_val value): _wavelengths(2)
                                                  , _values(2)
{
  _wavelengths[0] = DEFAULT_MIN_WAVELENGTH;
  _wavelengths[1] = DEFAULT_MAX_WAVELENGTH;
  _values[0]      = value;
  _values[1]      = value;
}

template<class T_wave, class T_val>
Spectrum_T<T_wave, T_val>::Spectrum_T(T_val r, T_val g, T_val b)
  : _wavelengths(N_SAMPLES_UPLIFTING)
  , _values(N_SAMPLES_UPLIFTING)
{
  for (size_t i = 0; i < N_SAMPLES_UPLIFTING; i++)
  {
    _wavelengths[i] = static_cast<T_wave>(START_WAVELENGHT_UPLIFITING + i * STEP_UPLIFITING);
    _values[i]
        = static_cast<T_val>(.8 / 3. * (r * RGB_R_UPLIFTING[i] + g * RGB_G_UPLIFTING[i] + b * RGB_B_UPLIFTING[i]));
  }
}


template<class T_wave, class T_val>
Spectrum_T<T_wave, T_val>::Spectrum_T(std::vector<T_wave> const &wavelengths, std::vector<T_val> const &values)
{
  _wavelengths = wavelengths;
  _values      = values;
}

template<class T_wave, class T_val>
void Spectrum_T<T_wave, T_val>::add(T_wave const &wavelength, T_val const &value)
{
  _wavelengths.push_back(wavelength);
  _values.push_back(value);
}

template<class T_wave, class T_val>
void Spectrum_T<T_wave, T_val>::clear()
{
  _wavelengths.clear();
  _values.clear();
}

template<class T_wave, class T_val>
bool mrf::radiometry::Spectrum_T<T_wave, T_val>::loadSPDFile(std::string const &file_path)
{
  std::ifstream file;
  file.open(file_path, std::ifstream::in);

  if (!file.is_open())
  {
    return false;
  }

  clear();

  std::string line;

  while (getline(file, line))
  {
    loadFromSPDString(line);
  }
  return true;
}

template<class T_wave, class T_val>
void mrf::radiometry::Spectrum_T<T_wave, T_val>::loadFromSPDString(std::string const &spd_string)
{
  /*
  std::vector<std::string> tokens;
  std::string token;
  std::istringstream tokenStream(spd_string);
  while (std::getline(tokenStream, token, ','))
  {
    auto pos = token.find(":");
    if (pos && pos + 1 < token.length())
    {
      std::string wavelength_s = token.substr(0, pos);
      std::string value_s = token.substr(pos + 1);

      uint wavelength = uint(std::stof(wavelength_s));
      float value = std::stof(value_s);

      add(wavelength, T(value));
    }
  }
  */


  std::vector<std::string> spectrum_vec;
  mrf::util::StringParsing::tokenize(spd_string, spectrum_vec, ",");

  for (std::size_t i = 0; i < spectrum_vec.size(); i++)
  {
    std::vector<std::string> spectrum_value;
    mrf::util::StringParsing::tokenize(spectrum_vec[i], spectrum_value, ":");
    if (spectrum_value.size() == 2)
    {
      T_wave wavelength = T_wave(std::stof(spectrum_value[0]));
      T_val  power      = T_val(std::stof(spectrum_value[1]));
      add(wavelength, power);
    }
    else
    {}
  }
}
template<class T_wave, class T_val>
void mrf::radiometry::Spectrum_T<T_wave, T_val>::saveToSPDFile(std::string const &filepath)
{
  std::ofstream file(filepath);
  if (!file.is_open()) throw std::runtime_error("Could not open file for saving");

  for (uint num_wavelength = 0; num_wavelength < _wavelengths.size(); num_wavelength++)
  {
    file << _wavelengths[num_wavelength] << ":" << _values[num_wavelength] << "," << std::endl;
  }

  file.close();
}

template<class T_wave, class T_val>
typename std::vector<T_val>::iterator Spectrum_T<T_wave, T_val>::valuesBegin()
{
  return _values.begin();
}

template<class T_wave, class T_val>
typename std::vector<T_val>::iterator Spectrum_T<T_wave, T_val>::valuesEnd()
{
  return _values.end();
}

template<class T_wave, class T_val>
typename std::vector<T_val>::const_iterator Spectrum_T<T_wave, T_val>::valuesCbegin() const
{
  return _values.cbegin();
}

template<class T_wave, class T_val>
typename std::vector<T_val>::const_iterator Spectrum_T<T_wave, T_val>::valuesCend() const
{
  return _values.cend();
}

template<class T_wave, class T_val>
typename std::vector<T_wave>::iterator Spectrum_T<T_wave, T_val>::wavelengthsBegin()
{
  return _wavelengths.begin();
}

template<class T_wave, class T_val>
typename std::vector<T_wave>::iterator Spectrum_T<T_wave, T_val>::wavelengthsEnd()
{
  return _wavelengths.end();
}

template<class T_wave, class T_val>
typename std::vector<T_wave>::const_iterator Spectrum_T<T_wave, T_val>::wavelengthsCbegin() const
{
  return _wavelengths.cbegin();
}

template<class T_wave, class T_val>
typename std::vector<T_wave>::const_iterator Spectrum_T<T_wave, T_val>::wavelengthsCend() const
{
  return _wavelengths.cend();
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> &Spectrum_T<T_wave, T_val>::operator*=(float const &f)
{
  for (std::size_t i = 0; i < _values.size(); i++)
  {
    _values[i] *= f;
  }
  return *this;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::operator*(float const &f) const
{
  Spectrum_T<T_wave, T_val> res = *this;
  for (std::size_t i = 0; i < _values.size(); i++)
  {
    res._values[i] *= f;
  }
  return res;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> &Spectrum_T<T_wave, T_val>::operator/=(float const &f)
{
  for (std::size_t i = 0; i < _values.size(); i++)
  {
    _values[i] /= f;
  }
  return *this;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::operator/(float const &f) const
{
  Spectrum_T<T_wave, T_val> res = *this;
  for (std::size_t i = 0; i < _values.size(); i++)
  {
    res._values[i] /= f;
  }
  return res;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> &Spectrum_T<T_wave, T_val>::operator+=(float const &f)
{
  for (std::size_t i = 0; i < _values.size(); i++)
  {
    _values[i] += f;
  }
  return *this;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::operator+(float const &f) const
{
  Spectrum_T<T_wave, T_val> res = *this;
  for (std::size_t i = 0; i < _values.size(); i++)
  {
    res._values[i] += f;
  }
  return res;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> &Spectrum_T<T_wave, T_val>::operator-=(float const &f)
{
  for (std::size_t i = 0; i < _values.size(); i++)
  {
    _values[i] += f;
  }
  return *this;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::operator-(float const &f) const
{
  Spectrum_T<T_wave, T_val> res = *this;
  for (std::size_t i = 0; i < _values.size(); i++)
  {
    res._values[i] += f;
  }
  return res;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> &Spectrum_T<T_wave, T_val>::operator*=(Spectrum_T<T_wave, T_val> const &s)
{
  assert(s.size() == size());
  for (uint i = 0; i < _values.size(); i++)
  {
    _values[i] *= s._values[i];
  }
  return *this;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::operator*(Spectrum_T<T_wave, T_val> const &s) const
{
  Spectrum_T<T_wave, T_val> res = *this;
  assert(s.size() == size());
  for (uint i = 0; i < _values.size(); i++)
  {
    res._values[i] *= s._values[i];
  }
  return res;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> &Spectrum_T<T_wave, T_val>::operator/=(Spectrum_T<T_wave, T_val> const &s)
{
  assert(s.size() == size());
  for (uint i = 0; i < _values.size(); i++)
  {
    if (s._values[i] != T_val(0))
      _values[i] /= s._values[i];
    else
      _values[i] = T_val(0);
  }
  return *this;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::operator/(Spectrum_T<T_wave, T_val> const &s) const
{
  Spectrum_T<T_wave, T_val> res = *this;
  assert(s.size() == size());
  for (uint i = 0; i < _values.size(); i++)
  {
    if (s._values[i] != T_val(0))
      res._values[i] /= s._values[i];
    else
      res._values[i] = T_val(0);
  }
  return res;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> &Spectrum_T<T_wave, T_val>::operator+=(Spectrum_T<T_wave, T_val> const &s)
{
  assert(s.size() == size());
  for (uint i = 0; i < _values.size(); i++)
  {
    _values[i] += s._values[i];
  }
  return *this;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::operator+(Spectrum_T<T_wave, T_val> const &s) const
{
  Spectrum_T<T_wave, T_val> res = *this;
  assert(s.size() == size());
  for (uint i = 0; i < _values.size(); i++)
  {
    res._values[i] += s._values[i];
  }
  return res;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> &Spectrum_T<T_wave, T_val>::operator-=(Spectrum_T<T_wave, T_val> const &s)
{
  assert(s.size() == size());
  for (uint i = 0; i < _values.size(); i++)
  {
    _values[i] -= s._values[i];
  }
  return *this;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::operator-(Spectrum_T<T_wave, T_val> const &s) const
{
  Spectrum_T<T_wave, T_val> res = *this;
  assert(s.size() == size());
  for (uint i = 0; i < _values.size(); i++)
  {
    res._values[i] -= s._values[i];
  }
  return res;
}

template<class T_wave, class T_val>
inline bool Spectrum_T<T_wave, T_val>::operator==(Spectrum_T<T_wave, T_val> const &s) const
{
  Spectrum_T<T_wave, T_val> res = *this;
  assert(s.size() == size());
  bool ret = true;
  for (uint i = 0; i < _values.size(); i++)
  {
    ret &= res._values[i] == s._values[i];
  }
  return ret;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::sqrt() const
{
  Spectrum_T<T_wave, T_val> res = *this;
  for (uint i = 0; i < _values.size(); i++)
  {
    res._values[i] = std::sqrt(res._values[i]);
  }
  return res;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::pow(float exponent) const
{
  Spectrum_T<T_wave, T_val> res = *this;
  for (uint i = 0; i < _values.size(); i++)
  {
    res._values[i] = std::pow(res._values[i], exponent);
  }
  return res;
}

template<class T_wave, class T_val>
inline Spectrum_T<T_wave, T_val> Spectrum_T<T_wave, T_val>::sqr() const
{
  Spectrum_T<T_wave, T_val> res = *this;
  for (uint i = 0; i < _values.size(); i++)
  {
    res._values[i] = res._values[i] * res._values[i];
  }
  return res;
}

template<class T_wave, class T_val>
inline T_val Spectrum_T<T_wave, T_val>::luminance() const
{
  uint  norm = 0;
  T_val res  = T_val(0);
  for (uint i = 0; i < _values.size(); i++)
  {
    res += _values[i];
    norm++;
  }
  if (norm > 0)
  {
    return res / T_val(norm);
  }
  return T_val(0);
}

template<class T_wave, class T_val>
inline T_val Spectrum_T<T_wave, T_val>::avgCmp() const
{
  return luminance();
}

template<class T_wave, class T_val>
size_t Spectrum_T<T_wave, T_val>::size() const
{
  //values and wavelengths have the same size
  return _values.size();
}

template<class T_wave, class T_val>
std::vector<T_wave> const &Spectrum_T<T_wave, T_val>::wavelengths() const
{
  return _wavelengths;
}

template<class T_wave, class T_val>
std::vector<T_wave> &Spectrum_T<T_wave, T_val>::wavelengths()
{
  return _wavelengths;
}

template<class T_wave, class T_val>
std::vector<T_val> const &Spectrum_T<T_wave, T_val>::values() const
{
  return _values;
}

template<class T_wave, class T_val>
std::vector<T_val> &Spectrum_T<T_wave, T_val>::values()
{
  return _values;
}

/*
template <class T_wave, class T_val>
T Spectrum_T<T>::findValue(uint wavelength)const
{
  for (uint i = 0; i < _wavelengths.size(); i++)
  {
    if (_wavelengths[i] == wavelength)
      return _values[i];
  }
  return T(0);
}
*/

template<class T_wave, class T_val>
inline T_val &Spectrum_T<T_wave, T_val>::operator[](std::size_t const index)
{
  return _values[index];
}

template<class T_wave, class T_val>
inline T_val const &Spectrum_T<T_wave, T_val>::operator[](std::size_t const index) const
{
  return _values[index];
}

template<class T_wave, class T_val>
T_val Spectrum_T<T_wave, T_val>::findLinearyInterpolatedValue(T_wave wavelength, int upper_index) const
{
  if (_wavelengths.size() == 0) return T_val(0);

  std::size_t i;
  if (upper_index == 0)
  {
    //find nearest upper wavelength if not provided
    i = 0;
    while (i < _wavelengths.size() && wavelength > _wavelengths[i])
    {
      i++;
    }
  }
  else
    i = upper_index;

  //out of range
  if (i >= _wavelengths.size()) return T_val(0);

  if (i == 0) return (wavelength == _wavelengths[i]) ? _values[i] : T_val(0);

  //linear interpolation
  double factor = double(wavelength - _wavelengths[i - 1]) / double(_wavelengths[i] - _wavelengths[i - 1]);
  if (std::is_integral<T_val>::value)
    return T_val(round(factor * _values[i] + (1.0 - factor) * _values[i - 1]));
  else
    return T_val(factor * _values[i] + (1.0 - factor) * _values[i - 1]);
}

template<class T_wave, class T_val>
inline T_val mrf::radiometry::Spectrum_T<T_wave, T_val>::normalizedIntegral() const
{
  T_val res(0);
  //dirac and empty spectrum return 0
  if (_values.size() < 2)
  {
    return res;
  }

  for (std::size_t i = 0; i < _values.size() - 1; i++)
  {
    //integrate using trapezoid method, divide by two at the end when normalizing the integral
    //formula is (f(a)+f(b)) * (b-a)/2
    res += (_values[i + 1] + _values[i]) * T_val(_wavelengths[i + 1] - _wavelengths[i]);
  }
  //normalize the integral and apply division by two to respect trapezoid integration method
  res = res / (T_val(2) * T_val(_wavelengths[_wavelengths.size() - 1] - _wavelengths[0]));
  return res;
}


template<class T_wave, class T_val>
T_val Spectrum_T<T_wave, T_val>::findIntegratedValue(T_wave wavelength, T_wave interval) const
{
  if (_wavelengths.size() == 0) return T_val(0);
  if (interval == 0) return findLinearyInterpolatedValue(wavelength);

  //find nearest upper wavelength
  std::size_t i = 0;
  while (i < _wavelengths.size() && wavelength > _wavelengths[i])
  {
    i++;
  }
  //out of range
  if (i >= _wavelengths.size()) return T_val(0);

  if (i == 0) return (wavelength == _wavelengths[i]) ? _values[i] : T_val(0);

  //We are now in a case were the interval may cover multiple bins. Need to find which.
  int integral_start = wavelength - interval / 2;
  int integral_end   = wavelength + interval / 2;

  //If integration interval is completely contained between 2 bin, no need to integrate (integral = linear interpolation)
  if (_wavelengths[i] > integral_end && _wavelengths[i - 1] < integral_start)
    return findLinearyInterpolatedValue(wavelength, i);

  std::size_t first_in = 0;
  //find lower bound interval (IN the interval for convenience)
  while (first_in < _wavelengths.size() && integral_start > _wavelengths[first_in])
  {
    first_in++;
  }
  std::size_t last_in = first_in;
  //find upper bound interval (IN the interval for convenience)
  while (last_in < _wavelengths.size() && integral_end > _wavelengths[last_in])
  {
    last_in++;
  }
  //-- as current last_in is first_out after while loop...
  if (last_in > 0) last_in--;

  //std::vector<float> node_values;
  //node_values.resize(first_out - first_in + 1);
  //node_values[0] = (findLinearyInterpolatedValue(integral_start, first_in) + _values[first_in]) * 0.5f;
  //node_values[first_out - first_in]
  //    = (findLinearyInterpolatedValue(integral_end, first_out) + _values[first_out - 1]) * 0.5f;
  //for (int j = first_in; j < first_out; ++j)
  //{
  //  node_values[j - first_in] = (_values[i] + _values[i + 1]) * 0.5f;
  //}
  //T_val ret_value(0.f);
  //float weight_sum = 0.f;
  //for (int j = first_in; j <= first_out; ++j)
  //{
  //  float cur_weight = fabs(wavelength - _wavelengths[i]) / float(_wavelengths[first_out] - _wavelengths[first_in]);
  //  //ret_value += cur_weight * findLinearyInterpolatedValue(wavelength, j);
  //  ret_value += cur_weight * node_values[i - first_in];
  //  weight_sum += cur_weight;
  //}

  float cur_weight = 1.f - fabs(wavelength - integral_start) / float(integral_end - integral_start);
  float weight_sum = cur_weight;

  //First subpart from integral_start to first_in
  T_val ret_value = (findLinearyInterpolatedValue(integral_start, first_in) + _values[first_in]) * 0.5f;

  //Middle accum between first_in and last_in, included
  for (int j = first_in; j < last_in; ++j)
  {
    cur_weight = 1.f - fabs(wavelength - _wavelengths[j]) / float(integral_end - integral_start);
    ret_value += cur_weight * (_values[j] + _values[j + 1]) * 0.5f;
    weight_sum += cur_weight;
  }

  //Last subpart last_in to integral_end
  cur_weight = 1.f - fabs(wavelength - integral_end) / float(integral_end - integral_start);
  ret_value  = (findLinearyInterpolatedValue(integral_end, last_in + 1) + _values[last_in]) * 0.5f;

  return ret_value / weight_sum;
}


//External Operator
template<class T_wave, class T_val>
inline std::ostream &operator<<(std::ostream &os, mrf::radiometry::Spectrum_T<T_wave, T_val> const &s)
{
  for (size_t i = 0; i < s.size(); i++)
  {
    os << s.wavelengths()[i] << ": " << s.values()[i] << std::endl;
  }
  return os;
}
