/*
 *
 * author: Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

namespace mrf
{
namespace radiometry
{
enum SpectrumType
{
  SPECTRUM_NONE       = 0,                         // 0b0000
  SPECTRUM_EMISSIVE   = 2,                         // 0b0001,
  SPECTRUM_REFLECTIVE = 4,                         // 0b0010,
  SPECTRUM_BISPECTRAL = 8 | SPECTRUM_REFLECTIVE,   // 0b0110,
  SPECTRUM_POLARISED  = 16,                        // 0b1000
};

inline SpectrumType operator|(SpectrumType a, SpectrumType b)
{
  return static_cast<SpectrumType>(static_cast<int>(a) | static_cast<int>(b));
}

inline SpectrumType operator^(SpectrumType a, SpectrumType b)
{
  return static_cast<SpectrumType>(static_cast<int>(a) ^ static_cast<int>(b));
}

inline bool isReflective(SpectrumType s)
{
  return (s & SPECTRUM_REFLECTIVE) == SPECTRUM_REFLECTIVE;
}

inline bool isEmissive(SpectrumType s)
{
  return (s & SPECTRUM_EMISSIVE) == SPECTRUM_EMISSIVE;
}

inline bool isBispectral(SpectrumType s)
{
  return (s & SPECTRUM_BISPECTRAL) == SPECTRUM_BISPECTRAL;
}

inline bool isPolarised(SpectrumType s)
{
  return (s & SPECTRUM_POLARISED) == SPECTRUM_POLARISED;
}

}   // namespace radiometry
}   // namespace mrf
