// ==========================================================================
//Halton sequence computation
// ==========================================================================
// (C)opyright:
//
//   Max-Planck-Institut fuer Informatik
//   Im Stadtwald, 66123 Saarbruecken
//   http://www.mpi-sb.mpg.de
//
// Creator: mcstammi (Marc Stamminger)
// Email:   mcstammi@mpi-sb.mpg.de
// ==========================================================================
// Integrated to LAIR by xgranier (Xavier Granier)
// Integrated to MRF by Romain Pacanowski

#pragma once

#ifdef _WIN32
// this header file must be included before all the others
#  include <windows.h>
#endif

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//STL
#include <vector>

namespace mrf
{
namespace sampling
{
/** \briefHalton sequence computation */
class MRF_CORE_EXPORT Halton
{
public:
  static unsigned int       basis[];
  static const unsigned int bases;
  static std::vector<float> value[];
  static void               compute(const unsigned int i, const unsigned int j);
  static inline float       halton(const unsigned int i, const unsigned int j);
};

inline float Halton::halton(const unsigned int i, const unsigned int j)
{
  if (Halton::value[i].size() <= j || Halton::value[i][j] < 0.)
  {
    Halton::compute(i, j);
  }

  return Halton::value[i][j];
}


}   // namespace sampling
}   // namespace mrf
