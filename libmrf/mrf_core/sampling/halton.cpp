// ==========================================================================
// Halton sequence computation
// ==========================================================================
// (C)opyright:
//
//   Max-Planck-Institut fuer Informatik
//   Im Stadtwald, 66123 Saarbruecken
//   http://www.mpi-sb.mpg.de
//
// Creator: mcstammi (Marc Stamminger)
// Email:   mcstammi@mpi-sb.mpg.de
// ==========================================================================
// Integrated to LAIR by xgranier (Xavier Granier)
// Integrated to MRF by Romain Pacanowski

#include <mrf_core/sampling/halton.hpp>

namespace mrf
{
namespace sampling
{
using namespace std;


unsigned int       Halton::basis[] = {2, 3, 5, 7, 11, 13, 17, 19, 23, 29, 31, 37, 41, 43, 47};
const unsigned int Halton::bases   = sizeof(Halton::basis) / sizeof(unsigned int);
vector<float>      Halton::value[Halton::bases];

void Halton::compute(const unsigned int i, const unsigned int j)
{
  unsigned int b   = basis[i];
  unsigned int val = j + 1;

  float mant = 1.;
  float res  = 0.;
  while (val)
  {
    mant /= b;
    unsigned int digit = val % b;
    res += digit * mant;
    val /= b;
  }
  if (j >= value[i].size())
  {
    value[i].resize(j + 1, -1.);
  }

  value[i][j] = res;
}


}   // namespace sampling
}   // namespace mrf
