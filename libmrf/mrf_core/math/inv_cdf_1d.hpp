/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/piecewise_cdf_1d.hpp>

#include <vector>
#include <cassert>
#include <cmath>
#include <algorithm>
#include <iterator>
#include <cstdlib>

namespace mrf
{
namespace math
{}


}   // namespace mrf