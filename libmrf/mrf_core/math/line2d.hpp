/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 *
 **/

#pragma once

#include <mrf_core/math/vec2.hpp>

namespace mrf
{
namespace math
{
/**
 * @brief      A class describing a 2D line
 *             The line is represented as y = a*x + b
 *             where a and b are attributes of the class.
 *
 *
 * @tparam     T     Precision of the stored attributes
 */
template<typename T, class VECTORIAL_TYPE>
class _Line2D
{
private:
  /**
   * _a as vectorial coefficients for Line
   */
  VECTORIAL_TYPE _a;

  /**
   * _b as vectorial coefficients for Line
   */
  VECTORIAL_TYPE _b;

public:
  inline _Line2D(VECTORIAL_TYPE const &a, VECTORIAL_TYPE const &b);

  /**
   * @brief      Construct a 2D Line from two points
   *
   * @param      x1, y1    The first point
   * @param      x2, y2    The second point
   */
  inline _Line2D(T x1, VECTORIAL_TYPE const &y1, T x2, VECTORIAL_TYPE const &y2);

  /**
   * Returns the value of the Line at a given abscissa
   */
  inline VECTORIAL_TYPE operator()(T x) const;
  inline VECTORIAL_TYPE valueAt(T x) const;

  inline VECTORIAL_TYPE const &a() const;
  inline VECTORIAL_TYPE const &b() const;
};




template<typename T, class VECTORIAL_TYPE>
inline _Line2D<T, VECTORIAL_TYPE>::_Line2D(VECTORIAL_TYPE const &a, VECTORIAL_TYPE const &b): _a(a)
                                                                                            , _b(b)
{}


template<typename T, class VECTORIAL_TYPE>
inline _Line2D<T, VECTORIAL_TYPE>::_Line2D(T x1, VECTORIAL_TYPE const &y1, T x2, VECTORIAL_TYPE const &y2)
  : _a((y2 - y1) / (x2 - x1))
  , _b(y2 - _a * x2)
{
  assert(_Math<T>::absVal(x2 - x1) > 0.0);
  //assert( (y1 - _a*x1) == _b );
}

template<typename T, class VECTORIAL_TYPE>
inline VECTORIAL_TYPE _Line2D<T, VECTORIAL_TYPE>::operator()(T x) const
{
  return valueAt(x);
}

template<typename T, class VECTORIAL_TYPE>
inline VECTORIAL_TYPE _Line2D<T, VECTORIAL_TYPE>::valueAt(T x) const
{
  return _a * x + _b;
}

template<typename T, class VECTORIAL_TYPE>
inline VECTORIAL_TYPE const &_Line2D<T, VECTORIAL_TYPE>::a() const
{
  return _a;
}

template<typename T, class VECTORIAL_TYPE>
inline VECTORIAL_TYPE const &_Line2D<T, VECTORIAL_TYPE>::b() const
{
  return _b;
}




}   // namespace math
}   // namespace mrf
