
//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::identity()
{
  return Quat(0, 0, 0, 1);
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T>::Quat(): _vec(0, 0, 0)
                      , _scalar(1)
{}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T>::Quat(const T &x, const T &y, const T &z, const T &w): _vec(x, y, z)
                                                                    , _scalar(w)
{}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T>::Quat(const Vec3<T> &vec, const T &scalar): _vec(vec)
                                                         , _scalar(scalar)
{}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T>::Quat(const Quat<T> &quat): _vec(quat._vec)
                                         , _scalar(quat._scalar)
{}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T>::Quat(const T vec[])
{
  _vec    = Vec3<T>(vec);
  _scalar = vec[3];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Quat<T>::getVec()
{
  return _vec;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Quat<T>::getX()
{
  return _vec(0);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Quat<T>::getY()
{
  return _vec(1);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Quat<T>::getZ()
{
  return _vec(2);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Quat<T>::getW()
{
  return _scalar;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const Vec3<T> &Quat<T>::getVec() const
{
  return _vec;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Quat<T>::getX() const
{
  return _vec(0);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Quat<T>::getY() const
{
  return _vec(1);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Quat<T>::getZ() const
{
  return _vec(2);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Quat<T>::getW() const
{
  return _scalar;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromAxisAngle(const Vec3<T> &axis, const T &angle)
{
  T halfAngle = (T)(0.5) * angle;
  T sina      = (T)(sin(halfAngle));

  _scalar = (T)(cos(halfAngle));
  _vec(0) = sina * axis(0);
  _vec(1) = sina * axis(1);
  _vec(2) = sina * axis(2);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromAxisCos(const Vec3<T> &axis, const T &cosAngle)
{
  T sina  = (T)sqrt((T)(0.5) * (1 - cosAngle));
  _scalar = (T)sqrt((T)(0.5) * (1 + cosAngle));
  _vec(0) = sina * axis(0);
  _vec(1) = sina * axis(1);
  _vec(2) = sina * axis(2);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromAxes(const Vec3<T> &x, const Vec3<T> &y, const Vec3<T> &z)
{
  T   tr, s;
  int i, j, k;

  int nxt[3] = {1, 2, 0};

  tr = x(0) + y(1) + z(2);

  // check the diagonal
  if (tr > 0.0)
  {
    s       = (T)sqrt(tr + 1);
    _scalar = s * (T)0.5;
    s       = (T)0.5 / s;
    _vec(0) = (y(2) - z(1)) * s;
    _vec(1) = (z(0) - x(2)) * s;
    _vec(2) = (x(1) - y(0)) * s;
  }
  else
  {
    // diagonal is negative
    i = 0;
    if (y(1) > x(0))
    {
      i = 1;
    }

    T m[3][3];
    m[0][0] = x(0);
    m[0][1] = y(0);
    m[0][2] = z(0);
    m[1][0] = x(1);
    m[1][1] = y(1);
    m[1][2] = z(1);
    m[2][0] = x(2);
    m[2][1] = y(2);
    m[2][2] = z(2);

    if (z(2) > m[i][i])
    {
      i = 2;
    }

    j = nxt[i];
    k = nxt[j];

    s = (T)sqrt(m[i][i] - m[j][j] - m[k][k] + 1);

    _vec(i) = s * (T)0.5;

    if (s != 0.0)
    {
      s = (T)0.5 / s;
    }

    _scalar = (m[k][j] - m[j][k]) * s;
    _vec(j) = (m[i][j] + m[j][i]) * s;
    _vec(k) = (m[i][k] + m[k][i]) * s;
  }
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromEulerXYZ(const Vec3<T> &angles)
{
  Quat<T> temp;

  fromAxisAngle(Vec3<T>(1, 0, 0), angles(0));
  temp.fromAxisAngle(Vec3<T>(0, 1, 0), angles(1));
  (*this) *= temp;
  temp.fromAxisAngle(Vec3<T>(0, 0, 1), angles(2));
  (*this) *= temp;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromEulerXZY(const Vec3<T> &angles)
{
  Quat<T> temp;

  fromAxisAngle(Vec3<T>(1, 0, 0), angles(0));
  temp.fromAxisAngle(Vec3<T>(0, 0, 1), angles(2));
  (*this) *= temp;
  temp.fromAxisAngle(Vec3<T>(0, 1, 0), angles(1));
  (*this) *= temp;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromEulerYXZ(const Vec3<T> &angles)
{
  Quat<T> temp;

  fromAxisAngle(Vec3<T>(0, 1, 0), angles(1));
  temp.fromAxisAngle(Vec3<T>(1, 0, 0), angles(0));
  (*this) *= temp;
  temp.fromAxisAngle(Vec3<T>(0, 0, 1), angles(2));
  (*this) *= temp;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromEulerYZX(const Vec3<T> &angles)
{
  Quat<T> temp;

  fromAxisAngle(Vec3<T>(0, 1, 0), angles(1));
  temp.fromAxisAngle(Vec3<T>(0, 0, 1), angles(2));
  (*this) *= temp;
  temp.fromAxisAngle(Vec3<T>(1, 0, 0), angles(0));
  (*this) *= temp;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromEulerZXY(const Vec3<T> &angles)
{
  Quat<T> temp;

  fromAxisAngle(Vec3<T>(0, 0, 1), angles(2));
  temp.fromAxisAngle(Vec3<T>(1, 0, 0), angles(0));
  (*this) *= temp;
  temp.fromAxisAngle(Vec3<T>(0, 1, 0), angles(1));
  (*this) *= temp;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromEulerZYX(const Vec3<T> &angles)
{
  Quat<T> temp;

  fromAxisAngle(Vec3<T>(0, 0, 1), angles(2));
  temp.fromAxisAngle(Vec3<T>(0, 1, 0), angles(1));
  (*this) *= temp;
  temp.fromAxisAngle(Vec3<T>(1, 0, 0), angles(0));
  (*this) *= temp;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromTwoVecs(const Vec3<T> &vFrom, const Vec3<T> &vTo)
{
  Vec3<T> axis = vFrom.cross(vTo);

  T len = axis.length();

  if (len < 0.0001)
  {
    _vec    = Vec3<T>::zero();
    _scalar = 1;

    return;
  }

  axis *= (T)(1) / len;

  T cosAngle = vFrom.dot(vTo) / (vFrom.length() * vTo.length());

  //    BG_DEBUG(" angle: %.2f", acos( cosAngle ) );
  fromAxisCos(axis, cosAngle);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromYawPitchRollValues(const T &_yaw, const T &_pitch, const T &_roll)
{
  T y2 = _yaw / 2.0;
  T p2 = _pitch / 2.0;
  T r2 = _roll / 2.0;

  T cosy2 = cos(y2);
  T siny2 = sin(y2);
  T cosp2 = cos(p2);
  T sinp2 = sin(p2);
  T cosr2 = cos(r2);
  T sinr2 = sin(r2);

  _scalar = cosy2 * cosp2 * cosr2 + siny2 * sinp2 * sinr2;
  _vec(0) = cosy2 * cosp2 * sinr2 - siny2 * sinp2 * cosr2;
  _vec(1) = cosy2 * sinp2 * cosr2 + siny2 * cosp2 * sinr2;
  _vec(2) = siny2 * cosp2 * cosr2 - cosy2 * sinp2 * sinr2;
}


//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::fromMatrix(const Mat4<T> &mat)
{
  T trace = mat(0, 0) + mat(1, 1) + mat(2, 2) + (T)1;

  if (trace > T(0))
  {
    T s = (T)(0.5 / sqrt(trace));

    _vec(0) = (T)((mat(2, 1) - mat(1, 2)) * s);
    _vec(1) = (T)((mat(0, 2) - mat(2, 0)) * s);
    _vec(2) = (T)((mat(1, 0) - mat(0, 1)) * s);

    _scalar = (T)(0.25 / s);
  }
  else   //( trace <= (T)0 )
  {
    if ((mat(0, 0) > mat(1, 1)) & (mat(0, 0) > mat(2, 2)))
    {
      T s = (T)(sqrt(1 + mat(0, 0) - mat(1, 1) - mat(2, 2)) * 2);

      _vec(0) = (T)(0.25 * s);
      _vec(1) = (T)((mat(0, 1) + mat(1, 0)) / s);
      _vec(2) = (T)((mat(0, 2) + mat(2, 0)) / s);

      _scalar = (T)((mat(1, 2) - mat(2, 1)) / s);
    }
    else if (mat(1, 1) > mat(2, 2))
    {
      T s = (T)(sqrt(1 + mat(1, 1) - mat(0, 0) - mat(2, 2)) * 2);

      _vec(0) = (T)((mat(0, 1) + mat(1, 0)) / s);
      _vec(1) = (T)(0.25 * s);
      _vec(2) = (T)((mat(1, 2) + mat(2, 1)) / s);

      _scalar = (T)((mat(0, 2) - mat(2, 0)) / s);
    }
    else
    {
      T s = (T)(sqrt(1 + mat(2, 2) - mat(0, 0) - mat(1, 1)) * 2);

      _vec(0) = (T)((mat(0, 2) + mat(2, 0)) / s);
      _vec(1) = (T)((mat(1, 2) + mat(2, 1)) / s);
      _vec(2) = (T)(0.25 * s);

      _scalar = (T)((mat(0, 1) - mat(1, 0)) / s);
    }
  }
}


//------------------------------------------------------------------------------
//!
template<class T>
inline T Quat<T>::norm() const
{
  return _vec.sqrLength() + _scalar * _scalar;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Quat<T>::dot(const Quat<T> &quat) const
{
  return _vec.dot(quat._vec) + _scalar * quat._scalar;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> &Quat<T>::negateEq()
{
  _vec.negateEq();
  _scalar = -_scalar;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::conjugate() const
{
  return Quat<T>(-_vec(0), -_vec(1), -_vec(2), _scalar);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> &Quat<T>::conjugateEq()
{
  _vec.negateEq();
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::inverse() const
{
  T div = (T)1 / norm();

  return Quat<T>(_vec(0) * -div, _vec(1) * -div, _vec(2) * -div, _scalar * div);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> &Quat<T>::inverseEq()
{
  T div = (T)1 / norm();
  _vec(0) *= -div;
  _vec(1) *= -div;
  _vec(2) *= -div;
  _scalar *= div;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Mat4<T> Quat<T>::getMatrix() const
{
  // calculate coefficients
  T x2 = _vec(0) + _vec(0);
  T y2 = _vec(1) + _vec(1);
  T z2 = _vec(2) + _vec(2);
  T xx = _vec(0) * x2;
  T xy = _vec(0) * y2;
  T xz = _vec(0) * z2;
  T yy = _vec(1) * y2;
  T yz = _vec(1) * z2;
  T zz = _vec(2) * z2;
  T wx = _scalar * x2;
  T wy = _scalar * y2;
  T wz = _scalar * z2;

  return Mat4<T>(
      1 - (yy + zz),
      xy - wz,
      xz + wy,
      0,
      xy + wz,
      1 - (xx + zz),
      yz - wx,
      0,
      xz - wy,
      yz + wx,
      1 - (xx + yy),
      0,
      0,
      0,
      0,
      1);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Quat<T>::getAxes(Vec3<T> &x, Vec3<T> &y, Vec3<T> &z) const
{
  // calculate coefficients
  T x2 = _vec(0) + _vec(0);
  T y2 = _vec(1) + _vec(1);
  T z2 = _vec(2) + _vec(2);
  T xx = _vec(0) * x2;
  T xy = _vec(0) * y2;
  T xz = _vec(0) * z2;
  T yy = _vec(1) * y2;
  T yz = _vec(1) * z2;
  T zz = _vec(2) * z2;
  T wx = _scalar * x2;
  T wy = _scalar * y2;
  T wz = _scalar * z2;

  x(0) = 1 - (yy + zz);
  x(1) = xy + wz;
  x(2) = xz - wy;

  y(0) = xy - wz;
  y(1) = 1 - (xx + zz);
  y(2) = yz + wx;

  z(0) = xz + wy;
  z(1) = yz - wx;
  z(2) = 1 - (xx + yy);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Quat<T>::getAxisX() const
{
  // calculate coefficients
  T y2 = _vec(1) + _vec(1);
  T z2 = _vec(2) + _vec(2);
  T xy = _vec(0) * y2;
  T xz = _vec(0) * z2;
  T yy = _vec(1) * y2;
  T zz = _vec(2) * z2;
  T wy = _scalar * y2;
  T wz = _scalar * z2;

  return Vec3<T>(1 - (yy + zz), xy + wz, xz - wy);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Quat<T>::getAxisY() const
{
  // calculate coefficients
  T x2 = _vec(0) + _vec(0);
  T y2 = _vec(1) + _vec(1);
  T z2 = _vec(2) + _vec(2);
  T xx = _vec(0) * x2;
  T xy = _vec(0) * y2;
  T yz = _vec(1) * z2;
  T zz = _vec(2) * z2;
  T wx = _scalar * x2;
  T wz = _scalar * z2;

  return Vec3<T>(xy - wz, 1 - (xx + zz), yz + wx);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Quat<T>::getAxisZ() const
{
  // calculate coefficients
  T x2 = _vec(0) + _vec(0);
  T y2 = _vec(1) + _vec(1);
  T z2 = _vec(2) + _vec(2);
  T xx = _vec(0) * x2;
  T xz = _vec(0) * z2;
  T yy = _vec(1) * y2;
  T yz = _vec(1) * z2;
  T wx = _scalar * x2;
  T wy = _scalar * y2;

  return Vec3<T>(xz + wy, yz - wx, 1 - (xx + yy));
}

template<class T>
void Quat<T>::getEulerAngles(T &_yaw, T &_pitch, T &_roll) const
{
  T r1 = 2 * (_vec(0) * _scalar + _vec(2) * _vec(1));
  T r2 = -SQUARE(_vec(0)) - SQUARE(_vec(1)) + SQUARE(_vec(2)) + SQUARE(_scalar);
  T p1 = -2 * (-_vec(1) * _scalar + _vec(0) * _vec(2));
  T y1 = 2 * (_vec(0) * _vec(1) + _vec(2) * _scalar);
  T y2 = SQUARE(_vec(0)) - SQUARE(_vec(1)) - SQUARE(_vec(2)) + SQUARE(_scalar);

  _roll  = (T)atan2(r1, r2);
  _pitch = (T)asin(p1);
  _yaw   = (T)atan2(y1, y2);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::slerp(const Quat<T> &quat, T a) const
{
  T cosa = dot(quat);

  T s1;
  T s2;
  T flip = 1;

  if (cosa < 0)
  {
    cosa = -cosa;
    flip = -1;
  }

  // Linear interpolation, for near orientation.
  if (1 - cosa < 0.0001)
  {
    s1 = 1 - a;
    s2 = a * flip;
  }
  else
  {
    T angle   = acos(cosa);
    T sina    = sin(angle);
    T invsina = 1 / sina;
    s1        = sin((1 - a) * angle) * invsina;
    s2        = sin(a * angle) * invsina * flip;
  }


  return *this * s1 + quat * s2;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::squad(const Quat<T> &, T) const
{
  //TODO: Implement ME ???
  assert(0);
  return Quat<T>();
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::operator+(const Quat<T> &quat) const
{
  return Quat<T>(_vec + quat._vec, _scalar + quat._scalar);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::operator-(const Quat<T> &quat) const
{
  return Quat<T>(_vec - quat._vec, _scalar - quat._scalar);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::operator-() const
{
  return Quat<T>(-_vec, -_scalar);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::operator*(const Quat<T> &quat) const
{
  T v0 = (_vec(2) - _vec(1)) * (quat._vec(1) - quat._vec(2));
  T v1 = (_scalar + _vec(0)) * (quat._scalar + quat._vec(0));
  T v2 = (_scalar - _vec(0)) * (quat._vec(1) + quat._vec(2));
  T v3 = (_vec(2) + _vec(1)) * (quat._scalar - quat._vec(0));
  T v4 = (_vec(2) - _vec(0)) * (quat._vec(0) - quat._vec(1));
  T v5 = (_vec(2) + _vec(0)) * (quat._vec(0) + quat._vec(1));
  T v6 = (_scalar + _vec(1)) * (quat._scalar - quat._vec(2));
  T v7 = (_scalar - _vec(1)) * (quat._scalar + quat._vec(2));
  T v8 = v5 + v6 + v7;
  T v9 = (v4 + v8) / 2;

  return Quat<T>(v1 + v9 - v8, v2 + v9 - v7, v3 + v9 - v6, v0 + v9 - v5);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::operator*(const T &val) const
{
  return Quat<T>(_vec * val, _scalar * val);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> Quat<T>::operator/(const T &val) const
{
  return Quat<T>(_vec / val, _scalar / val);
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Quat<T>::operator*(const Vec3<T> &vec) const
{
  // calculate coefficients
  T x2 = _vec(0) + _vec(0);
  T y2 = _vec(1) + _vec(1);
  T z2 = _vec(2) + _vec(2);
  T xx = _vec(0) * x2;
  T xy = _vec(0) * y2;
  T xz = _vec(0) * z2;
  T yy = _vec(1) * y2;
  T yz = _vec(1) * z2;
  T zz = _vec(2) * z2;
  T wx = _scalar * x2;
  T wy = _scalar * y2;
  T wz = _scalar * z2;

  return Vec3<T>(
      vec(0) * (1 - (yy + zz)) + vec(1) * (xy - wz) + vec(2) * (xz + wy),
      vec(0) * (xy + wz) + vec(1) * (1 - (xx + zz)) + vec(2) * (yz - wx),
      vec(0) * (xz - wy) + vec(1) * (yz + wx) + vec(2) * (1 - (xx + yy)));
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> &Quat<T>::operator+=(const Quat<T> &quat)
{
  _vec += quat._vec;
  _scalar += quat._scalar;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> &Quat<T>::operator-=(const Quat<T> &quat)
{
  _vec -= quat._vec;
  _scalar -= quat._scalar;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> &Quat<T>::operator*=(const Quat<T> &quat)
{
  T v0 = (_vec(2) - _vec(1)) * (quat._vec(1) - quat._vec(2));
  T v1 = (_scalar + _vec(0)) * (quat._scalar + quat._vec(0));
  T v2 = (_scalar - _vec(0)) * (quat._vec(1) + quat._vec(2));
  T v3 = (_vec(2) + _vec(1)) * (quat._scalar - quat._vec(0));
  T v4 = (_vec(2) - _vec(0)) * (quat._vec(0) - quat._vec(1));
  T v5 = (_vec(2) + _vec(0)) * (quat._vec(0) + quat._vec(1));
  T v6 = (_scalar + _vec(1)) * (quat._scalar - quat._vec(2));
  T v7 = (_scalar - _vec(1)) * (quat._scalar + quat._vec(2));
  T v8 = v5 + v6 + v7;
  T v9 = (v4 + v8) / 2;

  _vec(0) = v1 + v9 - v8;
  _vec(1) = v2 + v9 - v7;
  _vec(2) = v3 + v9 - v6;
  _scalar = v0 + v9 - v5;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> &Quat<T>::operator*=(const T &val)
{
  _vec *= val;
  _scalar *= val;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> &Quat<T>::operator/=(const T &val)
{
  _vec /= val;
  _scalar /= val;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Quat<T> &Quat<T>::operator=(const Quat<T> &quat)
{
  _vec    = quat._vec;
  _scalar = quat._scalar;
  return *this;
}


//------------------------------------------------------------------------------
//!
template<class T>
inline T &Quat<T>::operator()(int idx)
{
  if (idx == 3)
  {
    return _scalar;
  }
  return _vec(idx);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Quat<T>::operator()(int idx) const
{
  if (idx == 3)
  {
    return _scalar;
  }
  return _vec(idx);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Quat<T>::operator[](int idx)
{
  if (idx == 3)
  {
    return _scalar;
  }
  return _vec(idx);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Quat<T>::operator[](int idx) const
{
  if (idx == 3)
  {
    return _scalar;
  }
  return _vec(idx);
}


/*==============================================================================
TYPEDEF
==============================================================================*/

typedef Quat<float>  Quatf;
typedef Quat<double> Quatd;


template<class T>
inline std::ostream &operator<<(std::ostream &os, Quat<T> const &quat)
{
  os << " Quat [ vector: " << quat.getVec() << "; scalar:" << quat.getW() << " ] " << std::endl;
  return os;
}