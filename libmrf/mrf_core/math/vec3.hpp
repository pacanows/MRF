/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2015,2017
 *
 * Copyright INRIA 2022
 *
 **/

#pragma once

#include <mrf_core/mrf_cuda_support.hpp>

#include <cstring>
#include <iostream>
#include <cmath>

//#include <mrf_core/util/string_parsing.hpp>

namespace mrf
{
namespace math
{
/*==============================================================================
  CLASS Vec3
  ==============================================================================*/

//! 3D vector class.
template<typename T>
class _Math;

template<class T>
class Vec3
{
public:
  /*----- static methods -----*/

  inline static Vec3 zero();
  inline static Vec3 xaxis();
  inline static Vec3 yaxis();
  inline static Vec3 zaxis();

  inline static Vec3 gravity();

  inline static T dot(Vec3<T> const &v0, Vec3<T> const &v1);

  static Vec3 generateOrthogonalWithProjection(Vec3 normal, Vec3 &tangent, Vec3 &bitangent);
  static Vec3 generateOrthogonalWithUV(Vec3 normal, Vec3 &tangent, Vec3 &bitangent);
  static Vec3 generateOrthogonalWithJCGT(Vec3 normal, Vec3 &tangent, Vec3 &bitangent);
  static Vec3 generateOrthogonalAsBRDFExplorer(Vec3 normal, Vec3 &tangent, Vec3 &bitangent);


  /*----- methods -----*/

  template<class S>
  Vec3(const Vec3<S> &vec)
  {
    _e[0] = (T)vec(0);
    _e[1] = (T)vec(1);
    _e[2] = (T)vec(2);
  }

  MRF_DEVICE_FUNC Vec3() { _e[0] = _e[1] = _e[2] = (T)0; }

  MRF_DEVICE_FUNC Vec3(const Vec3<T> &vec);
  MRF_DEVICE_FUNC Vec3(const Vec3<T> &_v1, const Vec3<T> &_v2);
  MRF_DEVICE_FUNC Vec3(const T e0, const T e1, const T e2);
  MRF_DEVICE_FUNC Vec3(const T vec[]);
  MRF_DEVICE_FUNC Vec3(const T e);   //all components to e

  ~Vec3() {}

  T *      ptr();
  const T *ptr() const;

  T *      getArray();
  const T *getArray() const;

  void setValues(const T _v1, const T _v2, const T _v3);
  void set(const T _v1, const T _v2, const T _v3);

  T length() const;
  T sqrLength() const;
  T norm() const;    //an alias for length
  T norm2() const;   //an alias for sqrLength

  T dot(const Vec3<T> &vec) const;

  MRF_DEVICE_FUNC Vec3 cross(const Vec3<T> &vec) const;
  MRF_DEVICE_FUNC Vec3 normal() const;
  MRF_DEVICE_FUNC Vec3 &normalEq();
  //alias for normalEq
  Vec3 &normalize();

  Vec3 &normalEq(const T length);
  Vec3 &negateEq();
  Vec3 &clampToMaxEq(const T &max);

  // Sets all components to their absolute value
  Vec3 &absVal();

  /**
   *  Set all component to their square value
   *  @return *this
   */
  Vec3 &squareEq();

  Vec3 square() const;

  Vec3 sqrt() const;


  /**
   * Generate Orthogonal does not garantee orthogonality between
   * three vectors but only of two (this, and returned value)
   *
   * To get thre orthogonal vectors, use one of the static methods below:
   *
   *  - generateOrthogonalWithProjection(...)
   *  - generateOrthogonalWithUV(...)
   *  - generateOrthogonalWithJCGT(...)
   *  - generateOrthogonalAsBRDFExplorer(...)
   **/
  Vec3 generateOrthogonal() const;

  /**
   * Round vector components to zero if they are below the epsilon threshold
   */
  void epsilonRoundZero(T const epsilon);


  //   T operator*( const Vec3<T>& _v ) const;

  /**
   * This is equivalent to the cross() method
   *
   *  Note:  use pow() method for per-component power function
   */
  Vec3 operator^(const Vec3<T> &_v) const;

  Vec3 operator+(const Vec3<T> &rhs) const;
  Vec3 operator+(const T &_v) const;
  Vec3 operator-(const Vec3<T> &rhs) const;
  Vec3 operator-(const T &_v) const;
  Vec3 operator-() const;
  Vec3 operator*(const T &rhs) const;
  Vec3 operator*(const Vec3<T> &rhs) const;
  Vec3 operator/(const T &rhs) const;
  Vec3 operator/(const Vec3<T> &rhs) const;

  Vec3 &operator+=(const Vec3<T> &rhs);
  Vec3 &operator+=(const T &_v);
  Vec3 &operator-=(const Vec3<T> &rhs);
  Vec3 &operator-=(const T &_v);
  Vec3 &operator*=(const T &rhs);
  Vec3 &operator*=(const Vec3<T> &rhs);
  Vec3 &operator/=(const T &rhs);
  Vec3 &operator/=(const Vec3<T> &rhs);
  Vec3 &operator=(const Vec3<T> &rsh);

  bool operator==(const Vec3<T> &rhs) const;
  bool operator!=(const Vec3<T> &rhs) const;

  bool operator>(const Vec3<T> &_v) const;
  bool operator>=(const Vec3<T> &_v) const;
  bool operator<(const Vec3<T> &_v) const;
  bool operator<=(const Vec3<T> &_v) const;

  T &      operator()(int idx);
  T const &operator()(int idx) const;

  T &      operator[](int idx);
  T const &operator[](int idx) const;

  // T& operator()( unsigned int idx );
  // T const & operator()( unsigned int idx ) const;

  // T& operator[]( unsigned int idx );
  // T const & operator[]( unsigned int idx ) const;


  void setX(const T &);
  void setY(const T &);
  void setZ(const T &);

  MRF_DEVICE_FUNC T x();
  MRF_DEVICE_FUNC T y();
  MRF_DEVICE_FUNC T z();

  MRF_DEVICE_FUNC const T &x() const;
  MRF_DEVICE_FUNC const T &y() const;
  MRF_DEVICE_FUNC const T &z() const;

  inline std::string info() const;

  /**
   *
   * Returns the mean of the three components
   **/
  inline T mean() const;
  inline T sum() const;


  /** Given a candidate vector, replace
      elements whose corresponding elements in Candidate are
      smaller.  This function is used for finding objects' bounds,
      among other things.
      @brief change the Vect3 D if we use D.minimize
  */
  inline void minimize(Vec3<T> const &Candidate);

  /** Given a candidate vector, replace
      elements whose corresponding elements in Candidate are
      larger.  This function is used for finding objects' bounds,
      among other things.
      @brief change the Vect3 D if we use D.maximize
  */
  inline void maximize(Vec3<T> const &Candidate);

  /* Apply a std::pow power function on each component:
   */
  inline Vec3<T> &pow(T const &power);

  /**
   * Returns a vector where  std::pow has been applied to each component
   */
  //inline Vec3<T> pow(T const &power) const;


  static inline Vec3<T> interpolate(Vec3<T> const &u, Vec3<T> const &v, T alpha = T(0.5));


  inline void clampToZero(T EPSILON);

  inline bool allPosOrNull(T EPSILON);

  inline bool isInfOrNan() const;
  inline bool isFinite() const;

  inline void setConstant(T cte_value);

private:
  /*----- data members -----*/

  T _e[3];
};

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::zero()
{
  return Vec3(0, 0, 0);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::xaxis()
{
  return Vec3(T(1), T(0), T(0));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::yaxis()
{
  return Vec3(T(0), T(1), T(0));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::zaxis()
{
  return Vec3(T(0), T(0), T(1));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::gravity()
{
  return Vec3(T(0), T(0), T(-9.8));
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::generateOrthogonalWithUV(Vec3 n, Vec3 &t, Vec3 &bt)
{
  //TODO recode me !!!!
  if ((n._e[0] < n._e[1]) && (n._e[0] < n._e[2]))
  {
    t = Vec3(0.f, n._e[2], -n._e[1]);
  }
  else if (n._e[1] < n._e[2])
  {
    t = Vec3(n._e[2], 0.f, -n._e[0]);
  }
  else
  {
    t = Vec3(-n._e[1], n._e[0], 0.f);
  }
  bt = n.cross(t);
  //TODO: void function ?
  return t;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::generateOrthogonalWithJCGT(Vec3 n, Vec3 &t, Vec3 &bt)
{
  if (n._e[2] < 0.)
  {
    const T a = T(1) / (T(1) - n._e[2]);
    const T b = n._e[0] * n._e[1] * a;
    t         = Vec3(T(1) - n._e[0] * n._e[0] * a, -b, n._e[0]);
    bt        = Vec3(b, n._e[1] * n._e[1] * a - T(1), -n._e[1]);
  }
  else
  {
    const T a = T(1) / (T(1) + n._e[2]);
    const T b = -n._e[0] * n._e[1] * a;
    t         = Vec3(T(1) - n._e[0] * n._e[0] * a, b, -n._e[0]);
    bt        = Vec3(b, T(1) - n._e[1] * n._e[1] * a, -n._e[1]);
  }
  //TODO: void function ?
  return t;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::generateOrthogonalWithProjection(Vec3 n, Vec3 &t, Vec3 &bt)
{
  Vec3  Y     = Vec3<T>::yaxis();
  Vec3  X     = Vec3<T>::xaxis();
  float YdotN = dot(Y, n);

  if (_Math<T>::absVal(YdotN) == T(1))
  {
    t  = Vec3<T>::xaxis();
    bt = n.cross(t);
  }
  else
  {
    bt = Y - n * YdotN;
    t  = bt.cross(n);
  }
  //TODO: void function ?
  return t;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::generateOrthogonalAsBRDFExplorer(Vec3 n, Vec3 &t, Vec3 &bt)
{
  t  = _Math<T>::absVal(n._e[0]) < T(0.999) ? Vec3<T>::xaxis() : Vec3<T>::yaxis();
  t  = (n.cross(t)).normalize();
  bt = (n.cross(t)).normalize();
  //TODO: void function ?
  return t;
}


//------------------------------------------------------------------------------


//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T>::Vec3(const Vec3<T> &vec)
{
  _e[0] = vec._e[0];
  _e[1] = vec._e[1];
  _e[2] = vec._e[2];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T>::Vec3(const Vec3<T> &_v1, const Vec3<T> &_v2)
{
  _e[0] = _v2._e[0] - _v1._e[0];
  _e[1] = _v2._e[1] - _v1._e[1];
  _e[2] = _v2._e[2] - _v1._e[2];
}


//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T>::Vec3(const T e0, const T e1, const T e2)
{
  _e[0] = e0;
  _e[1] = e1;
  _e[2] = e2;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T>::Vec3(const T vec[])
{
  memcpy(_e, vec, 3 * sizeof(T));
}

//------------------------------------------------------------------------------
//!

template<class T>
inline Vec3<T>::Vec3(const T e)
{
  _e[0] = e;
  _e[1] = e;
  _e[2] = e;
}



//------------------------------------------------------------------------------
//!
template<class T>
inline T *Vec3<T>::ptr()
{
  return _e;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T *Vec3<T>::ptr() const
{
  return _e;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T *Vec3<T>::getArray()
{
  return this->ptr();
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T *Vec3<T>::getArray() const
{
  return this->ptr();
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec3<T>::setValues(const T _v1, const T _v2, const T _v3)
{
  _e[0] = _v1;
  _e[1] = _v2;
  _e[2] = _v3;
}

template<class T>
inline void Vec3<T>::set(const T _v1, const T _v2, const T _v3)
{
  setValues(_v1, _v2, _v3);
}


//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec3<T>::length() const
{
  return (T)std::sqrt(_e[0] * _e[0] + _e[1] * _e[1] + _e[2] * _e[2]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec3<T>::sqrLength() const
{
  return _e[0] * _e[0] + _e[1] * _e[1] + _e[2] * _e[2];
}

template<class T>
inline T Vec3<T>::norm() const
{
  return length();
}

template<class T>
inline T Vec3<T>::norm2() const
{
  return sqrLength();
}


//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec3<T>::dot(const Vec3<T> &vec) const
{
  return _e[0] * vec._e[0] + _e[1] * vec._e[1] + _e[2] * vec._e[2];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::cross(const Vec3<T> &vec) const
{
  return Vec3<T>(
      _e[1] * vec._e[2] - _e[2] * vec._e[1],
      _e[2] * vec._e[0] - _e[0] * vec._e[2],
      _e[0] * vec._e[1] - _e[1] * vec._e[0]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::normal() const
{
  const T tmp = (T)1 / length();
  return Vec3<T>(_e[0] * tmp, _e[1] * tmp, _e[2] * tmp);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::normalEq()
{
  const T tmp = (T)1 / length();
  _e[0] *= tmp;
  _e[1] *= tmp;
  _e[2] *= tmp;
  return *this;
}


template<class T>
inline Vec3<T> &Vec3<T>::normalize()
{
  return normalEq();
}

template<class T>
inline Vec3<T> &Vec3<T>::absVal()
{
  _e[0] = mrf::math::_Math<T>::absVal(_e[0]);
  _e[1] = mrf::math::_Math<T>::absVal(_e[1]);
  _e[2] = mrf::math::_Math<T>::absVal(_e[2]);

  return *this;
}

template<class T>
inline Vec3<T> &Vec3<T>::squareEq()
{
  _e[0] = _e[0] * _e[0];
  _e[1] = _e[1] * _e[1];
  _e[2] = _e[2] * _e[2];
  return *this;
}

template<class T>
inline Vec3<T> Vec3<T>::square() const
{
  return Vec3<T>(_e[0] * _e[0], _e[1] * _e[1], _e[2] * _e[2]);
}

template<class T>
inline Vec3<T> Vec3<T>::sqrt() const
{
  return Vec3<T>(std::sqrt(_e[0]), std::sqrt(_e[1]), std::sqrt(_e[2]));
}




//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::normalEq(const T tlength)
{
  const T tmp = tlength / length();
  _e[0] *= tmp;
  _e[1] *= tmp;
  _e[2] *= tmp;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::negateEq()
{
  _e[0] = -_e[0];
  _e[1] = -_e[1];
  _e[2] = -_e[2];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::clampToMaxEq(const T &max)
{
  if (_e[0] > max)
  {
    _e[0] = max;
  }
  if (_e[1] > max)
  {
    _e[1] = max;
  }
  if (_e[2] > max)
  {
    _e[2] = max;
  }
  return *this;
}

template<class T>
inline Vec3<T> Vec3<T>::generateOrthogonal() const
{
  T const ax = _Math<T>::absVal(_e[0]);
  T const ay = _Math<T>::absVal(_e[1]);
  T const az = _Math<T>::absVal(_e[2]);
  if ((ax < ay) && (ax < az))
  {
    return Vec3<T>(T(0), _e[2], -_e[1]);
  }
  else if (ay < az)
  {
    return Vec3<T>(_e[2], T(0), -_e[0]);
  }
  else
  {
    return Vec3<T>(-_e[1], _e[0], T(0));
  }
}

template<class T>
inline void Vec3<T>::epsilonRoundZero(T const epsilon)
{
  // _e[0] = (std::abs(_e[0]) < epsilon) ? T(0) : _e[0];
  // _e[1] = (std::abs(_e[1]) < epsilon) ? T(0) : _e[1];
  // _e[2] = (std::abs(_e[2]) < epsilon) ? T(0) : _e[2];

  _e[0] = (_Math<T>::absVal(_e[0]) < epsilon) ? T(0) : _e[0];
  _e[1] = (_Math<T>::absVal(_e[1]) < epsilon) ? T(0) : _e[1];
  _e[2] = (_Math<T>::absVal(_e[2]) < epsilon) ? T(0) : _e[2];
}




//------------------------------------------------------------------------------
//!
//template< class T >
//inline T Vec3<T>::operator*( const Vec3<T>& _v ) const {
//  return this->dot(_v);
//}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::operator^(const Vec3<T> &_v) const
{
  return this->cross(_v);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::operator+(const Vec3<T> &rhs) const
{
  return Vec3<T>(_e[0] + rhs._e[0], _e[1] + rhs._e[1], _e[2] + rhs._e[2]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::operator+(const T &_v) const
{
  return Vec3<T>(_e[0] + _v, _e[1] + _v, _e[2] + _v);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::operator-(const Vec3<T> &rhs) const
{
  return Vec3<T>(_e[0] - rhs._e[0], _e[1] - rhs._e[1], _e[2] - rhs._e[2]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::operator-(const T &_v) const
{
  return Vec3<T>(_e[0] - _v, _e[1] - _v, _e[2] - _v);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::operator-() const
{
  return Vec3<T>(-_e[0], -_e[1], -_e[2]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::operator*(const T &rhs) const
{
  return Vec3<T>(_e[0] * rhs, _e[1] * rhs, _e[2] * rhs);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::operator*(const Vec3<T> &rhs) const
{
  return Vec3<T>(_e[0] * rhs._e[0], _e[1] * rhs._e[1], _e[2] * rhs._e[2]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::operator/(const T &rhs) const
{
  return Vec3<T>(_e[0] / rhs, _e[1] / rhs, _e[2] / rhs);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> Vec3<T>::operator/(const Vec3<T> &rhs) const
{
  return Vec3<T>(_e[0] / rhs._e[0], _e[1] / rhs._e[1], _e[2] / rhs._e[2]);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::operator+=(const Vec3<T> &rhs)
{
  _e[0] += rhs._e[0];
  _e[1] += rhs._e[1];
  _e[2] += rhs._e[2];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::operator+=(const T &_v)
{
  _e[0] += _v;
  _e[1] += _v;
  _e[2] += _v;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::operator-=(const Vec3<T> &rhs)
{
  _e[0] -= rhs._e[0];
  _e[1] -= rhs._e[1];
  _e[2] -= rhs._e[2];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::operator-=(const T &_v)
{
  _e[0] -= _v;
  _e[1] -= _v;
  _e[2] -= _v;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::operator*=(const T &rhs)
{
  _e[0] *= rhs;
  _e[1] *= rhs;
  _e[2] *= rhs;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::operator*=(const Vec3<T> &rhs)
{
  _e[0] *= rhs._e[0];
  _e[1] *= rhs._e[1];
  _e[2] *= rhs._e[2];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::operator/=(const T &rhs)
{
  _e[0] /= rhs;
  _e[1] /= rhs;
  _e[2] /= rhs;
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::operator/=(const Vec3<T> &rhs)
{
  _e[0] /= rhs._e[0];
  _e[1] /= rhs._e[1];
  _e[2] /= rhs._e[2];
  return *this;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> &Vec3<T>::operator=(const Vec3<T> &rhs)
{
  _e[0] = rhs._e[0];
  _e[1] = rhs._e[1];
  _e[2] = rhs._e[2];
  return *this;
}


//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec3<T>::operator==(const Vec3<T> &rhs) const
{
  return _e[0] == rhs._e[0] && _e[1] == rhs._e[1] && _e[2] == rhs._e[2];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec3<T>::operator!=(const Vec3<T> &rhs) const
{
  return !(*this == rhs);
  //_e[0] != rhs._e[0] || _e[1] != rhs._e[1] || _e[2] != rhs._e[2];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec3<T>::operator>(const Vec3<T> &_v) const
{
  return !(*this <= _v);
  //_e[0] > _v._e[0] && _e[1] > _v._e[1] && _e[2] > _v._e[2];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec3<T>::operator>=(const Vec3<T> &_v) const
{
  return _e[0] >= _v._e[0] && _e[1] >= _v._e[1] && _e[2] >= _v._e[2];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec3<T>::operator<(const Vec3<T> &_v) const
{
  return !(*this >= _v);
  //return _e[0] < _v._e[0] && _e[1] < _v._e[1] && _e[2] < _v._e[2];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline bool Vec3<T>::operator<=(const Vec3<T> &_v) const
{
  return _e[0] <= _v._e[0] && _e[1] <= _v._e[1] && _e[2] <= _v._e[2];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Vec3<T>::operator()(int idx)
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Vec3<T>::operator()(int idx) const
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T &Vec3<T>::operator[](int idx)
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Vec3<T>::operator[](int idx) const
{
  return _e[idx];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline Vec3<T> operator*(const T &val, const Vec3<T> &vec)
{
  return Vec3<T>(vec(0) * val, vec(1) * val, vec(2) * val);
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec3<T>::setX(const T &_v)
{
  _e[0] = _v;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec3<T>::setY(const T &_v)
{
  _e[1] = _v;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline void Vec3<T>::setZ(const T &_v)
{
  _e[2] = _v;
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec3<T>::x()
{
  return _e[0];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec3<T>::y()
{
  return _e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline T Vec3<T>::z()
{
  return _e[2];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Vec3<T>::x() const
{
  return _e[0];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Vec3<T>::y() const
{
  return _e[1];
}

//------------------------------------------------------------------------------
//!
template<class T>
inline const T &Vec3<T>::z() const
{
  return _e[2];
}


template<class T>
inline T Vec3<T>::mean() const
{
  return (_e[0] + _e[1] + _e[2]) / T(3);
}

template<class T>
inline T Vec3<T>::sum() const
{
  return (_e[0] + _e[1] + _e[2]);
}


template<class T>
inline void Vec3<T>::minimize(Vec3<T> const &candidate)
{
  if (candidate[0] < _e[0]) _e[0] = candidate[0];
  if (candidate[1] < _e[1]) _e[1] = candidate[1];
  if (candidate[2] < _e[2]) _e[2] = candidate[2];
}

template<class T>
inline void Vec3<T>::maximize(Vec3<T> const &candidate)
{
  if (candidate[0] > _e[0]) _e[0] = candidate[0];
  if (candidate[1] > _e[1]) _e[1] = candidate[1];
  if (candidate[2] > _e[2]) _e[2] = candidate[2];
}

template<class T>
inline Vec3<T> &Vec3<T>::pow(T const &power)
{
  _e[0] = std::pow(_e[0], power);
  _e[1] = std::pow(_e[1], power);
  _e[2] = std::pow(_e[2], power);

  return *this;
}

// RP: This cannot work since pow has to identical signature one returning
//     void the other a Vec3<T>
//
// template<class T>
// inline Vec3<T> Vec3<T>::pow(T const &power) const
// {
//   return Vec3<T>(std::pow(_e[0], power), std::pow(_e[1], power), std::pow(_e[2], power));
// }


//-------------------------------------------------------------------------
//!
template<class T>
inline T Vec3<T>::dot(Vec3<T> const &v0, Vec3<T> const &v1)
{
  return v0.dot(v1);
}

template<class T>
inline Vec3<T> Vec3<T>::interpolate(const Vec3<T> &u, const Vec3<T> &v, T alpha)
{
  return (u * (1.0 - alpha) + v * alpha);
}

#if 0
//EXTERNAL OPERATOR
// PEr component max and min oeprator
template<class T>
inline Vec3<T> max( Vec3<T> const & v0, Vec3<T> const & v1 )
{
  return Vec3<T>( std::max(v0[0], v1[0]),
                  std::max(v0[1], v1[1]),
                  std::max(v0[2], v1[2]) );
}

template<class T>
inline Vec3<T> min( Vec3<T> const & v0, Vec3<T> const & v1 )
{
  return Vec3<T>( std::min(v0[0], v1[0]),
                  std::min(v0[1], v1[1]),
                  std::min(v0[2], v1[2]) );
}
#endif




template<class T>
inline void Vec3<T>::clampToZero(T EPSILON)
{
  _e[0] = (_Math<T>::absVal(_e[0]) < EPSILON) ? T(0.0) : _e[0];
  _e[1] = (_Math<T>::absVal(_e[1]) < EPSILON) ? T(0.0) : _e[1];
  _e[2] = (_Math<T>::absVal(_e[2]) < EPSILON) ? T(0.0) : _e[2];
}

template<class T>
inline bool Vec3<T>::allPosOrNull(T EPSILON)
{
  return (_e[0] > -EPSILON) && (_e[1] > -EPSILON) && (_e[2] > -EPSILON);
}

template<class T>
inline bool Vec3<T>::isInfOrNan() const
{
  //
  double const e0 = static_cast<double>(_e[0]);
  double const e1 = static_cast<double>(_e[1]);
  double const e2 = static_cast<double>(_e[2]);

  return std::isnan(e0) || std::isnan(e1) || std::isnan(e2) || std::isinf(e0) || std::isinf(e1) || std::isinf(e2);
}


template<class T>
inline bool Vec3<T>::isFinite() const
{
  return !isInfOrNan();
}

template<class T>
inline void Vec3<T>::setConstant(T cte_value)
{
  setX(cte_value);
  setY(cte_value);
  setZ(cte_value);
}


/*==============================================================================
  TYPEDEF
  ==============================================================================*/

typedef Vec3<int>          Vec3i;
typedef Vec3<unsigned int> Vec3ui;
typedef Vec3<float>        Vec3f;
typedef Vec3<double>       Vec3d;
typedef Vec3<bool>         Vec3b;


//External Operator
template<class T>
inline std::ostream &operator<<(std::ostream &os, Vec3<T> const &v)
{
  return os << " [ " << v.x() << "; " << v.y() << "; " << v.z() << " ] ";
}

template<class T>
inline std::string Vec3<T>::info() const
{
  std::string info;

  info = " [ " + std::to_string(_e[0]) + "; " + std::to_string(_e[1]) + "; " + std::to_string(_e[2]) + " ] ";

  //info.append( mrf::util::StringParsing::numericsToString<T>(_e[0]));
  //info.append("; ");
  //info.append( mrf::util::StringParsing::numericsToString<T>(_e[1]));
  //info.append("; ");
  //info.append( mrf::util::StringParsing::numericsToString<T>(_e[2]));

  //info.append(" ] ");

  return info;
}



}   // namespace math
}   // namespace mrf

//#endif
