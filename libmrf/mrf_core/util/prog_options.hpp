/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2015,2017
 *
 **/

#pragma once

#include <stdexcept>
#include <iostream>

#include <mrf_core_dll.hpp>
#include <mrf_core/util/command_line.hpp>
#include <externals/termcolor/termcolor.hpp>

namespace mrf
{
namespace util
{
/**
 * @brief      This class represents one command-line argument or parameter of
 *             of an option.
 *             A program can have multiple command-line  option  and each option can have
 *             can have several parameters or arguments
 *
 *             Example:   my_program -option_1 parameter_1 parameter_2 parameter_3 -option_2
 */
class ProgArg
{
protected:
  bool _isMandatory;   // Stores whether this argument is Mandatory or not

  // -arg_name param_1 param_2 params_3
  std::vector<std::string> _parameters;   //The different string representing each one paramter.

  std::string _helpMessage;   // The help message

  unsigned int _nbParameter;   // The number of expected parameter for the option

  bool _wasProvided;   // Was the option provided on the command line ?

public:
  inline ProgArg(
      bool         is_mandatory = true,
      std::string  help_message = "",
      unsigned int nb_parameter = 0,
      bool         was_provided = false)
    : _isMandatory(is_mandatory)
    , _helpMessage(help_message)
    , _nbParameter(nb_parameter)
    , _wasProvided(was_provided)
  {}

  inline void setParameters(std::vector<std::string> const &params)
  {
    if (params.size() != _nbParameter)
    {
      std::string const message = " The nunber of parameter is wrong  option. Expected " + std::to_string(_nbParameter)
                                  + " Got " + std::to_string(params.size());

      throw std::logic_error(message);
    }

    _parameters.clear();

    for (unsigned int i = 0; i < params.size(); i++)
    {
      _parameters.push_back(params[i]);
    }

    _wasProvided = true;
  }


  inline void paramValue(unsigned int param_index, float &value_of_the_parameter) const
  {
    assert(param_index < _parameters.size());
    value_of_the_parameter = std::stof(_parameters[param_index]);
  }

  inline void paramValue(unsigned int param_index, double &value_of_the_parameter) const
  {
    assert(param_index < _parameters.size());
    value_of_the_parameter = std::stod(_parameters[param_index]);
  }

  inline void paramValue(unsigned int param_index, unsigned int &value_of_the_parameter) const
  {
    assert(param_index < _parameters.size());
    value_of_the_parameter = std::stoul(_parameters[param_index]);
  }

  inline void paramValue(unsigned int param_index, int &value_of_the_parameter) const
  {
    assert(param_index < _parameters.size());
    value_of_the_parameter = std::stoul(_parameters[param_index]);
  }

  inline void paramValue(unsigned int param_index, std::string &value_of_the_parameter) const
  {
    assert(param_index < _parameters.size());
    value_of_the_parameter = _parameters[param_index];
  }

  inline unsigned int nbParameter() const { return _nbParameter; }

  inline bool hasParameters() const { return _parameters.size() > 0; }

  inline std::string const &parameter(unsigned int param_index) const
  {
    assert(param_index < _parameters.size());
    return _parameters[param_index];
  }

  inline std::string const &helpMessage() const { return _helpMessage; }

  inline bool isMandatory() const { return _isMandatory; }

  inline bool wasProvided() const { return _wasProvided; }
};


/**
 * @brief      ProgOptions stores all command-line options of a program as well
 *             as each argument associated with the correspondig option
 *
 * Programmers should construct ProgOptions and ProgArg according
 * to what their application supports.
 *
 * Use Function validateProgramOptions( int argc, char** argv, mrf::ProgOptions & program_options )
 * in order to validate a give set of ProgOptions wrt. to the arguments (char** argv) passed to the program
 *
 */
class ProgOptions
{
private:
  //  -option_name_1 param_1_1 param_1_2 -option_name_2 param_2_1 -option_name_3
  std::map<std::string, ProgArg> _arguments;

  std::string _name_of_the_program;

public:
  inline ProgOptions(std::string const &name_of_the_program = ""): _name_of_the_program(name_of_the_program) {}

  inline bool hasOption(std::string const &option_name) const
  {
    auto it = _arguments.find(option_name);
    return !(it == _arguments.end());
  }


  inline ProgOptions &addOption(std::string const &option_name, ProgArg const &arg)
  {
    if (hasOption(option_name))
    {
      throw std::logic_error(" Option already exists: " + option_name);
    }

    _arguments[option_name] = arg;
    return *this;
  }

  inline ProgArg const &option(std::string const &option_name) const
  {
    if (hasOption(option_name))
    {
      auto it = _arguments.find(option_name);
      return it->second;
    }
    else
      throw std::runtime_error(" NO option named " + option_name + " was found");
    //return ProgArg();
  }


  inline std::map<std::string, ProgArg> const &options() const { return _arguments; }


  inline std::map<std::string, ProgArg> &options() { return _arguments; }

  inline void displayHelp() const
  {
    using namespace std;

    cout << termcolor::blue << "******** Help message for " << _name_of_the_program << "********" << endl;
    cout << " Usage is: " << _name_of_the_program << " mandatory_options  [optional_options] " << endl
         << " where mandatory_options are: " << endl;

    cout << termcolor::red;
    for (auto it = _arguments.begin(); it != _arguments.end(); ++it)
    {
      if (it->second.isMandatory())
      {
        cout << termcolor::red << it->first << ": " << termcolor::reset << it->second.helpMessage() << endl;
      }
    }
    cout << endl;
    cout << " and where [optional_options] are: " << endl;

    for (auto it = _arguments.begin(); it != _arguments.end(); ++it)
    {
      if (!it->second.isMandatory())
      {
        cout << termcolor::green << " [" << it->first << "] :" << termcolor::reset << it->second.helpMessage() << endl;
      }
    }

    cout << termcolor::blue << "***********************************************************" << endl;
    cout << termcolor::reset << endl;
  }
};



/**
 * @brief      Parse the command line and update the passed ProgOptions object according
 *             to the requested values and options stored in ProgOptions
 *
 *
 * @param[in]  argc             represents the number of argument on the command line
 * @param      argv             "array" of char* each representing an argument on the command line
 * @param      program_options  The program_options as defined by the program
 *
 * @return     true if the command line was correct according to the requested options specified
 *             in program_options, false otherwise.
 */
inline bool validateProgramOptions(int argc, char **argv, mrf::util::ProgOptions &program_options)
{
  using namespace std;

  try
  {
    // --> Valid CommandLineOptions for Manioc ?
    CommandLine cmd_line(argc, argv);

    //Check that the parsed command line is what is expected from the program
    for (auto it = program_options.options().begin(); it != program_options.options().end(); ++it)
    {
      if (cmd_line.hasOption("h") || cmd_line.hasOption("-h") || cmd_line.hasOption("help"))
      {
        // RP : DO NOT DISPLAY ANYTHING/ LET THE APPLICATION HANDLE THIS CASE
        //program_options.displayHelp();

        //Options are added to the program Options to be sure
        program_options = program_options.addOption("h", ProgArg(false, "Print this help message", 0, true));
        program_options = program_options.addOption("help", ProgArg(false, "Print this help message", 0, true));

        // ASKING FOR HELP ON COMMAND LINE IS VALID
        return true;
      }

      if (cmd_line.hasOption(it->first))
      {
        if (it->second.nbParameter() != cmd_line.option(it->first).nbParameter())
        {
          throw logic_error(
              "ERROR The Number of parameter for argument named " + it->first + " is not correct " + "Expected "
              + std::to_string(it->second.nbParameter()) + " Got "
              + std::to_string(cmd_line.option(it->first).nbParameter()));
        }
        else   // OK
        {
#ifdef MRF_PROGOPTIONS_DEBUG
          cout << __FILE__ << " Adding Option " << it->first << " to maniox_optionc with its paramter" << __LINE__
               << endl;
#endif

          //Adding parameters to program_options
          it->second.setParameters(cmd_line.option(it->first).parameters());
        }
      }
      else
      {
        if (it->second.isMandatory())   //is the argument mandatory ?
        {
          throw logic_error("ERROR One Mandatory argument named " + it->first + " was not provided ");
        }
      }

    }   //end of for-loop
  }
  catch (std::exception const &e)
  {
    cout << termcolor::red << e.what() << termcolor::reset << endl << endl;

    program_options.displayHelp();

    return false;
  }

  return true;

}   //end of validateProgramOptions


}   //namespace util

}   // namespace mrf
