/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016,2017
 *
 **/


#pragma once

#include <mrf_core/math/math.hpp>
#include <mrf_core/math/line2d.hpp>

#include <iostream>
#include <map>


namespace mrf
{
namespace util
{
template<class KEY_TYPE, class AMAP_TYPE, class ITERATOR_TYPE>
static inline void nearestIT(KEY_TYPE t_key, AMAP_TYPE const &map, ITERATOR_TYPE &n_it)
{
  using namespace mrf::math;

  // element which is >= targeted_key
  ITERATOR_TYPE upper_it = map.lower_bound(t_key);

  ITERATOR_TYPE lower_it = upper_it;
  lower_it--;

  //if( upper_it != map.end() && lower_it == map.end() ) //  < min_map
  if (upper_it == map.begin())   //  < min_map
  {
    n_it = upper_it;
  }
  else if (upper_it == map.end() && lower_it != map.end())   // > max_map
  {
    n_it = lower_it;
  }
  else if (upper_it != map.end() && lower_it != map.end())   // between two keys
  {
    KEY_TYPE const diff_upper = _Math<KEY_TYPE>::absVal(upper_it->first - t_key);
    KEY_TYPE const diff_lower = _Math<KEY_TYPE>::absVal(lower_it->first - t_key);

    if (diff_upper <= diff_lower)
    {
      n_it = upper_it;
    }
    else
    {
      n_it = lower_it;
    }
  }
  else   // Map empty?
  {
    assert(0);
  }
}

template<typename T, class DATA_TYPE>
class _OneDimMap
{
protected:
  std::map<T, DATA_TYPE> _map;

public:
  inline unsigned int size() const;

  typename std::map<T, DATA_TYPE>::const_iterator begin() const;
  typename std::map<T, DATA_TYPE>::const_iterator end() const;

  inline std::map<T, DATA_TYPE> const &map() const;
  inline std::map<T, DATA_TYPE> &      map();

  /*
   * Insert an element into the map
   **/
  inline bool insert(T key, DATA_TYPE const &data);

  DATA_TYPE &operator[](T key);

  /*
   * Returns the nearest element
   **/
  DATA_TYPE const &operator()(T key) const;
  void             nearest(T target_key, typename std::map<T, DATA_TYPE>::const_iterator &) const;
  T                nearestKey(T targeted_key) const;
  DATA_TYPE const &nearestValue(T targeted_key) const;

  /**
   * Returns the interpolated element for a given key
   * This is not a cyclic access
   **/
  DATA_TYPE linear(T targeted_key) const;

  /**
   * Returns the element which key is lesser or equal to targeted_key
   **/
  DATA_TYPE inf(T targeted_key) const;


  /**
   * Returns the interpolated element for a given key.
   * Key are assumed to be cyclic (first and last element
   **/
  //DATA_TYPE  cyclic( T targeted_key ) const;


  /**
   * Returns the two keys that bound the targeted_key if targeted_key
   * is < min_key or > max_key
   * key_1 = min_key (or max_key) as well as key_2 and weight is 0.5
   **/
  void linearKeys(T targeted_key, T &key_1, T &key_2, T &weight_key_1) const;


  /**
   * Compute the discreet derivative at a specific location
   * This will only work is the map has at least two parameters
   * and if targeted_key can be bounded by two keys belonging to the map
   **/
  //TODO: COMMENTED FOR NOW.
  //TODO: SEEMS BUGGED OR USED FOR A VERY SPECIFIC USAGE
  //TODO: CHECK IMPLEMENTATION
  //T derivative(T targeted_key) const;


  void keysAndValues(T targeted_key, T &key_inf, T &key_sup, T &value_inf, T &value_sup) const;


  //     template< class KEY_TYPE, class MAP_TYPE, class ITERATOR_TYPE>
  //     inline static void nearest( KEY_TYPE t_key , MAP_TYPE const & map, ITERATOR_TYPE & n_it );

  /**
   * @brief      Removes all-elements from the map
   */
  inline void clear();
};

template<typename T, class DATA_TYPE>
inline unsigned int _OneDimMap<T, DATA_TYPE>::size() const
{
  return static_cast<unsigned int>(_map.size());
}

template<typename T, class DATA_TYPE>
typename std::map<T, DATA_TYPE>::const_iterator _OneDimMap<T, DATA_TYPE>::begin() const
{
  return _map.begin();
}

template<typename T, class DATA_TYPE>
typename std::map<T, DATA_TYPE>::const_iterator _OneDimMap<T, DATA_TYPE>::end() const
{
  return _map.end();
}

template<typename T, class DATA_TYPE>
inline std::map<T, DATA_TYPE> const &_OneDimMap<T, DATA_TYPE>::map() const
{
  return _map;
}

template<typename T, class DATA_TYPE>
inline std::map<T, DATA_TYPE> &_OneDimMap<T, DATA_TYPE>::map()
{
  return _map;
}



template<typename T, class DATA_TYPE>
bool _OneDimMap<T, DATA_TYPE>::insert(T key, DATA_TYPE const &data)
{
  std::pair<typename std::map<T, DATA_TYPE>::iterator, bool> inserted = _map.insert(std::make_pair(key, data));
  return inserted.second;
}




template<typename T, class DATA_TYPE>
void _OneDimMap<T, DATA_TYPE>::nearest(T target_key, typename std::map<T, DATA_TYPE>::const_iterator &n_it) const
{
  nearestIT(target_key, _map, n_it);
}


template<typename T, class DATA_TYPE>
T _OneDimMap<T, DATA_TYPE>::nearestKey(T target_key) const
{
  typename std::map<T, DATA_TYPE>::const_iterator nearest_it;
  nearest(target_key, nearest_it);
  return nearest_it->first;
}

template<typename T, class DATA_TYPE>
DATA_TYPE const &_OneDimMap<T, DATA_TYPE>::nearestValue(T target_key) const
{
  typename std::map<T, DATA_TYPE>::const_iterator nearest_it;
  nearest(target_key, nearest_it);
  return nearest_it->second;
}

template<typename T, class DATA_TYPE>
DATA_TYPE const &_OneDimMap<T, DATA_TYPE>::operator()(T key) const
{
  typename std::map<T, DATA_TYPE>::const_iterator nearest_it;
  nearest(key, nearest_it);
  return nearest_it->second;
}


template<typename T, class DATA_TYPE>
DATA_TYPE &_OneDimMap<T, DATA_TYPE>::operator[](T key)
{
  return _map[key];
}



//------------------------------------------------------------------------------
// TODO:  ???
//------------------------------------------------------------------------------
// template< class KEY_TYPE, class DATA_TYPE >
// static inline DATA_TYPE const &
// linearValue( KEY_TYPE k, std::map<KEY_TYPE, DATA_TYPE> const & map )
// {

// }
//------------------------------------------------------------------------------
//
//------------------------------------------------------------------------------

template<typename T, class DATA_TYPE>
DATA_TYPE _OneDimMap<T, DATA_TYPE>::linear(T targeted_key) const
{
  using namespace mrf::math;

  //std::cout << "  targeted_key = " << targeted_key << std::endl ;


  // element which is >= targeted_key
  typename std::map<T, DATA_TYPE>::const_iterator upper_it = _map.lower_bound(targeted_key);
  typename std::map<T, DATA_TYPE>::const_iterator lower_it = upper_it;

  //    --lower_it;



  if (upper_it == _map.begin())   //  < min_map
  {
    // std::cout << "One Dim Map upper is at the end" << std::endl;
    // std::cout << " One Dim Map upper_it->first: " << upper_it->first << std::endl;

    if (_map.size() >= 2)   //Line Extrapolation
    {
      typename std::map<T, DATA_TYPE>::const_iterator up_upper_it = upper_it;
      up_upper_it++;

      // std::cout << "Linear interpolation by lower bound " << std::endl
      //          << " from " << upper_it->first << " " << upper_it->second << std::endl
      //          << " to " << up_upper_it->first << " " << up_upper_it->second << "####" << std::endl;

      T const          x1 = upper_it->first;
      T const          x2 = up_upper_it->first;
      DATA_TYPE const &y1 = upper_it->second;
      DATA_TYPE const &y2 = up_upper_it->second;

      _Line2D<T, DATA_TYPE> l(x1, y1, x2, y2);


      return l.valueAt(targeted_key);
    }
    else   // only one value. We cannot interpolate linearly. Fall-back to nearest
    {
      return upper_it->second;
    }
  }
  else   // upper_it != _map.begin()
  {
    lower_it--;

    if (upper_it == _map.end() && lower_it != _map.end())   // > max_map
    {
      // std::cout  << " One Dim Map  CASE Upper_it at the end " << std::endl;

      if (_map.size() >= 2)   // Line Extrapolation
      {
        typename std::map<T, DATA_TYPE>::const_iterator low_lower_it = lower_it;
        low_lower_it--;

        _Line2D<T, DATA_TYPE> l(low_lower_it->first, low_lower_it->second, lower_it->first, lower_it->second);
        return l.valueAt(targeted_key);
      }
      else /* Fall-back to nearest*/
      {
        return lower_it->second;
      }
    }
    else if (upper_it != _map.end() && lower_it != _map.end())   // between two keys
    {
      // std::cout  << "One Dim Map CASE Bilinear interpolation " << std::endl;

      T const diff_upper = _Math<T>::absVal(upper_it->first - targeted_key);
      T const diff_lower = _Math<T>::absVal(lower_it->first - targeted_key);


      T const il = _Math<T>::absVal(upper_it->first - lower_it->first);

      T const alpha = T(1.0) - (diff_upper / il);
      T const beta  = T(1.0) - (diff_lower / il);


      return (upper_it->second * alpha + lower_it->second * beta);
    }
    else   // Map empty?
    {
      throw std::runtime_error("VALUE NOT FOUND IN ONE DIM MAP");
      assert(false);
    }
  }
  assert(false);
  throw std::runtime_error("VALUE NOT FOUND IN ONE DIM MAP");
}



template<typename T, class DATA_TYPE>
DATA_TYPE _OneDimMap<T, DATA_TYPE>::inf(T targeted_key) const
{
  using namespace mrf::math;
  // element which is >= targeted_key
  typename std::map<T, DATA_TYPE>::const_iterator upper_it = _map.upper_bound(targeted_key);
  typename std::map<T, DATA_TYPE>::const_iterator lower_it = upper_it;
  --lower_it;

  if (lower_it == _map.end())
  {
    // Map empty or with one element?
    assert(0);
  }

  return lower_it->second;
}




template<typename T, class DATA_TYPE>
void _OneDimMap<T, DATA_TYPE>::linearKeys(T targeted_key, T &key_1, T &key_2, T &weight_key_1) const
{
  using namespace mrf::math;
  // element which is >= targeted_key
  typename std::map<T, DATA_TYPE>::const_iterator upper_it = _map.lower_bound(targeted_key);
  typename std::map<T, DATA_TYPE>::const_iterator lower_it = upper_it;
  --lower_it;

  if (upper_it == _map.begin())   //  < min_map
  {
    key_1        = upper_it->first;
    key_2        = key_1;
    weight_key_1 = T(0.5);
  }
  else if (upper_it == _map.end() && lower_it != _map.end())   // > max_map
  {
    key_1        = lower_it->first;
    key_2        = key_1;
    weight_key_1 = T(0.5);
  }
  else if (upper_it != _map.end() && lower_it != _map.end())   // between two keys
  {
    T const diff_upper = _Math<T>::absVal(upper_it->first - targeted_key);
    T const diff_lower = _Math<T>::absVal(lower_it->first - targeted_key);

    T const il    = _Math<T>::absVal(upper_it->first - lower_it->first);
    T const alpha = T(1.0) - (diff_upper / il);
    T const beta  = T(1.0) - (diff_lower / il);



    key_1        = lower_it->first;
    key_2        = upper_it->first;
    weight_key_1 = beta;
  }
  else
  {
    assert(0);
  }
}


// template<typename T, class DATA_TYPE>
// T _OneDimMap<T, DATA_TYPE>::derivative(T targeted_key) const
// {
//   assert(_map.size() >= 2);

//   // Iterator on element which key is > targeted_key
//   typename std::map<T, DATA_TYPE>::const_iterator upper_it = _map.upper_bound(targeted_key);
//   typename std::map<T, DATA_TYPE>::const_iterator lower_it = upper_it;
//   --lower_it;

//   if (lower_it == _map.end() || upper_it == _map.end())
//   {
//     // key outside range?
//     assert(0);
//   }
//TODO: CHECK  WHY THE COSINE ???
//   return (-std::cos(upper_it->second) + std::cos(lower_it->second)) / (upper_it->first - lower_it->first);
// }



template<typename T, class DATA_TYPE>
void _OneDimMap<T, DATA_TYPE>::keysAndValues(T targeted_key, T &key_inf, T &key_sup, T &value_inf, T &value_sup) const
{
  assert(_map.size() >= 2);

  // Iterator on element which key is > targeted_key
  typename std::map<T, DATA_TYPE>::const_iterator upper_it = _map.upper_bound(targeted_key);
  typename std::map<T, DATA_TYPE>::const_iterator lower_it = upper_it;
  --lower_it;

  if (lower_it == _map.end() || upper_it == _map.end())
  {
    // key outside range?
    assert(0);
  }

  key_inf = lower_it->first;
  key_sup = upper_it->first;

  value_inf = lower_it->second;
  value_sup = upper_it->second;
}


template<typename T, class DATA_TYPE>
inline void _OneDimMap<T, DATA_TYPE>::clear()
{
  _map.clear();
}


//-------------------------------------------------------------------------
// Extern Operator
//-------------------------------------------------------------------------
template<typename T, class DATA_TYPE>
inline std::ostream &operator<<(std::ostream &os, _OneDimMap<T, DATA_TYPE> const &one_dim_map)
{
  typename std::map<T, DATA_TYPE>::const_iterator it;

  os << " [ ";

  for (it = one_dim_map.begin(); it != one_dim_map.end(); ++it)
  {
    os << " (" << it->first << " , " << it->second << ") ";
  }

  os << " ] " << std::endl;

  return os;
}


}   // namespace util

}   // namespace mrf
