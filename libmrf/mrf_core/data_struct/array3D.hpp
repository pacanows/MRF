/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 *
 **/


#pragma once


namespace mrf
{
namespace util
{
/**
 * @brief A 3D Array
 *
 *  Dimensions of the 3D array are : width x height x depth
 *
 * depth determines the number of layers of the 3D array
 *
 * Data are stored to be used as OpenGL 3D Texture directly
 * i.e., the data ordering is row-major order per layer
 *
 *
 * @tparam     T     { precision of each element }
 */
template<class T>
class _Array3D
{
private:
  /**
   * Stores the dimension of the 3D Array
   * _dim[0] = width;
   * _dim[1] = height;
   * _dim[2] = depth;
   */
  mrf::math::Vec3ui _dim;
  T *               _data;


public:
  inline _Array3D(unsigned int width, unsigned int height, unsigned int depth);
  inline ~_Array3D();

  /**
   * Returns the linear index in memory from 3 indexes
   **/
  inline unsigned int linIdx(unsigned int i_row_index, unsigned int j_column_index, unsigned int depth_index) const;

  inline T const &operator()(unsigned int i_row_index, unsigned int j_column_index, unsigned int depth_index) const;
  inline T &      operator()(unsigned int i_row_index, unsigned int j_column_index, unsigned int depth_index);

  inline T &      operator()(unsigned int linear_index);
  inline T const &operator()(unsigned int linear_index) const;

  // Access like a 3D texture with 3 coordinates
  // There is just a swap between this row and column for this call
  inline T &xyz(unsigned int index_on_first_dim, unsigned int index_on_second_dim, unsigned int index_on_third_dim);
  inline T const &
  xyz(unsigned int index_on_first_dim, unsigned int index_on_second_dim, unsigned int index_on_third_dim);

  inline unsigned int      dimSize(unsigned int dimension) const;
  inline mrf::math::Vec3ui dimSize() const;


  inline unsigned int width() const;
  inline unsigned int height() const;
  inline unsigned int depth() const;
  inline unsigned int size() const;

  inline T const *ptr() const;
  inline T *      ptr();

  //TODO: Implement linear interpolation inside the Grid
  //inline T linear( float i, float j, float k);
};


template<class T>
inline _Array3D<T>::_Array3D(unsigned int width, unsigned int height, unsigned int depth)
  : _dim(width, height, depth)
  , _data(new T[width * height * depth])
{}

template<class T>
inline _Array3D<T>::~_Array3D()
{
  delete[] _data;
}

template<class T>
inline unsigned int
_Array3D<T>::linIdx(unsigned int i_row_index, unsigned int j_colum_index, unsigned int k_depth_index) const
{
  return j_colum_index + _dim[0] * i_row_index + _dim[0] * _dim[1] * k_depth_index;
}


template<class T>
T const &_Array3D<T>::operator()(unsigned int i_row_index, unsigned int j_colum_index, unsigned int k_depth_index) const
{
  return _data[linIdx(i_row_index, j_colum_index, k_depth_index)];
}

template<class T>
T &_Array3D<T>::operator()(unsigned int i_row_index, unsigned int j_colum_index, unsigned int k_depth_index)
{
  return _data[linIdx(i_row_index, j_colum_index, k_depth_index)];
}

template<class T>
inline T &_Array3D<T>::operator()(unsigned int linear_index)
{
  return _data[linear_index];
}

template<class T>
inline T &
_Array3D<T>::xyz(unsigned int index_on_first_dim, unsigned int index_on_second_dim, unsigned int index_on_third_dim)
{
  return _data[linIdx(index_on_first_dim, index_on_first_dim, index_on_third_dim)];
}

template<class T>
inline T const &
_Array3D<T>::xyz(unsigned int index_on_first_dim, unsigned int index_on_second_dim, unsigned int index_on_third_dim)
{
  return _data[linIdx(index_on_first_dim, index_on_first_dim, index_on_third_dim)];
}

template<class T>
inline unsigned int _Array3D<T>::dimSize(unsigned int dimension) const
{
  assert(dimension <= 2);
  return _dim[dimension];
}
template<class T>
inline mrf::math::Vec3ui _Array3D<T>::dimSize() const
{
  return _dim;
}

template<class T>
inline unsigned int _Array3D<T>::width() const
{
  return _dim[0];
}

template<class T>
inline unsigned int _Array3D<T>::height() const
{
  return _dim[1];
}

template<class T>
inline unsigned int _Array3D<T>::depth() const
{
  return _dim[2];
}

template<class T>
inline unsigned int _Array3D<T>::size() const
{
  return width() * height() * depth();
}

template<class T>
inline T const *_Array3D<T>::ptr() const
{
  return _data;
}

template<class T>
inline T *_Array3D<T>::ptr()
{
  return _data;
}



}   // namespace util

}   // namespace mrf
