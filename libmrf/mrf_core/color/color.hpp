/*
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 * 4 channels float color
 * could represent RGBA, XYZA ...
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/mat4.hpp>
#include <mrf_core/math/vec4.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/radiometry/illuminants.hpp>
#include <mrf_core/color/color_data.hpp>

#include <algorithm>
#include <iostream>
#include <cmath>
#include <functional>

namespace mrf
{
namespace color
{
/**
 * Represents a 4 channel float color.
 * Can be XYZA, RGBA, with the last component as an alpha channel.
 */
template<typename T>
class _Color: public math::Vec4<T>
{
public:
  inline _Color(T const &f = 0): math::Vec4<T>(f, T(1)) {}
  inline _Color(T const &r, T const &g, T const &b, T const &a = T(1)): math::Vec4<T>(r, g, b, a) {}
  inline _Color(std::array<T, 3> values): math::Vec4<T>(values[0], values[1], values[2], T(1)) {}
  inline _Color(std::array<T, 4> values): math::Vec4<T>(values[0], values[1], values[2], values[3]) {}
  inline _Color(math::Vec3<T> const &values): math::Vec4<T>(values, T(1)) {}
  inline _Color(math::Vec4<T> const &values): math::Vec4<T>(values) {}

  inline bool   isInvalid() const { return mrf::math::Vec3<T>::isInfOrNan(); }
  inline bool   isValid() const { return !isInvalid(); }
  inline bool   isNegative() const { return (r() < T(0)) || (g() < T(0)) || (b() < T(0)) || (a() < T(0)); }
  inline bool   isPositive() const { return !isNegative(); }
  inline float  normalizedIntegral() const { return mrf::math::Vec3<T>::length(); }
  inline size_t size() const { return 4; }

  inline T &r() { return (*this)[0]; }
  inline T &g() { return (*this)[1]; }
  inline T &b() { return (*this)[2]; }
  inline T &a() { return (*this)[3]; }

  inline T const &r() const { return (*this)[0]; }
  inline T const &g() const { return (*this)[1]; }
  inline T const &b() const { return (*this)[2]; }
  inline T const &a() const { return (*this)[3]; }

  inline T *raw_ptr() { return &((*this)[0]); }

  inline void get(T &r, T &g, T &b) const
  {
    r = this->r();
    g = this->g();
    b = this->b();
  }

  inline void get(T &r, T &g, T &b, T &a) const
  {
    r = this->r();
    g = this->g();
    b = this->b();
    a = this->a();
  }

  inline _Color<T> clamped(T min = T(0), T max = T(1)) const
  {
    return _Color<T>(
        std::min(std::max(r(), min), max),
        std::min(std::max(g(), min), max),
        std::min(std::max(b(), min), max),
        std::min(std::max(a(), min), max));
  }

  inline _Color<T> clampNegativeToZero()
  {
    for (int c = 0; c < 4; c++)
    {
      (*this)[c] = std::max(T(0), (*this)[c]);
    }

    return *this;
  }

  inline void pow(T const &power)
  {
    for (int c = 0; c < 4; c++)
    {
      (*this)[c] = std::pow((*this)[c], power);
    }
  }

  inline _Color<T> pow(T const &power) const
  {
    return _Color<T>(
        std::pow((*this)[0], power),
        std::pow((*this)[1], power),
        std::pow((*this)[2], power),
        std::pow((*this)[3], power));
  }

  inline T         avgCmp() const { return mrf::math::_Math<T>::mean(r(), g(), b()); }
  inline _Color<T> sqr() const { return _Color<T>(r() * r(), g() * g(), b() * b(), a() * a()); }
  inline T luminance() const { return LUMINANCE_COEFF[0] * r() + LUMINANCE_COEFF[1] * g() + LUMINANCE_COEFF[2] * b(); }

  /**
   * This is supposed to the implementation of the following paper
   *
   * Dynamic Range Reduction Inspired by Photoreceptor Physiology
   * by Erik Reinhard and Kate Devlin
   *
   * https://ieeexplore.ieee.org/document/1359728
   **/
  inline _Color<T> reinhard05Tonemapping(
      T const &        chromatic_adaptation,
      _Color<T> const &means,
      T const &        lum_mean,
      T const &        light_adaptation,
      T const &        contrast,
      T const &        intensity) const;

  inline _Color<T> XYZtoLinearRGB(MRF_COLOR_SPACE colorSpace = SRGB, MRF_WHITE_POINT whitePoint = D65) const;

  inline _Color<T> linearRGBtoXYZ(MRF_COLOR_SPACE colorSpace = SRGB, MRF_WHITE_POINT whitePoint = D65) const;
};   //end of Color Class

template<typename T = float>
inline std::function<_Color<T>(const _Color<T> &)> getGammaFunction(MRF_COLOR_SPACE colorSpace = SRGB)
{
  switch (colorSpace)
  {
    case SRGB:
      return [](_Color<T> const &c) {
        _Color<T> retVal;
        for (int i = 0; i < 3; i++)
        {
          if (c[i] <= T(0.0031308))
          {
            retVal[i] = T(12.92) * c[i];
          }
          else
          {
            retVal[i] = T(1.055) * std::pow(c[i], T(1. / 2.4)) - T(0.055);
          }
        }
        return retVal;
      };

    case ADOBE_RGB:
      return [](_Color<T> const &c) {
        return c.pow(T(1) / T(ADOBE_RGB_GAMMA));
      };
    default:
      return [](_Color<T> const &c) {
        return c.pow(T(1. / 2.2));
      };
  }
}

template<typename T = float>
inline std::function<_Color<T>(const _Color<T> &)> getInverseGammaFunction(MRF_COLOR_SPACE colorSpace = SRGB)
{
  switch (colorSpace)
  {
    case SRGB:
      return [](_Color<T> const &c) {
        _Color<T> retVal;
        for (int i = 0; i < 3; i++)
        {
          if (c[i] <= T(0.04045))
          {
            retVal[i] = c[i] / T(12.92);
          }
          else
          {
            retVal[i] = std::pow((c[i] + T(0.055)) / T(1.055), T(2.4));
          }
        }
        return retVal;
      };

    case ADOBE_RGB:
      return [](_Color<T> const &c) {
        return c.pow(T(ADOBE_RGB_GAMMA));
      };
    default:
      return [](_Color<T> const &c) {
        return c.pow(T(2.2));
      };
  }
}

// ----------------------------------------------------------------------------
// Utility functions
// ----------------------------------------------------------------------------

template<typename T>
inline bool operator<(_Color<T> const &a_color, T a_scalar)
{
  for (int i = 0; i < 3; ++i)
  {
    if (a_color[i] >= a_scalar)
    {
      return false;
    }
  }
  return true;
}

template<typename T>
inline bool operator>(_Color<T> const &a_color, T a_scalar)
{
  for (int i = 0; i < 3; ++i)
  {
    if (a_color[i] <= a_scalar)
    {
      return false;
    }
  }
  return true;
}

template<typename T>
inline _Color<T> _Color<T>::reinhard05Tonemapping(
    T const &        chromatic_adaptation,
    _Color<T> const &means,
    T const &        lum_mean,
    T const &        light_adaptation,
    T const &        contrast,
    T const &        intensity) const
{
  T         lum = luminance();
  _Color<T> lc  = chromatic_adaptation * (*this) + _Color<T>(T(1. - chromatic_adaptation) * lum);
  _Color<T> gc  = chromatic_adaptation * means + _Color<T>(T(1. - chromatic_adaptation) * lum_mean);
  _Color<T> ca  = light_adaptation * lc + T(1.f - light_adaptation) * gc;

  ca *= intensity;
  ca.pow(contrast);

  _Color<T> res = *this;
  res /= (res + ca);

  return res;
}

template<typename T>
inline _Color<T> _Color<T>::XYZtoLinearRGB(MRF_COLOR_SPACE colorSpace, MRF_WHITE_POINT whitePoint) const
{
  return getXYZtoRGBMat(colorSpace, whitePoint) * (*this);
}

template<typename T>
inline _Color<T> _Color<T>::linearRGBtoXYZ(MRF_COLOR_SPACE colorSpace, MRF_WHITE_POINT whitePoint) const
{
  return getRGBtoXYZMat(colorSpace, whitePoint) * (*this);
}

typedef _Color<float> Color;

}   // namespace color
}   // namespace mrf
