/*
 * author: Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 * Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2018
 *
 * Alban Fichet @ institutoptique.fr
 * Copyright CNRS 2020
 *
 **/

#pragma once

#include <array>
#include <functional>

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/data_struct/array2d.hpp>
#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/radiometry/spectrum.hpp>
#include <mrf_core/math/integral_1d.hpp>
#include <mrf_core/math/math.hpp>

#include <mrf_core/color/sensitivity_curve_cie_1931_2deg.hpp>
#include <mrf_core/color/sensitivity_curve_cie_1964_10deg.hpp>

namespace mrf
{
namespace color
{
enum SensitivityCurveName
{
  CIE_1931_2DEG,
  CIE_XYZ_1964_10DEG
};

template<typename T_wave, typename T_val>
class _SpectrumConverter
{
public:
  enum SENSITIVITY_CHANNEL
  {
    SENSITIVITY_X = 0,
    SENSITIVITY_Y,
    SENSITIVITY_Z,
    N_SENSITIVITY_CHANNELS
  };

  _SpectrumConverter(SensitivityCurveName curve = CIE_1931_2DEG);

  virtual ~_SpectrumConverter() {}

  inline T_wave sensCurveFirstWavelength() const { return _sensitivity_curve_first_wavelength; }
  inline size_t nbWavelengths() const { return _wavelengths.size(); }
  inline size_t sensCurveSize() const { return nbWavelengths(); }

  inline std::vector<T_wave> const &wavelengths() const { return _wavelengths; }

  T_wave wavelengthValue(size_t wavelength_index) const;
  size_t wavelengthIndex(T_wave wavelength_value) const;

  T_val xSensitivityAtWavelength(T_wave wavelength_value) const;
  T_val ySensitivityAtWavelength(T_wave wavelength_value) const;
  T_val zSensitivityAtWavelength(T_wave wavelength_value) const;

  mrf::math::Vec3<T_val> xyzValuesAtWavelength(T_wave wavelength) const;

  T_val                  xSensitivityAtIndex(size_t wavelength_index) const;
  T_val                  ySensitivityAtIndex(size_t wavelength_index) const;
  T_val                  zSensitivityAtIndex(size_t inwavelength_indexdex) const;
  mrf::math::Vec3<T_val> xyzValuesAtIndex(size_t wavelength_index) const;

  // ------------------------------------------------------------------------
  // Spectra and spectral images integrals computations
  // ------------------------------------------------------------------------

  /**
   * @brief Computes the integral a spectrum.
   * If only one value in the spectrum, return this values
   * Otherwise, Integration using trapezoidal rule (linear interpolation)
   * The spectrumWavelength values must be sorted before calling this
   * function.
   *
   * @param spectrumWavelength Wavelengths corresponding to the values to
   *                           integrate. Must be sorted from lower to
   *                           highter.
   * @param spectrumValues     Values of the spectrum.
   *
   * @returns The integral of the spectrum values.
   **/
  static T_val
  spectrumIntegral(std::vector<T_wave> const &spectrumWavelengths, std::vector<T_val> const &spectrumValues);

  /**
   * @brief Computes the integral a spectrum.
   *
   * @param spectrumWavelength Wavelengths corresponding to the values to
   *                           integrate. Must be sorted from lower to
   *                           highter.
   * @param spectrumValues     Values of the spectrum.
   * @param start_wavelength   Wavelength to start integration from.
   * @param end_wavelength     Wavelength where to stop integrate.
   *
   * @returns The integral of the spectrum values.
   **/
  static T_val spectrumIntegral(
      std::vector<T_wave> const &spectrumWavelengths,
      std::vector<T_val> const & spectrumValues,
      T_wave                     start_wavelength,
      T_wave                     end_wavelength);

  /**
   * @brief Computes the integral a spectrum.
   * @param s The spectrum to integrate.
   * @returns The integral of the spectrum values.
   **/
  static T_val spectrumIntegral(mrf::radiometry::Spectrum_T<T_wave, T_val> const &s);

  /**
   * @brief Computes the integral a spectrum.
   * @param s The spectrum to integrate.
   * @param start_wavelength   Wavelength to start integration from.
   * @param end_wavelength     Wavelength where to stop integrate.
   *
   * @returns The integral of the spectrum values.
   **/
  static T_val
  spectrumIntegral(mrf::radiometry::Spectrum_T<T_wave, T_val> const &s, T_wave start_wavelength, T_wave end_wavelength);

  /**
   * @brief Converts to an Array2D gray image.
   *
   * This is not a perceptual gray image. It computes for each pixel the
   * integral of the spectrum.
   *
   * @param spectral_image   The spectral image to convert.
   * @param gray_image       Resulting gray image.
   * @param start_wavelength The lower bound of the integral.
   * @param end_wavelength   The higher bound of the integral.
   */
  static void spectralImageToGrayImage(
      mrf::image::UniformSpectralImage const &spectral_image,
      mrf::data_struct::_Array2D<T_val> &     gray_image,
      T_wave                                  start_wavelength,
      T_wave                                  end_wavelength);

  /**
   * @brief Converts to an Array2D gray image.
   *
   * This is not a perceptual gray image. It computes for each pixel the
   * integral of the spectrum.
   *
   * @param spectral_buffer    The buffer containing the spectral image to
   *                           convert. Each element of the vector contains
   *                           a 2D gray image corresponding to the spectrum
   *                           value for a given wavelength.
   * @param buffer_wavelengths The corresponding wavelengths.
   * @param gray_image         Resulting gray image.
   * @param start_wavelength   The lower bound of the integral.
   * @param end_wavelength     The higher bound of the integral.
   */
  static void spectralBufferToGrayImage(
      std::vector<mrf::data_struct::Array2D> const &spectral_buffer,
      std::vector<T_wave> const &                   buffer_wavelengths,
      mrf::data_struct::_Array2D<T_val> &           gray_image,
      T_wave                                        start_wavelength,
      T_wave                                        end_wavelength);


  // ------------------------------------------------------------------------
  // Spectrum photoscopic luminance
  // ------------------------------------------------------------------------

  /**
   * @brief Computes the photoscopic response for a spectrum
   *
   * The photoscopic response is:
   * $K_m \int_{\Lambda} y(\lambda) \cdot s(\lambda) \mathrm{d}\lambda$
   * with:
   * $K_m = 683 lm.W^{-1}$ Luminous efficacy for photopic vision.
   * $y(\lambda)$ The luminance sensitivity curve of the current CIE-XYZ
   *             Color space.
   * $s(\lambda)$ The radiance spectrum ($W.m^{2}.sr{-1}$).
   *
   * @param a_spectrum Spectrum in $W.m^{2}.sr{-1}$.
   * @return The photoscopic value of the given emittance spectrum in
   *         $cd.m^{-2}$.
   */
  T_val photoscopicLuminance(mrf::radiometry::Spectrum_T<T_wave, T_val> const &a_spectrum) const;


  // ------------------------------------------------------------------------
  // Emissive spectra and spectral images handling
  // ------------------------------------------------------------------------

  /**
   * @brief Converts an emissive spectrum to XYZ values.
   *
   * Using the color matching functions curves $\bar{x}$ $\bar{y}$ $\bar{z}$
   * we calculate a XYZ value corresponding to an emissive spectrum.
   *
   * The given spectrum is oversampled at 1nm and values in between those
   * provided are lineraly interpolated.
   *
   * @param s The spectrum to convert to XYZ.
   * @return A color containing the XYZ values.
   **/
  _Color<T_val> emissiveSpectrumToXYZ(mrf::radiometry::Spectrum_T<T_wave, T_val> const &s) const;

  /**
   * @brief Converts an emissive spectrum to XYZ values.
   *
   * @param spectrumWavelengths The spectrum wavelenths (must be sorted from
   *                            lower to higher).
   * @param spectrumValues      The spectrum values.
   * @return A color containing the XYZ values.
   **/
  _Color<T_val>
  emissiveSpectrumToXYZ(std::vector<T_wave> const &spectrumWavelengths, std::vector<T_val> const &spectrumValues) const;


  void emissiveFramebufferToXYZImage(
      std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
      std::vector<T_wave> const &                   framebuffer_wavelengths,
      mrf::data_struct::_Array2D<T_val> &           x,
      mrf::data_struct::_Array2D<T_val> &           y,
      mrf::data_struct::_Array2D<T_val> &           z,
      std::function<void(int)>                      progress = [](int) {
      }) const;

  void emissiveFramebufferToXYZImage(
      std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
      std::vector<T_wave> const &                   framebuffer_wavelengths,
      mrf::image::ColorImage &                      xyz_image,
      std::function<void(int)>                      progress = [](int) {
      }) const;


  /**
   * @brief Converts an emissive spectral image to a X, Y, Z 2D arrays.
   *
   * @param spectral_image The spectral image to convert.
   * @param x              Resulting conversion for X component.
   * @param y              Resulting conversion for Y component.
   * @param z              Resulting conversion for Z component.
   * @param progress       A function to call to have a feedback on the
   *                       conversion progression in percent.
   **/
  void emissiveSpectralImageToXYZImage(
      mrf::image::UniformSpectralImage const &spectral_image,
      mrf::data_struct::_Array2D<T_val> &     x,
      mrf::data_struct::_Array2D<T_val> &     y,
      mrf::data_struct::_Array2D<T_val> &     z,
      std::function<void(int)>                progress = [](int) {
      }) const;

  /**
   * @brief Converts an emissive spectral image to a X, Y, Z 2D arrays.
   *
   * @param spectral_image The spectral image to convert.
   * @param xyz_image      The resulting image with XYZ components.
   * @param progress       A function to call to have a feedback on the
   *                       conversion progression in percent.
   **/
  void emissiveSpectralImageToXYZImage(
      mrf::image::UniformSpectralImage const &spectral_image,
      mrf::image::ColorImage &                xyz_image,
      std::function<void(int)>                progress = [](int) {
      }) const;


  void emissiveFramebufferToRGBImage(
      std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
      std::vector<T_wave> const &                   framebuffer_wavelengths,
      image::ColorImage &                           rgb_image,
      MRF_COLOR_SPACE                               colorSpace,
      MRF_WHITE_POINT                               whitePoint,
      std::function<void(int)>                      progress = [](int) {
      }) const;


  void emissiveSpectralImageToRGBImage(
      mrf::image::UniformSpectralImage const &spectral_image,
      mrf::image::ColorImage &                rgb_image,
      MRF_COLOR_SPACE                         colorSpace = SRGB,
      MRF_WHITE_POINT                         whitePoint = D65,
      std::function<void(int)>                progress   = [](int) {
      }) const;


  // ------------------------------------------------------------------------
  // Reflective spectra and spectral images handling
  // ------------------------------------------------------------------------

  /**
   * @brief Converts a reflective spectrum to XYZ values.
   *
   * Using the color matching functions curves $\bar{x}$ $\bar{y}$ $\bar{z}$
   * we calculate a XYZ value corresponding to a reflective spectrum.
   *
   * The given spectrum is oversampled at 1nm and values in between those
   * provided are lineraly interpolated.
   *
   * @param s The spectrum to convert to XYZ.
   * @param illuminat_spd The illuminant spectral power distribution (SPD).
   * @param illuminant_first_wavelength The SPD first wavelength.
   * @param illuminant_precision The SPD sampling in the same units as the
   *                             spectrum wavelength.
   * @param illuminant_array_size The number of samples in the SPD.
   * @return A color containing the XYZ values.
   **/
  _Color<T_val> reflectiveSpectrumToXYZ(
      mrf::radiometry::Spectrum_T<T_wave, T_val> const &s,
      const T_val *const                                illuminat_spd,
      T_wave                                            illuminant_first_wavelength,
      T_wave                                            illuminant_precision,
      size_t                                            illuminant_array_size) const;

  /**
   * @brief Converts a reflective spectrum to XYZ values.
   * @param spectrumWavelengths The spectrum wavelengths.
   * @param spectrumValues The spectrum values.
   * @param illuminant_spd The illuminant spectral power distribution (SPD).
   * @param illuminant_first_wavelength The SPD first wavelength.
   * @param illuminant_precision The SPD sampling in the same units as the
   *                             spectrum wavelength.
   * @param illuminant_array_size The number of samples in the SPD.
   * @return A color containing the XYZ values.
   **/
  _Color<T_val> reflectiveSpectrumToXYZ(
      std::vector<T_wave> const &spectrumWavelengths,
      std::vector<T_val> const & spectrumValues,
      const T_val *const         illuminant_spd,
      T_wave                     illuminant_first_wavelength,
      T_wave                     illuminant_precision,
      size_t                     illuminant_array_size) const;


  void reflectiveFramebufferToXYZImage(
      std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
      std::vector<T_wave> const &                   framebuffer_wavelengths,
      const T_val *const                            illuminant_spd,
      T_wave                                        illuminant_first_wavelength,
      T_wave                                        illuminant_precision,
      size_t                                        illuminant_array_size,
      mrf::data_struct::_Array2D<T_val> &           x,
      mrf::data_struct::_Array2D<T_val> &           y,
      mrf::data_struct::_Array2D<T_val> &           z,
      std::function<void(int)>                      progress = [](int) {
      }) const;


  void reflectiveFramebufferToXYZImage(
      std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
      std::vector<T_wave> const &                   framebuffer_wavelengths,
      const T_val *const                            illuminant_spd,
      T_wave                                        illuminant_first_wavelength,
      T_wave                                        illuminant_precision,
      size_t                                        illuminant_array_size,
      mrf::image::ColorImage &                      xyz_image,
      std::function<void(int)>                      progress = [](int) {
      }) const;

  /**
   * @brief Converts a reflective spectral image to XYZ values.
   * @param spectral_image The spectral image.
   * @param spectrumValues The spectrum values.
   * @param illuminant_spd The illuminant spectral power distribution (SPD).
   * @param illuminant_first_wavelength The SPD first wavelength.
   * @param illuminant_precision The SPD sampling in the same units as the
   *                             spectrum wavelength.
   * @param illuminant_array_size The number of samples in the SPD.
   * @param xyz_image      The resulting image with XYZ components.
   * @param progress       A function to call to have a feedback on the
   *                       conversion progression in percent.
   **/
  void reflectiveSpectralImageToXYZImage(
      mrf::image::UniformSpectralImage const &spectral_image,
      const T_val *const                      illuminant_spd,
      T_wave                                  illuminant_first_wavelength,
      T_wave                                  illuminant_precision,
      size_t                                  illuminant_array_size,
      mrf::image::ColorImage &                xyz_image,
      std::function<void(int)>                progress = [](int) {
      }) const;


  void reflectiveSpectralImageToXYZImage(
      mrf::image::UniformSpectralImage const &spectral_image,
      const T_val *const                      illuminant_spd,
      T_wave                                  illuminant_first_wavelength,
      T_wave                                  illuminant_precision,
      size_t                                  illuminant_array_size,
      mrf::data_struct::_Array2D<T_val> &     x,
      mrf::data_struct::_Array2D<T_val> &     y,
      mrf::data_struct::_Array2D<T_val> &     z,
      std::function<void(int)>                progress = [](int) {
      }) const;


  void reflectiveFramebufferToRGBImage(
      std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
      std::vector<T_wave> const &                   framebuffer_wavelengths,
      const T_val *const                            illuminant_spd,
      T_wave                                        illuminant_first_wavelength,
      T_wave                                        illuminant_precision,
      size_t                                        illuminant_array_size,
      mrf::image::ColorImage &                      rgb_image,
      MRF_COLOR_SPACE                               colorSpace = SRGB,
      MRF_WHITE_POINT                               whitePoint = D65,
      std::function<void(int)>                      progress   = [](int) {
      }) const;


  void reflectiveSpectralImageToRGBImage(
      mrf::image::UniformSpectralImage const &spectral_image,
      const T_val *const                      illuminant_spd,
      T_wave                                  illuminant_first_wavelength,
      T_wave                                  illuminant_precision,
      size_t                                  illuminant_array_size,
      mrf::image::ColorImage &                rgb_image,
      MRF_COLOR_SPACE                         colorSpace = SRGB,
      MRF_WHITE_POINT                         whitePoint = D65,
      std::function<void(int)>                progress   = [](int) {
      }) const;


  void bispectralFramebufferToXYZImage(
      const std::vector<mrf::data_struct::Array2D> &reflective_framebuffer,
      const std::vector<mrf::data_struct::Array2D> &reradiation,
      const std::vector<T_wave> &                   framebuffer_wavelengths,
      const T_val *const                            illuminant_spd,
      T_wave                                        illuminant_first_wavelength,
      T_wave                                        illuminant_precision,
      size_t                                        illuminant_array_size,
      mrf::data_struct::_Array2D<T_val> &           x,
      mrf::data_struct::_Array2D<T_val> &           y,
      mrf::data_struct::_Array2D<T_val> &           z,
      std::function<void(int)>                      progress = [](int) {
      }) const;


  void bispectralFramebufferToRGBImage(
      const std::vector<mrf::data_struct::Array2D> &spectral_framebuffer,
      const std::vector<mrf::data_struct::Array2D> &reradiation,
      const std::vector<T_wave> &                   framebuffer_wavelengths,
      const T_val *const                            illuminant_spd,
      T_wave                                        illuminant_first_wavelength,
      T_wave                                        illuminant_precision,
      size_t                                        illuminant_array_size,
      mrf::image::ColorImage &                      rgb_image,
      MRF_COLOR_SPACE                               colorSpace = SRGB,
      MRF_WHITE_POINT                               whitePoint = D65,
      std::function<void(int)>                      progress   = [](int) {
      }) const;


private:
  uint _sensitivity_curve_first_wavelength;

  std::array<const float *, N_SENSITIVITY_CHANNELS> _sensitivity_curves;
  std::vector<T_wave>                               _wavelengths;
};

// ----------------------------------------------------------------------------
// Implementation
// ----------------------------------------------------------------------------

template<typename T_wave, typename T_val>
_SpectrumConverter<T_wave, T_val>::_SpectrumConverter(SensitivityCurveName curve)
  : _sensitivity_curve_first_wavelength(360)
  , _sensitivity_curves({SENSITIVITY_CIE_1931_2DEG_X, SENSITIVITY_CIE_1931_2DEG_Y, SENSITIVITY_CIE_1931_2DEG_Z})
  , _wavelengths(sizeof(SENSITIVITY_CIE_1931_2DEG_X) / sizeof(float))
{
  switch (curve)
  {
    case CIE_1931_2DEG:
      _sensitivity_curves = {SENSITIVITY_CIE_1931_2DEG_X, SENSITIVITY_CIE_1931_2DEG_Y, SENSITIVITY_CIE_1931_2DEG_Z};
      break;
    case CIE_XYZ_1964_10DEG:
      _sensitivity_curves = {SENSITIVITY_CIE_1964_10DEG_X, SENSITIVITY_CIE_1964_10DEG_Y, SENSITIVITY_CIE_1964_10DEG_Z};
      break;
  }

  for (size_t wavelength_index = 0; wavelength_index < nbWavelengths(); wavelength_index++)
  {
    _wavelengths[wavelength_index] = wavelengthValue(wavelength_index);
  }
}


template<typename T_wave, typename T_val>
T_wave _SpectrumConverter<T_wave, T_val>::wavelengthValue(size_t wavelength_index) const
{
  assert(wavelength_index < nbWavelengths());

  return T_wave(_sensitivity_curve_first_wavelength + wavelength_index);
}


template<typename T_wave, typename T_val>
size_t _SpectrumConverter<T_wave, T_val>::wavelengthIndex(T_wave wavelength_value) const
{
  assert(wavelength_value >= _wavelengths.front());
  assert(wavelength_value <= _wavelengths.back());

  T_val idx_f = T_val(wavelength_value - _sensitivity_curve_first_wavelength);

  assert(idx_f >= 0);
  assert(idx_f < nbWavelengths());

  return static_cast<size_t>(std::round(idx_f));
}


template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::xSensitivityAtWavelength(T_wave wavelength_value) const
{
  if (wavelength_value < _wavelengths.front() || wavelength_value > _wavelengths.back())
  {
    return T_val(0);
  }

  assert(wavelength_value >= _wavelengths.front());
  assert(wavelength_value <= _wavelengths.back());

  const size_t wavelength_idx = wavelengthIndex(wavelength_value);

  return this->xValuesAtIndex(wavelength_idx);
}


template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::ySensitivityAtWavelength(T_wave wavelength_value) const
{
  if (wavelength_value < _wavelengths.front() || wavelength_value > _wavelengths.back())
  {
    return T_val(0);
  }

  assert(wavelength_value >= _wavelengths.front());
  assert(wavelength_value <= _wavelengths.back());

  const size_t wavelength_idx = wavelengthIndex(wavelength_value);

  return this->yValuesAtIndex(wavelength_idx);
}


template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::zSensitivityAtWavelength(T_wave wavelength_value) const
{
  if (wavelength_value < _wavelengths.front() || wavelength_value > _wavelengths.back())
  {
    return T_val(0);
  }

  assert(wavelength_value >= _wavelengths.front());
  assert(wavelength_value <= _wavelengths.back());

  const size_t wavelength_idx = wavelengthIndex(wavelength_value);

  return this->zValuesAtIndex(wavelength_idx);
}


template<typename T_wave, typename T_val>
mrf::math::Vec3<T_val> _SpectrumConverter<T_wave, T_val>::xyzValuesAtWavelength(T_wave wavelength_value) const
{
  if (wavelength_value < _wavelengths.front() || wavelength_value > _wavelengths.back())
  {
    return T_val(0);
  }

  assert(wavelength_value >= _wavelengths.front());
  assert(wavelength_value <= _wavelengths.back());

  const size_t wavelength_idx = wavelengthIndex(wavelength_value);

  return this->xyzValuesAtIndex(wavelength_idx);
}


template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::xSensitivityAtIndex(size_t wavelength_index) const
{
  assert(wavelength_index < nbWavelengths());

  return _sensitivity_curves[SENSITIVITY_X][wavelength_index];
}


template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::ySensitivityAtIndex(size_t wavelength_index) const
{
  assert(wavelength_index < nbWavelengths());

  return _sensitivity_curves[SENSITIVITY_Y][wavelength_index];
}


template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::zSensitivityAtIndex(size_t wavelength_index) const
{
  assert(wavelength_index < nbWavelengths());

  return _sensitivity_curves[SENSITIVITY_Z][wavelength_index];
}


template<typename T_wave, typename T_val>
mrf::math::Vec3<T_val> _SpectrumConverter<T_wave, T_val>::xyzValuesAtIndex(size_t wavelength_index) const
{
  assert(wavelength_index < nbWavelengths());

  return mrf::math::Vec3f(
      xSensitivityAtIndex(wavelength_index),
      ySensitivityAtIndex(wavelength_index),
      zSensitivityAtIndex(wavelength_index));
}

template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::spectrumIntegral(
    std::vector<T_wave> const &spectrumWavelengths,
    std::vector<T_val> const & spectrumValues)
{
  assert(spectrumWavelengths.size() == spectrumValues.size());

  return mrf::math::Integral1D::trapz(spectrumWavelengths, spectrumValues);
}

// ----------------------------------------------------------------------------
// Spectra and spectral images integrals computations
// ----------------------------------------------------------------------------

template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::spectrumIntegral(
    std::vector<T_wave> const &spectrumWavelengths,
    std::vector<T_val> const & spectrumValues,
    T_wave                     start_wavelength,
    T_wave                     end_wavelength)
{
  assert(spectrumWavelengths.size() == spectrumValues.size());

  if (spectrumWavelengths.size() == 0)
  {
    return T_val(0);
  }

  // Only one value and inside integration domain return it
  if (spectrumWavelengths.size() == 1 && spectrumWavelengths[0] >= start_wavelength
      && spectrumWavelengths[0] <= end_wavelength)
  {
    return spectrumValues[0];
  }

  // Early exit, selection out of range
  if (!mrf::math::_Math<uint>::intervalOverlap(
          start_wavelength,
          end_wavelength,
          spectrumWavelengths.front(),
          spectrumWavelengths.back(),
          start_wavelength,
          end_wavelength))
  {
    return T_val(0);
  }

  assert(start_wavelength <= end_wavelength);

  /* Short unoptimized version. Keep it there for reference
T_val result(0);

for (int i = 0; i < spectrumValues.size() - 1; i++) {
    T_wave wl_a = spectrumWavelengths[i];
    T_wave wl_b = spectrumWavelengths[i + 1];

    if (start_wavelength > wl_b) { continue; }
    if (end_wavelength < wl_a) { break; }

    if (start_wavelength > wl_a) {
        wl_a = start_wavelength;
    }

    if (end_wavelength < wl_b) {
        wl_b = end_wavelength;
    }

    const T_val v_a = mrf::math::_Math<T_val>::interp(
            wl_a,
            spectrumWavelengths[i], spectrumWavelengths[i + 1],
            spectrumValues[i], spectrumValues[i + 1]);

    const T_val v_b = mrf::math::_Math<T_val>::interp(
            wl_b,
            spectrumWavelengths[i], spectrumWavelengths[i + 1],
            spectrumValues[i], spectrumValues[i + 1]);

    result += (wl_b - wl_a) * (v_b + v_a);
}

return result * T_val(0.5);
*/

  // Look for the first and last index matching bounds
  size_t idx_start = 0;
  size_t idx_end   = spectrumWavelengths.size() - 1;

  while (start_wavelength > spectrumValues[idx_start])
  {
    idx_start++;
  }

  while (end_wavelength < spectrumValues[idx_end])
  {
    idx_end--;
  }

  // Very specific case:
  // when the integration bound is defined right in the middle of the first bin
  if (start_wavelength == spectrumWavelengths[idx_start] && end_wavelength != start_wavelength && idx_start == idx_end)
  {
    const T_val xa = static_cast<T_val>(spectrumWavelengths[0]);
    const T_val ya = spectrumValues[0];

    // Linear interpolation for spectrum at end wavelength
    const T_val xb = static_cast<T_val>(end_wavelength);
    const T_val yb = mrf::math::_Math<T_val>::interp(
        xb,
        static_cast<T_val>(spectrumWavelengths[0]),
        static_cast<T_val>(spectrumWavelengths[1]),
        spectrumValues[0],
        spectrumValues[1]);

    return T_val(0.5) * (xb - xa) * (yb + ya);
  }

  T_val retVal(0);

  // Start fall between two x values -> need to add a trapezoid

  assert(spectrumWavelengths[idx_start] >= start_wavelength);

  if (start_wavelength < spectrumWavelengths[idx_start])
  {
    assert(idx_start > 0);

    // Linear interpolation for spectrum at start wavelength
    const T_val xa = static_cast<T_val>(start_wavelength);
    const T_val ya = mrf::math::_Math<T_val>::interp(
        xa,
        static_cast<T_val>(spectrumWavelengths[idx_start - 1]),
        static_cast<T_val>(spectrumWavelengths[idx_start]),
        spectrumValues[idx_start - 1],
        spectrumValues[idx_start]);

    T_val xb;
    T_val yb;

    // Check if upper bound is not also greater than the starting wavelength
    // This can happen when idx_start == idx_end
    if (spectrumWavelengths[idx_start] <= end_wavelength)
    {
      xb = static_cast<T_val>(spectrumWavelengths[idx_start]);
      yb = static_cast<T_val>(spectrumValues[idx_start]);
    }
    // We also need to interpolate the value at idx_start because it
    // might fall further than end_wavelength hence the call to
    // std::min(spectrumWavelengths[idx_start], end_wavelength)
    else
    {
      // Linear interpolation for spectrum at end wavelength
      xb = static_cast<T_val>(end_wavelength);
      yb = mrf::math::_Math<T_val>::interp(
          xb,
          static_cast<T_val>(spectrumWavelengths[idx_start - 1]),
          static_cast<T_val>(spectrumWavelengths[idx_start]),
          spectrumValues[idx_start - 1],
          spectrumValues[idx_start]);
    }

    // Trapezoidal integration
    retVal += (xb - xa) * (yb + ya);
  }

  if (idx_end <= idx_start)
  {
    return T_val(0.5) * retVal;
  }

  // End fall between two wavelengths of the spectrum -> need to interpolate
  if (end_wavelength > spectrumWavelengths[idx_end])
  {
    assert(idx_end <= spectrumWavelengths.size() - 2);

    const T_val xa = static_cast<T_val>(spectrumWavelengths[idx_end]);
    const T_val ya = spectrumValues[idx_end];

    // Linear interpolation for spectrum at end wavelength
    const T_val xb = static_cast<T_val>(end_wavelength);
    const T_val yb = mrf::math::_Math<T_val>::interp(
        xb,
        static_cast<T_val>(spectrumWavelengths[idx_end]),
        static_cast<T_val>(spectrumWavelengths[idx_end + 1]),
        spectrumValues[idx_end],
        spectrumValues[idx_end + 1]);

    // Trapezoidal integration
    retVal += (xb - xa) * (yb + ya);
  }

  // Potentially non uniform grid
  for (size_t i = idx_start; i < idx_end; i++)
  {
    // Trapezoidal integration
    retVal += (spectrumWavelengths[i + 1] - spectrumWavelengths[i]) * (spectrumValues[i + 1] + spectrumValues[i]);
  }

  return T_val(0.5) * retVal;
}


template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::spectrumIntegral(mrf::radiometry::Spectrum_T<T_wave, T_val> const &s)
{
  return spectrumIntegral(s.wavelengths(), s.values());
}

template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::spectrumIntegral(
    mrf::radiometry::Spectrum_T<T_wave, T_val> const &s,
    T_wave                                            start_wavelength,
    T_wave                                            end_wavelength)
{
  return spectrumIntegral(s.wavelengths(), s.values(), start_wavelength, end_wavelength);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::spectralImageToGrayImage(
    image::UniformSpectralImage const &spectral_image,
    mrf::data_struct::_Array2D<T_val> &gray_image,
    T_wave                             start_wavelength,
    T_wave                             end_wavelength)
{
  spectralBufferToGrayImage(
      spectral_image.data(),
      spectral_image.wavelengths(),
      gray_image,
      start_wavelength,
      end_wavelength);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::spectralBufferToGrayImage(
    std::vector<mrf::data_struct::Array2D> const &spectral_buffer,
    std::vector<T_wave> const &                   buffer_wavelengths,
    mrf::data_struct::_Array2D<T_val> &           gray_image,
    T_wave                                        start_wavelength,
    T_wave                                        end_wavelength)
{
  if (buffer_wavelengths.size() == 0)
  {
    return;
  }

  // Only one value and inside integration domain return it
  if (buffer_wavelengths.size() == 1 && buffer_wavelengths[0] >= start_wavelength
      && buffer_wavelengths[0] <= end_wavelength)
  {
    gray_image = spectral_buffer[0];
    return;
  }

  gray_image = mrf::data_struct::Array2D(spectral_buffer[0].width(), spectral_buffer[0].height());

  // Early exit, selection out of range
  if (!mrf::math::_Math<uint>::intervalOverlap(
          start_wavelength,
          end_wavelength,
          buffer_wavelengths.front(),
          buffer_wavelengths.back(),
          start_wavelength,
          end_wavelength))
  {
    return;
  }

  // Look for the first and last index matching bounds
  size_t idx_start = 0;
  size_t idx_end   = buffer_wavelengths.size() - 1;

  while (start_wavelength > buffer_wavelengths[idx_start])
  {
    idx_start++;
  }

  while (end_wavelength < buffer_wavelengths[idx_end])
  {
    idx_end--;
  }

  // Very specific case:
  // when the integration bound is defined right in the middle of the first bin
  if (start_wavelength == buffer_wavelengths[idx_start] && end_wavelength != start_wavelength && idx_start == idx_end)
  {
    const T_val                     xa = static_cast<T_val>(buffer_wavelengths.front());
    const mrf::data_struct::Array2D ya = spectral_buffer.front();

    // Linear interpolation for spectrum at end wavelength
    const T_val xb = static_cast<T_val>(end_wavelength);
    const T_val t  = mrf::math::_Math<T_val>::alpha(
        static_cast<T_val>(buffer_wavelengths[0]),
        static_cast<T_val>(buffer_wavelengths[1]),
        xb);
    const mrf::data_struct::Array2D yb = spectral_buffer[0] + (spectral_buffer[1] - spectral_buffer[0]) * t;

    gray_image = (yb + ya) * (xb - xa) * 0.5;
    return;
  }

  // Start fall between two x values -> need to add a trapezoid

  assert(buffer_wavelengths[idx_start] >= start_wavelength);

  if (start_wavelength < buffer_wavelengths[idx_start])
  {
    assert(idx_start > 0);

    // Linear interpolation for spectrum at start wavelength
    const T_val xa = static_cast<T_val>(start_wavelength);
    const T_val ta = mrf::math::_Math<T_val>::alpha(
        static_cast<T_val>(buffer_wavelengths[idx_start - 1]),
        static_cast<T_val>(buffer_wavelengths[idx_start]),
        xa);
    const mrf::data_struct::Array2D ya
        = spectral_buffer[idx_start - 1] + (spectral_buffer[idx_start] - spectral_buffer[idx_start - 1]) * ta;

    T_val                     xb;
    mrf::data_struct::Array2D yb;

    // Check if upper bound is not also greater than the starting wavelength
    // This can happen when idx_start == idx_end
    if (buffer_wavelengths[idx_start] <= end_wavelength)
    {
      xb = static_cast<T_val>(buffer_wavelengths[idx_start]);
      yb = spectral_buffer[idx_start];
    }
    // We also need to interpolate the value at idx_start because it
    // might fall further than end_wavelength
    else
    {
      // Linear interpolation for spectrum at end wavelength
      xb             = static_cast<T_val>(end_wavelength);
      const T_val tb = mrf::math::_Math<T_val>::alpha(
          static_cast<T_val>(buffer_wavelengths[idx_start - 1]),
          static_cast<T_val>(buffer_wavelengths[idx_start]),
          xb);
      yb = spectral_buffer[idx_start - 1] + (spectral_buffer[idx_start] - spectral_buffer[idx_start - 1]) * tb;
    }

    // Trapezoidal integration
    gray_image += (yb + ya) * (xb - xa);
  }

  if (idx_end <= idx_start)
  {
    gray_image *= 0.5;
    return;
  }

  // End fall between two wavelengths of the spectrum -> need to interpolate
  if (end_wavelength > buffer_wavelengths[idx_end])
  {
    assert(idx_end <= buffer_wavelengths.size() - 2);

    const T_val                     xa = static_cast<T_val>(buffer_wavelengths[idx_end]);
    const mrf::data_struct::Array2D ya = spectral_buffer[idx_end];

    // Linear interpolation for spectrum at end wavelength
    const T_val xb = static_cast<T_val>(end_wavelength);
    const T_val tb = mrf::math::_Math<T_val>::alpha(
        static_cast<T_val>(buffer_wavelengths[idx_end]),
        static_cast<T_val>(buffer_wavelengths[idx_end + 1]),
        xb);
    const mrf::data_struct::Array2D yb
        = spectral_buffer[idx_end] + (spectral_buffer[idx_end + 1] - spectral_buffer[idx_end]) * tb;

    // Trapezoidal integration
    gray_image += (yb + ya) * (xb - xa);
  }


  // Potentially non uniform grid
  for (size_t i = idx_start; i < idx_end; i++)
  {
    // Trapezoidal integration
    gray_image
        += (spectral_buffer[i + 1] + spectral_buffer[i]) * T_val(buffer_wavelengths[i + 1] - buffer_wavelengths[i]);
  }

  gray_image *= 0.5;
}


// ----------------------------------------------------------------------------
// Spectrum photoscopic luminance
// ----------------------------------------------------------------------------

template<typename T_wave, typename T_val>
T_val _SpectrumConverter<T_wave, T_val>::photoscopicLuminance(
    mrf::radiometry::Spectrum_T<T_wave, T_val> const &a_spectrum) const
{
  T_val photoscopic_luminance_value(0);

  // integration cte should be one. We integrate at 1nm resolution
  for (uint i = 0; i < nbWavelengths(); i++)
  {
    const T_val spectrum_value = a_spectrum.findLinearyInterpolatedValue(_sensitivity_curve_first_wavelength + i);
    photoscopic_luminance_value += spectrum_value * _sensitivity_curves[SENSITIVITY_Y][i];
  }

  return T_val(683) * photoscopic_luminance_value;
}

// ----------------------------------------------------------------------------
// Emissive spectra and spectral images handling
// ----------------------------------------------------------------------------

template<typename T_wave, typename T_val>
_Color<T_val>
_SpectrumConverter<T_wave, T_val>::emissiveSpectrumToXYZ(mrf::radiometry::Spectrum_T<T_wave, T_val> const &s) const
{
  return emissiveSpectrumToXYZ(s.wavelengths(), s.values());
}


template<typename T_wave, typename T_val>
_Color<T_val> _SpectrumConverter<T_wave, T_val>::emissiveSpectrumToXYZ(
    std::vector<T_wave> const &spectrumWavelengths,
    std::vector<T_val> const & spectrumValues) const
{
  assert(spectrumWavelengths.size() == spectrumValues.size());

  if (spectrumValues.size() == 0)
  {
    return _Color<T_val>(0);
  }

  _Color<T_val> retVal(0);

  const T_wave start_wavelength = std::max<T_wave>(spectrumWavelengths.front(), _wavelengths.front());
  const T_wave end_wavelength   = std::min<T_wave>(spectrumWavelengths.back(), _wavelengths.back());

  // Early exit, selection out of range
  if (end_wavelength < start_wavelength)
  {
    return _Color<T_val>(0);
  }

  assert(start_wavelength <= end_wavelength);

  for (size_t idx_value = 0; idx_value < spectrumWavelengths.size() - 1; idx_value++)
  {
    T_wave wl_a = spectrumWavelengths[idx_value];
    T_wave wl_b = spectrumWavelengths[idx_value + 1];

    // We have not reached yet the starting point
    if (start_wavelength > wl_b)
    {
      continue;
    }

    // We have finished the integration
    if (end_wavelength < wl_a)
    {
      break;
    }

    if (start_wavelength > wl_a)
    {
      wl_a = start_wavelength;
    }

    if (end_wavelength < wl_b)
    {
      wl_b = end_wavelength;
    }

    const size_t idx_curve_start = wavelengthIndex(wl_a);
    size_t       idx_curve_end   = wavelengthIndex(wl_b);

    // On last intervall we need to include the last wavelength of the spectrum
    if (idx_value == spectrumWavelengths.size() - 2)
    {
      idx_curve_end = idx_curve_end + 1;
    }

    for (size_t idx_curve = idx_curve_start; idx_curve < idx_curve_end; idx_curve++)
    {
      const T_wave curr_wl    = wavelengthValue(idx_curve);
      const T_val  curr_value = mrf::math::_Math<T_val>::interp(
          static_cast<T_val>(curr_wl),
          static_cast<T_val>(spectrumWavelengths[idx_value]),
          static_cast<T_val>(spectrumWavelengths[idx_value + 1]),
          spectrumValues[idx_value],
          spectrumValues[idx_value + 1]);

      for (int c = 0; c < 3; c++)
      {
        retVal[c] += static_cast<const T_val>(_sensitivity_curves[c][idx_curve]) * curr_value;
      }
    }
  }

  return retVal;
}



template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::emissiveFramebufferToXYZImage(
    std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
    std::vector<T_wave> const &                   framebuffer_wavelengths,
    mrf::data_struct::_Array2D<T_val> &           x,
    mrf::data_struct::_Array2D<T_val> &           y,
    mrf::data_struct::_Array2D<T_val> &           z,
    std::function<void(int)>                      progress) const
{
  progress(0);

  const size_t w = spectral_framebuffer[0].width();
  const size_t h = spectral_framebuffer[0].height();

#ifdef MRF_WITH_EIGEN_SUPPORT
  x.setZero(h, w);
  y.setZero(h, w);
  z.setZero(h, w);
#else
  // This version involves memory allocations and deep copies
  // whereas all we want here is calls to something like std::memset
  x = mrf::data_struct::_Array2D<T_val>(w, h);
  y = mrf::data_struct::_Array2D<T_val>(w, h);
  z = mrf::data_struct::_Array2D<T_val>(w, h);
#endif

  const T_wave start_wavelength = std::max(framebuffer_wavelengths.front(), _wavelengths.front());
  const T_wave end_wavelength   = std::min(framebuffer_wavelengths.back(), _wavelengths.back());

  // Early exit, selection out of range
  if (end_wavelength < start_wavelength)
  {
    return;
  }

  assert(start_wavelength <= end_wavelength);

  // We iterate on each wavelength of the sensitivity curve and interpolate the spectrum of the image
  // We do not extrapolate the spectrum, we need to move the integration
  // domain to fit the spectral image domain
  for (size_t i = 0; i < framebuffer_wavelengths.size() - 1; i++)
  {
    T_wave wl_a = framebuffer_wavelengths[i];
    T_wave wl_b = framebuffer_wavelengths[i + 1];

    // We have not reached yet the starting point
    if (start_wavelength > wl_b)
    {
      continue;
    }

    // We have finished the integration
    if (end_wavelength < wl_a)
    {
      break;
    }

    assert(start_wavelength < wl_b);
    if (start_wavelength > wl_a)
    {
      wl_a = start_wavelength;
    }

    assert(end_wavelength > wl_a);
    if (end_wavelength < wl_b)
    {
      wl_b = end_wavelength;
    }

    const size_t idx_curve_start = wavelengthIndex(wl_a);
    size_t       idx_curve_end   = wavelengthIndex(wl_b);

    // On last intervall we need to include the last wavelength of the spectrum
    if (i == framebuffer_wavelengths.size() - 2)
    {
      idx_curve_end = idx_curve_end + 1;
    }

    for (size_t idx_curve = idx_curve_start; idx_curve < idx_curve_end; idx_curve++)
    {
      const T_wave curr_wl = wavelengthValue(idx_curve);

      // Compute the linear interpolation factor
      const double t
          = mrf::math::_Math<double>::alpha(framebuffer_wavelengths[i], framebuffer_wavelengths[i + 1], curr_wl);

      #pragma omp parallel for schedule(static)
      for (int j = 0; j < static_cast<int>(h); ++j)
      {
#ifdef MRF_WITH_EIGEN_SUPPORT
        const auto interpolated = spectral_framebuffer[i].row(j)
                                  + (spectral_framebuffer[i + 1].row(j) - spectral_framebuffer[i].row(j)) * t;
        x.row(j) += interpolated * xSensitivityAtIndex(idx_curve);
        y.row(j) += interpolated * ySensitivityAtIndex(idx_curve);
        z.row(j) += interpolated * zSensitivityAtIndex(idx_curve);
#else
        for (int k = 0; k < static_cast<int>(w); ++k)
        {
          const T_val interpolated = T_val(
              spectral_framebuffer[i](j, k) + (spectral_framebuffer[i + 1](j, k) - spectral_framebuffer[i](j, k)) * t);
          x(j, k) += interpolated * xSensitivityAtIndex(idx_curve);
          y(j, k) += interpolated * ySensitivityAtIndex(idx_curve);
          z(j, k) += interpolated * zSensitivityAtIndex(idx_curve);
        }
#endif
      }
    }

    progress(int(100.f * float(i) / float(framebuffer_wavelengths.size() - 1)));
  }

  progress(100);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::emissiveFramebufferToXYZImage(
    std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
    std::vector<T_wave> const &                   framebuffer_wavelengths,
    image::ColorImage &                           xyz_image,
    std::function<void(int)>                      progress) const
{
  if (spectral_framebuffer.size() == 0) return;

  const size_t w = spectral_framebuffer[0].width();
  const size_t h = spectral_framebuffer[0].height();

  std::array<mrf::data_struct::_Array2D<T_val>, 3> xyz;

  emissiveFramebufferToXYZImage(spectral_framebuffer, framebuffer_wavelengths, xyz[0], xyz[1], xyz[2], progress);

  xyz_image.init(w, h);

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < int(w * h); i++)
  {
    for (int c = 0; c < 3; c++)
    {
      xyz_image[i][c] = xyz[c](i);
    }
  }
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::emissiveSpectralImageToXYZImage(
    image::UniformSpectralImage const &spectral_image,
    mrf::data_struct::_Array2D<T_val> &x,
    mrf::data_struct::_Array2D<T_val> &y,
    mrf::data_struct::_Array2D<T_val> &z,
    std::function<void(int)>           progress) const
{
  emissiveFramebufferToXYZImage(spectral_image.data(), spectral_image.wavelengths(), x, y, z, progress);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::emissiveSpectralImageToXYZImage(
    image::UniformSpectralImage const &spectral_image,
    image::ColorImage &                xyz_image,
    std::function<void(int)>           progress) const
{
  emissiveFramebufferToXYZImage(spectral_image.data(), spectral_image.wavelengths(), xyz_image, progress);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::emissiveFramebufferToRGBImage(
    std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
    std::vector<T_wave> const &                   framebuffer_wavelengths,
    image::ColorImage &                           rgb_image,
    MRF_COLOR_SPACE                               colorSpace,
    MRF_WHITE_POINT                               whitePoint,
    std::function<void(int)>                      progress) const
{
  const size_t w = spectral_framebuffer[0].width();
  const size_t h = spectral_framebuffer[0].height();

  std::array<mrf::data_struct::Array2D, 3> xyz;

  emissiveFramebufferToXYZImage(spectral_framebuffer, framebuffer_wavelengths, xyz[0], xyz[1], xyz[2], progress);

  rgb_image.init(w, h);

  for (size_t y = 0; y < h; y++)
  {
    #pragma omp parallel for schedule(static)
    for (int x = 0; x < int(w); x++)
    {
      mrf::color::Color c(xyz[0](y, x), xyz[1](y, x), xyz[2](y, x));

      rgb_image(x, y) = c.XYZtoLinearRGB(colorSpace, whitePoint);
    }

    progress(int(float(100 * y) / float(h - 1)));
  }
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::emissiveSpectralImageToRGBImage(
    image::UniformSpectralImage const &spectral_image,
    image::ColorImage &                rgb_image,
    MRF_COLOR_SPACE                    colorSpace,
    MRF_WHITE_POINT                    whitePoint,
    std::function<void(int)>           progress) const
{
  emissiveFramebufferToRGBImage(
      spectral_image.data(),
      spectral_image.wavelengths(),
      rgb_image,
      colorSpace,
      whitePoint,
      progress);
}


// ----------------------------------------------------------------------------
// Reflective spectra and spectral images handling
// ----------------------------------------------------------------------------


template<typename T_wave, typename T_val>
_Color<T_val> _SpectrumConverter<T_wave, T_val>::reflectiveSpectrumToXYZ(
    mrf::radiometry::Spectrum_T<T_wave, T_val> const &s,
    T_val const *const                                illuminat_spd,
    T_wave const                                      illuminant_first_wavelength,
    T_wave const                                      illuminant_precision,
    size_t const                                      illuminant_array_size) const
{
  return reflectiveSpectrumToXYZ(
      s.wavelengths(),
      s.values(),
      illuminat_spd,
      illuminant_first_wavelength,
      illuminant_precision,
      illuminant_array_size);
}


template<typename T_wave, typename T_val>
_Color<T_val> _SpectrumConverter<T_wave, T_val>::reflectiveSpectrumToXYZ(
    std::vector<T_wave> const &spectrumWavelengths,
    std::vector<T_val> const & spectrumValues,
    const T_val *const         illuminant_spd,
    const T_wave               illuminant_first_wavelength,
    const T_wave               illuminant_precision,
    const size_t               illuminant_array_size) const
{
  assert(spectrumWavelengths.size() == spectrumValues.size());

  if (spectrumValues.size() == 0)
  {
    return _Color<T_val>(0);
  }

  const T_wave illuminant_last_wavelength
      = illuminant_first_wavelength + static_cast<T_wave>(illuminant_array_size + 1) * illuminant_precision;

  const T_wave start_wavelength
      = std::max(illuminant_first_wavelength, std::max(spectrumWavelengths.front(), _wavelengths.front()));

  const T_wave end_wavelength
      = std::min(illuminant_last_wavelength, std::min(spectrumWavelengths.back(), _wavelengths.back()));

  // Early exit, selection out of range
  if (end_wavelength < start_wavelength)
  {
    return _Color<T_val>(0);
  }

  assert(start_wavelength <= end_wavelength);

  T_val         normalisation_factor(0);
  _Color<T_val> retVal(0);

  for (size_t idx_value = 0; idx_value < spectrumWavelengths.size() - 1; idx_value++)
  {
    T_wave wl_a = spectrumWavelengths[idx_value];
    T_wave wl_b = spectrumWavelengths[idx_value + 1];

    // We have not reached yet the starting point
    if (start_wavelength > wl_b)
    {
      continue;
    }

    // We have finished the integration
    if (end_wavelength < wl_a)
    {
      break;
    }

    if (start_wavelength > wl_a)
    {
      wl_a = start_wavelength;
    }

    if (end_wavelength < wl_b)
    {
      wl_b = end_wavelength;
    }

    const size_t idx_curve_start = wavelengthIndex(wl_a);
    size_t       idx_curve_end   = wavelengthIndex(wl_b);

    // On last intervall we need to include the last wavelength of the spectrum
    if (idx_value == spectrumWavelengths.size() - 2)
    {
      idx_curve_end = idx_curve_end + 1;
    }

    for (size_t idx_curve = idx_curve_start; idx_curve < idx_curve_end; idx_curve++)
    {
      T_val        illu_value;
      const T_wave curr_wl = wavelengthValue(idx_curve);
      assert(curr_wl > illuminant_first_wavelength);

      const size_t idx_illu_cur = static_cast<size_t>((curr_wl - illuminant_first_wavelength) / illuminant_precision);
      assert(idx_illu_cur < illuminant_array_size);

      // We need to interpollate illuminant: sampling is not 1 nm
      if (illuminant_precision > 1 && idx_illu_cur < illuminant_array_size - 1)
      {
        const size_t idx_illu_next = idx_illu_cur + 1;

        const T_val wl_cur = static_cast<T_val>(
            illuminant_first_wavelength + static_cast<T_wave>(idx_illu_cur) * illuminant_precision);
        const T_val wl_next = static_cast<T_val>(
            illuminant_first_wavelength + static_cast<T_wave>(idx_illu_next) * illuminant_precision);

        const T_val value_cur  = illuminant_spd[idx_illu_cur];
        const T_val value_next = illuminant_spd[idx_illu_next];

        illu_value
            = mrf::math::_Math<T_val>::interp(static_cast<T_val>(curr_wl), wl_cur, wl_next, value_cur, value_next);
      }
      else
      {
        illu_value = illuminant_spd[idx_illu_cur];
      }

      normalisation_factor += illu_value * ySensitivityAtIndex(idx_curve);

      const T_val curr_value = illu_value
                               * mrf::math::_Math<T_val>::interp(
                                   static_cast<T_val>(curr_wl),
                                   static_cast<T_val>(spectrumWavelengths[idx_value]),
                                   static_cast<T_val>(spectrumWavelengths[idx_value + 1]),
                                   spectrumValues[idx_value],
                                   spectrumValues[idx_value + 1]);

      for (int c = 0; c < 3; c++)
      {
        retVal[c] += curr_value * static_cast<T_val>(_sensitivity_curves[c][idx_curve]);
      }
    }
  }

  retVal /= normalisation_factor;
  retVal.a() = 1.f;

  return retVal;
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::reflectiveFramebufferToXYZImage(
    std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
    std::vector<T_wave> const &                   framebuffer_wavelengths,
    const T_val *const                            illuminant_spd,
    T_wave                                        illuminant_first_wavelength,
    T_wave                                        illuminant_precision,
    size_t                                        illuminant_array_size,
    mrf::data_struct::_Array2D<T_val> &           x,
    mrf::data_struct::_Array2D<T_val> &           y,
    mrf::data_struct::_Array2D<T_val> &           z,
    std::function<void(int)>                      progress) const
{
  progress(0);

  const size_t w = spectral_framebuffer[0].width();
  const size_t h = spectral_framebuffer[0].height();

#ifdef MRF_WITH_EIGEN_SUPPORT
  x.setZero(h, w);
  y.setZero(h, w);
  z.setZero(h, w);
#else
  // This version involves memory allocations and deep copies
  // whereas all we want here is calls to something like std::memset
  x = mrf::data_struct::_Array2D<T_val>(w, h);
  y = mrf::data_struct::_Array2D<T_val>(w, h);
  z = mrf::data_struct::_Array2D<T_val>(w, h);
#endif

  const T_wave illuminant_last_wavelength
      = illuminant_first_wavelength + static_cast<T_wave>(illuminant_array_size + 1) * illuminant_precision;

  const T_wave start_wavelength
      = std::max(illuminant_first_wavelength, std::max(framebuffer_wavelengths.front(), _wavelengths.front()));

  const T_wave end_wavelength
      = std::min(illuminant_last_wavelength, std::min(framebuffer_wavelengths.back(), _wavelengths.back()));

  T_val normalisation_factor(0);

  // We iterate on each wavelength of the sensitivity curve and interpolate the spectrum of the image
  // We do not extrapolate the spectrum, we need to move the integration
  // domain to fit the spectral image domain
  for (size_t i = 0; i < framebuffer_wavelengths.size() - 1; i++)
  {
    T_wave wl_a = framebuffer_wavelengths[i];
    T_wave wl_b = framebuffer_wavelengths[i + 1];

    // We have not reached yet the starting point
    if (start_wavelength > wl_b)
    {
      continue;
    }

    // We have finished the integration
    if (end_wavelength < wl_a)
    {
      break;
    }

    assert(start_wavelength < wl_b);
    if (start_wavelength > wl_a)
    {
      wl_a = start_wavelength;
    }

    assert(end_wavelength > wl_a);
    if (end_wavelength < wl_b)
    {
      wl_b = end_wavelength;
    }

    const size_t idx_curve_start = wavelengthIndex(wl_a);
    size_t       idx_curve_end   = wavelengthIndex(wl_b);

    // On last intervall we need to include the last wavelength of the spectrum
    if (i == framebuffer_wavelengths.size() - 2)
    {
      idx_curve_end = idx_curve_end + 1;
    }

    for (size_t idx_curve = idx_curve_start; idx_curve < idx_curve_end; idx_curve++)
    {
      T_val        illu_value;
      const T_wave curr_wl = wavelengthValue(idx_curve);

      const size_t idx_illu_a = (curr_wl - illuminant_first_wavelength) / illuminant_precision;
      assert(idx_illu_a < illuminant_array_size);

      // We need to interpollate illuminant: sampling is not 1 nm
      if (illuminant_precision > 1 && idx_illu_a < illuminant_array_size - 1)
      {
        const size_t idx_illu_b = idx_illu_a + 1;

        const T_val wl_illu_a
            = static_cast<T_val>(illuminant_first_wavelength + static_cast<T_wave>(idx_illu_a) * illuminant_precision);
        const T_val wl_illu_b
            = static_cast<T_val>(illuminant_first_wavelength + static_cast<T_wave>(idx_illu_b) * illuminant_precision);

        const T_val value_a = illuminant_spd[idx_illu_a];
        const T_val value_b = illuminant_spd[idx_illu_b];

        illu_value
            = mrf::math::_Math<T_val>::interp(static_cast<T_val>(curr_wl), wl_illu_a, wl_illu_b, value_a, value_b);
      }
      else
      {
        illu_value = illuminant_spd[idx_illu_a];
      }

      normalisation_factor += illu_value * ySensitivityAtIndex(idx_curve);

      std::array<T_val, N_SENSITIVITY_CHANNELS> illu_sensitivity;
      for (size_t c = 0; c < N_SENSITIVITY_CHANNELS; c++)
      {
        illu_sensitivity[c] = illu_value * _sensitivity_curves[c][idx_curve];
      }

      // Compute the linear interpolation factor
      const double t
          = mrf::math::_Math<double>::alpha(framebuffer_wavelengths[i], framebuffer_wavelengths[i + 1], curr_wl);

      #pragma omp parallel for schedule(static)
      for (int j = 0; j < static_cast<int>(h); ++j)
      {
#ifdef MRF_WITH_EIGEN_SUPPORT
        const auto interpolated = spectral_framebuffer[i].row(j)
                                  + (spectral_framebuffer[i + 1].row(j) - spectral_framebuffer[i].row(j)) * t;
        x.row(j) += interpolated * illu_sensitivity[SENSITIVITY_X];
        y.row(j) += interpolated * illu_sensitivity[SENSITIVITY_Y];
        z.row(j) += interpolated * illu_sensitivity[SENSITIVITY_Z];
#else
        for (int k = 0; k < static_cast<int>(w); ++k)
        {
          const T_val interpolated = static_cast<T_val>(
              spectral_framebuffer[i](j, k) + (spectral_framebuffer[i + 1](j, k) - spectral_framebuffer[i](j, k)) * t);
          x(j, k) += interpolated * static_cast<T_val>(illu_sensitivity[SENSITIVITY_X]);
          y(j, k) += interpolated * static_cast<T_val>(illu_sensitivity[SENSITIVITY_Y]);
          z(j, k) += interpolated * static_cast<T_val>(illu_sensitivity[SENSITIVITY_Z]);
        }
#endif
      }
    }

    progress(int(float(100 * i) / float(framebuffer_wavelengths.size() - 1)));
  }


  x /= normalisation_factor;
  y /= normalisation_factor;
  z /= normalisation_factor;

  progress(100);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::reflectiveFramebufferToXYZImage(
    std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
    std::vector<T_wave> const &                   framebuffer_wavelengths,
    const T_val *const                            illuminant_spd,
    T_wave                                        illuminant_first_wavelength,
    T_wave                                        illuminant_precision,
    size_t                                        illuminant_array_size,
    mrf::image::ColorImage &                      xyz_image,
    std::function<void(int)>                      progress) const
{
  if (spectral_framebuffer.size() == 0) return;

  const size_t w = spectral_framebuffer[0].width();
  const size_t h = spectral_framebuffer[0].height();

  std::array<mrf::data_struct::_Array2D<T_val>, 3> xyz;

  reflectiveFramebufferToXYZImage(
      spectral_framebuffer,
      framebuffer_wavelengths,
      illuminant_spd,
      illuminant_first_wavelength,
      illuminant_precision,
      illuminant_array_size,
      xyz[0],
      xyz[1],
      xyz[2],
      progress);

  xyz_image.init(w, h);

  #pragma omp parallel for schedule(static)
  for (int i = 0; i < int(w * h); i++)
  {
    for (int c = 0; c < 3; c++)
    {
      xyz_image[i][c] = xyz[c](i);
    }
  }
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::reflectiveSpectralImageToXYZImage(
    const image::UniformSpectralImage &spectral_image,
    const T_val *const                 illuminant_spd,
    T_wave                             illuminant_first_wavelength,
    T_wave                             illuminant_precision,
    size_t                             illuminant_array_size,
    image::ColorImage &                xyz_image,
    std::function<void(int)>           progress) const
{
  reflectiveFramebufferToXYZImage(
      spectral_image.data(),
      spectral_image.wavelengths(),
      illuminant_spd,
      illuminant_first_wavelength,
      illuminant_precision,
      illuminant_array_size,
      xyz_image,
      progress);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::reflectiveSpectralImageToXYZImage(
    const image::UniformSpectralImage &spectral_image,
    const T_val *const                 illuminant_spd,
    T_wave                             illuminant_first_wavelength,
    T_wave                             illuminant_precision,
    size_t                             illuminant_array_size,
    mrf::data_struct::_Array2D<T_val> &x,
    mrf::data_struct::_Array2D<T_val> &y,
    mrf::data_struct::_Array2D<T_val> &z,
    std::function<void(int)>           progress) const
{
  reflectiveFramebufferToXYZImage(
      spectral_image.data(),
      spectral_image.wavelengths(),
      illuminant_spd,
      illuminant_first_wavelength,
      illuminant_precision,
      illuminant_array_size,
      x,
      y,
      z,
      progress);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::reflectiveFramebufferToRGBImage(
    std::vector<mrf::data_struct::Array2D> const &spectral_framebuffer,
    std::vector<T_wave> const &                   framebuffer_wavelengths,
    const T_val *const                            illuminant_spd,
    T_wave                                        illuminant_first_wavelength,
    T_wave                                        illuminant_precision,
    size_t                                        illuminant_array_size,
    image::ColorImage &                           rgb_image,
    MRF_COLOR_SPACE                               colorSpace,
    MRF_WHITE_POINT                               whitePoint,
    std::function<void(int)>                      progress) const
{
  progress(0);

  const size_t w = spectral_framebuffer[0].width();
  const size_t h = spectral_framebuffer[0].height();

  std::array<mrf::data_struct::Array2D, 3> xyz;

  reflectiveFramebufferToXYZImage(
      spectral_framebuffer,
      framebuffer_wavelengths,
      illuminant_spd,
      illuminant_first_wavelength,
      illuminant_precision,
      illuminant_array_size,
      xyz[0],
      xyz[1],
      xyz[2],
      progress);

  rgb_image.init(w, h);

  for (size_t y = 0; y < h; y++)
  {
    #pragma omp parallel for schedule(static)
    for (int x = 0; x < int(w); x++)
    {
      mrf::color::Color c(xyz[0](y, x), xyz[1](y, x), xyz[2](y, x));

      rgb_image(x, y) = c.XYZtoLinearRGB(colorSpace, whitePoint);
    }

    progress(float(100 * y) / float(h - 1));
  }

  progress(100);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::reflectiveSpectralImageToRGBImage(
    const image::UniformSpectralImage &spectral_image,
    const T_val *const                 illuminant_spd,
    T_wave                             illuminant_first_wavelength,
    T_wave                             illuminant_precision,
    size_t                             illuminant_array_size,
    image::ColorImage &                rgb_image,
    MRF_COLOR_SPACE                    colorSpace,
    MRF_WHITE_POINT                    whitePoint,
    std::function<void(int)>           progress) const
{
  reflectiveFramebufferToRGBImage(
      spectral_image.data(),
      spectral_image.wavelengths(),
      illuminant_spd,
      illuminant_first_wavelength,
      illuminant_precision,
      illuminant_array_size,
      rgb_image,
      colorSpace,
      whitePoint,
      progress);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::bispectralFramebufferToXYZImage(
    const std::vector<mrf::data_struct::Array2D> &reflective_framebuffer,
    const std::vector<mrf::data_struct::Array2D> &reradiation,
    const std::vector<T_wave> &                   framebuffer_wavelengths,
    const T_val *const                            illuminant_spd,
    T_wave                                        illuminant_first_wavelength,
    T_wave                                        illuminant_precision,
    size_t                                        illuminant_array_size,
    mrf::data_struct::_Array2D<T_val> &           x,
    mrf::data_struct::_Array2D<T_val> &           y,
    mrf::data_struct::_Array2D<T_val> &           z,
    std::function<void(int)>                      progress) const
{
  progress(0);

  const size_t w = reflective_framebuffer[0].width();
  const size_t h = reflective_framebuffer[0].height();

  //  for (const auto& a: reflective_framebuffer) {
  //      if (a.width() != w || a.height() != h) {
  //          std::cerr << "Unmatched size in reflective buffer" << std::endl;
  //      }
  //  }

  //  for (const auto& a: reradiation) {
  //      if (a.width() != w || a.height() != h) {
  //          std::cerr << "Unmatched size in reradiation buffer" << std::endl;
  //      }
  //  }

  //  const size_t nbands = framebuffer_wavelengths.size();
  //  if (reflective_framebuffer.size() != nbands) {
  //      std::cerr << "Unmatched size in reflective buffer" << std::endl;
  //  }

  //  if (reradiation.size() != (nbands * (nbands -1) / 2)) {
  //      std::cerr << "Unmatched size in reflective buffer" << std::endl;
  //  }
  mrf::data_struct::Array2D zeros;

#ifdef MRF_WITH_EIGEN_SUPPORT
  x.setZero(h, w);
  y.setZero(h, w);
  z.setZero(h, w);
  zeros.setZero(h, w);
#else
  // This version involves memory allocations and deep copies
  // whereas all we want here is calls to something like std::memset
  x     = mrf::data_struct::_Array2D<T_val>(w, h);
  y     = mrf::data_struct::_Array2D<T_val>(w, h);
  z     = mrf::data_struct::_Array2D<T_val>(w, h);
  zeros = mrf::data_struct::_Array2D<T_val>(w, h);
#endif

  const T_wave illuminant_last_wavelength
      = illuminant_first_wavelength + static_cast<T_wave>(illuminant_array_size - 1) * illuminant_precision;

  const T_wave start_wavelength
      = std::max(illuminant_first_wavelength, std::max(framebuffer_wavelengths.front(), _wavelengths.front()));

  const T_wave end_wavelength
      = std::min(illuminant_last_wavelength, std::min(framebuffer_wavelengths.back(), _wavelengths.back()));

  if (end_wavelength < start_wavelength)
  {
    return;
  }

  T_val normalisation_factor(0);

  std::function<mrf::data_struct::Array2D(size_t, size_t)> value
      = [reflective_framebuffer, reradiation, zeros](size_t wi, size_t wo) {
          if (wi > wo)
          {
            return zeros;
          }
          if (wi == wo)
          {
            return reflective_framebuffer[wi];
          }
          assert(wo > 0);

          assert(wi < wo);
          return reradiation[wo * (wo - 1) / 2 + wi];
        };

  int done = 0;

  #pragma omp parallel for schedule(static)
  for (int scanline_idx = 0; scanline_idx < static_cast<int>(h); scanline_idx++)
  {
    // We iterate on each wavelength of the sensitivity curve and interpolate the spectrum of the image
    // We do not extrapolate the spectrum, we need to move the integration
    // domain to fit the spectral image domain
    for (size_t wl_idx_i = 0; wl_idx_i < framebuffer_wavelengths.size() - 1; wl_idx_i++)
    {
      T_wave wl_i_a = framebuffer_wavelengths[wl_idx_i];
      T_wave wl_i_b = framebuffer_wavelengths[wl_idx_i + 1];

      // We have not reached yet the starting point
      if (start_wavelength > wl_i_b)
      {
        continue;
      }

      // We have finished the integration
      if (end_wavelength < wl_i_a)
      {
        break;
      }

      assert(start_wavelength < wl_i_b);
      if (start_wavelength > wl_i_a)
      {
        wl_i_a = start_wavelength;
      }

      assert(end_wavelength > wl_i_a);
      if (end_wavelength < wl_i_b)
      {
        wl_i_b = end_wavelength;
      }

      const size_t idx_illu_start = wl_i_a - illuminant_first_wavelength;
      size_t       idx_illu_end   = wl_i_b - illuminant_first_wavelength;

      // On last interval we need to include the last wavelength of the spectrum
      if (wl_idx_i == framebuffer_wavelengths.size() - 2)
      {
        idx_illu_end = idx_illu_end + 1;
      }

      for (size_t wl_idx_o = wl_idx_i; wl_idx_o < framebuffer_wavelengths.size() - 1; wl_idx_o++)
      {
        T_wave wl_o_a = framebuffer_wavelengths[wl_idx_o];
        T_wave wl_o_b = framebuffer_wavelengths[wl_idx_o + 1];

        // We have not reached yet the starting point
        if (start_wavelength > wl_o_b)
        {
          continue;
        }

        // We have finished the integration
        if (end_wavelength < wl_o_a)
        {
          break;
        }

        if (start_wavelength > wl_o_a)
        {
          wl_o_a = start_wavelength;
        }

        if (end_wavelength < wl_o_b)
        {
          wl_o_b = end_wavelength;
        }

        const size_t idx_cmf_start = wavelengthIndex(wl_o_a);
        size_t       idx_cmf_end   = wavelengthIndex(wl_o_b);

        // On last interval we need to include the last wavelength of the spectrum
        if (wl_idx_o == framebuffer_wavelengths.size() - 2)
        {
          idx_cmf_end = idx_cmf_end + 1;
        }


        for (size_t idx_illu = idx_illu_start; idx_illu < idx_illu_end; idx_illu++)
        {
          const T_wave curr_wl_i  = static_cast<T_wave>(idx_illu) + illuminant_first_wavelength;
          const T_val  illu_value = illuminant_spd[idx_illu];

          assert(idx_illu < illuminant_array_size);

          const T_val interp_illu = mrf::math::_Math<T_val>::alpha(
              (T_val)framebuffer_wavelengths[wl_idx_i],
              (T_val)framebuffer_wavelengths[wl_idx_i + 1],
              (T_val)curr_wl_i);

          normalisation_factor += illu_value * _sensitivity_curves[SENSITIVITY_Y][idx_cmf_start];

          for (size_t idx_cmf = idx_cmf_start; idx_cmf < idx_cmf_end; idx_cmf++)
          {
            const T_wave curr_wl_o   = wavelengthValue(idx_cmf);
            const T_val  cmf_value_x = _sensitivity_curves[SENSITIVITY_X][idx_cmf];
            const T_val  cmf_value_y = _sensitivity_curves[SENSITIVITY_Y][idx_cmf];
            const T_val  cmf_value_z = _sensitivity_curves[SENSITIVITY_Z][idx_cmf];

            const T_val interp_rerad = mrf::math::_Math<T_val>::alpha(
                (T_val)framebuffer_wavelengths[wl_idx_o],
                (T_val)framebuffer_wavelengths[wl_idx_o + 1],
                (T_val)curr_wl_o);

#ifdef MRF_WITH_EIGEN_SUPPORT
            // Interpolation
            const auto interpolated =   // nearest... faster but less accurate:
                                        //                       value(wl_idx_i, wl_idx_o).row(scanline_idx);
                (1 - interp_rerad)
                    * ((1 - interp_illu) * value(wl_idx_i, wl_idx_o).row(scanline_idx)
                       + (interp_illu)*value(wl_idx_i + 1, wl_idx_o).row(scanline_idx))
                + (interp_rerad)
                      * ((1 - interp_illu) * value(wl_idx_i, wl_idx_o + 1).row(scanline_idx)
                         + (interp_illu)*value(wl_idx_i + 1, wl_idx_o + 1).row(scanline_idx));


            x.row(scanline_idx) += interpolated * illu_value * cmf_value_x;
            y.row(scanline_idx) += interpolated * illu_value * cmf_value_y;
            z.row(scanline_idx) += interpolated * illu_value * cmf_value_z;
#else
            for (int k = 0; k < static_cast<int>(w); ++k)
            {
              const auto interpolated = (1 - interp_rerad)
                                            * ((1 - interp_illu) * value(wl_idx_i, wl_idx_o)(scanline_idx, k)
                                               + (interp_illu)*value(wl_idx_i + 1, wl_idx_o)(scanline_idx, k))
                                        + (interp_rerad)
                                              * ((1 - interp_illu) * value(wl_idx_i, wl_idx_o + 1)(scanline_idx, k)
                                                 + (interp_illu)*value(wl_idx_i + 1, wl_idx_o + 1)(scanline_idx, k));


              x(scanline_idx, k) += interpolated * illu_value * cmf_value_x;
              y(scanline_idx, k) += interpolated * illu_value * cmf_value_y;
              z(scanline_idx, k) += interpolated * illu_value * cmf_value_z;
            }
#endif
          }
        }
      }
    }
    #pragma omp critical
    {
      progress(float(100 * done++) / float(h - 1));
    }
  }

  x /= normalisation_factor;
  y /= normalisation_factor;
  z /= normalisation_factor;

  progress(100);
}


template<typename T_wave, typename T_val>
void _SpectrumConverter<T_wave, T_val>::bispectralFramebufferToRGBImage(
    const std::vector<mrf::data_struct::Array2D> &reflective_framebuffer,
    const std::vector<mrf::data_struct::Array2D> &reradiation,
    const std::vector<T_wave> &                   framebuffer_wavelengths,
    const T_val *const                            illuminant_spd,
    T_wave                                        illuminant_first_wavelength,
    T_wave                                        illuminant_precision,
    size_t                                        illuminant_array_size,
    mrf::image::ColorImage &                      rgb_image,
    MRF_COLOR_SPACE                               colorSpace,
    MRF_WHITE_POINT                               whitePoint,
    std::function<void(int)>                      progress) const
{
  progress(0);

  const size_t w = reflective_framebuffer[0].width();
  const size_t h = reflective_framebuffer[0].height();

  std::array<mrf::data_struct::Array2D, 3> xyz;

  bispectralFramebufferToXYZImage(
      reflective_framebuffer,
      reradiation,
      framebuffer_wavelengths,
      illuminant_spd,
      illuminant_first_wavelength,
      illuminant_precision,
      illuminant_array_size,
      xyz[0],
      xyz[1],
      xyz[2],
      progress);

  rgb_image.init(w, h);

  for (size_t y = 0; y < h; y++)
  {
    #pragma omp parallel for schedule(static)
    for (int x = 0; x < int(w); x++)
    {
      mrf::color::Color c(xyz[0](y, x), xyz[1](y, x), xyz[2](y, x));

      rgb_image(x, y) = c.XYZtoLinearRGB(colorSpace, whitePoint);
    }

    progress(float(100 * y) / float(h - 1));
  }

  progress(100);
}


typedef _SpectrumConverter<mrf::uint, float> SpectrumConverter;


}   // namespace color
}   // namespace mrf
