/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2015,2017
 *
 **/

#pragma once

#include <mrf_core/mrf_cuda_support.hpp>
#include <mrf_core/math/math.hpp>

#include <iostream>
#include <cmath>

namespace mrf
{
namespace geom
{
/**
 *
 * This is the future AABBox with MRF Vector
 * TO BE INTEGRATED AND TESTED !!!
 * Code adapted from LAIR thanks to Xavier Granier
 **/
template<typename T1 = float, template<typename = T1> class VEC_TYPE = mrf::math::Vec3>
class _AABBox
{
protected:
  VEC_TYPE<T1> _min;
  VEC_TYPE<T1> _max;

public:
  enum Bounds
  {
    Min_X = 0,
    Min_Y = 1,
    Min_Z = 2,
    Max_X = 3,
    Max_Y = 4,
    Max_Z = 5
  };

  inline _AABBox(): _min(T1(0), T1(0), T1(0)), _max(T1(0), T1(0), T1(0)) {}

  inline _AABBox(VEC_TYPE<T1> const &Min, VEC_TYPE<T1> const &Max): _min(Min), _max(Max) {}

  inline _AABBox(_AABBox const &aab): _min(aab._min), _max(aab._max) {}



  inline bool contains(VEC_TYPE<T1> const &v) const
  {
    return (_min[0] <= v[0]) && (_min[1] <= v[1]) && (_min[2] <= v[2]) && (v[0] <= _max[0]) && (v[1] <= _max[1])
           && (v[2] <= _max[2]);
  }


  /// compute the projected area along a direction
  inline T1 getProjectedArea(VEC_TYPE<T1> const &_v) const
  {
    return mrf::math::_Math<T1>::absVal(_v[0]) * (getY() - gety()) * (getZ() - getz())
           + mrf::math::_Math<T1>::absVal(_v[1]) * (getZ() - getz()) * (getX() - getx())
           + mrf::math::_Math<T1>::absVal(_v[2]) * (getX() - getx()) * (getY() - gety());
  }


  inline void getBoundingBox(VEC_TYPE<T1> &min, VEC_TYPE<T1> &max) const
  {
    min = _min;
    max = _max;
  }

  /**
   * @return The radius of the scene, aka. ( center(), getRadius )
   * is the Bounding Sphere
   * */
  inline T1 getRadius() const
  {
    VEC_TYPE<T1> diagonal(_max - _min);
    return diagonal.length() * static_cast<T1>(0.5);
  }


  /** Check if there is an intersection and return Point and Normal At the intersection
  @param _org origine
  @param _dir direction
  @param _mid minimum distance
  @param _mad maximum distance
  @param _pnt return the point intersection
  @param _nrm return the normal at point intersection
  */
  inline bool isIntersected(
      VEC_TYPE<T1> const &_org,
      VEC_TYPE<T1> const &_dir,
      T1 const            _mind,
      T1 const            _maxd,
      VEC_TYPE<T1> &      _pnt,
      VEC_TYPE<T1> &      _nrm) const
  {
    T1 t1, t2;

    T1 min[3];
    T1 max[3];

    for (unsigned int i = 0; i < 3; i++)
    {
      T1 div = 1.f / _dir[i];
      if (div >= 0)
      {
        min[i] = (getMin()[i] - _org[i]) * div;
        max[i] = (getMax()[i] - _org[i]) * div;
      }
      else
      {
        max[i] = (getMin()[i] - _org[i]) * div;
        min[i] = (getMax()[i] - _org[i]) * div;
      }
      if ((max[i] < _mind) || (min[i] > _maxd)) return false;
    }
    t1 = _mind;
    if (min[0] > t1) t1 = min[0];
    if (min[1] > t1) t1 = min[1];
    if (min[2] > t1) t1 = min[2];

    t2 = _maxd;
    if (max[0] < t2) t2 = max[0];
    if (max[1] < t2) t2 = max[1];
    if (max[2] < t2) t2 = max[2];

    if (t2 < t1)
    {
      return false;
    }
    // The ray hit the bounding box
    // with parameter t1.
    _pnt = _org + t1 * _dir;
    _nrm = -_dir;
    return true;
  }



  /** Check if there is an intersection
  @param _org origine
  @param _dir direction
  @param _mid minimum distance
  @param _mad maximum distance
  */
  inline bool isIntersected(VEC_TYPE<T1> const &_org, VEC_TYPE<T1> const &_dir, T1 const _mind, T1 const _maxd) const
  {
    T1 t1, t2;
    T1 min[3];
    T1 max[3];

    for (unsigned int i = 0; i < 3; i++)
    {
      T1 div = 1.f / _dir[i];
      if (div >= 0)
      {
        min[i] = (getMin()[i] - _org[i]) * div;
        max[i] = (getMax()[i] - _org[i]) * div;
      }
      else
      {
        max[i] = (getMin()[i] - _org[i]) * div;
        min[i] = (getMax()[i] - _org[i]) * div;
      }
      if ((max[i] < _mind) || (min[i] > _maxd)) return false;
    }
    t1 = _mind;
    if (min[0] > t1) t1 = min[0];
    if (min[1] > t1) t1 = min[1];
    if (min[2] > t1) t1 = min[2];

    t2 = _maxd;
    if (max[0] < t2) t2 = max[0];
    if (max[1] < t2) t2 = max[1];
    if (max[2] < t2) t2 = max[2];

    return (t2 >= t1);
  }



  //@}
  /** @name Sampling */
  //@{
  ///Try to compute a point with three T1 between 0 and 1 (no check). return false if not.
  inline bool getUniformSample(T1 const _e1, T1 const _e2, T1 const _e3, VEC_TYPE<T1> &_res) const
  {
    _res.set(_e1 * (getX() - getx()) + getx(), _e2 * (getY() - gety()) + gety(), _e3 * (getZ() - getz()) + getz());

    return true;
  }


  //@}
  /** @name Affine Transformation */
  //@{
  ///
  inline void translate(VEC_TYPE<T1> const &dt)
  {
    _min += dt;
    _max += dt;
  }

  ///
  inline T1 operator[](unsigned char c) const { return (c < 3) ? _min[c] : _max[c - 3]; }

  ///
  inline T1 getx() const { return _min[0]; }

  ///
  inline T1 getX() const { return _max[0]; }

  ///
  inline T1 gety() const { return _min[1]; }

  ///
  inline T1 getY() const { return _max[1]; }

  ///
  inline T1 getz() const { return _min[2]; }

  ///
  inline T1 getZ() const { return _max[2]; }

  ///
  inline VEC_TYPE<T1> const  getMin() const { return _min; }
  inline VEC_TYPE<T1> const &min() const { return _min; };



  ///
  inline VEC_TYPE<T1> const  getMax() const { return _max; }
  inline VEC_TYPE<T1> const &max() const { return _max; }

  ///
  inline void getCenter(VEC_TYPE<T1> &_center) const { _center = (_max + _min) * T1(0.5); }

  inline VEC_TYPE<T1> center() const { return (_max + _min) * T1(0.5); }

  /**
   * Returns an unsigned int representing which AABBox dimension
   * is the largest
   * 0 : X axis
   * 1 : Y axis
   * 2 : Z axis
   **/
  inline unsigned int getMaximumDimension() const
  {
    T1 const dX = _max.x() - _min.x();
    T1 const dY = _max.y() - _min.y();
    T1 const dZ = _max.z() - _min.z();

    if (dX >= dY && dX >= dZ)
    {
      return 0;
    }

    if (dY >= dX && dY >= dZ)
    {
      return 1;
    }

    if (dZ >= dX && dZ >= dX)
    {
      return 2;
    }

    assert(0);
    return 0;
  }


  /// Volume
  inline T1 getMeasure() const { return (getX() - getx()) * (getY() - gety()) * (getZ() - getz()); }

  /// Surface
  inline T1 getSurface() const
  {
    VEC_TYPE<T1> s = _max - _min;
    return (T1)2.0 * (s.x() * s.y() + s.y() * s.z() + s.x() * s.z());
  }


  /// Expand the axis-aligned box to include the given object.
  inline void expandBy(_AABBox const &_aabb)
  {
    if (_aabb._max[0] > _max[0]) _max[0] = _aabb._max[0];
    if (_aabb._max[1] > _max[1]) _max[1] = _aabb._max[1];
    if (_aabb._max[2] > _max[2]) _max[2] = _aabb._max[2];

    if (_aabb._min[0] < _min[0]) _min[0] = _aabb._min[0];
    if (_aabb._min[1] < _min[1]) _min[1] = _aabb._min[1];
    if (_aabb._min[2] < _min[2]) _min[2] = _aabb._min[2];
  }

  /// Expand the axis-aligned box to include the given point.
  inline void expandBy(VEC_TYPE<T1> const &_v)
  {
    if (_v[0] > _max[0]) _max[0] = _v[0];
    if (_v[1] > _max[1]) _max[1] = _v[1];
    if (_v[2] > _max[2]) _max[2] = _v[2];

    if (_v[0] < _min[0]) _min[0] = _v[0];
    if (_v[1] < _min[1]) _min[1] = _v[1];
    if (_v[2] < _min[2]) _min[2] = _v[2];
  }

  /**
   * Contains returns true if the axis-aligned box passed is entirely contained
   * in the current object
   **/
  inline bool isContainedBy(_AABBox const &_aabb) const
  {
    return (getx() >= _aabb.getx()) && (gety() >= _aabb.gety()) && (getz() >= _aabb.getz()) && (getX() <= _aabb.getX())
           && (getY() <= _aabb.getY()) && (getZ() <= _aabb.getZ());
  }

  /**
   * Outside returns true if the axis-aligned box passed is entirely outside
   * in the current object
   **/
  inline bool isOutsideOf(_AABBox const &_aabb) const
  {
    return (getx() >= _aabb.getX()) || (_aabb.getx() >= getX()) || (gety() >= _aabb.getY()) || (_aabb.gety() >= getY())
           || (getz() >= _aabb.getZ()) || (_aabb.getz() >= getZ());
  }

  /**
   * Overlap returns true if the two axis-aligned boxes overlap.
   **/
  inline bool isOverlapedBy(_AABBox const &_aabb) const
  {
    return (getX() <= _aabb.getx()) && (getx() >= _aabb.getX()) && (getY() <= _aabb.gety()) && (gety() >= _aabb.getY())
           && (getZ() <= _aabb.getz()) && (getz() >= _aabb.getZ());
  }

  /**
   * Returns the smallest axis-aligned box that includes all points
   * inside the two given boxes.
   **/
  inline _AABBox createUnion(_AABBox const &_aabb) const
  {
    VEC_TYPE<T1> min = _min;
    VEC_TYPE<T1> max = _max;
    min.minimize(_aabb._min);
    max.maximize(_aabb._max);
    return _AABBox(min, max);
  }

  inline void mergeBox(_AABBox const &a_aabb)
  {
    VEC_TYPE<T1> min = _min;
    VEC_TYPE<T1> max = _max;
    min.minimize(a_aabb._min);
    max.maximize(a_aabb._max);

    _min = min;
    _max = max;
  }

  inline void makeFromTriangle(VEC_TYPE<T1> const &a, VEC_TYPE<T1> const &b, VEC_TYPE<T1> const &c)
  {
    _min = a;
    _min.minimize(b);
    _min.minimize(c);

    _max = a;
    _max.maximize(b);
    _max.maximize(c);
  }


  inline _AABBox createIntersection(_AABBox const &_aabb) const
  {
    _AABBox ret = *(this);
    if (ret.isOverlapedBy(_aabb))
    {
      ret._min.maximize(_aabb._min);
      ret._max.minimize(_aabb._max);
      return ret;
    }
    else
    {
      // Null intersection.
      return _AABBox(VEC_TYPE<T1>(0, 0, 0), VEC_TYPE<T1>(0, 0, 0));
    }
  }

  inline std::string info() const
  {
    std::string the_info = " AABBox { min  = ";
    the_info.append(_min.info());
    the_info.append("; max = ");
    the_info.append(_max.info());
    the_info.append(" } ");
    return the_info;
  }

  inline bool operator==(_AABBox const &a_aabbox) { return (a_aabbox.min() == _min) && (a_aabbox.max() == _max); }

  inline _AABBox &operator=(const _AABBox &other)
  {
    if (this != &other)
    {
      _min = other._min;
      _max = other._max;
    }
    return *this;
  }
};

template<typename T1, template<typename = T1> class VEC_TYPE>
inline std::ostream &operator<<(std::ostream &os, _AABBox<T1, VEC_TYPE> const &bBox)
{
  return os << "min = " << bBox.min() << "max = " << bBox.max() << "(center = " << bBox.center()
            << " radius = " << bBox.getRadius() << ")" << std::endl;
}


// TO BE TESTED WHEN READY
typedef _AABBox<float, mrf::math::Vec3> AABBox;
typedef AABBox                          MRF_AABBox;


#if defined(_WIN32)
static inline unsigned int memorySize(AABBox const & /*a_bbox*/)
#else
static inline unsigned int memorySize(MRF_AABBox const & /*a_bbox*/)
#endif
{
  //Two vertices each of them has 3 coordinates
  return 2 * 3 * sizeof(float);
}

#if defined(_WIN32)
static inline void print(AABBox const &box)
#else
static inline void         print(MRF_AABBox const &box)
#endif
{
  std::cout << " AABBox = [ Min =  " << box.getMin() << " ; Max = " << box.getMax() << " ] " << std::endl;
}


template<typename T1, template<typename = T1> class VEC_TYPE>
static inline _AABBox<T1, VEC_TYPE> scale(_AABBox<T1, VEC_TYPE> const &box, T1 sx, T1 sy, T1 sz)
{
  VEC_TYPE<T1> min = box.getMin();
  VEC_TYPE<T1> max = box.getMax();

  min[0] *= sx;
  min[1] *= sy;
  min[2] *= sz;
  max[0] *= sx;
  max[1] *= sy;
  max[2] *= sz;

  return _AABBox<T1, VEC_TYPE>(min, max);
}
}   // namespace geom
}   // namespace mrf
