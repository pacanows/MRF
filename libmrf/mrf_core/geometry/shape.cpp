#include <mrf_core/geometry/shape.hpp>

namespace mrf
{
namespace geom
{
using namespace mrf::math;


Shape::Shape(float pos_x, float pos_y, float pos_z, unsigned int index_material)
  : Object(index_material, Vec3f(pos_x, pos_y, pos_z), Vec3f(pos_x, pos_y, pos_z))
  , _cs(mrf::math::Vec3f(pos_x, pos_y, pos_z))
  , _scaleTransform(mrf::math::Mat4f::identity())
  , _objectToWorldTransform(_cs.getMatrixFrom() * _scaleTransform)
  , _worldToObjectTransform(_scaleTransform.inverse() * _cs.getMatrixTo())
  , _mesh(nullptr)
{}


Shape::Shape(float pos_x, float pos_y, float pos_z, MRF_AABBox const &box, unsigned int index_material)
  : Object(index_material, box)
  , _cs(mrf::math::Vec3f(pos_x, pos_y, pos_z))
  , _scaleTransform(mrf::math::Mat4f::identity())
  , _objectToWorldTransform(_cs.getMatrixFrom() * _scaleTransform)
  , _worldToObjectTransform(_scaleTransform.inverse() * _cs.getMatrixTo())
  , _mesh(nullptr)
{}

Shape::Shape(mrf::math::Reff &ref, mrf::math::Vec3f &scale, unsigned int index_material)
  : Object(index_material, ref.getPosition(), ref.getPosition())
  , _cs(ref)
  , _scaleTransform(mrf::math::Mat4f::scale(scale(0), scale(1), scale(2)))
  , _objectToWorldTransform(_cs.getMatrixFrom() * _scaleTransform)
  , _worldToObjectTransform(_scaleTransform.inverse() * _cs.getMatrixTo())
  , _mesh(nullptr)
{}

Shape::Shape(Shape const &ashape)
  : Object(ashape.materialIndex(), ashape.bbox())
  , _cs(ashape._cs)
  , _scaleTransform(ashape._scaleTransform)
  , _objectToWorldTransform(_cs.getMatrixFrom() * _scaleTransform)
  , _worldToObjectTransform(_scaleTransform.inverse() * _cs.getMatrixTo())
  , _mesh(nullptr)
{}


Shape::~Shape() {}


/**
 * Transforms the Ray defined in World Space to the Object Space
 **/
void Shape::transformRayToObjectSpace(mrf::lighting::Ray const &r, mrf::lighting::Ray &transformed_ray) const
{
  //Transform the origin of the ray
  transformed_ray.setOrigin(_worldToObjectTransform * r.origin());

  //Transform the Direction
  mrf::math::Vec3f d             = r.dir();
  mrf::math::Vec3f new_direction = _worldToObjectTransform.transformVector(d);

  transformed_ray.setDir(new_direction);
}


void Shape::intersectionToWorldSpace(mrf::lighting::Ray const &world_ray, Intersection &hit_info) const
{
  //Point BACK TO WORLD COORDINATES
  hit_info.point = _objectToWorldTransform * hit_info.point;

  //Normal BACK TO WORLD COORDINATES
  Vec3f new_normal = _worldToObjectTransform.transformNormal(hit_info.normal);
  new_normal.normalize();

  hit_info.normal = new_normal;
  hit_info.t      = (hit_info.point - world_ray.origin()).norm();
}



//-------------------------------------------------------------------------
// Partial Implementation of ILight interface implementation
//-------------------------------------------------------------------------
//        mrf::geom::AABBox&
//        Shape::bbox()
//        {
//           return _bbox;
//        }

mrf::geom::MRF_AABBox const &Shape::bbox() const
{
  return _bbox;
}

mrf::geom::MRF_AABBox &Shape::bbox()
{
  return _bbox;
}

//-------------------------------------------------------------------------
// Partial Implementation of Intersectable interface
//-------------------------------------------------------------------------
unsigned int Shape::memorySize() const
{
  return Object::memorySize();
}


}   // namespace geom
}   // namespace mrf
