#include <mrf_core/geometry/point.hpp>
#include <mrf_core/geometry/aabbox.hpp>

namespace mrf
{
namespace geom
{
using namespace mrf::math;

Point::Point(float x, float y, float z)
  : _position(x, y, z)
  ,
  //_lf( ),
  _aabbox(
      MRF_AABBox(Vec3f(x, y, z) - Vec3f(EPSILON, EPSILON, EPSILON), Vec3f(x, y, z) + Vec3f(EPSILON, EPSILON, EPSILON)))
{}

Point::~Point() {}

mrf::math::Vec3f Point::center() const
{
  return _position;
}


//-------------------------------------------------------------------------
// ILight interface implementation
//-------------------------------------------------------------------------
mrf::geom::MRF_AABBox &Point::bbox()
{
  return _aabbox;
}

float Point::surfaceArea() const
{
  return 1.0f;

  // TODO : ReturnTHIS ?
  //return 4*Math::PI;
}

float Point::surfaceSampling(float /*random_var_1*/, float /*random_var_2*/, mrf::geom::LocalFrame &light_local_frame)
    const
{
#ifdef DEBUG
  std::cout << "  random_var_1 = " << random_var_1 << "  random_var_2 = " << random_var_2 << std::endl;
#endif


  Vec3f a_position(_position.x(), _position.y(), _position.z());

  light_local_frame.position(a_position);

  //Note : we do not set ANY Normal & co in the light local
  //frame because this has no uses for a Point Light source


  // Since the area is 1.0 the probability to choose this point
  // is also always 1.0
  return 1.0f;
}


//       //TODO : FIXE ME !!!
//       LocalFrame const &
//       Point::localFrame() const
//       {
//          //return new LocalFrame();
//       }

float Point::lightGeometricFactor(mrf::math::Vec3f const & /*light_vector*/) const
{
#ifdef POINT_CLASS_DEBUG
  std::cout << "  light_vector = " << light_vector << std::endl;
#endif

  // For a Point Light source this factor is always equal to 1
  // because a point like a sphere as every possible normals
  return 1.0f;
}



mrf::math::Vec3f Point::lightVector(
    Vec3f const &point_shaded,
    Vec3f const &light_position,
    float &      distance_point_light,
    float &      square_distance_point_light) const
{
  Vec3f light_vector = light_position - point_shaded;



  square_distance_point_light = light_vector.sqrLength();

#ifdef POINT_CLASS_DEBUG
  if (square_distance_point_light < DBL_EPSILON)
  {
    std::cerr << __FILE__ << " " << __LINE__ << std::endl;
    std::cerr << " Error distance between point being shaded and light position is to small " << std::endl;
  }
#endif

  distance_point_light = sqrtf(square_distance_point_light);


#ifdef POINT_CLASS_DEBUG
  std::cout << "  distance_point_light = " << distance_point_light << __FILE__ << " " << __LINE__ << std::endl;
#endif


  light_vector /= distance_point_light;

#ifdef POINT_CLASS_DEBUG
  std::cout << "  distance_point_light = " << distance_point_light << __FILE__ << " " << __LINE__ << std::endl;
#endif


  return light_vector;
}



}   // namespace geom
}   // namespace mrf
