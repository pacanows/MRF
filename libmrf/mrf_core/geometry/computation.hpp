/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>

#include <vector>
#include <iostream>

namespace mrf
{
namespace geom
{
class MRF_CORE_EXPORT Computation
{
public:
  static void computeNormals(
      float *       vertices,
      unsigned int  num_of_vertices,
      unsigned int *index_faces,
      unsigned int  num_of_faces,
      float **      normals);

  static void computeNormals(
      std::vector<float> const &       vertices,
      std::vector<unsigned int> const &index_faces,
      std::vector<float> &             normals);



  static mrf::math::Vec3f computeTriangleNormal(float *vertices, unsigned int i1, unsigned int i2, unsigned int i3);

  static mrf::math::Vec3f
  computeTriangleNormal(std::vector<float> const &vertices, unsigned int i1, unsigned int i2, unsigned int i3);

  static mrf::math::Vec3f vertex(float const *vertices, unsigned int index_v);

  static mrf::math::Vec3f vertex(std::vector<float> const &vertices, unsigned int index_v);

  static void vertex(float const *vertices, unsigned int index_v, mrf::math::Vec3f &v);


  static void vertex(std::vector<float> const &vertices, unsigned int index_v, mrf::math::Vec3f &v);


  static void vertexByFace(
      float const *       vertices,
      unsigned int        index_face,
      unsigned int const *faces,
      mrf::math::Vec3f &  v1,
      mrf::math::Vec3f &  v2,
      mrf::math::Vec3f &  v3);


  static void indicesByFace(
      unsigned int        index_face,
      unsigned int const *faces,
      unsigned int &      i1,
      unsigned int &      i2,
      unsigned int &      i3);

  static void indicesByFace(
      unsigned int                     index_face,
      std::vector<unsigned int> const &faces,
      unsigned int &                   i1,
      unsigned int &                   i2,
      unsigned int &                   i3);


  static void saveOneVertex(mrf::math::Vec3f const &v, std::vector<float> &vertices, unsigned int &index_saved);

  static void saveOneVertex(mrf::math::Vec3f const &v, std::vector<float> &vertices);



  static void computeBoxGreatDiagonalV(
      mrf::math::Vec3f const &min,
      mrf::math::Vec3f const &max,
      mrf::math::Vec3f &      d1,
      mrf::math::Vec3f &      d2,
      mrf::math::Vec3f &      d3,
      mrf::math::Vec3f &      d4);


  static void
  computeBoxGreatDiagonalV(mrf::math::Vec3f const &min, mrf::math::Vec3f const &max, std::vector<mrf::math::Vec3f> &d);


  static void computeBoxGreatDiagonals(
      mrf::math::Vec3f const &min,
      mrf::math::Vec3f const &max,
      float &                 d1,
      float &                 d2,
      float &                 d3,
      float &                 d4);


  static inline float pointToPlaneDistance(
      mrf::math::Vec3f const &point,
      mrf::math::Vec3f const &plane_point,
      mrf::math::Vec3f const &plane_normal);

  static inline float pointToPlaneDistanceN(
      mrf::math::Vec3f const &point,
      mrf::math::Vec3f const &plane_point,
      mrf::math::Vec3f const &plane_normal);




  //This is to compute the Geometry term of the Rendering Equation
  static inline float geometryTerm(
      mrf::math::Vec3f const &point,
      mrf::math::Vec3f const &npoint,   //normal at point
      mrf::math::Vec3f const &point_light,
      mrf::math::Vec3f const &npoint_light,
      mrf::math::Vec3f &      psi   // this is the normalized
                                    // light vector
                                    // which direction is
                                    // point_light - point
  );


  /** Compute the perfector mirror direction
   * Incoming direction is points away from the surface
   * defined by point_normal
   *
   * use this function only in a hemisphere
   **/
  static inline mrf::math::Vec3f mirrorDir(mrf::math::Vec3f const &point_normal, mrf::math::Vec3f const &incomming_dir)
  {
    float dotN_PSI = mrf::math::Vec3f::dot(incomming_dir, point_normal);

    //             if ( dotN_PSI < EPSILON)
    //             {
    //                return - incomming_dir;
    //             }
    //             else
    //             {
    return point_normal * 2 * dotN_PSI - incomming_dir;
    //             }
  }

  static inline void orthoBasis(mrf::math::Vec3f const &N, mrf::math::Vec3f &X, mrf::math::Vec3f &Y);


  static inline mrf::math::Vec3f cartesianCoordinatesByLocalFrame(
      mrf::math::Vec3f const &X,
      mrf::math::Vec3f const &Y,
      mrf::math::Vec3f const &Z,
      float                   theta,
      float                   phi);


  static inline mrf::math::Vec3f toLocalFrame(
      mrf::math::Vec3f const &normal,   // point normal
      mrf::math::Vec3f const &global_direction);




};   //end of class Definition



inline float Computation::geometryTerm(
    mrf::math::Vec3f const &point,
    mrf::math::Vec3f const &npoint,
    mrf::math::Vec3f const &point_light,
    mrf::math::Vec3f const &npoint_light,
    mrf::math::Vec3f &      psi)
{
  psi = (point_light - point);

  float sq2 = psi.norm2();

#ifdef DEBUG
  std::cout << "  psi = " << psi << std::endl;
  std::cout << "  sq2 = " << sq2 << std::endl;
#endif



  if (sq2 < EPSILON)
  {
    return 0.0f;
  }

  //squareDistance *= squareDistance;
  psi /= mrf::math::sqr(sq2);

  float NP_DOT_PSI = mrf::math::Math::max(mrf::math::Vec3f::dot(npoint, psi), 0.0f);
  if (NP_DOT_PSI < EPSILON)
  {
#ifdef DEBUG
    std::cout << " Null DOT ON POINT " << std::endl;
#endif
    return 0.0f;
  }

  float NL_DOT_INV_PSI = mrf::math::Math::max(mrf::math::Vec3f::dot(npoint_light, -psi), 0.0f);
  if (NL_DOT_INV_PSI < EPSILON)
  {
#ifdef DEBUG
    std::cout << " NULL DOT ON Light POINT " << std::endl;
#endif
    return 0.0f;
  }

  return ((NP_DOT_PSI * NL_DOT_INV_PSI) / sq2);
  //return  ( (NP_DOT_PSI * NL_DOT_INV_PSI) );
}



inline float Computation::pointToPlaneDistance(
    mrf::math::Vec3f const &point,
    mrf::math::Vec3f const &plane_point,
    mrf::math::Vec3f const &plane_normal)
{
  float normN = plane_normal.norm();
  assert(normN > EPSILON);

  float d              = -mrf::math::Vec3f::dot(plane_point, plane_normal);
  float dotPointNormal = mrf::math::Vec3f::dot(point, plane_normal);

  return mrf::math::Math::absVal(dotPointNormal + d) / normN;
}



/**
 * This method assumes that the normal is normalized
 */
inline float Computation::pointToPlaneDistanceN(
    mrf::math::Vec3f const &point,
    mrf::math::Vec3f const &plane_point,
    mrf::math::Vec3f const &plane_normal)
{
  float d              = -mrf::math::Vec3f::dot(plane_point, plane_normal);
  float dotPointNormal = mrf::math::Vec3f::dot(point, plane_normal);

  return mrf::math::Math::absVal(dotPointNormal + d);
}


inline void Computation::orthoBasis(mrf::math::Vec3f const &N, mrf::math::Vec3f &X, mrf::math::Vec3f &Y)
{
  double min       = INFINITY;
  int    min_index = -1;

  // find the minor axis of the ray
  for (int i = 0; i < 3; i++)
  {
    if (mrf::math::Math::absVal(N[i]) < min)
    {
      min       = mrf::math::Math::absVal(N[i]);
      min_index = i;
    }
  }

  if (min_index == 0)
  {
    X = mrf::math::Vec3f(0, -N[2], N[1]);
    X.normalize();

    Y = mrf::math::Vec3f(X.cross(N));
    Y.normalize();
  }
  else if (min_index == 1)
  {
    X = mrf::math::Vec3f(-N[2], 0, N[0]);
    X.normalize();

    Y = mrf::math::Vec3f(X.cross(N));
    Y.normalize();
  }
  else if (min_index == 2)
  {
    X = mrf::math::Vec3f(-N[1], N[0], 0);
    X.normalize();

    Y = mrf::math::Vec3f(X.cross(N));
    Y.normalize();
  }
}



inline mrf::math::Vec3f Computation::cartesianCoordinatesByLocalFrame(
    mrf::math::Vec3f const &X,
    mrf::math::Vec3f const &Y,
    mrf::math::Vec3f const &Z,
    float                   theta,
    float                   phi)
{
  return X * cos(phi) * sin(theta) + Y * sin(phi) * sin(theta) + Z * cos(theta);
}




inline mrf::math::Vec3f Computation::toLocalFrame(mrf::math::Vec3f const &N, mrf::math::Vec3f const &global_direction)
{
  mrf::math::Vec3f X(N.generateOrthogonal());
  X.normalize();

  mrf::math::Vec3f Y(N.cross(X));
  Y.normalize();

  return (global_direction[0] * X + global_direction[1] * Y + global_direction[2] * N);
}




}   // namespace geom

}   // namespace mrf
