#include <mrf_core/geometry/sphere.hpp>
#include <mrf_core/math/math.hpp>

using namespace mrf::math;

namespace mrf
{
namespace geom
{
using namespace mrf::lighting;

Sphere::Sphere(unsigned int material_index, float pos_x, float pos_y, float pos_z, float radius)
  : Shape(
      pos_x,
      pos_y,
      pos_z,
      MRF_AABBox(
          Vec3f(pos_x - radius - EPSILON, pos_y - radius - EPSILON, pos_z - radius - EPSILON),
          Vec3f(pos_x + radius + EPSILON, pos_y + radius + EPSILON, pos_z + radius + EPSILON)),
      material_index)
  , _radius(radius)
{
  //_qobj = gluNewQuadric( );
}

Sphere::Sphere(unsigned int material_index, mrf::math::Vec3f const &position, float radius)
  : Shape(
      position.x(),
      position.y(),
      position.z(),
      MRF_AABBox(
          Vec3f(position.x() - radius - EPSILON, position.y() - radius - EPSILON, position.z() - radius - EPSILON),
          Vec3f(position.x() + radius + EPSILON, position.y() + radius + EPSILON, position.z() + radius + EPSILON)),
      material_index)
  , _radius(radius)
{}



Sphere::~Sphere() {}

//-------------------------------------------------------------------------
// Meshable Implementation
//-------------------------------------------------------------------------

mrf::geom::Mesh Sphere::createMesh() const
{
  /*** Creation icosaedre de base ***/
  std::vector<mrf::math::Vec3f> vertices;
  std::vector<mrf::math::Vec3f> normals;

  const float g_ratio = (1 + sqrt(5.f)) / 2;

  normals.push_back(mrf::math::Vec3f(g_ratio, 1, 0).normalize());
  vertices.push_back(normals.back() * _radius);

  normals.push_back(mrf::math::Vec3f(g_ratio, -1, 0).normalize());
  vertices.push_back(normals.back() * _radius);

  normals.push_back(mrf::math::Vec3f(-g_ratio, 1, 0).normalize());
  vertices.push_back(normals.back() * _radius);

  normals.push_back(mrf::math::Vec3f(-g_ratio, -1, 0).normalize());
  vertices.push_back(normals.back() * _radius);


  normals.push_back(mrf::math::Vec3f(0, g_ratio, 1).normalize());
  vertices.push_back(normals.back() * _radius);

  normals.push_back(mrf::math::Vec3f(0, g_ratio, -1).normalize());
  vertices.push_back(normals.back() * _radius);

  normals.push_back(mrf::math::Vec3f(0, -g_ratio, 1).normalize());
  vertices.push_back(normals.back() * _radius);

  normals.push_back(mrf::math::Vec3f(0, -g_ratio, -1).normalize());
  vertices.push_back(normals.back() * _radius);


  normals.push_back(mrf::math::Vec3f(1, 0, g_ratio).normalize());
  vertices.push_back(normals.back() * _radius);

  normals.push_back(mrf::math::Vec3f(-1, 0, g_ratio).normalize());
  vertices.push_back(normals.back() * _radius);

  normals.push_back(mrf::math::Vec3f(1, 0, -g_ratio).normalize());
  vertices.push_back(normals.back() * _radius);

  normals.push_back(mrf::math::Vec3f(-1, 0, -g_ratio).normalize());
  vertices.push_back(normals.back() * _radius);

  std::vector<uint> face_indexes
      = {1, 0, 8, 8, 0, 4, 4, 0, 5, 5, 0, 10, 10, 0, 1, 10, 1, 7, 7, 1, 6, 6, 1, 8,  5, 10, 11, 11, 10, 7,
         6, 8, 9, 9, 8, 4, 9, 2, 3, 3, 2, 11, 11, 2, 5, 5,  2, 4, 4, 2, 9, 7, 3, 11, 6, 3,  7,  9,  3,  6};
  /*
  face_indexes.push_back(Triplet_uint(1,0,8));
  face_indexes.push_back(Triplet_uint(8,0,4));
  face_indexes.push_back(Triplet_uint(4,0,5));
  face_indexes.push_back(Triplet_uint(5,0,10));
  face_indexes.push_back(Triplet_uint(10,0,1));
  face_indexes.push_back(Triplet_uint(10,1,7));
  face_indexes.push_back(Triplet_uint(7,1,6));
  face_indexes.push_back(Triplet_uint(6,1,8));
  face_indexes.push_back(Triplet_uint(5,10,11));
  face_indexes.push_back(Triplet_uint(11,10,7));
  face_indexes.push_back(Triplet_uint(6,8,9));
  face_indexes.push_back(Triplet_uint(9,8,4));
  face_indexes.push_back(Triplet_uint(9,2,3));
  face_indexes.push_back(Triplet_uint(3,2,11));
  face_indexes.push_back(Triplet_uint(11,2,5));
  face_indexes.push_back(Triplet_uint(5,2,4));
  face_indexes.push_back(Triplet_uint(4,2,9));
  face_indexes.push_back(Triplet_uint(7,3,11));
  face_indexes.push_back(Triplet_uint(6,3,7));
  face_indexes.push_back(Triplet_uint(9,3,6));
  */


  /*** Creation of sphere by iteration ***/

  std::vector<uint> previousFaces;

  unsigned int const numIter = 3;
  for (unsigned int i = 0; i < numIter; i++)
  {
    previousFaces = face_indexes;
    face_indexes.clear();

    for (unsigned int j = 0; j < previousFaces.size() / 3; j++)
    {
      unsigned int a = previousFaces[3 * j];       // std::get<0>(previousFaces[j]);
      unsigned int b = previousFaces[3 * j + 1];   // std::get<1>(previousFaces[j]);
      unsigned int c = previousFaces[3 * j + 2];   //std::get<2>(previousFaces[j]);

      /*** Creation of new points ***/
      mrf::math::Vec3f new_vertice;
      mrf::math::Vec3f new_normal;

      new_vertice = (vertices[a] + vertices[b]) / 2.0f;
      new_normal  = new_vertice.normal();
      new_vertice = new_normal * radius();
      vertices.push_back(new_vertice);
      normals.push_back(new_normal);
      unsigned int d = static_cast<unsigned int>(vertices.size() - 1);

      new_vertice = (vertices[a] + vertices[c]) / 2.0f;
      new_normal  = new_vertice.normal();
      new_vertice = new_normal * radius();
      vertices.push_back(new_vertice);
      normals.push_back(new_normal);
      unsigned int e = static_cast<unsigned int>(vertices.size() - 1);

      new_vertice = (vertices[b] + vertices[c]) / 2.0f;
      new_normal  = new_vertice.normal();
      new_vertice = new_normal * radius();
      vertices.push_back(new_vertice);
      normals.push_back(new_normal);
      unsigned int f = static_cast<unsigned int>(vertices.size() - 1);

      /*face_indexes.push_back(Triplet_uint(a,d,e));
      face_indexes.push_back(Triplet_uint(d,b,f));
      face_indexes.push_back(Triplet_uint(e,d,f));
      face_indexes.push_back(Triplet_uint(e,f,c));*/
      face_indexes.push_back(a);
      face_indexes.push_back(d);
      face_indexes.push_back(e);
      face_indexes.push_back(d);
      face_indexes.push_back(b);
      face_indexes.push_back(f);
      face_indexes.push_back(e);
      face_indexes.push_back(d);
      face_indexes.push_back(f);
      face_indexes.push_back(e);
      face_indexes.push_back(f);
      face_indexes.push_back(c);
    }

    previousFaces.clear();
  }

  for (unsigned int i = 0; i < vertices.size(); ++i)
  {
    vertices[i] += position();
  }

  std::vector<mrf::math::Vec3f> uvw(vertices.size(), mrf::math::Vec3f(0, 0, 0));
  std::vector<unsigned int>     material_per_faces;

  return mrf::geom::Mesh(vertices, normals, uvw, face_indexes, material_per_faces);
}
//-------------------------------------------------------------------------
// Intersectable Interface
//-------------------------------------------------------------------------
mrf::geom::MRF_AABBox const &Sphere::bbox() const
{
  return Shape::bbox();
}

mrf::geom::MRF_AABBox &Sphere::bbox()
{
  return Shape::bbox();
}


bool Sphere::minMaxParametricDistanceFromPoint(mrf::math::Vec3f const &point, float &tmin, float &tmax) const
{
  //Center of Sphere
  Vec3f const &position = _cs.getPosition();

  Vec3f const ray_dir = (position - point).normalize();
  Ray         r(point, ray_dir);

  // Ray Direction
  double const Xd = r.dir()[0];
  double const Yd = r.dir()[1];
  double const Zd = r.dir()[2];

  // Ray Origin
  double const X0 = r.origin()[0];
  double const Y0 = r.origin()[1];
  double const Z0 = r.origin()[2];

  //Center of the Sphere
  double const Xc = position[0];
  double const Yc = position[1];
  double const Zc = position[2];

  double const sqr_sphere_radius = _radius * _radius;

  //-------------------------------------------------------------------------
  // Compute the B and C coefficients

  double const B = 2 * (Xd * (X0 - Xc) + Yd * (Y0 - Yc) + Zd * (Z0 - Zc));

  Vec3d const  diff(X0 - Xc, Y0 - Yc, Z0 - Zc);
  double const C = diff.sqrLength() - sqr_sphere_radius;


  double t0 = -std::numeric_limits<double>::max();
  double t1 = -t0;

  if (!solveQuadratic(B, C, t0, t1))
  {
    std::cerr << " How could this case happened?" << std::endl;
    assert(0);
  }

  tmin = Math::min(static_cast<float>(t0), static_cast<float>(t1));
  tmax = Math::max(static_cast<float>(t0), static_cast<float>(t1));


  return true;
}




bool Sphere::isIntersected(mrf::lighting::Ray const &r) const
{
  //CHECK The discriminant sign

  // Compute the solution of the quadratic form : At^2 + Bt + C
  // Notice that we solve t^2 + Bt + C
  // since the direction of the ray is normalized.
  //
  // IF the discriminant is >= 0 verify that r.tmin and r.tmax are
  // coherent with possible intersections at t0 and t1


  //with: A = Xd^2 + Yd^2 + Zd^2
  //B = 2 * (Xd * (X0 - Xc) + Yd * (Y0 - Yc) + Zd * (Z0 - Zc))
  // C = (X0 - Xc)^2 + (Y0 - Yc)^2 + (Z0 - Zc)^2 - Sr^2

  //-------------------------------------------------------------------------
  // Ray Direction
  double const Xd = r.dir()[0];
  double const Yd = r.dir()[1];
  double const Zd = r.dir()[2];

  // Ray Origin
  double const X0 = r.origin()[0];
  double const Y0 = r.origin()[1];
  double const Z0 = r.origin()[2];

  //Center of Sphere
  Vec3f const &position = _cs.getPosition();
  double const Xc       = position[0];
  double const Yc       = position[1];
  double const Zc       = position[2];

  double const sqr_sphere_radius = _radius * _radius;

  //-------------------------------------------------------------------------
  // Compute the B and C coefficients

  double const B = 2 * (Xd * (X0 - Xc) + Yd * (Y0 - Yc) + Zd * (Z0 - Zc));

  Vec3d const  diff(X0 - Xc, Y0 - Yc, Z0 - Zc);
  double const C = diff.sqrLength() - sqr_sphere_radius;


  double t0, t1;
  if (!solveQuadratic(B, C, t0, t1))
  {
    return false;
  }

  //We did intersect somehting
  //We now check if the intersection occured between the specific bounds of the ray

  if ((t0 >= r.tMin()) && (t0 <= r.tMax()))
  {
    return true;
  }
  else if ((t1 >= r.tMin()) && (t1 <= r.tMax()))
  {
    return true;
  }


  return false;
}

bool Sphere::isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info) const
{
  //See previous method for explanation
  //-------------------------------------------------------------------------
  // Ray Direction
  double const Xd = r.dir()[0];
  double const Yd = r.dir()[1];
  double const Zd = r.dir()[2];

  // Ray Origin
  double const X0 = r.origin()[0];
  double const Y0 = r.origin()[1];
  double const Z0 = r.origin()[2];

  //Center of Sphere
  Vec3f const &position = _cs.getPosition();
  double const Xc       = position[0];
  double const Yc       = position[1];
  double const Zc       = position[2];

  double const sqr_sphere_radius = _radius * _radius;

  //-------------------------------------------------------------------------
  // Compute the B and C coefficients

  double const B = 2 * (Xd * (X0 - Xc) + Yd * (Y0 - Yc) + Zd * (Z0 - Zc));

  Vec3f const  diff(static_cast<float>(X0 - Xc), static_cast<float>(Y0 - Yc), static_cast<float>(Z0 - Zc));
  double const C = diff.sqrLength() - sqr_sphere_radius;


  double t0, t1;
  if (!solveQuadratic(B, C, t0, t1))
  {
    return false;
  }

  //We did intersect somehting
  //We now check if the intersection occured between the specific bounds of the ray

  if ((t0 >= r.tMin()) && (t0 <= r.tMax()))
  {
    //-------------------------------------------------------------------------
    hit_info.t     = static_cast<float>(t0);
    hit_info.point = r.origin() + r.dir() * static_cast<float>(t0);

    Vec3f n = hit_info.point - Vec3f(static_cast<float>(Xc), static_cast<float>(Yc), static_cast<float>(Zc));
    //double const inv_radius = 1.0 / _radius;
    //hit_info.normal = n * inv_radius;
    hit_info.normal = n.normalize();

    //-------------------------------------------------------------------------
    //#ifdef DEBUG
    //std::cout << "  _material_index = " << _material_index << "at " << __LINE__ << std::endl ;
    //#endif

    hit_info.material = _material_index;
    hit_info.object   = _id;
    //-------------------------------------------------------------------------
    //TODO : CHECK ME !!!
    float theta, phi;
    mrf::math::sphCoordNR(hit_info.normal[0], hit_info.normal[1], hit_info.normal[2], theta, phi);
    hit_info.uv = Vec2f(theta * static_cast<float>(Math::INV_PI), phi * static_cast<float>(Math::INV_2PI));


    // Tangent along  longitude lines
    //hit_info.tangent = Vec3f( Math::TWO_PI * hit_info.point.z(), 0.0f, -Math::TWO_PI * hit_info.point.x() );
    hit_info.tangent = Vec3f(hit_info.point.z(), 0.0f, -hit_info.point.x());
    hit_info.tangent.normalize();

    // Tangent along  latitude lines
    //                hit_info.tangent = Vec3f( Math::PI * std::sin(phi) * hit_info.point.y(),
    //                                          - _radius * Math::PI * std::sin(theta),
    //                                          Math::PI * std::cos(phi) * hit_info.point.y());
    //                hit_info.tangent.normalize();
    //-------------------------------------------------------------------------


    return true;
  }
  else if ((t1 >= r.tMin()) && (t1 <= r.tMax()))
  {
    //-------------------------------------------------------------------------
    hit_info.t     = static_cast<float>(t1);
    hit_info.point = r.origin() + r.dir() * static_cast<float>(t1);


    Vec3f n         = hit_info.point - Vec3f(static_cast<float>(Xc), static_cast<float>(Yc), static_cast<float>(Zc));
    hit_info.normal = n.normalize();
    //double const inv_radius = 1.0 / _radius;
    //hit_info.normal = n * inv_radius;

    //-------------------------------------------------------------------------
    //#ifdef DEBUG
    //std::cout << "  _material_index = " << _material_index << "at " << __LINE__ << std::endl ;
    //#endif

    hit_info.material = _material_index;
    hit_info.object   = _id;

    //-------------------------------------------------------------------------
    //TODO : CHECK ME !!!
    float theta, phi;
    mrf::math::sphCoordNR(hit_info.normal[0], hit_info.normal[1], hit_info.normal[2], theta, phi);
    hit_info.uv = Vec2f(theta * static_cast<float>(Math::INV_PI), phi * static_cast<float>(Math::INV_2PI));

    //hit_info.tangent = Vec3f( Math::TWO_PI * hit_info.point.z(), 0.0f, -Math::TWO_PI * hit_info.point.x() );
    hit_info.tangent = Vec3f(hit_info.point.z(), 0.0f, -hit_info.point.x());
    hit_info.tangent.normalize();

    //                hit_info.tangent = Vec3f( Math::PI * std::sin(phi) * hit_info.point.y(),
    //                                          - _radius * Math::PI * std::sin(theta),
    //                                          Math::PI * std::cos(phi) * hit_info.point.y());

    //                hit_info.tangent.normalize();

    //-------------------------------------------------------------------------

    return true;
  }

  return false;
}


bool Sphere::isIntersected(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned) const
{
  //NO SUBOBJECT FOR A SPHERE
  return isIntersected(r, hit_info);
}


bool Sphere::isIntersected(mrf::lighting::Ray const &r, unsigned int) const
{
  //NO SUBOBJECT FOR A SPHERE
  return isIntersected(r);
}



bool Sphere::isIntersectedWNBFC(mrf::lighting::Ray const & /*r*/, Intersection & /*hit_info*/) const
{
  std::cout << __FILE__ << " at " << __LINE__ << std::endl;
  std::cout << " CODE ME " << std::endl;
  assert(0);
  return false;
}



bool Sphere::isIntersectedWNBFC(mrf::lighting::Ray const &r, Intersection &hit_info, unsigned int) const
{
  return isIntersectedWNBFC(r, hit_info);
}


unsigned int Sphere::numSubObjects() const
{
  return 0;
}

bool Sphere::hasSubObjects() const
{
  return false;
}

MRF_AABBox Sphere::subObjectAABBox(unsigned int) const
{
  return _bbox;
}

bool Sphere::isIntersected(MRF_AABBox const &box, unsigned /*index_face*/) const
{
  return Sphere::isIntersected(box.min(), box.max());
}

bool Sphere::isIntersected(Vec3f const &min, Vec3f const &max, unsigned /*index_face*/) const
{
  return Sphere::isIntersected(min, max);
}


bool Sphere::isIntersected(Vec3f const &min, Vec3f const &max) const
{
  //
  // Graphics Gems 1 : 1990
  // Arvo's Algorithm
  // Case Solid Box and Hollow Sphere
  // This means that we consider our Sphere as a Surface and not a volume

  float dmin = 0.0f;
  float dmax = 0.0f;

  float a, b;


  Vec3f const &position = _cs.getPosition();

  for (unsigned int i = 0; i < 3; i++)
  {
    a = (position[i] - min[i]);
    a *= a;

    b = (position[i] - max[i]);
    b *= b;

    dmax += Math::max(a, b);

    if ((position[i] < min[i]) && (position[i] > max[i]))
    {
      dmin += Math::min(a, b);
    }
  }

  float const sqr_radius = _radius * _radius;


  if ((sqr_radius >= dmin) && (sqr_radius <= dmax))
  {
    return true;
  }

  return false;
}



bool Sphere::isIntersected(
    mrf::math::Vec3f const &min,
    mrf::math::Vec3f const &max,
    std::vector<unsigned int> & /*sub_objects*/) const
{
  return isIntersected(min, max);
}


bool Sphere::isIntersected(MRF_AABBox const &box, std::vector<unsigned int> & /*objects*/) const
{
  return isIntersected(box.min(), box.max());
}


unsigned int Sphere::intersectionCost() const
{
  return 1;
}

unsigned int Sphere::memorySize() const
{
  return Shape::memorySize() + sizeof(float);
}

//-------------------------------------------------------------------------
// ILight Implementation
//-------------------------------------------------------------------------
float Sphere::surfaceArea() const
{
  return 4 * static_cast<float>(Math::PI) * _radius * _radius;
}

float Sphere::surfaceSampling(float /*random_var_1*/, float /*random_var_2*/, LocalFrame & /*light_local_frame*/) const
{
  assert(0);
  //TODO : CODE ME !!!
  // ?? ? ??
  return 1.f / surfaceArea();
}


float Sphere::lightGeometricFactor(Vec3f const & /*light_vector*/) const
{
  assert(0);
  //TODO: CODE ME
  return 1.0f;
}

Vec3f Sphere::lightVector(
    Vec3f const &point_shaded,
    Vec3f const &light_position,
    float &      distance_point_light,
    float &      square_distance_point_light) const
{
  Vec3f light_vector = light_position - point_shaded;

  square_distance_point_light = light_vector.sqrLength();


#ifdef MRF_GEOM_SPHERE_CLASS_DEBUG
  if (square_distance_point_light < DBL_EPSILON)
  {
    std::cerr << __FILE__ << " " << __LINE__ << std::endl;
    std::cerr << " Error distance between point being shaded and light position is to small " << std::endl;
  }
#endif

  distance_point_light = sqrtf(square_distance_point_light);
  light_vector /= distance_point_light;

  return light_vector;
}


//Protected functions


}   // namespace geom
}   // namespace mrf
