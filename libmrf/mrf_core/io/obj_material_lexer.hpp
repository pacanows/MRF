/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace io
{
class MRF_CORE_EXPORT ObjMaterialLexer
{
public:
  static constexpr char const *const OBJ_MATERIAL_DEF = "newmtl";

  static constexpr char const *const OBJ_MATERIAL_KA         = "Ka";   // emission
  static constexpr char const *const OBJ_MATERIAL_KD         = "Kd";   // diffuse reflection
  static constexpr char const *const OBJ_MATERIAL_KS         = "Ks";   // specular reflection
  static constexpr char const *const OBJ_MATERIAL_TF         = "Tf";   // transmittance
  static constexpr char const *const OBJ_MATERIAL_DISSOLVE   = "d";    // Alpha value known as dissolve parameter
  static constexpr char const *const OBJ_MATERIAL_DISSOLVE_2 = "Tr";   // Alternative definition
  static constexpr char const *const OBJ_HALO                = "-halo";


  static constexpr char const *const OBJ_MATERIAL_NS  = "Ns";   //Shininess
  static constexpr char const *const OBJ_MATERIAL_IOR = "Ni";   //Index of refraction

  static constexpr char const *const OBJ_MATERIAL_SPEC = "spectral";   //Spectral Definition
  static constexpr char const *const OBJ_MATERIAL_XYZ  = "xyz";        //XYZ Color Definition


  // Texture map

  //Specifies that a color texture file or a color procedural texture file is applied to the ambient reflectivity of the material
  static constexpr char const *const OBJ_MATERIAL_MAP_KA = "map_Ka";

  //Specifies that a color texture file or color procedural texture file is linked to the diffuse reflectivity of the material
  static constexpr char const *const OBJ_MATERIAL_MAP_KD = "map_Kd";

  //Specifies that a color texture file or color procedural texture file is linked to the specular reflectivity of the material
  static constexpr char const *const OBJ_MATERIAL_MAP_KS = "map_Ks";

  //Specifies that a scalar texture file or scalar procedural texture file is linked to the specular exponent of the material.
  static constexpr char const *const OBJ_MATERIAL_MAP_NS = "map_Ns";

  static constexpr char const *const OBJ_MATERIAL_MAP_D
      = "map_d";   // The alpha map that spatially modulates the dissolve coefficient

  static constexpr char const *const OBJ_MATERIAL_REFLECTION_MAP   = "refl";       // official token
  static constexpr char const *const OBJ_MATERIAL_REFLECTION_MAP_2 = "map_refl";   // alternative token


  static constexpr char const *const OBJ_BUMPMAP   = "bump";   // The token for the name of the bump map texture file
  static constexpr char const *const OBJ_BUMPMAP_2 = "map_bump";


  //Type of illumination Model
  // a value between 0 and 10
  static constexpr char const *const OBJ_ILL_MODEL = "illum";
};
}   // namespace io
}   // namespace mrf
