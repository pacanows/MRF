/**
 * Copyright(C) CNRS. 2015, 2016.
 *
 * Author: Romain Pacnowski: romain.pacanowski@institutoptique.fr
 *
 *
 */


#include <mrf_core/io/parser_helper.hpp>

namespace mrf
{
namespace io
{
ParserHelper::ParserHelper() {}

ParserHelper::ParserHelper(std::string &path): _scene_path(path) {}
ParserHelper::ParserHelper(std::string &path, std::vector<std::string> &ext_path)
  : _scene_path(path)
  , _ext_paths(ext_path)
{}

void ParserHelper::setExtPath(std::vector<std::string> &ext_path)
{
  _ext_paths = ext_path;
}

float ParserHelper::scalar_from_value(tinyxml2::XMLElement *markup_element)
{
  return scalar_from_value<float>(markup_element);
}

//Helper functions
/**
 * @brief      This method parse a <rgb_color> markup to retrieve the red, green blue
 * attributes from it and sets the given color with the retrieved values
 *
 * @param      rgb_markup  the TiXmlElement representing the <rgb_color>
 * @param      c           The color sets with the attributes
 *
 * @return     true if the parsing went well false otherwise
 */
mrf::color::Color ParserHelper::rgbFromMarkup(tinyxml2::XMLElement *rgb_markup)
{
  float r, g, b;
  int   ret = rgb_markup->QueryFloatAttribute(SceneLexer::RED_AT, &r);
  ret &= rgb_markup->QueryFloatAttribute(SceneLexer::GREEN_AT, &g);
  ret &= rgb_markup->QueryFloatAttribute(SceneLexer::BLUE_AT, &b);

  if (ret == tinyxml2::XMLError::XML_SUCCESS)
    return mrf::color::Color(r, g, b);
  else
    return mrf::color::Color(0.f);
}

mrf::radiometry::Spectrum_T<float, float> ParserHelper::spectrumFromMarkup(tinyxml2::XMLElement *spectrum_markup)
{
  mrf::radiometry::Spectrum_T<float, float> spectrum;
  if (spectrum_markup->Attribute(SceneLexer::FILE_PATH_AT))
  {
    std::string spectrum_relative_path = spectrum_markup->Attribute(SceneLexer::FILE_PATH_AT);

    //std::string spectrum_filename;
    //mrf::util::StringParsing::fileNameFromAbsPath(spectrum_relative_path.c_str(), spectrum_filename);
    //std::string spectrum_folder_path;
    //mrf::util::StringParsing::getDirectory(spectrum_relative_path.c_str(), spectrum_folder_path);
    //spectrum_folder_path = s_parsing_directory + spectrum_folder_path;

    //std::string spectrum_absolute_path = spectrum_folder_path + spectrum_filename;
    std::string spectrum_absolute_path = fs::absolute(fs::path(_scene_path + spectrum_relative_path)).string();

    spectrum.loadSPDFile(spectrum_absolute_path);
    return spectrum;
  }
  else
  {
    std::string spectrum_str = spectrum_markup->Attribute(SceneLexer::VALUE_AT);
    spectrum.loadFromSPDString(spectrum_str);
  }

  return spectrum;
}

bool ParserHelper::retrieveColors(
    tinyxml2::XMLElement *              elem,
    std::vector<mrf::materials::COLOR> &colors,
    bool                                reflective)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  const char *elem_str[2] = {SceneLexer::SPECTRUM_MK, SceneLexer::RGB_COLOR_MK};
#else
  const char *elem_str[2] = {SceneLexer::RGB_COLOR_MK, SceneLexer::SPECTRUM_MK};
#endif

  for (int i = 0; i < 2; i++)
  {
    for (tinyxml2::XMLElement *child = elem->FirstChildElement(elem_str[i]); child; child = child->NextSiblingElement())
    {
      if (std::string(child->Name()) != std::string(elem_str[i])) continue;

      mrf::materials::COLOR current_color;

#ifdef MRF_RENDERING_MODE_SPECTRAL
      if (i == 0)
      {
        // We have a spectral value
        current_color = spectrumFromMarkup(child);
      }
      else
      {
        // We have an RGB value, trying to uplift
        //mrf::gui::fb::Loger::getInstance()->warn("No spectral value found! Basic RGB uplifting");
        mrf::color::Color col = rgbFromMarkup(child);
        current_color         = mrf::materials::COLOR(col.r(), col.g(), col.b());
      }
#else
      if (i == 0)
      {
        // We have an RGB value
        current_color = ParserHelper::rgbFromMarkup(child);
      }
      else
      {
        // We have a spectral value, converting to RGB
        mrf::radiometry::Spectrum_T<float, float>    spec = ParserHelper::spectrumFromMarkup(child);
        mrf::color::_SpectrumConverter<float, float> sc;
        if (reflective)
          current_color = sc.reflectiveSpectrumToXYZ(
              spec,
              mrf::radiometry::D_65_SPD,
              mrf::radiometry::D_65_FIRST_WAVELENGTH,
              mrf::radiometry::D_65_PRECISION,
              mrf::radiometry::D_65_ARRAY_SIZE);
        else
          current_color = sc.emissiveSpectrumToXYZ(spec);
        current_color = current_color.XYZtoLinearRGB();
      }
#endif

      //if (success)
      //{
      colors.push_back(current_color);
      //}
    }

    if (colors.size() != 0)
    {
      return true;
    }
  }

  return false;
}

mrf::materials::COLOR ParserHelper::retrieveColor(tinyxml2::XMLElement *elem, bool reflective)
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  const char *elem_str[2] = {SceneLexer::SPECTRUM_MK, SceneLexer::RGB_COLOR_MK};
#else
  const char *elem_str[2] = {SceneLexer::RGB_COLOR_MK, SceneLexer::SPECTRUM_MK};
#endif

  tinyxml2::XMLElement *color_markup = nullptr;
  int                   color_type   = 0;

  // We try to find the first color markup specified depending on the rendering mode:
  //-spectral: look for spectrum first, then for rgb if no spectrum
  //-rgb: look for rgb first, then for spectrum if no rgb
  for (int i = 0; i < 2; i++)
  {
    color_markup = elem->FirstChildElement(elem_str[i]);

    if (color_markup != nullptr)
    {
      color_type = i;
      break;
    }
  }

  if (color_markup == nullptr)
  {
    throw std::logic_error(" No Color Element in this markup");
  }


#ifdef MRF_RENDERING_MODE_SPECTRAL
  if (color_type == 0)
  {
    // We have a spectral value
    return mrf::materials::COLOR(ParserHelper::spectrumFromMarkup(color_markup));
  }
  else
  {
    // We have an RGB value, trying to uplift
    mrf::color::Color col = rgbFromMarkup(color_markup);
    return mrf::materials::COLOR(col.r(), col.g(), col.b());
  }
#else
  if (color_type == 0)
  {
    // We have an RGB value
    return rgbFromMarkup(color_markup);
  }
  else
  {
    // We have a spectral value, converting to RGB
    mrf::radiometry::Spectrum_T<float, float>    spec = spectrumFromMarkup(color_markup);
    mrf::color::_SpectrumConverter<float, float> sc;
    mrf::materials::COLOR                        c;

    if (reflective)
      c = sc.reflectiveSpectrumToXYZ(
          spec,
          mrf::radiometry::D_65_SPD,
          mrf::radiometry::D_65_FIRST_WAVELENGTH,
          mrf::radiometry::D_65_PRECISION,
          mrf::radiometry::D_65_ARRAY_SIZE);
    else
      c = sc.emissiveSpectrumToXYZ(spec);
    c = c.XYZtoLinearRGB();
    return c;
  }
#endif
}

bool ParserHelper::retrieveFresnel(tinyxml2::XMLElement *an_elem, mrf::materials::COLOR &eta, mrf::materials::COLOR &k)
{
  bool res = true;
  //Fresnel
  tinyxml2::XMLElement *sub_spec_elem = an_elem->FirstChildElement(SceneLexer::FRESNEL_MK);
  if (sub_spec_elem)
  {
    tinyxml2::XMLElement *eta_element = sub_spec_elem->FirstChildElement(SceneLexer::ETA_MK);
    if (eta_element)
    {
      eta = ParserHelper::retrieveColor(eta_element);
    }
    else
    {
      eta = mrf::materials::COLOR(0.f);
      res = false;
    }

    tinyxml2::XMLElement *kappa_element = sub_spec_elem->FirstChildElement(SceneLexer::KAPPA_MK);
    if (kappa_element)
    {
      k = ParserHelper::retrieveColor(kappa_element);
    }
    else
    {
      //For compatibility woth old scenes using k instead of kappa
      tinyxml2::XMLElement *k_element = sub_spec_elem->FirstChildElement(SceneLexer::K_MK);
      if (k_element)
      {
        k = ParserHelper::retrieveColor(k_element);
      }
      else
      {
        k   = mrf::materials::COLOR(0.f);
        res = false;
      }
    }
  }
  else
  {
    res = false;
  }

  return res;
}

bool ParserHelper::retrieveReflectivity(tinyxml2::XMLElement *elem, float &albedo)
{
  float ret(0.f);
  try
  {
    ret = ParserHelper::scalar_from_attribute_name<float>(elem, SceneLexer::ALBEDO_MK, Lexer::VALUE_AT);
  }
  catch (std::error_code e)
  {
    std::cerr << "Error while retrieving reflectivity: " << e << std::endl;
    return false;
  }
  albedo = ret;
  return true;
}

std::string ParserHelper::retrieveEmittanceType(tinyxml2::XMLElement *)
{
  return "";
}

bool ParserHelper::retrievePosition(tinyxml2::XMLElement *elem, mrf::math::Vec3f &pos)
{
  mrf::math::Vec3f ret(0.f);
  try
  {
    ret = vec3_from_markup<mrf::math::Vec3f>(elem, SceneLexer::POSITION_MK);
  }
  catch (std::error_code e)
  {
    std::cerr << "Error while retrieving position: " << e << std::endl;
    return false;
  }
  pos = ret;
  return true;
}

bool ParserHelper::retrieveNormal(tinyxml2::XMLElement *elem, mrf::math::Vec3f &normal)
{
  mrf::math::Vec3f ret(0.f);
  try
  {
    ret = vec3_from_markup<mrf::math::Vec3f>(elem, SceneLexer::NORMAL_MK);
  }
  catch (std::error_code e)
  {
    std::cerr << "Error while retrieving normal: " << e << std::endl;
    return false;
  }
  normal = ret;
  return true;
}

bool ParserHelper::retrieveDirection(tinyxml2::XMLElement *elem, mrf::math::Vec3f &dir)
{
  mrf::math::Vec3f ret(0.f);
  try
  {
    ret = vec3_from_markup<mrf::math::Vec3f>(elem, SceneLexer::DIRECTION_MK);
  }
  catch (std::error_code e)
  {
    std::cerr << "Error while retrieving direction: " << e << std::endl;
    return false;
  }
  dir = ret;
  return true;
}

template<class VEC3_TYPE>
VEC3_TYPE ParserHelper::vec3_from_markup(tinyxml2::XMLElement *parent_markup_element, char const *const markup_name)
{
  tinyxml2::XMLNode const *markup_node = parent_markup_element->FirstChildElement(markup_name);
  if (!markup_node)
  {
    throw std::logic_error(" No Child Element in this markup");
  }

  tinyxml2::XMLElement const *markup_element = markup_node->ToElement();

  if (!markup_element)
  {
    throw std::logic_error(" No Child Element in this markup");
  }

  double x, y, z;

  int xOK = markup_element->QueryDoubleAttribute(Lexer::X_AT, &x);
  int yOK = markup_element->QueryDoubleAttribute(Lexer::Y_AT, &y);
  int zOK = markup_element->QueryDoubleAttribute(Lexer::Z_AT, &z);

  if ((xOK != tinyxml2::XML_SUCCESS) || (yOK != tinyxml2::XML_SUCCESS) || (zOK != tinyxml2::XML_SUCCESS))
  {
    std::string message("Wrong attributes for Markup_element named ");
    message += markup_element->Value();

    throw std::logic_error(message);
  }

  return VEC3_TYPE(mrf::math::Vec3d(x, y, z));
}

template<typename T, template<typename = T> class VEC2_TYPE>
VEC2_TYPE<T>
ParserHelper::vec2_for_width_and_height(tinyxml2::XMLElement *parent_markup_element, char const *const markup_name)
{
  tinyxml2::XMLNode const *markup_node = parent_markup_element->FirstChildElement(markup_name);
  if (!markup_node)
  {
    throw std::logic_error(" No Child Element in this markup");
  }

  tinyxml2::XMLElement const *markup_element = markup_node->ToElement();

  double width;
  double height;

  int wOK = markup_element->QueryDoubleAttribute(Lexer::WIDTH_AT, &width);
  int hOK = markup_element->QueryDoubleAttribute(Lexer::HEIGHT_AT, &height);

  if ((wOK != tinyxml2::XML_SUCCESS) || (hOK != tinyxml2::XML_SUCCESS))
  {
    std::string message("Wrong attributes for Markup_element named ");
    message += markup_element->Value();

    throw std::logic_error(message);
  }

  return VEC2_TYPE<T>(static_cast<T>(width), static_cast<T>(height));
}


template<typename T>
T ParserHelper::scalar_from_attribute_name(
    tinyxml2::XMLElement *parent_markup_element,
    char const *const     markup_name,
    char const *const     attribute_name)
{
  tinyxml2::XMLNode const *markup_node = parent_markup_element->FirstChildElement(markup_name);
  if (!markup_node)
  {
    throw std::logic_error(" No Child Element in this markup");
  }

  tinyxml2::XMLElement const *markup_element = markup_node->ToElement();

  double scalar;
  int    scalar_OK = markup_element->QueryDoubleAttribute(attribute_name, &scalar);

  if (scalar_OK != tinyxml2::XML_SUCCESS)
  {
    std::string message("Wrong attributes for Markup_element named ");
    message += markup_element->Value();
    message += " . with attribute: " + std::string(attribute_name);

    throw std::logic_error(message);
  }

  return static_cast<T>(scalar);
}

template<typename T>
T ParserHelper::scalar_from_value_in_mk(
    tinyxml2::XMLElement *parent_markup_element,
    char const *const     markup_name,
    char const *const     attribute_name)
{
  return ParserHelper::scalar_from_attribute_name<T>(parent_markup_element, markup_name, attribute_name);
}

template<typename T>
T ParserHelper::scalar_from_value(tinyxml2::XMLElement *markup_element)
{
  float r;
  int   valueOK = markup_element->QueryFloatAttribute(SceneLexer::VALUE_AT, &r);

  if (valueOK != tinyxml2::XML_SUCCESS)
  {
    //mrf::gui::fb::Loger::getInstance()->warn(" COULD NOT RETRIEVE VALUE Attribute ");
    //return false;
    throw(" COULD NOT RETRIEVE VALUE Attribute ");
  }

  return r;
}

}   // namespace io
}   // namespace mrf
