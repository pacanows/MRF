#pragma once


/**
 * Copyright(C) CNRS. 2015, 2016.
 *
 * Author: Romain Pacnowski: romain.pacanowski@institutoptique.fr
 *
 *
 */

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/io/lexer.hpp>

namespace mrf
{
namespace io
{
class MRF_CORE_EXPORT CameraLexer: public mrf::io::Lexer
{
public:
  static constexpr char const *const CAMERAS_MK    = "cameras";
  static constexpr char const *const SINGLE_CAM_MK = "camera";


  //Field of View
  static constexpr char const *const CAM_FOVY_MK = "fovy";


  //Geometrical Connfiguration of the Camera
  static constexpr char const *const CAM_LOOKAT_MK   = "lookat";
  static constexpr char const *const CAM_POSITION_MK = "position";
  static constexpr char const *const CAM_UP_MK       = "up";

  static constexpr char const *const CAM_APERTURE_MK = "aperture";

  //SENSOR properties
  static constexpr char const *const SENSOR_MK = "sensor";

  static constexpr char const *const RESOLUTION_MK      = "resolution";
  static constexpr char const *const SENSOR_DISTANCE_MK = "distance";


  //SAMPLING of the SENSOR
  static constexpr char const *const SAMPLES_PER_PIXEL_AT = "spp";




  //LENS
  static constexpr char const *const FOCUS_DISTANCE_MK = "focus_distance";
  static constexpr char const *const LENS_MK           = "lens";

  static constexpr char const *const FSTOP_AT        = "fstop";
  static constexpr char const *const FOCAL_LENGTH_AT = "focal_length";

  //SAMPLING of the Lens
  static constexpr char const *const SAMPLES_PER_LENS_AT = "spl";
};



}   // namespace io
}   // namespace mrf