/*
*
* author : Romain Pacanowski @ institutoptique.fr
* Copyright CNRS 2016
* Copyright CNRS 2017
*
**/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

namespace mrf
{
namespace io
{
class MRF_CORE_EXPORT Extensions
{
 public:
  static constexpr char const * const OBJ_EXT ="obj";
  static constexpr char const * const PLY_EXT = "ply";
  static constexpr char const * const DS_EXT = "3ds"; // 3DS file
  static constexpr char const * const DS2_EXT = "3DS"; // 3DS file
  static constexpr char const * const MRF_SCENE_EXT = "msf";
};
} // end of namespace io
}// end of namespace mrf
