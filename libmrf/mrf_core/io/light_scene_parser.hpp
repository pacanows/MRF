///*
// *
// * author : Romain Pacanowski @ institutoptique.fr
// * Copyright CNRS 2016
// * Copyright CNRS 2017
// *
// **/
//
//#pragma once
//
//#include <mrf_core_dll.hpp>
//#include <mrf_core/mrf_types.hpp>
//#include <mrf_core/feedback/loger.hpp>
//
//#include <mrf_core/rendering/scene.hpp>
//
//#include <mrf_core/materials/materials.hpp>
//#include <mrf_core/materials/all_materials.hpp>
//
//#include <mrf_core/io/parser_helper.hpp>
//#include <mrf_core/io/lexer.hpp>
//#include <mrf_core/io/scene_lexer.hpp>
//#include <mrf_core/io/parsing_errors.hpp>
//#include <mrf_core/io/base_plugin_parser.hpp>
//
//#include <mrf_core/color/color.hpp>
//#include <mrf_core/color/spectrum_converter.hpp>
//
//#include <string>
//#include <vector>
//#include <map>
//
//#include <tinyxml2/tinyxml2.h>
//
//
//namespace mrf
//{
//namespace io
//{
////Forward declaration;
//
///**
// * This is the new Scene parser for new MRF Scene.
// * Note : Singleton class
// **/
//class MRF_CORE_EXPORT LightSceneParser
//{
//public:
//protected:
//  mrf::gui::fb::Loger *_loger;
//
//  tinyxml2::XMLDocument _doc;
//  tinyxml2::XMLHandle   _doc_handle;
//
//  std::string _current_directory;
//
//  //MaterialRegistrator*
//
//  std::vector<std::string>                    _id_map;
//  std::map<std::string, BaseMaterialParser *> _parse_map;
//
//private:
//  void cleanString(std::string &str);
//
//public:
//  LightSceneParser(): _loger(nullptr), _doc_handle(nullptr) {}
//  LightSceneParser(tinyxml2::XMLHandle doc_handle): _loger(nullptr), _doc_handle(doc_handle) {}
//  LightSceneParser(mrf::gui::fb::Loger *loger, tinyxml2::XMLHandle doc_handle)
//    : _loger(loger)
//    , _doc_handle(doc_handle)
//  {}
//
//  LightSceneParser(
//      mrf::gui::fb::Loger *                       loger,
//      tinyxml2::XMLHandle                         doc_handle,
//      std::vector<std::string>                    id_fct_map,
//      std::map<std::string, BaseMaterialParser *> parse_fct_map)
//    : _loger(loger)
//    , _doc_handle(doc_handle)
//    , _id_map(id_fct_map)
//    , _parse_map(parse_fct_map)
//  {}
//
//  ~LightSceneParser();
//
//  bool parsingScene(tinyxml2::XMLElement *all_light_elem, mrf::rendering::Scene &scene);
//
//protected:
//
//  mrf::materials::UMat *addEmittance(tinyxml2::XMLElement *an_emitt_elem, mrf::rendering::Scene &scene);
//
//};
//
//}   // namespace io
//}   // namespace mrf
