/**
 * Copyright(C) CNRS. 2015, 2016.
 *
 * Author: Romain Pacnowski: romain.pacanowski@institutoptique.fr
 *
 *
 */

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/io/lexer.hpp>

#include <tinyxml2/tinyxml2.h>

#include <exception>
#include <iostream>
#include <stdexcept>




namespace mrf
{
namespace io
{
/**
 * @brief      Class  providing common static function to help parse some common markups
 */
class MRF_CORE_EXPORT Parser
{
public:
  /**
   * @brief      Parse from a given markup the
   *             "x", "y" "z" attributes to return a VEC3_TYPE
   *
   * @param      parent_markup_element  Pointer to TiXmlElement markup that is the parent markup
   * @param      markup_name the name of the child markup to be looked for.
   *
   * @tparam     VEC3_TYPE    Template parameter representing the 3D Vector type
   *
   * @return     The Vector type parsed
   *
   * @throw      <exception_object> A logic_error is thrown
   *             if the markup attributes could not be parsed of if <the markup_name>
   *             does not exist
   */
  template<class VEC3_TYPE>
  static VEC3_TYPE vec3_from_markup(tinyxml2::XMLElement const &parent_markup_element, char const *const markup_name);

  /**
   * @brief      Parse a given markup for the "width" and "height" attribute
   *             and returns a 2D Vector
   *
   * @param      parent_markup_element  Pointer to TiXmlElement markup that is the parent marku
   * @param      markup_name            the name of the child markup to be looked for.
   *
   * @tparam     VEC2_TYPE              Template parameter representing the 2D Vector type
   *
   * @return     { description_of_the_return_value }
   * @throw      logic_error A logic_error is the markup_name could not be found
   *             or if the attributes "width" and "height" could not be parsed
   */
  template<typename T, template<typename = T> class VEC2_TYPE>
  static VEC2_TYPE<T>
  vec2_for_width_and_height(tinyxml2::XMLElement const &parent_markup_element, char const *const markup_name);




  template<typename T>
  static T scalar_from_attribute_name(
      tinyxml2::XMLElement const &parent_markup_element,
      char const *const           markup_name,
      char const *const           attribute_name);

  /**
   * @brief      Returns a scalar parser from the markup named markup_name fomr the parant_markup_element.
   *             The value returned is parsed from the attribute name "value"
   *
   * @param      parent_markup_element  { parameter_description }
   * @param      markup_name            { parameter_description }
   *
   * @tparam     T                      { description }
   *
   * @return     the parsed scalar
   * @throw      logic_error A logic_error exception is thrown in case of error
   */
  template<typename T>
  static T scalar_from_value(tinyxml2::XMLElement const &parent_markup_element, char const *const markup_name);
};

template<class VEC3_TYPE>
VEC3_TYPE Parser::vec3_from_markup(tinyxml2::XMLElement const &parent_markup_element, char const *const markup_name)
{
  tinyxml2::XMLNode const *markup_node = parent_markup_element.FirstChildElement(markup_name);
  if (!markup_node)
  {
    throw std::logic_error(" No Child Element in this markup");
  }

  tinyxml2::XMLElement const *markup_element = markup_node->ToElement();

  if (!markup_element)
  {
    throw std::logic_error(" No Child Element in this markup");
  }

  double x, y, z;

  int xOK = markup_element->QueryDoubleAttribute(Lexer::X_AT, &x);
  int yOK = markup_element->QueryDoubleAttribute(Lexer::Y_AT, &y);
  int zOK = markup_element->QueryDoubleAttribute(Lexer::Z_AT, &z);

  if ((xOK != tinyxml2::XML_SUCCESS) || (yOK != tinyxml2::XML_SUCCESS) || (zOK != tinyxml2::XML_SUCCESS))
  {
    std::string message("Wrong attributes for Markup_element named ");
    message += markup_element->Value();

    throw std::logic_error(message);
  }

  return VEC3_TYPE(mrf::math::Vec3d(x, y, z));
}


template<typename T, template<typename = T> class VEC2_TYPE>
VEC2_TYPE<T>
Parser::vec2_for_width_and_height(tinyxml2::XMLElement const &parent_markup_element, char const *const markup_name)
{
  tinyxml2::XMLNode const *markup_node = parent_markup_element.FirstChildElement(markup_name);
  if (!markup_node)
  {
    throw std::logic_error(" No Child Element in this markup");
  }

  tinyxml2::XMLElement const *markup_element = markup_node->ToElement();

  double width;
  double height;

  int wOK = markup_element->QueryDoubleAttribute(Lexer::WIDTH_AT, &width);
  int hOK = markup_element->QueryDoubleAttribute(Lexer::HEIGHT_AT, &height);

  if ((wOK != tinyxml2::XML_SUCCESS) || (hOK != tinyxml2::XML_SUCCESS))
  {
    std::string message("Wrong attributes for Markup_element named ");
    message += markup_element->Value();

    throw std::logic_error(message);
  }

  return VEC2_TYPE<T>(static_cast<T>(width), static_cast<T>(height));
}


template<typename T>
T Parser::scalar_from_attribute_name(
    tinyxml2::XMLElement const &parent_markup_element,
    char const *const           markup_name,
    char const *const           attribute_name)
{
  tinyxml2::XMLNode const *markup_node = parent_markup_element.FirstChildElement(markup_name);
  if (!markup_node)
  {
    throw std::logic_error(" No Child Element in this markup");
  }

  tinyxml2::XMLElement const *markup_element = markup_node->ToElement();

  double scalar;
  int    scalar_OK = markup_element->QueryDoubleAttribute(attribute_name, &scalar);

  if (scalar_OK != tinyxml2::XML_SUCCESS)
  {
    std::string message("Wrong attributes for Markup_element named ");
    message += markup_element->Value();
    message += " . with attribute: " + std::string(attribute_name);

    throw std::logic_error(message);
  }

  return static_cast<T>(scalar);
}

template<typename T>
T Parser::scalar_from_value(tinyxml2::XMLElement const &parent_markup_element, char const *const markup_name)
{
  return Parser::scalar_from_attribute_name<T>(parent_markup_element, markup_name, Lexer::VALUE_AT);
}



}   // namespace io

}   // namespace mrf
