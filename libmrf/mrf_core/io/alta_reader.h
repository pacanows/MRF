/*
 * Initial Author: Romain Pacanowski
 * email: romain dot pacanowski at institutoptique dot fr
 *  Copyright CNRS 2018
 **/
#pragma once

#include <mrf_core/feedback/loger.hpp>

#include <iostream>
#include <fstream>
#include <map>
#include <set>
#include <memory>
#include <list>

/**
 * Module containing everything to parse ALTA-based Measurement files
 */

namespace mrf
{
static std::string const SPACE = " ";

static std::string const ALTA_HEADER_END_TOKEN = "#ALTA_END_HEADER";

static std::string const ALTA_SAMPLE_COUNT_TOKEN = "#SAMPLE_COUNT";
static std::string const ALTA_PRECISION_TOKEN    = "#PRECISION";

static std::string const ALTA_GRID_TOKEN = "#GRID";

static std::string const ALTA_DIMENSION_TOKEN = "#DIM";

static std::string const ALTA_MIN_TOKEN = "#MIN_DIM";
static std::string const ALTA_MAX_TOKEN = "#MAX_DIM";

static std::string const DIRAC_SPECTRAL_TOKEN = "#DIRAC";
static std::string const DIRAC_RGB_TOKEN      = "#DIRAC_RGB";

static std::string const WAVELENGTH_TOKEN = "#WAVELENGTH";


static std::set<std::string> const VALID_TOKENS_FOR_ALTA_HEADER
    = {"#ALTA_BEGIN_HEADER",
       ALTA_DIMENSION_TOKEN,
       ALTA_GRID_TOKEN,
       "#PARAM_IN",
       "#PARAM_OUT",
       ALTA_PRECISION_TOKEN,
       "#MIN_DIM",
       "#MAX_DIM",
       "#VS",
       "#FORMAT",
       "ENDIAN",
       ALTA_SAMPLE_COUNT_TOKEN,
       "#THETA_INCI",
       "#DIRAC_RGB",
       ALTA_HEADER_END_TOKEN};

static std::string const ALTA_SINGLE_PRECISION_VALUE = "ieee754-single";
static std::string const ALTA_DOUBLE_PRECISION_VALUE = "ieee754-double";


class MRF_CORE_EXPORT AltaHEADER
{
private:
  /**
   * @brief _header represented as a map of keyword (e.g. #PARAM_IN) and a list of strings
   * representing the remaining part of the line one which the keyword appears
   */
  std::map<std::string, std::list<std::string>> _header;
  long unsigned int                             _size;       // Size of the header in bytes
  long unsigned int                             _fileSize;   //Total size of the file in bytes.

  /* Number of bytes used to encode each value (usually 4 for single precision and 8 for double) */
  unsigned int _precisionSize;

public:
  // Setters and Getters
  long unsigned int  fileSize() const { return _fileSize; }
  long unsigned int &fileSize() { return _fileSize; }

  long unsigned int  size() const { return _size; }
  long unsigned int &size() { return _size; }

  // Effective Data Size: ( _fileSize - _size) obviously
  long unsigned int dataSize() const { return _fileSize - _size; }

  inline std::map<std::string, std::list<std::string>> const &header() const { return _header; }
  inline std::map<std::string, std::list<std::string>> &      header() { return _header; }

  /**
   * @brief Returns the encoding precision of the data in bytes from a given string
   * @return
   */
  inline unsigned int precision() const noexcept(true) { return _precisionSize; }

  /**
   * @brief Performs a check between the expected samples size and the file and header sizes
   * @details This method computes the size of the expected dats according to the number of samples
   *          and their encoded precision. This number is compared to the total size of the file
   *          minus the size of the header. The different computed sizes are computed in bytes.
   *
   * @return true if everything is coherent, false otherwise
   */
  inline bool checkDataSize() const noexcept(false);

  inline unsigned int updatePrecision() noexcept(false);

  /**
   * @brief Returns true if #GRID N1 N2 ... N is in the header and if N1 N2 ... N2 are > 0
   **/
  inline bool isGrid() const noexcept(true);

  /**
   * @brief true if the header contains #WAVELENGTH directive false otherwise
   * @return true if the header contains #WAVELENGTH directive false otherwise
   */
  inline bool isSpectral() const noexcept(true);

  /**
   * @brief true if the header contains #DIRAC directive false otherwise
   * @return true if the header contains #DIRAC directive false otherwise
   */
  inline bool hasSpectralDirac() const noexcept(true);

  /**
   * @brief true if the header contains #DIRAC_RGB directive false otherwise
   * @return true if the header contains #DIRAC_RGB directive false otherwise
   */
  inline bool hasRGBDirac() const noexcept(true);

  /**
   * @brief true if the header contains #GRID directive false otherwise
   * @return true if the header contains #GRID directive false otherwise
   */
  inline bool hasScatteringTerm() const noexcept(true);

private:
  inline unsigned int gridSize(std::list<std::string> const &grid_size_str) const;
};

inline bool AltaHEADER::isGrid() const noexcept(true)
{
  auto grid_it = _header.find(ALTA_GRID_TOKEN);
  if (grid_it == _header.cend())
  {
    return false;
  }
  else   // #GRID was in the Header
  {
    //Checking the Grid Size
    unsigned int check_grid_size = gridSize(grid_it->second);
    return (check_grid_size > 0) ? true : false;
  }
}

inline bool AltaHEADER::isSpectral() const noexcept(true)
{
  auto wavelength_it = _header.find(WAVELENGTH_TOKEN);

  if (wavelength_it == _header.cend())
  {
    return false;
  }
  return true;
}


inline bool AltaHEADER::hasSpectralDirac() const noexcept(true)
{
  auto dirac_it = _header.find(DIRAC_SPECTRAL_TOKEN);

  if (dirac_it == _header.cend())
  {
    return false;
  }
  return true;
}

inline bool AltaHEADER::hasRGBDirac() const noexcept(true)
{
  auto dirac_it = _header.find(DIRAC_RGB_TOKEN);

  if (dirac_it == _header.cend())
  {
    return false;
  }
  return true;
}

inline bool AltaHEADER::hasScatteringTerm() const noexcept(true)
{
  //TODO. Improve this
  return this->isGrid();
}


inline unsigned int AltaHEADER::updatePrecision() noexcept(false)
{
  auto precision_it = _header.find(ALTA_PRECISION_TOKEN);
  if (precision_it == _header.cend())
  {
    std::runtime_error(std::string("HEADER IS NOT CORRECT: Value for PRECISION is missing"));
  }

  std::string const &precision_str = *(precision_it->second.cbegin());

  if (precision_str.find(ALTA_SINGLE_PRECISION_VALUE) != std::string::npos)
  {
    _precisionSize = sizeof(float);
    return sizeof(float);
  }
  else if (precision_str.find(ALTA_DOUBLE_PRECISION_VALUE) != std::string::npos)
  {
    _precisionSize = sizeof(double);
    return sizeof(double);
  }
  else
  {
    std::runtime_error(std::string("HEADER IS NOT CORRECT: Value for PRECISION is not CORRECT"));
  }

  _precisionSize = 0;
  return 0;
}

inline unsigned int AltaHEADER::gridSize(std::list<std::string> const &grid_tokens) const
{
  unsigned int grid_size = 1;

  for (auto it = grid_tokens.cbegin(); it != grid_tokens.cend(); ++it)
  {
    grid_size *= std::stoul(*it);
  }
  return grid_size;
}



inline bool AltaHEADER::checkDataSize() const noexcept(false)
{
  auto sample_it    = _header.find(ALTA_SAMPLE_COUNT_TOKEN);
  auto precision_it = _header.find(ALTA_PRECISION_TOKEN);
  auto dimension_it = _header.find(ALTA_DIMENSION_TOKEN);

#ifdef ALTAREADER_DEBUG
  std::cout << " " << sample_it->first << std::endl;
  std::cout << " " << precision_it->first << std::endl;
  std::cout << " " << dimension_it->first << std::endl;
#endif

  long unsigned int data_size_from_header = 0;

  if (sample_it != _header.cend() && precision_it != _header.cend() && dimension_it != _header.cend())
  {
    // Sample count est le nombre d echantillons * DIM_SORTIE * SIZEOF(PRECISION)
    std::string const &sample_count_str = *(sample_it->second.cbegin());     //Getting the first string of the list
    std::string const &out_dim_str = *(++(dimension_it->second.cbegin()));   //Getting the second string of the list

#ifdef ALTAREADER_DEBUG
    std::cout << " sample_count_str = " << sample_count_str << std::endl;
    std::cout << " out_dim_str = " << out_dim_str << std::endl;
#endif

    long unsigned int const sample_count = std::stoul(sample_count_str);
    unsigned int const      out_dim      = std::stoul(out_dim_str);

    data_size_from_header = precision() * out_dim * sample_count;
  }
  else   //on Essaie avec le grid si jamais c'est possible
  {
    auto grid_it = _header.find(ALTA_GRID_TOKEN);


    if (grid_it != _header.end() && precision_it != _header.end() && dimension_it != _header.end())
    {
      //GRID SIZE * DIM_SORTIE * SIZEOF(PRECISION) DEVRAIT AUSSI RENVOYER LA MEME CHOSE

      std::string const &out_dim_str = *(++(dimension_it->second.cbegin()));   //Getting the second string of the list
      unsigned int const out_dim     = std::stoul(out_dim_str);


      unsigned int const grid_size = gridSize(grid_it->second);

      data_size_from_header = precision() * out_dim * grid_size;
    }
    else
    {
      throw std::runtime_error(std::string(
          "HEADER IS NOT CORRECT: Missing dimensions or grid or sample_count information"));   // WHAT THE FUCK ? HEADER IS NOT CORRECT
    }
  }
  long unsigned int const data_size_from_file = _fileSize - _size;

  return data_size_from_header == data_size_from_file;
}


inline std::ostream &operator<<(std::ostream &os, AltaHEADER const &header_data)
{
  os << "Header Data. File Size: " << header_data.fileSize() << "  Header Size " << header_data.size() << std::endl
     << " Header Keys " << std::endl;

  for (auto it = header_data.header().begin(); it != header_data.header().end(); ++it)
  {
    os << "[" << it->first << "] : { ";

    if (!it->second.begin()->empty())
    {
      for (auto deep_it = it->second.cbegin(); deep_it != it->second.cend(); ++deep_it)
      {
        os << *deep_it << ", ";
      }
      os << " } " << std::endl;
    }

  }   //end of for-loop

  return os;
}


/**
 * @brief The AltaReader provides low-level interface to parse an ALTA File
 */
class AltaReader
{
public:
  /**
   * @brief parseHeader
   * @param filename
   * @param header
   * @return The number of bytes of the header part of the file
   */
  static inline long unsigned int
  parseHeader(std::string const &filename, AltaHEADER &headerData, mrf::gui::fb::Loger *loger) noexcept(false);

  template<class T>
  static inline void
  parseData(std::string const &filename, AltaHEADER const &header, T *&data, mrf::gui::fb::Loger *loger) noexcept(
      false);

  template<class T>
  static inline void
  readData(std::string const &filename, AltaHEADER &header, T *&data, mrf::gui::fb::Loger *loger) noexcept(false);

private:
  //Code  Copy Pasted From MRF V2 and V3
  static inline void
  tokenize(std::string const &str, std::vector<std::string> &tokens, std::string const &delimiters = SPACE);
};


inline long unsigned int
AltaReader::parseHeader(std::string const &filename, AltaHEADER &headerData, mrf::gui::fb::Loger *loger)
{
  loger->trace("Alta Opening File ...", filename);

  //Detecting the size of the file
  //First we open the file at the end
  std::ifstream in(filename, std::ios::ate | std::ios::in | std::ios::binary);

  if (in.fail())
  {
    loger->fatal("COULD NOT OPEN FILE: ", filename);
    throw;
  }
  in.sync();

  long unsigned int size_of_file = static_cast<unsigned long int>(in.tellg());
  in.close();
  headerData.fileSize() = size_of_file;

  loger->trace("Alta File size (in MegaBytes): ", size_of_file / (1024 * 1024.0));

  //First Read the Header to determine the size of the DATA
  std::ifstream file_reader;
  try
  {
    file_reader.open(filename, std::ios_base::in | std::ios_base::binary);
  }
  catch (...)
  {
    loger->fatal("Problem while reading file: ", filename);
    loger->fatal("ABORTING");
    throw;
  }

  //Now Parse Header
  for (std::string line; std::getline(file_reader, line);)
  {
    //cout << "[DEBUG] line = " << line << endl;

    //END OF HEADER
    if (line == ALTA_HEADER_END_TOKEN)
    {
      break;
    }

    //Break each line in a list of tokens
    std::vector<std::string> line_tokens;
    AltaReader::tokenize(line, line_tokens);

    std::map<std::string, std::list<std::string>> &tmp = headerData.header();
    if (line_tokens.size() > 1)
    {
      tmp[line_tokens[0]] = std::list<std::string>(line_tokens.begin() + 1, line_tokens.end());
    }
    else
    {
      loger->debug("", line_tokens[0]);
      loger->debug("is alone");
    }
  }
  //HEADER SIZE NEEDS TO COMPUTED NOW
  long unsigned int tellg_header = static_cast<unsigned long int>(file_reader.tellg());

  loger->debug("Size of header from tellg_header = ", tellg_header);

  headerData.size() = tellg_header;

  file_reader.sync();
  file_reader.close();

  //Update the precision data/
  headerData.updatePrecision();


  return headerData.size();
}

template<class T>
inline void
AltaReader::parseData(std::string const &filename, AltaHEADER const &header, T *&data, mrf::gui::fb::Loger *loger)
{
  std::ifstream file_reader;
  try
  {
    file_reader.open(filename, std::ios_base::in | std::ios_base::binary);
  }
  catch (...)
  {
    loger->fatal("Problem while reading file: ", filename);
    loger->fatal("ABORTING");
    data = nullptr;
    throw;
  }

  //TODO Make me more robust! The type required is not necessarily the one stored
  // in the file

  //Allocating Data
  //TODO: use make_shared be careful with shared pointer for array
  //PRIOR TO C++ 2017: std::shared_ptr<float> sp(new float[ header.dataSize()], std::default_delete<float[]>());
  // After C++ 2017: shared_ptr<float[]> sp(new  header.dataSize()[10]);
  //TODO return a shared_ptr then
  unsigned int const nb_of_floats = header.dataSize() / header.precision();

  loger->debug("Number of floating point values of type T  = ", nb_of_floats);

  data = new T[header.dataSize() / header.precision()];

  // Move the reader to appropriate position
  file_reader.seekg(header.size());

  //Reading in one shot
  file_reader.read((char *)data, header.dataSize());

  loger->trace("ALTA READING COMPLETED");
  file_reader.close();
}



template<class T>
inline void AltaReader::readData(std::string const &filename, AltaHEADER &header, T *&data, mrf::gui::fb::Loger *loger)
{
  AltaReader::parseHeader(filename, header, loger);
  AltaReader::parseData(filename, header, data, loger);
}



inline void
AltaReader::tokenize(std::string const &str, std::vector<std::string> &tokens, std::string const &delimiters)
{
  // Skip delimiters at beginning.
  std::string::size_type lastPos = str.find_first_not_of(delimiters, 0);
  // Find first "non-delimiter".
  std::string::size_type pos = str.find_first_of(delimiters, lastPos);

  while (std::string::npos != pos || std::string::npos != lastPos)
  {
    // Found a token, add it to the vector.
    tokens.push_back(str.substr(lastPos, pos - lastPos));
    // Skip delimiters.  Note the "not_of"
    lastPos = str.find_first_not_of(delimiters, pos);
    // Find next "non-delimiter"
    pos = str.find_first_of(delimiters, lastPos);
  }
}


}   // namespace mrf