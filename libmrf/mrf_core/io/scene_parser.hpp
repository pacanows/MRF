/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/io/parser_helper.hpp>
#include <mrf_core/io/material_scene_parser.hpp>
#include <mrf_core/io/light_scene_parser.hpp>

#include <mrf_core/feedback/loger.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/color/color.hpp>
#include <mrf_core/radiometry/spectrum.hpp>
#include <mrf_core/rendering/scene.hpp>
#include <mrf_core/io/scene_lexer.hpp>
#include <mrf_core/io/parsing_errors.hpp>
#include <mrf_core/lighting/envmap.hpp>
#include <mrf_core/util/precision_timer.hpp>

#include <string>
#include <unordered_map>

#include <tinyxml2/tinyxml2.h>

//Forward declations
namespace mrf
{
namespace rendering
{
class Camera;
}

}   // namespace mrf

namespace mrf
{
namespace io
{
/**
 * This is the new Scene parser for new MRF Scene.
 * Note : Singleton class
 **/
class MRF_CORE_EXPORT SceneParser
{
protected:
  tinyxml2::XMLDocument _doc;
  tinyxml2::XMLHandle   _doc_handle;

  std::string _current_directory;

  std::string _scene_path;

  MaterialPlugins _plugins;

  /**
   * Handling Materials mapping betweeen id stored in the Scene class
   * and their name in the .mrf scene
   * No need to delete the materials in this class
   * they are always added to the scene object. The scene object
   * is responsible of their deletion
   **/
  std::map<std::string, unsigned int> _brdf_map;

  std::vector<mrf::materials::MultiMaterial *> _multi_mat_list;

  /**
   * For ENVMAP Ressource Management
   *
   **/
  std::map<std::string, mrf::lighting::EnvMap *> _envmap_map;

  /**
   * For external paths
   **/
  std::vector<std::string> _ext_paths;

  ParserHelper _parser;

private:
  void cleanString(std::string &str);
  bool parsingScene(mrf::rendering::Scene &scene);

public:
  SceneParser(MaterialPlugins &plugins): _plugins(plugins), _doc_handle(nullptr) {}

  SceneParser(MaterialPlugins &plugins, std::string &path): _plugins(plugins), _scene_path(path), _doc_handle(nullptr)
  {}

  void setPath(std::string path) { _scene_path = path; }

  static bool loadScene(std::string const &scene_file, mrf::rendering::Scene *&scenee);

  static SceneParser &Instance();

  bool loadScene(char const *filename, mrf::rendering::Scene *&scene);

  bool loadScene(mrf::rendering::Scene *&scene);

protected:
  bool parseGlobalMaterials(mrf::rendering::Scene &scene);
  bool parseLights(mrf::rendering::Scene &scene);

  bool parseShapes(mrf::rendering::Scene &scene);

  mrf::rendering::mesh_map::iterator
  addMesh(tinyxml2::XMLElement *a_mesh_element, mrf::rendering::Scene &scene, int current_material_index);

  bool addSphere(
      tinyxml2::XMLElement *  elem,
      mrf::rendering::Scene & scene,
      mrf::uint               material_index_from_shape,
      mrf::math::Reff const & a_ref,
      mrf::math::Vec3f const &a_scale);

  //-------------------------------------------------------------------------
  // External Geometric Ressources Loader
  //-------------------------------------------------------------------------
  bool loadPLYObject(tinyxml2::XMLElement *elem, std::vector<float> &vertices, std::vector<unsigned int> &faces);

  bool retrieveValue(tinyxml2::XMLElement *an_element, float &value);

  mrf::materials::UMat *addEmittance(tinyxml2::XMLElement *an_emitt_elem, mrf::rendering::Scene &scene);

  /**
   * @brief      This method parse a <rgb_color> markup to retrieve the red, green blue
   * attributes from it and sets the given color with the retrieved values
   *
   * @param      rgb_markup  the TiXmlElement representing the <rgb_color>
   * @param      c           The color sets with the attributes
   *
   * @return     true if the parsing went well false otherwise
   */
  bool rgbFromMarkup(tinyxml2::XMLElement *rgb_markup, mrf::color::Color &c);
  bool spectrumFromMarkup(tinyxml2::XMLElement *rgb_markup, mrf::radiometry::Spectrum_T<float, float> &c);

  bool retrieveColor(tinyxml2::XMLElement *elem, mrf::materials::COLOR &c);
  bool retrieveColors(tinyxml2::XMLElement *an_elem, std::vector<mrf::materials::COLOR> &colors);
  bool retrieveFresnel(tinyxml2::XMLElement *an_elem, mrf::materials::COLOR &eta, mrf::materials::COLOR &k);

  bool retrieveReflectivity(tinyxml2::XMLElement *elem, float &reflectivity);

  bool addPointLight(tinyxml2::XMLElement *a_light_element, mrf::rendering::Scene &scene);
  bool addQuadLight(tinyxml2::XMLElement *a_light_element, mrf::rendering::Scene &scene);
  bool addSphereLight(tinyxml2::XMLElement *a_light_element, mrf::rendering::Scene &scene);
  bool addDirectionalLight(tinyxml2::XMLElement *a_light_element, mrf::rendering::Scene &scene);

  bool retrieveEmittanceType(tinyxml2::XMLElement *elem, std::string &emittance_type);

  bool retrievePositionAttributes(tinyxml2::XMLElement *elem, float &pos_x, float &pos_y, float &pos_z);
  bool retrievePosition(tinyxml2::XMLElement *elem, mrf::math::Vec3f &position);
  bool retrieveNormal(tinyxml2::XMLElement *elem, mrf::math::Vec3f &normal);
  bool retrieveDirection(tinyxml2::XMLElement *elem, mrf::math::Vec3f &direction);

  bool retrieveFloatVector(tinyxml2::XMLElement *element, std::vector<float> &v);

  bool retrieveTransform(tinyxml2::XMLElement *elem, mrf::math::Mat4f &mat);
  bool retrieveMultipleTransforms(tinyxml2::XMLElement *elem, mrf::math::Mat4f &mat);

  bool retrieveTransform(tinyxml2::XMLElement *elem, mrf::math::Reff &ref, mrf::math::Vec3f &scale);
  bool retrieveMultipleTransforms(tinyxml2::XMLElement *elem, mrf::math::Reff &ref, mrf::math::Vec3f &scale);


  inline bool retrieveMaterialIndex(tinyxml2::XMLElement *elem, unsigned int &material_index) const;


  bool parseResourcesPath(mrf::rendering::Scene &scene);

  //-------------------------------------------------------------------------
  // External Resources
  //-------------------------------------------------------------------------
  bool parseExternalResources(mrf::rendering::Scene &scene);

  /**
   * Common function for addPanoramicEnvmap() and addSphericalEnvmap()
   * parse name, file_path, theta and phi.
   **/
  bool parseEnvmapGenericAttributes(
      tinyxml2::XMLElement *elem,
      std::string &         name,
      std::string &         path,
      float &               theta,
      float &               phi);
  bool addPanoramicEnvmap(tinyxml2::XMLElement *elem);
  bool addSphericalEnvmap(tinyxml2::XMLElement *elem);

  //-------------------------------------------------------------------------
  // Add a Background Manager for the Scene
  //-------------------------------------------------------------------------
  bool parseBackground(mrf::rendering::Scene &scene);

private:
  //Hidden due to Singleton Implementation
  SceneParser();
  virtual ~SceneParser();

  /**
   * clear the envmap map and release memory of the envmaps
   * that were not added to the scene
   * Always use this function instead of _envmap_map.clear();
   **/
  void clearEnvmaps();
  SceneParser(SceneParser const &);              // copy ctor is hidden
  SceneParser &operator=(SceneParser const &);   // assign op is hidden
};

inline bool SceneParser::retrieveMaterialIndex(tinyxml2::XMLElement *elem, unsigned int &material_index) const
{
  if (!elem->Attribute(mrf::io::SceneLexer::REF_MATERIAL_AT))
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::NO_MATERIALS);
    return false;
  }

  std::string ref_material = elem->Attribute(mrf::io::SceneLexer::REF_MATERIAL_AT);
  mrf::gui::fb::Loger::getInstance()->trace("REFERENCE MATERIAL ");
  mrf::gui::fb::Loger::getInstance()->trace(ref_material);

  //NOW Check if the material exists
  auto current = _brdf_map.find(ref_material);
  auto end     = _brdf_map.end();
  if (current == end)   //  THIS Key does not exists !
  {
    mrf::gui::fb::Loger::getInstance()->fatal(ParsingErrors::UNDEFINED_MATERIAL);
    return false;
  }
  //This returns a TYPE& and not a TYPE const & brdfMap[ref_material];
  material_index = current->second;
  return true;
}

MRF_CORE_EXPORT bool loadScene(rendering::Scene *&scene, std::string const &scene_filename);
}   // namespace io
}   // namespace mrf
