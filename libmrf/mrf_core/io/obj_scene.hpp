/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/io/obj_material.hpp>

#include <string>
#include <iostream>
#include <vector>
#include <map>
#include <list>

namespace mrf
{
class MRF_CORE_EXPORT OBJFace
{
public:
  /** Indices for each vertex **/
  std::vector<unsigned int> _vertexIndices;

  /** key = Vertex_index_in_face --> Normal_index_in_Mesh  **/
  std::map<unsigned int, unsigned int> _normalIndices;

  /** key = Vertex_index_in_face --> Texture_index_in_Mesh  **/
  std::map<unsigned int, unsigned int> _textureIndices;

  /** The names of the groups the face belong to. **/
  std::vector<std::string> _groups;

  /**The name of the object the face belongs to **/
  std::string _objectName;

  /** The material name associated with the face **/
  std::string _materialName;

  /** Attribute storing whether or not the face should have smooth normals **/
  bool _smoothNormal;

  OBJFace(): _smoothNormal(false) {}
};


//--------------------------------------------------------------------------------------------------
// EXTERNAL OPERATORS  FOR OBJFace
//--------------------------------------------------------------------------------------------------

inline std::ostream &operator<<(std::ostream &os, OBJFace const &a_face)
{
  os << " Face [ Material Name::::" << a_face._materialName << "; smoothNormal: " << a_face._smoothNormal << std::endl;

  os << "        Vertex Indices = { ";

  for (unsigned int i = 0; i < a_face._vertexIndices.size(); i++)
  {
    os << a_face._vertexIndices[i] << "; ";
  }
  os << " } " << std::endl;


  os << "      Normal Indices = { ";

  std::map<unsigned int, unsigned int>::const_iterator it;

  for (it = a_face._normalIndices.begin(); it != a_face._normalIndices.end(); ++it)
  {
    os << it->first << "  --> " << it->second << " ; ";
  }
  os << " } " << std::endl;

  os << "      Texture Indices = { ";
  for (it = a_face._textureIndices.begin(); it != a_face._textureIndices.end(); ++it)
  {
    os << it->first << "  --> " << it->second << " ; ";
  }
  os << " } " << std::endl;
  return os;
}


//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------
//--------------------------------------------------------------------------------------------------

class MRF_CORE_EXPORT OBJScene
{
public:
  std::vector<mrf::math::Vec4f> _vertices;
  std::vector<mrf::math::Vec3f> _normals;
  std::vector<mrf::math::Vec3f> _uvs;
  std::vector<OBJFace>          _faces;

  std::map<std::string, OBJMaterial> _materials;

  // We store the indices of the faces the material  is applied to
  // First parameter (key) is the name of the material
  std::map<std::string, std::list<unsigned int>> _materialPerFaces;

  //For each named object store the faces that belongs to it.
  std::map<std::string, std::list<unsigned int>> _objectPerFaces;


  typedef std::map<std::string, OBJMaterial>::const_iterator OBJMATERIAL_CIT_TYPE;

  typedef std::map<std::string, std::list<unsigned int>>::const_iterator PER_FACE_CIT_TYPE;

private:
  // A map used to detect if a vertex is shared between different faces
  //  vertex_index ---> ( normal_index, texture_index )
  // -1 means nothing was parsed read in the file
  std::map<Triplet_int, bool> _sharedIndexMap;

  std::map<Triplet_int, unsigned int> _oldToNewVertexIndex;


  std::map<unsigned int, bool> _normalIndexForNormalization;


public:
  void clear();
  void initializeMaterialPerFaces();
  void printMaterials() const;

  void triangulateScene();

  inline std::vector<mrf::math::Vec3f> const &uvs() const;

  /**
   * This function assumes that the scene has been triangulate
   *
   * WARNING: all none-const vector passed to this functions are cleared before being filled
   **/
  void exportLinearBuffers(
      std::vector<mrf::math::Vec3f> &positions,
      std::vector<mrf::math::Vec3f> &normals,
      std::vector<mrf::math::Vec3f> &uvs,
      std::vector<unsigned int> &    faces,               //list of indexes for each triangle
      std::vector<OBJMaterial> &     materials,           //list of materials
      std::vector<unsigned int> &    per_face_material,   //For each face stores its material
      std::vector<unsigned int> &    material_strides     // For each material stores how many faces are using it.
                                                          // Material 0 goes from face 0 to material_strides[0]
      // Material 1 goes from face material_strides[0]+1 to material_strides[1]
  );




private:
  void getVertexAndKeys(
      OBJFace const &   c_face,
      unsigned int      index_vertex_in_face,
      mrf::math::Vec3f &vertex_coordinates,
      int &             vertex_index,
      int &             vn_index,
      int &             vt_index);


  void linearizeGeometry(
      OBJFace const &                c_face,
      std::vector<mrf::math::Vec3f> &positions,
      std::vector<mrf::math::Vec3f> &normals,
      std::vector<mrf::math::Vec3f> &uvs,
      std::vector<unsigned int> &    faces);


  // Update the _sharedIndexMap if the vertex is shared
  // Returns true if an insertion has been made.
  // If false is returned:
  // this means that it is exactly the same vertex which is already stored
  bool updateSharedIndexMap(Triplet_int const &current_key);


  /**
   * This methods computes a normal by vertex unless one has been already
   * provided inside the obj file.
   *
   * The strategy to compute a vertex normal is a weighted area sum of
   * each triangle sharing the vertex
   **/
  void computeNormalPerVertex(
      std::vector<mrf::math::Vec3f> const &positions,
      std::vector<unsigned int> const &    faces,
      std::vector<mrf::math::Vec3f> &      normals);
};


void OBJScene::clear()
{
  //Remove everything from scene
  _vertices.clear();
  _normals.clear();
  _uvs.clear();
  _faces.clear();
  _materialPerFaces.clear();
  _objectPerFaces.clear();
}

void OBJScene::initializeMaterialPerFaces()
{
  OBJMATERIAL_CIT_TYPE it;

  std::list<unsigned int> empty_list;

  for (it = _materials.begin(); it != _materials.end(); ++it)
  {
    _materialPerFaces[it->first] = empty_list;
  }
}

void OBJScene::printMaterials() const
{
  std::cout << " Scene Materials { ";

  OBJMATERIAL_CIT_TYPE it;
  for (it = _materials.begin(); it != _materials.end(); ++it)
  {
    std::cout << it->second << std::endl;
  }
  std::cout << " } " << std::endl;
}

std::vector<mrf::math::Vec3f> const &OBJScene::uvs() const
{
  return _uvs;
}



void OBJScene::triangulateScene()
{
  using namespace std;

  for (unsigned int i = 0; i < _faces.size(); i++)
  {
    if (_faces[i]._vertexIndices.size() == 4)   //WE HAVE A QUAD
    {
      OBJFace &c_face = _faces[i];

#ifdef MRF_OBJSCENE_DEBUG
      cout << "Triangulating Face: " << i << endl;
      cout << " Current face is face[" << i << "] = " << c_face << endl;
#endif


      OBJFace new_face;
      new_face._vertexIndices.resize(3);

      new_face._vertexIndices[0] = c_face._vertexIndices[0];
      new_face._vertexIndices[1] = c_face._vertexIndices[2];
      new_face._vertexIndices[2] = c_face._vertexIndices[3];


      //Suppressing last vertex of the quad to transform it into a triangle
      c_face._vertexIndices.pop_back();

      // COPY NORMAL COORDINATES IF THEY EXIST !!!
      if (c_face._normalIndices.find(0) != c_face._normalIndices.end())
      {
        new_face._normalIndices[0] = c_face._normalIndices[0];
      }
      if (c_face._normalIndices.find(2) != c_face._normalIndices.end())
      {
        new_face._normalIndices[1] = c_face._normalIndices[2];
      }

      std::map<unsigned int, unsigned int>::iterator it = c_face._normalIndices.find(3);
      if (it != c_face._normalIndices.end())
      {
        new_face._normalIndices[2] = c_face._normalIndices[3];

        //Suppressing last vertex normal of the quad to transform it into a triangle
#ifdef MRF_OBJSCENE_DEBUG
        cout << " ATTEMPTING TO ERRASE LAST NORMALS of " << c_face._normalIndices[3] << endl;
#endif

        c_face._normalIndices.erase(it->first);
      }

      // DO THE SAME FOR TEXTURE COORDINATES
      if (c_face._textureIndices.find(0) != c_face._textureIndices.end())
      {
        new_face._textureIndices[0] = c_face._textureIndices[0];
      }
      if (c_face._textureIndices.find(2) != c_face._textureIndices.end())
      {
        new_face._textureIndices[1] = c_face._textureIndices[2];
      }

      it = c_face._textureIndices.find(3);
      if (it != c_face._textureIndices.end())
      {
        new_face._textureIndices[2] = c_face._textureIndices[3];

        //Suppressing last vertex texture of the quad to transfor m it into a triangle
        c_face._textureIndices.erase(it);
      }

      // NOW COPY THE REST OF THE ATTRIBUTES
      std::copy(c_face._groups.begin(), c_face._groups.end(), new_face._groups.begin());
      new_face._smoothNormal = c_face._smoothNormal;
      new_face._objectName   = c_face._objectName;
      new_face._materialName = c_face._materialName;

      //Now pushing the new face
      _faces.push_back(new_face);
      _materialPerFaces[new_face._materialName].push_back(static_cast<uint>(_faces.size() - 1));
      _objectPerFaces[new_face._objectName].push_back(static_cast<uint>(_faces.size() - 1));


#ifdef MRF_OBJSCENE_DEBUG
      cout << "[INFO] Face is now: " << c_face << std::endl;
      cout << "[INFO] new_Face (from Face : " << i << ")" << new_face << std::endl;
#endif
    }
    else if (_faces[i]._vertexIndices.size() > 4)
    {
      cout << "[WARNING] Face " << i << " has more than 4 vertices. Wont be triangulate." << endl;
    }

  }   //end of for loop on faces of the scene

}   //end of triangulateScene method


void OBJScene::getVertexAndKeys(
    OBJFace const &   c_face,
    unsigned int      index_vertex_in_face,
    mrf::math::Vec3f &vertex_coordinates,
    int &             vertex_index,
    int &             vn_index,
    int &             vt_index)
{
  //A face without vertex SHOULD NOT BE  possible
  assert(c_face._vertexIndices.size() > 0);
  //Check boundaries of the index_vertex
  assert(index_vertex_in_face < c_face._vertexIndices.size());

  // Construct the different indexes as key
  vertex_index = static_cast<int>(c_face._vertexIndices[index_vertex_in_face]);
  vt_index     = -1;
  vn_index     = -1;

  std::map<unsigned int, unsigned int>::const_iterator it = c_face._textureIndices.find(index_vertex_in_face);
  if (it != c_face._textureIndices.end())
  {
    vt_index = it->second;
  }

  it = c_face._normalIndices.find(index_vertex_in_face);
  if (it != c_face._normalIndices.end())
  {
    vn_index = it->second;
  }

#ifdef MRF_OBJSCENE_DEBUG
  std::cout << " FroM getVertexAndKeys " << __FILE__ << "  at " << __LINE__
            << " [v_index, vn_index, vt_index ] = " << vertex_index << "  " << vn_index << " " << vt_index << std::endl;
#endif

  mrf::math::Vec4f const c_v = _vertices[vertex_index];

  //Dividing by homegenous coordinate.
  float const inv_homogene = 1.0f / c_v[3];
  vertex_coordinates       = mrf::math::Vec3f(c_v[0], c_v[1], c_v[2]);
  vertex_coordinates *= inv_homogene;
}

bool OBJScene::updateSharedIndexMap(Triplet_int const &current_key)
{
  typedef std::map<Triplet_int, bool> mapT;

  mapT::iterator it = _sharedIndexMap.find(current_key);

  if (it != _sharedIndexMap.end())
  {
#ifdef MRF_OBJSCENE_DEBUG
    std::cout << __FILE__ << "  " << __LINE__ << std::endl;
    std::cout << " current_key  " << current_key << " was already in map" << std::endl;
#endif

    return false;
  }



  _sharedIndexMap[current_key] = true;
  return true;
}

void OBJScene::linearizeGeometry(
    OBJFace const &                c_face,
    std::vector<mrf::math::Vec3f> &positions,
    std::vector<mrf::math::Vec3f> &normals,
    std::vector<mrf::math::Vec3f> &uvs,
    std::vector<unsigned int> &    faces)
{
  using namespace mrf::math;

  //Enforce that the face is a triangle
  assert(c_face._vertexIndices.size() == 3);


#ifdef MRF_OBJSCENE_DEBUG
  std::cout << std::endl;
  std::cout << __FILE__ << "  " << __LINE__ << std::endl;
  std::cout << " Face  [" << c_face._vertexIndices[0] << " " << c_face._vertexIndices[1] << " "
            << c_face._vertexIndices[2] << " ] " << std::endl
            << std::endl;
#endif


  // VERTICES: For each vertex of the face
  for (unsigned int i = 0; i < c_face._vertexIndices.size(); i++)
  {
    Vec3f current_v3;
    int   vertex_index, vn_index, vt_index;

    getVertexAndKeys(c_face, i, current_v3, vertex_index, vn_index, vt_index);


    Triplet_int current_key(vertex_index, vn_index, vt_index);

#ifdef MRF_OBJSCENE_DEBUG
    std::cout << __FILE__ << "  " << __LINE__ << std::endl;
    std::cout << current_key << std::endl;
#endif

    if (updateSharedIndexMap(current_key))
    {
      //First the Vertex Position needs to be linearized
      positions.push_back(current_v3);
      unsigned int const linearized_vertex_index = static_cast<uint>(positions.size() - 1);

      _oldToNewVertexIndex[current_key] = linearized_vertex_index;

#ifdef MRF_OBJSCENE_DEBUG
      std::cout << " current_key =" << current_key << " with  lineaRized_vertex_indes = " << linearized_vertex_index
                << std::endl;
#endif

      //Update the Face Vector
      faces.push_back(linearized_vertex_index);

      //-------------------------------------------------------------------
      //Now the Vertex Normal
      if (vn_index != -1)
      {
        normals.push_back(_normals[vn_index]);
      }
      else
      {
        // No normal defined in the file.
        // Pushing a WRONG default one and also storing the index
        // in the map so that later, we can compute it correctly
        normals.push_back(mrf::math::Vec3f(0.0f, 0.0f, 0.0f));
        _normalIndexForNormalization[static_cast<uint>(normals.size() - 1)] = true;
      }

      //-------------------------------------------------------------------
      //Now the Vertex texture coordinates
      if (vt_index != -1)
      {
#ifdef MRF_OBJSCENE_DEBUG
        std::cout << " Pusing texture coordinates = " << _uvs[vt_index] << " AT vt_index = " << vt_index << std::endl;
#endif

        uvs.push_back(_uvs[vt_index]);
      }
      else
      {
        //No texture coordinates. Pushing zero as default value
        uvs.push_back(mrf::math::Vec3f(0.0f, 0.0f, 0.0f));
      }
    }
    else   // The vertex was already stored with the same vt_index and vn_index
    {
      unsigned int const linearized_vertex_index = _oldToNewVertexIndex[current_key];
      faces.push_back(linearized_vertex_index);

#ifdef MRF_OBJSCENE_DEBUG
      std::cout << __FILE__ << "  AT " << __LINE__ << std::endl;
      std::cout << " VERTEX ALREADY STORED "
                << " With current_key " << current_key << " and " << linearized_vertex_index << std::endl;
#endif
    }

  }   //end of for-loop on each vertex of the c_face

}   //end of linearizeGeometry method


void OBJScene::exportLinearBuffers(
    std::vector<mrf::math::Vec3f> &positions,
    std::vector<mrf::math::Vec3f> &normals,
    std::vector<mrf::math::Vec3f> &uvs,
    std::vector<unsigned int> &    faces,
    std::vector<OBJMaterial> &     materials,
    std::vector<unsigned int> &    per_face_material,   //For each face stores its material
    std::vector<unsigned int> &    material_strides     // For each material stores how many faces are using it.
                                                        // Material 0 goes from face 0 to material_strides[0]
    // Material 1 goes from face material_strides[0]+1 to material_strides[1]
)
{
  using namespace std;

  positions.clear();
  normals.clear();
  uvs.clear();
  faces.clear();
  materials.clear();
  per_face_material.clear();
  material_strides.clear();

  _sharedIndexMap.clear();
  _oldToNewVertexIndex.clear();
  _normalIndexForNormalization.clear();

#ifdef MRF_OBJSCENE_DEBUG
  std::cout << __FILE__ << "  " << __LINE__ << std::endl;
  std::cout << "  STARTING THE EXPORT LINEAR BUFFERS  " << std::endl;
  for (unsigned int i = 0; i < 10; i++)
  {
    cout << " Vertex[" << i << "] = " << _vertices[i] << endl << " Uvs[" << i << "] = " << _uvs[i] << endl;
  }
#endif


  if (_materialPerFaces.size() == 0)   //No material used in this scene
  {
#ifdef MRF_OBJSCENE_DEBUG
    std::cout << __FILE__ << " NO MATERIAL PER FACES " << __LINE__ << std::endl;
#endif

    for (unsigned int i = 0; i < _faces.size(); i++)
    {
      linearizeGeometry(_faces[i], positions, normals, uvs, faces);
    }

  }      // end of if no material used in the scene
  else   //Arrange the faces such that continous faces use the same material
  {
    PER_FACE_CIT_TYPE it;

    //For each material
    for (it = _materialPerFaces.begin(); it != _materialPerFaces.end(); ++it)
    {
      materials.push_back(_materials[it->first]);

      std::list<unsigned int> const &list_faces = it->second;

      //For each face
      for (std::list<unsigned int>::const_iterator face_it = list_faces.begin(); face_it != list_faces.end(); ++face_it)
      {
        unsigned int const face_index = *face_it;
        OBJFace const &    c_face     = _faces[face_index];

#ifdef MRF_OBJSCENE_DEBUG
        std::cout << __FILE__ << " " << __LINE__ << std::endl;
        std::cout << " face index = " << face_index << std::endl;
        std::cout << " Face = " << c_face << std::endl;
#endif

        linearizeGeometry(c_face, positions, normals, uvs, faces);

        // Store the material for this face
        per_face_material.push_back(static_cast<uint>(materials.size() - 1));


      }   // end of for loop for each face

      // Strides
      material_strides.push_back(static_cast<uint>(list_faces.size() - 1));

    }   //end of for loop for each material per face

    assert(per_face_material.size() == _faces.size());
    assert(materials.size() == _materialPerFaces.size());
    assert(materials.size() == material_strides.size());

  }   //end of else


  //CHECK Some assertions
  assert(positions.size() == normals.size());
  assert(normals.size() == uvs.size());
  assert(faces.size() == _faces.size() * 3);   // triangular faces

  /*TODO USE LOGGING SYSTEM*/
  /*
  cout << "[INFO] Linearization of the OBJ File completed " << endl
       << "[INFO]  Positions size: " << positions.size() << " Normals size: " << normals.size()
       << " Uvs size: " << uvs.size() << endl
       << "[INFO]  Faces size: " << faces.size() << " which is equivalent to " << faces.size() / 3.0 << " triangles. " << endl
       << "[INFO] Material per faces size = " << per_face_material.size()
       << " Materials size: " << materials.size()
       << " Material Strides size: "  << material_strides.size() << endl;
  */
  //Smooth normals
  computeNormalPerVertex(positions, faces, normals);




}   //end of exportLinearBuffer() methods


void OBJScene::computeNormalPerVertex(
    std::vector<mrf::math::Vec3f> const &positions,
    std::vector<unsigned int> const &    faces,
    std::vector<mrf::math::Vec3f> &      normals)
{
  using namespace std;
  using namespace mrf::math;



  for (unsigned int i = 0; i < faces.size(); i += 3)
  {
    unsigned int const i1 = faces[i];
    unsigned int const i2 = faces[i + 1];
    unsigned int const i3 = faces[i + 2];

#ifdef MRF_OBJSCENE_DEBUG
    std::cout << " Index face = " << i / 3 << std::endl;
    std::cout << " i1 " << i1 << std::endl;
    std::cout << " i2 " << i2 << std::endl;
    std::cout << " i3 " << i3 << std::endl;
#endif

    assert(i1 < positions.size());
    assert(i2 < positions.size());
    assert(i3 < positions.size());

    // Compute the weighted normal of the face
    mrf::math::Vec3f const v1 = positions[i1];
    mrf::math::Vec3f const v2 = positions[i2];
    mrf::math::Vec3f const v3 = positions[i3];

    mrf::math::Vec3f const normal_face = (v2 - v1).cross(v3 - v1);

    if (_normalIndexForNormalization.find(i1) != _normalIndexForNormalization.end())
    {
      normals[i1] += normal_face;
    }

    if (_normalIndexForNormalization.find(i2) != _normalIndexForNormalization.end())
    {
      normals[i2] += normal_face;
    }

    if (_normalIndexForNormalization.find(i3) != _normalIndexForNormalization.end())
    {
      normals[i3] += normal_face;
    }
  }

  unsigned int                                 number_of_normals_recomputed = 0;
  std::map<unsigned int, bool>::const_iterator it;

  for (it = _normalIndexForNormalization.begin(); it != _normalIndexForNormalization.end(); ++it)
  {
    unsigned int const index = it->first;
    normals[index].normalize();

    number_of_normals_recomputed++;
  }

  /*TODO USE LOGGING SYSTEM*/
  /*
  cout << "[INFO] Number of normals required: " << normals.size() << endl
       << "[INFO] Number of normals recomputed:" << number_of_normals_recomputed << endl;
       */
}




//--------------------------------------------------------------------------------------------------
// EXTERNAL OPERATORS  FOR OBJScene
//--------------------------------------------------------------------------------------------------

inline std::ostream &operator<<(std::ostream &os, OBJScene const &the_scene)
{
  using namespace std;

  os << " OBJ Scene " << std::endl
     << " { Nb vertices: " << the_scene._vertices.size() << " Nb Normals: " << the_scene._normals.size()
     << " Nb Texture Coordinates: " << the_scene._uvs.size() << " Nb Materials: " << the_scene._materials.size()
     << " Number of named objects: " << the_scene._objectPerFaces.size()
     << " Number of faces: " << the_scene._faces.size() << std::endl;

  unsigned int count_non_triangular_faces = 0;
  for (unsigned int i = 0; i < the_scene._faces.size(); i++)
  {
    if (the_scene._faces[i]._vertexIndices.size() > 3)
    {
      // os << "Faces " << i << " has " << the_scene._faces[i]._vertexIndices.size()
      //    << " vertices." << std::endl;
      count_non_triangular_faces++;
    }
  }

#ifdef MRF_OBJSCENE_DEBUG
  cout << " OBJ SCENE DEBUG " << __FILE__ << " " << __LINE__ << endl;

  for (unsigned int i = 0; i < 45; i++)
  {
    cout << " Vertex[" << i << "] = " << the_scene._vertices[i] << endl
         << " Uvs[" << i << "] = " << the_scene._uvs[i] << endl;
  }
#endif



  os << " The scene has " << count_non_triangular_faces << " non-triangular faces. " << endl;

  OBJScene::PER_FACE_CIT_TYPE it;
  unsigned int                nb_faces = 0;
  for (it = the_scene._materialPerFaces.begin(); it != the_scene._materialPerFaces.end(); ++it)
  {
    nb_faces += static_cast<uint>(it->second.size());
    os << " Material " << it->first << " used for " << it->second.size() << " faces { ";

    unsigned int count = 0;
    for (std::list<unsigned int>::const_iterator face_it = it->second.begin(); face_it != it->second.end(); ++face_it)
    {
      if (count == 10)
      {
        break;
      }
      os << *face_it << " ";
      count++;
    }
    os << " ... } " << std::endl;
  }


  // for( it = the_scene._objectPerFaces.begin(); it != the_scene._objectPerFaces.end(); ++it )
  // {
  //      os << " Object  named \"" << it->first << "\" has faces. " << std::endl;
  // }

  os << " Number of faces from materials:  " << nb_faces << endl;

  os << " } " << std::endl;

  return os;
}
}   // namespace mrf