/**
 * Copyright(C) INRIA. 2018.
 *
 * Author: Arthur Dufay
 *
 *
 */

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/image/image.hpp>

#include <tinyexr/tinyexr.h>

#include <string>
#include <vector>

namespace mrf
{
namespace io
{
mrf::image::IMAGE_LOAD_SAVE_FLAGS saveAsExr(
    std::string const &file_path,
    std::vector<float> images[3],
    size_t             width,
    size_t             height,
    std::string        additional_information = "");
}
}   // namespace mrf
