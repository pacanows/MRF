/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/io/obj_material_lexer.hpp>
#include <mrf_core/io/file_io.hpp>
#include <mrf_core/io/obj_material.hpp>
#include <mrf_core/util/string_parsing.hpp>
#include <mrf_core/materials/umat.hpp>

#include <vector>
#include <map>
#include <iostream>
#include <fstream>
#include <string>

namespace mrf
{
namespace io
{
template<class FileIOPolicy>
class _OBJMaterialParser
{
private:
  static FileIOPolicy _fileIO;


  unsigned int _current_material_counter;
  std::string  _current_material_name;


  //Public Methods
public:
  static _OBJMaterialParser &Instance();

  bool parse(
      std::string const &                 directory_path,
      std::string const &                 mtl_file_name,
      std::map<std::string, OBJMaterial> &materials);


private:
  bool analyzeOneLine(
      std::string const &                 line,
      std::string const &                 directory_path,
      std::map<std::string, OBJMaterial> &materials,
      unsigned int                        current_line_number);


  bool analyzeOneLine(
      std::string &                        line,
      std::map<std::string, unsigned int> &material_id,
      std::vector<OBJMaterial> &           materials);

  //Returns true if parsing went good
  static inline bool
  convertToFloat(std::string const &s_x, std::string const &s_y, std::string const &s_z, float &x, float &y, float &z);

  //Hidden due to Singleton Implementation
  _OBJMaterialParser();
  virtual ~_OBJMaterialParser();
  _OBJMaterialParser(_OBJMaterialParser const &other);              // copy ctor is hidden
  _OBJMaterialParser &operator=(_OBJMaterialParser const &other);   // assign op is hidden

};   //end of class



//-------------------------------------------------------------------------
// utility
//-------------------------------------------------------------------------
static inline bool isLineAComment(std::string &line)
{
  if (ObjLexer::OBJ_COMMENT == line)
  {
    return true;
  }
  else if (strncmp(ObjLexer::OBJ_COMMENT, &line[0], 1) == 0)
  {
    return true;
  }
  else
  {
    return false;
  }
}



#include "obj_material_parser.cxx"

typedef _OBJMaterialParser<mrf::io::StreamFileIO> OBJMaterialParser;
typedef _OBJMaterialParser<mrf::io::CFileIO>      OBJMaterialParserCIO;


}   // namespace io
}   // namespace mrf
