/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 **/
#pragma once

#include <tuple>

#include <mrf_core/math/math.hpp>

namespace mrf
{
typedef unsigned int                 uint;
typedef unsigned char                uchar;
typedef std::tuple<uint, uint>       Doublet_uint;
typedef std::tuple<uint, uint, uint> Triplet_uint;
typedef std::tuple<int, int, int>    Triplet_int;
}   // namespace mrf