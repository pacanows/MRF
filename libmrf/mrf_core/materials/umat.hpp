/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

//STL
#include <string>
#include <vector>

//MRF
#include <mrf_core/color/color.hpp>
#include <mrf_core/math/vec3.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/geometry/local_frame.hpp>
#include <mrf_core/materials/scatter_events.hpp>
#include <mrf_core/materials/materials.hpp>
#include <mrf_core/materials/ior.hpp>

// TODO:
// This shall be replaced by an abstract function.
// We need implementation for derived classes though for now using only the GPU
// backend (af)
#define MRF_UNIMPLEMENTED_UMAT                                                                                         \
  assert(0);                                                                                                           \
  std::cout << __FILE__ << " " << __LINE__ << std::endl;

namespace mrf
{
namespace materials
{
/**
 *
 * This is the unified material Interface.
 *
 * Specialized classes like :
 *
 * BRDF
 * BTDF
 * Spatially Varying BRDF
 * Emittance functions
 *
 * have to implement this interface.
 *
 * Convention : Emissivity is NOT double sided while BxDF are.
 *
 **/
class MRF_CORE_EXPORT UMat
{
protected:
  std::string _name;
  std::string _type;

public:
  UMat(std::string const &name);
  UMat(std::string const &name, std::string const &type);
  virtual ~UMat();

  std::string getType() { return _type; }

  virtual std::string const &name() const;

  /**
   * @brief      Compute the Hemispherical Reflectivity for a given Number of view directions
   *
   * @param[in]  nb_samples_along_view_diretions  the number of view diretions taken along the zenital angle
   * @param[in]  nb_samples_for_integral          The number of samples for the integral computation
   *
   * @return     the integral values
   */
  std::vector<RADIANCE_TYPE> hemiReflectivity(
      unsigned int        nb_samples_along_view_diretions,
      std::vector<float> &view_angles,
      unsigned int        nb_samples_for_integral = 1000) const;


  //-------------------------------------------------------------------------
  // Light and Emissivity
  //-------------------------------------------------------------------------

  /**
   * Returns whether or not this material emits light
   **/
  virtual bool isEmissive() const = 0;

  /**
   * Spattialy Varying Material
   *
   * If not overloaded the default implementation calls
   * radianceEmitted( mrf::math::Vec3f cont & emit_out_dir )
   *
   * Note that emit_in_dir points toward the material surface
   * It is an incomming lighting direction
   **/
  virtual RADIANCE_TYPE radianceEmitted(mrf::math::Vec3f const &emit_in_dir, mrf::geom::LocalFrame const &lf) const = 0;

  /**
   * The total radiosity emitted by this Light Material
   * This is the sum of the radiance over the hemisphere
   **/
  virtual RADIOSITY_TYPE radiosityEmitted() const = 0;


  /**
   * For Spatially varying material
   *
   * If not overloaded the default implementation calls
   * radiosityEmitted()
   **/
  virtual RADIOSITY_TYPE radiosityEmitted(mrf::geom::LocalFrame const &) const;


  /**
   * Generate a lighting emissive direction used to generate an
   * initial particle
   *
   * Returns the emitted radiosity of the particle according to the following
   * equation
   *
   * L(x, w) * cos(N,w) / p(w)
   *
   * where
   *
   * L(x,w) is the emitted radiance from point x in direction w
   * cos(N, x) is the cosine between N (the normal at x ) and w
   * p(w) is the probability to choose the w direction
   *
   *
   * Values returned have only a sense when the material is really
   * an emissive material.
   **/
  virtual RADIOSITY_TYPE
  particleEmission(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2, mrf::math::Vec3f &direction)
      const = 0;


  //-------------------------------------------------------------------------
  // BRDF values
  //-------------------------------------------------------------------------

  /**
   * Returns the value of the BRDF for given in and out directions
   * and according to the local frame.
   * This function may be used for spatially as well as
   * anisotropic BRDFs
   *
   *
   * in_dir is the Ray direction pointing TOWARD the light surface
   * out_dir is the Light direction pointing AWAY from the light surface
   *
   * in_dir and out_dir are both defined in WORLD SPACE
   *
   **/
  virtual float
  brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const = 0;


  /**
   * Returns the value of the BRDF for given in and out directions
   * and according to the local frame.
   * This function may be used for spatially as well as
   * anisotropic BRDFs.
   *
   * Note: A BRDF is unitless and does not return any RADIANCE.
   * This function is here for convenience since the purpose in
   * a Ray/Path Tracer is to multiply the incomming radiance to
   * the BRDF to get the reflected radiance.
   *
   * in_dir is the Ray direction pointing TOWARD the light surface
   * out_dir is the Light direction pointing AWAY from the light surface
   *
   * in_dir and out_dir are both defined in WORLD SPACE
   *
   **/
  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const & /*lf*/,
      mrf::math::Vec3f const & /*in_dir*/,
      mrf::math::Vec3f const & /*out_dir*/) const
  {
    MRF_UNIMPLEMENTED_UMAT;
    return RADIANCE_TYPE();
  }


  //-------------------------------------------------------------------------
  // Albedos
  //-------------------------------------------------------------------------

  /**
   * Returns the diffuse albedo of the material
   * Albedo is always defined between [0,1]
   **/
  virtual float diffuseAlbedo() const = 0;

  /**
   * Returns the diffuse albedo of the material
   * for spatially varying material .
   **/
  virtual float diffuseAlbedo(mrf::geom::LocalFrame const &) const = 0;


  /**
   * Returns whether or not this material has a diffuse component
   **/
  virtual bool diffuseComponent() const = 0;

  //-------------------------------------------------------------------------
  virtual float specularAlbedo() const                              = 0;
  virtual float specularAlbedo(mrf::geom::LocalFrame const &) const = 0;
  virtual bool  specularComponent() const                           = 0;

  //-------------------------------------------------------------------------
  virtual float transmissionAlbedo() const                              = 0;
  virtual float transmissionAlbedo(mrf::geom::LocalFrame const &) const = 0;
  virtual bool  transmissionComponent() const                           = 0;


  //-------------------------------------------------------------------------
  virtual float nonDiffuseAlbedo() const    = 0;
  virtual bool  nonDiffuseComponent() const = 0;


  /**
   * Returns the sum of all albedos
   * This function is virtual to allow specific material implementation
   * to override the default implementation.
   */
  virtual float totalAlbedo() const;

  //-------------------------------------------------------------------------
  // Reflection
  //-------------------------------------------------------------------------
  virtual float
  diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const = 0;

  virtual float
  specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const = 0;

  virtual float
  transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const = 0;

  virtual float nonDiffusePDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const = 0;


  /**
   * Returns whether or not a reflection is possible according to
   * the given incident direction (incident_dir) and the local Normal
   *
   * Note that incident_dir points toward the position of the local frame
   * AND NOT toward the light source or the viewer !!!!
   *
   * dot_Normal_wi is accuratly computed when the reflection may occured
   *
   * Also note that the previous dot product is defined as:
   * Normal dot (- incident_dir)
   * i.e
   * wi = - incident_dir
   **/
  virtual bool reflectionIsPossible(
      mrf::math::Vec3f const &normal,
      mrf::math::Vec3f const &incident_dir,
      float &                 dot_Normal_wi) const;


  /**
   * Generate a Scattering Event.
   * If the event is different from ABSORPTION
   *
   * Incident radiance is modified according to the material
   * and becomes the reflected radiance
   *
   * and
   *
   * outgoing_direction is generate according to the Scattering Event.
   *
   * in_dir is pointing toward the surface
   *
   *
   **/
  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const = 0;


  ScatterEvent::SCATTER_EVT scatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f &           outgoing_dir,
      float &                      proba_outging_dir) const;


  virtual ScatterEvent::SCATTER_EVT scatteringDirection(
      mrf::geom::LocalFrame const & /*lf*/,
      mrf::math::Vec3f const & /*in_dir*/,
      mrf::math::Vec3f & /*outgoing_dir*/,
      float & /*proba_outging_dir*/,
      float /*e1*/,
      float /*e2*/) const
  {
    MRF_UNIMPLEMENTED_UMAT;
    return ScatterEvent::ABSORPTION;
  }


  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;


  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const & /*lf*/,
      mrf::math::Vec3f const & /*in_dir*/,
      float /*e1*/,
      float /*e2*/,
      mrf::math::Vec3f & /*outgoing_dir*/,
      RADIANCE_TYPE & /*brdf_corrected_by_pdf*/) const
  {
    MRF_UNIMPLEMENTED_UMAT;
    return ScatterEvent::ABSORPTION;
  }

  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBSDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              bsdf_corrected_by_pdf,
      mrf::materials::IOR const &  in_ior,
      mrf::materials::IOR const &  out_ior) const;


  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBSDF(
      mrf::geom::LocalFrame const & /*lf*/,
      mrf::math::Vec3f const & /*in_dir*/,
      float /*e1*/,
      float /*e2*/,
      mrf::math::Vec3f & /*outgoing_dir*/,
      RADIANCE_TYPE & /*bsdf_corrected_by_pdf*/,
      mrf::materials::IOR const & /*in_ior*/,
      mrf::materials::IOR const & /*out_ior*/) const
  {
    MRF_UNIMPLEMENTED_UMAT;
    return ScatterEvent::ABSORPTION;
  }

  //-------------------------------------------------------------------------
  // Refraction
  //-------------------------------------------------------------------------
  bool transDirection(
      mrf::math::Vec3f const &normal,
      mrf::math::Vec3f const &in_dir,
      float                   incoming_index_of_refraction,
      float                   out_index_of_refraction,
      mrf::math::Vec3f &      trans_dir) const;

  /**
   * Compute the mirror direction
   * in_dir points toward the surface e.g. the direction of a ray !
   **/
  virtual bool transDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        incoming_index_of_refraction,
      float                        out_index_of_refraction,
      mrf::math::Vec3f &           trans_dir) const;
  /**
   * Compute the mirror direction
   * in_dir points toward the surface e.g. the direction of a ray !
   **/
  virtual bool transDirection(
      mrf::geom::LocalFrame const & /*lf*/,
      mrf::math::Vec3f const & /*in_dir*/,
      float /*incoming_index_of_refraction*/,
      mrf::math::Vec3f & /*trans_dir*/) const
  {
    MRF_UNIMPLEMENTED_UMAT;
    return false;
  }

  virtual float ior(mrf::geom::LocalFrame const & /*lf*/) const
  {
    MRF_UNIMPLEMENTED_UMAT;
    return -1.f;
  }

  virtual mrf::materials::IOR ior() const
  {
    MRF_UNIMPLEMENTED_UMAT;
    return mrf::materials::IOR();
  }

  /**
   * Compute the mirror direction
   * in_dir points toward the surface e.g. the direction of a ray !
   **/
  virtual mrf::math::Vec3f mirrorDirection(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir) const;

  virtual mrf::math::Vec3f mirrorDirection(mrf::math::Vec3f const &normal, mrf::math::Vec3f const &in_dir) const;


  //-------------------------------------------------------------------------
  // Static function to debug
  //-------------------------------------------------------------------------
  static mrf::math::Vec3f hemiNormalDirection(mrf::geom::LocalFrame const &lf);


  //-------------------------------------------------------------------------
  // Static functions to sample the Hemisphere according to various PDF
  //-------------------------------------------------------------------------

  /**
   * Uniform sampling of an  Hemisphere centered around
   * the given local Frame.
   *
   * returns a direction defined in the global Referential Frame
   * (the Frame of the Scene NOT THE LOCAL ONE )
   **/
  static mrf::math::Vec3f randomHemiDirection(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2);

  static void randomHemiDirection(
      mrf::geom::LocalFrame const &lf,
      float                        random_var_1,
      float                        random_var_2,
      mrf::math::Vec3f &           dir_generated);

  /**
   * Cosine sampling of an Hemisphere centered around
   * the given local Frame.
   *
   * returns a direction defined in the global Referential Frame
   * (the Frame of the Scene NOT THE LOCAL ONE )
   **/
  static mrf::math::Vec3f
  randomHemiCosDirection(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2);

  static void randomHemiCosDirection(
      mrf::geom::LocalFrame const &lf,
      float                        random_var_1,
      float                        random_var_2,
      mrf::math::Vec3f &           dir_generated);


  static mrf::math::Vec3f randomSphereDirection(float random_var_1, float random_var_2);

  static void randomSphereDirection(float random_var_1, float random_var_2, mrf::math::Vec3f &dir_generated);

  //---------------------------------------------------------------------------
  // generator a stochastique sample around the direction dir according to
  // cos(theta)^exponent
  //---------------------------------------------------------------------------

  static mrf::math::Vec3f randomHemiPowerCosDirection(
      mrf::math::Vec3f const &dir,
      unsigned int const      exponent,
      float const             random_var_1,
      float const             random_var_2);

  static void randomHemiPowerCosDirection(
      mrf::math::Vec3f const &dir,
      unsigned int const      exponent,
      float const             random_var_1,
      float const             random_var_2,
      mrf::math::Vec3f &      dir_generated);


  /**
   *
   * Compute the fresnel coefficient for dielectric material
   * This method used the so known Fresnel Equation coming from the
   * Maxwell equation
   * The fresnel coefficient represents the amount of energy reflected
   * by a dielectric material. The amount of transmitted light is
   * given by 1 - Fresnel_Coefficient
   *
   * Note that in_dir points toward the surface like a incoming ray
   *
   * Note also that this method returns a unique value valid
   * for unpolarized light
   **/
  static inline float fresnelForDielectric(
      mrf::math::Vec3f const &normal,
      mrf::math::Vec3f const &in_dir,
      float                   incoming_refraction_index,
      mrf::math::Vec3f const &transmission_dir,
      float                   out_refraction_index);

  static inline float
  fresnelForConductor(mrf::math::Vec3f const &normal, mrf::math::Vec3f const &in_dir, float eta, float k);
};



inline float UMat::fresnelForDielectric(
    mrf::math::Vec3f const &normal,
    mrf::math::Vec3f const &in_dir,
    float                   in_ir,
    mrf::math::Vec3f const &transmission_dir,
    float                   out_ir)
{
  float const cos_theta_i = mrf::math::Math::max(normal.dot(-in_dir), 0.0f);
  float const cos_theta_t = mrf::math::Math::max(transmission_dir.dot(-normal), 0.0f);


  float const parallel_coeff
      = (out_ir * cos_theta_i - in_ir * cos_theta_t) / (out_ir * cos_theta_i + in_ir * cos_theta_t);
  float const ortho_coeff = (in_ir * cos_theta_i - out_ir * cos_theta_t) / (in_ir * cos_theta_i + out_ir * cos_theta_t);


  //#ifdef DEBUG
  // std::cout << "  normal = " << normal ;
  // std::cout << "  in_dir = " << in_dir << std::endl ;

  // std::cout << "  cos_theta_i = " << cos_theta_i << "  cos_theta_t = " << cos_theta_t << std::endl ;
  // std::cout << "  parallel_coeff = " << parallel_coeff << "  ortho_coeff = " << ortho_coeff << std::endl ;
  //#endif



  return 0.5f * (parallel_coeff * parallel_coeff + ortho_coeff * ortho_coeff);
}


inline float UMat::fresnelForConductor(
    mrf::math::Vec3f const & /*normal*/,
    mrf::math::Vec3f const & /*in_dir*/,
    float /*eta*/,
    float /*k*/)
{
  //float const cos_theta_i  = mrf::math::Math::max( normal.dot( - in_dir ), 0.0f );

  assert(0);
  return 1.0f;
}
}   // namespace materials
}   // namespace mrf
