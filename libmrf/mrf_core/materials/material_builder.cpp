#include <mrf_core/materials/material_builder.hpp>

#include <string>

namespace mrf
{
namespace materials
{
using namespace std;

//map<string, ptrFct> MaterialBuilder::_builtInMaterials;

MaterialBuilder &MaterialBuilder::Instance()
{
  static MaterialBuilder the_material_builder;
  return the_material_builder;
}



bool MaterialBuilder::registerMaterial(
    string const &materialType,
    mrf::materials::UMat *(*ptrFct)(OBJMaterial const &gm))
{
  map<string, Functor *>::iterator current = _builtInMaterials.find(materialType);
  map<string, Functor *>::iterator end     = _builtInMaterials.end();

  if (current != end)
  {
    cerr << "  Material Type all ready defined " << endl;
    return false;
  }
  else
  {
    Functor *fct = new Functor(ptrFct);
    //Functor fct(ptrFct);
    _builtInMaterials[materialType] = fct;
    return true;
  }
}


void MaterialBuilder::init()
{
  std::cout << "DEPRECATED, USE BLENDER BRIDGE FOR FULL MATERIAL CONVERSION" << std::endl;
  //registerMaterial(BRDFTypes::LAMBERT, &MaterialBuilder::createLambertMaterial);
  //registerMaterial(BRDFTypes::PHONG, &MaterialBuilder::createLPhongMaterial);

  // registerMaterial(BRDFTypes::AS_NGAN, &MaterialBuilder::createASNganMaterial);
  // registerMaterial(BTDFTypes::FBTDF, &MaterialBuilder::createFBTDFMaterial);

  // USELESS. IT IS IMPOSSIBLE TO CREATE A MATUSIK MATERIAL FROM a GENERICMaterial.
  // because Matusik Materials are measured BRDF
  //registerMaterial(BRDFTypes::MATUSIK, & MaterialBuilder::createMatusikMaterial );
}



UMat *MaterialBuilder::createLambertMaterial(OBJMaterial const &)
{
#ifdef DEBUG
  std::cout << __FILE__ << " at " << __LINE__ << " CREATING A LAMBER MATERIAL from GenericMaterial " << std::endl;
#endif

  //Lambert *mat = new Lambert(gm.name(), gm.kd(), gm.kd().avgCmp());

  //return mat;


  std::cout << "DEPRECATED, USE BLENDER BRIDGE FOR FULL MATERIAL CONVERSION" << std::endl;
  return nullptr;
}

UMat *MaterialBuilder::createLPhongMaterial(OBJMaterial const &)
{
  //LPhysicalPhong *mat = new LPhysicalPhong(gm.name());
  //mat->setDiffuseColor(gm.kd());
  //mat->setDiffuseReflectivity(gm.kd().avgCmp());
  //mat->setExponent(static_cast<unsigned int>(gm.ns().avgCmp()));
  //mat->setSpecularColor(gm.ks());
  //mat->setSpecularReflectivity(gm.ks().avgCmp());

  //return mat;

  std::cout << "DEPRECATED, USE BLENDER BRIDGE FOR FULL MATERIAL CONVERSION" << std::endl;
  return nullptr;
}

// UMat *MaterialBuilder::createASNganMaterial(OBJMaterial const &gm)
// {
//   /*
//    //Fresnel set to 0
//    ASNgan* mat = new ASNgan( gm.name(), gm.kd(), gm.ks(), gm.ns().avgCmp(), 0.0f);

//    return mat;
//    */
//   return nullptr;
// }

// UMat*
// MaterialBuilder::createMatusikMaterial( OBJMaterial const & gm )
// {
//     Matusik* mat = new Matusik( gm.name(), "filename");
//     return mat;
// }


// UMat *MaterialBuilder::createFBTDFMaterial(OBJMaterial const &gm)
// {
//   /*
//    FBTDF* mat = new FBTDF(gm.name(), gm.kd(), gm.tf(), gm.ks(), gm.ni());
//    return mat;
//    */
//   return nullptr;
// }

// UMat *MaterialBuilder::createDTextureMaterial(OBJMaterial const &gm, mrf::materials::BRDF &brdf)
// {
//   /*
//   FloatImage* img;
//   ImageLoader & imgLoader = ImageLoader::instance();

//   imgLoader.loadImage(gm.mapKd().c_str(), img);

//   DTexture* mat = new DTexture(gm.name(), *img, brdf );
//   assert( mat != NULL);

//   return mat;
//   */
//   return nullptr;
// }

////////////////////////////////////////
//Hidden methods for singleton method //
////////////////////////////////////////
MaterialBuilder::MaterialBuilder()
{
  init();
}

MaterialBuilder::~MaterialBuilder()
{
  map<string, Functor *>::iterator it = _builtInMaterials.begin();

  while (it != _builtInMaterials.end())
  {
    delete (*it).second;
    it++;
  }
}

MaterialBuilder::MaterialBuilder(MaterialBuilder const & /*a_loader*/) {}


MaterialBuilder &MaterialBuilder::operator=(MaterialBuilder const & /*a_loader*/)
{
  return *this;
}

}   // namespace materials
}   // namespace mrf
