#include <mrf_core/materials/emittance.hpp>
#include <mrf_core/math/math.hpp>

using namespace mrf::color;
using namespace mrf::math;


namespace mrf
{
namespace materials
{
//-------------------------------------------------------------------------
// Emittance Mother Class
//-------------------------------------------------------------------------
Emittance::Emittance(std::string const &name, std::string const &type): UMat(name, type) {}

Emittance::~Emittance() {}

mrf::materials::IOR Emittance::ior() const
{
  assert(0);
  return AIR_IOR;
}


bool Emittance::isEmissive() const
{
  return true;
}


RADIOSITY_TYPE
Emittance::radiosityEmitted(mrf::geom::LocalFrame const &lf) const
{
  return UMat::radiosityEmitted(lf);
}


//-------------------------------------------------------------------------
// Reflection methods return false or 0.0
//-------------------------------------------------------------------------
float Emittance::brdfValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}


RADIANCE_TYPE
Emittance::coloredBRDFValue(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return RADIANCE_TYPE();
}




float Emittance::diffuseAlbedo() const
{
  return 0.0f;
}

float Emittance::diffuseAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0f;
}

bool Emittance::diffuseComponent() const
{
  return false;
}

float Emittance::specularAlbedo() const
{
  return 0.0f;
}

float Emittance::specularAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0f;
}

bool Emittance::specularComponent() const
{
  return false;
}

float Emittance::transmissionAlbedo() const
{
  return 0.0f;
}

float Emittance::transmissionAlbedo(mrf::geom::LocalFrame const &) const
{
  return 0.0f;
}

bool Emittance::transmissionComponent() const
{
  return false;
}

float Emittance::nonDiffuseAlbedo() const
{
  return 0.0f;
}

bool Emittance::nonDiffuseComponent() const
{
  return false;
}

float Emittance::totalAlbedo() const
{
  return 0.0f;
}

float Emittance::diffPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

float Emittance::specPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

float Emittance::transPDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}

float Emittance::nonDiffusePDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f const & /*out_dir*/) const
{
  return 0.0f;
}


bool Emittance::reflectionIsPossible(
    mrf::math::Vec3f const & /*normal*/,
    mrf::math::Vec3f const & /*incident_dir*/,
    float & /*dot_Normal_incident_dir*/) const
{
  return false;
}

ScatterEvent::SCATTER_EVT Emittance::generateScatteringDirection(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    RADIANCE_TYPE & /*incident_radiance*/,
    mrf::math::Vec3f & /*outgoing_dir*/) const

{
  return ScatterEvent::ABSORPTION;
}

ScatterEvent::SCATTER_EVT Emittance::scatteringDirection(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    mrf::math::Vec3f & /*outgoing_dir*/,
    float & /*proba_outging_dir*/,
    float /*e1*/,
    float /*e2*/) const
{
  return ScatterEvent::ABSORPTION;
}


ScatterEvent::SCATTER_EVT Emittance::scatteringDirectionWithBRDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    float /*e1*/,
    float /*e2*/,
    mrf::math::Vec3f & /*outgoing_dir*/,
    RADIANCE_TYPE & /*brdf_corrected_by_pdf*/) const
{
  return ScatterEvent::ABSORPTION;
}

ScatterEvent::SCATTER_EVT Emittance::scatteringDirectionWithBSDF(
    mrf::geom::LocalFrame const & /*lf*/,
    mrf::math::Vec3f const & /*in_dir*/,
    float /*e1*/,
    float /*e2*/,
    mrf::math::Vec3f & /*outgoing_dir*/,
    RADIANCE_TYPE & /*bsdf_corrected_by_pdf*/,
    mrf::materials::IOR const & /*in_ior*/,
    mrf::materials::IOR const & /*out_ior*/) const
{
  return ScatterEvent::ABSORPTION;
}
}   // namespace materials
}   // namespace mrf
