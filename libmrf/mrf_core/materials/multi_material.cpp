#include "multi_material.hpp"

#include <mrf_core/image/image_formats.hpp>

namespace mrf
{
namespace materials
{
MultiMaterial::MultiMaterial(std::string const &name): BRDF(name), _normalized(false) {}
MultiMaterial::~MultiMaterial() {}

mrf::image::IMAGE_LOAD_SAVE_FLAGS MultiMaterial::loadIndexTexture(std::string const &path)
{
  try
  {
    _index_texture_path = std::string(path);
    _index_map          = mrf::image::loadColorImage(path, true);
  }
  catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS &error)
  {
    return error;
  }

  return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}


void MultiMaterial::normalizeMapIndices(std::string exportPath)
{
  if (!_normalized)
  {
    std::map<unsigned int, int> matToIndex;
    for (int i = 0; i < _local_indices.size(); ++i)
    {
      unsigned int mat = _local_indices[i];
      matToIndex[mat]  = i;
      std::cout << i << " | " << matToIndex[mat] << std::endl;
    }

    for (int i = 0; i < _index_map.get()->width() * _index_map.get()->height(); ++i)
    {
      mrf::color::Color pixel = _index_map.get()->getPixel(i);

      int mat;
      if (matToIndex.count((unsigned int)pixel.x() * 255) == 1)
      {
        mat = matToIndex.at((unsigned int)pixel.x() * 255);
      }
      else
      {
        //material is not among the provided UTIA files, disable it.
        mat = matToIndex.at(0);
      }

      pixel.r() = mat / 255.f;

      _index_map.get()->setPixel(i, pixel);
    }
    _normalized = true;
  }
}


}   // namespace materials
}   // namespace mrf