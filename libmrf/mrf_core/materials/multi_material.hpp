
#pragma once

#include <memory>
#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/brdf.hpp>

#include <mrf_core/image/color_image.hpp>

namespace mrf
{
namespace materials
{
//-------------------------------------------------------------------//
//---------------        Physical Phong Model by Lafortune CLASS   --//
//-------------------------------------------------------------------//
class MRF_CORE_EXPORT MultiMaterial: public BRDF
{
protected:
  std::vector<unsigned int> _mrf_indices;
  std::vector<unsigned int> _local_indices;
  std::vector<std::string>  _names;

  std::string _index_texture_path;

  std::unique_ptr<mrf::image::ColorImage> _index_map;
  bool                                    _normalized;

public:
  MultiMaterial(std::string const &name);
  ~MultiMaterial();

  //void addEntry(unsigned int idx, std::string name) { _material_map[idx] = name; }
  void addEntry(unsigned int mrf_idx, std::string name, unsigned int local_idx)
  {
    _mrf_indices.push_back(mrf_idx);
    _names.push_back(name);
    _local_indices.push_back(local_idx);
  }
  void addMRFIdEntry(unsigned int idx) { _mrf_indices.push_back(idx); }
  void addLocalIdEntry(unsigned int idx) { _local_indices.push_back(idx); }
  void addNameEntry(std::string name) { _names.push_back(name); }

  mrf::image::IMAGE_LOAD_SAVE_FLAGS loadIndexTexture(std::string const &path);

  std::vector<unsigned int> &getAllMRFId() { return _mrf_indices; }
  std::vector<unsigned int> &getAllLocalId() { return _local_indices; }
  std::vector<std::string> & getAllNames() { return _names; }

  std::string                   getTexturePath() const { return _index_texture_path; }
  mrf::image::ColorImage const &getIndexMap() const { return *_index_map.get(); }

  bool hasTexture() const { return _index_map.get() != nullptr; }

  void normalizeMapIndices(std::string exportPath);

  size_t size() { return _names.size(); }
};
}   // namespace materials
}   // namespace mrf
