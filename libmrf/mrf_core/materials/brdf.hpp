/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/color/color.hpp>
#include <mrf_core/radiometry/spectrum.hpp>
#include <mrf_core/materials/umat.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/sampling/random_generator.hpp>


namespace mrf
{
namespace materials
{
class MRF_CORE_EXPORT BRDF: public UMat
{
protected:
  //std::string _name;

public:
  BRDF(std::string const &name);
  BRDF(std::string const &name, std::string const &type);
  virtual ~BRDF();



  //-------------------------------------------------------------------------
  // Partial Implementation of UMat's methods
  //-------------------------------------------------------------------------

  //The default behaviour is to return false and to assert(0)
  virtual bool  diffuseComponent() const;
  virtual float diffuseAlbedo() const;

  virtual float specularAlbedo() const;
  virtual bool  specularComponent() const;

  virtual float nonDiffuseAlbedo() const;
  virtual bool  nonDiffuseComponent() const;


  virtual float
  diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual float
  specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual float
  nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;




  //There is no spatial dependencies for BRDF
  virtual float diffuseAlbedo(mrf::geom::LocalFrame const &) const;
  virtual float specularAlbedo(mrf::geom::LocalFrame const &) const;

  virtual bool           isEmissive() const;
  virtual RADIANCE_TYPE  radianceEmitted(mrf::math::Vec3f const &emit_in_dir, mrf::geom::LocalFrame const &lf) const;
  virtual RADIOSITY_TYPE radiosityEmitted() const;
  virtual RADIOSITY_TYPE radiosityEmitted(mrf::geom::LocalFrame const &) const;

  virtual RADIOSITY_TYPE
  particleEmission(mrf::geom::LocalFrame const &lf, float random_var_1, float random_var_2, mrf::math::Vec3f &direction)
      const;


  /**
   * The Common stragegy is to compute the colored BRDF Value and
   * to return the luminance value
   **/
  virtual float
  brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;


  /**
   * By default. No transmission.
   **/
  virtual float transmissionAlbedo() const;
  virtual float transmissionAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  transmissionComponent() const;
  virtual float
  transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;


  /**
   * By default AIR IOR. and assertion(0)
   **/
  virtual mrf::materials::IOR ior() const;

  /**
   * Returns the Fresnel Approximation of CS.
   **/
  static inline float fresnelApprox(float dot_product, float fresnel_at_incidence);

  template<typename T>
  static inline RADIANCE_TYPE fresnelApprox(T dot_product, RADIANCE_TYPE const &fresnel_at_incidence);

  /**
   * Half vector from in_dir and out_dir
   * Note that for this function in_dir AND out_dir point away from the surface
   **/
  template<typename T>
  static mrf::math::Vec3<T>
  halfVector(mrf::math::Vec3<T> const &in, mrf::math::Vec3<T> const &out, mrf::math::Vec3<T> const &normal);

  template<typename T>
  static inline mrf::math::Vec3<T>
  halfSampling(mrf::geom::_LocalFrame<T> const &lf, mrf::math::Vec3<T> const &in_dir, T exponent, T &proba_out_dir);

  template<typename T>
  static inline mrf::math::Vec3<T> halfSampling(
      mrf::geom::_LocalFrame<T> const &lf,
      mrf::math::Vec3<T> const &       in_dir,
      T                                exponent,
      T                                random_var_1,
      T                                random_var_2,
      T &                              proba_out_dir);


  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;



  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;

  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBSDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              bsdf_corrected_by_pdf,
      mrf::materials::IOR const &  in_ior,
      mrf::materials::IOR const &  out_ior) const;
};


inline float BRDF::fresnelApprox(float dot_product, float fresnel_at_incidence)
{
  float const one_minus_dot       = (1.0f - dot_product);
  float const one_minus_dot_pow_5 = one_minus_dot * one_minus_dot * one_minus_dot * one_minus_dot * one_minus_dot;
  return fresnel_at_incidence + (1.0f - fresnel_at_incidence) * one_minus_dot_pow_5;
}

template<typename T>
inline RADIANCE_TYPE BRDF::fresnelApprox(T dot_product, RADIANCE_TYPE const &fresnel_at_incidence)
{
  RADIANCE_TYPE white(T(1.0));

  T const one_minus_dot = (1.0f - dot_product);
  T const factor        = one_minus_dot * one_minus_dot * one_minus_dot * one_minus_dot * one_minus_dot;

  RADIANCE_TYPE res = fresnel_at_incidence + (white - fresnel_at_incidence) * factor;

  return res;
}

template<typename T>
mrf::math::Vec3<T> BRDF::halfVector(
    mrf::math::Vec3<T> const &view_dir,
    mrf::math::Vec3<T> const &out_dir,
    mrf::math::Vec3<T> const &normal)
{
  mrf::math::Vec3<T> h = view_dir + out_dir;

  T const h_sqr_length = h.sqrLength();

  if (h_sqr_length < T(EPSILON))
  {
    h = normal;
  }
  else
  {
    h /= std::sqrt(h_sqr_length);
  }

  return h;
}



template<typename T>
inline mrf::math::Vec3<T>
BRDF::halfSampling(mrf::geom::_LocalFrame<T> const &lf, mrf::math::Vec3<T> const &in_dir, T exponent, T &proba_out_dir)
{
  using namespace mrf::sampling;

  float const e1 = RandomGenerator::Instance().getFloat();
  float const e2 = RandomGenerator::Instance().getFloat();

  return halfSampling(lf, in_dir, exponent, e1, e2, proba_out_dir);
}

template<typename T>
inline mrf::math::Vec3<T> BRDF::halfSampling(
    mrf::geom::_LocalFrame<T> const &lf,
    mrf::math::Vec3<T> const &       in_dir,
    T                                exponent,
    T                                e1,
    T                                e2,
    T &                              proba_out_dir)
{
  using namespace mrf::math;
  using namespace mrf::geom;
  using namespace mrf::sampling;

  T const ONE_BY_EXPONENT_PLUS_ONE = T(1.0) / (exponent + T(1.0));
  T const phi                      = Math::TWO_PI * e1;
  T const cos_theta                = std::pow((T(1.0) - e2), ONE_BY_EXPONENT_PLUS_ONE);
  T const sin_theta                = std::sqrt(T(1.0) - cos_theta * cos_theta);

  mrf::math::Vec3<T> const h
      = std::cos(phi) * sin_theta * lf.binormal() + std::sin(phi) * sin_theta * lf.tangent() + cos_theta * lf.normal();

  mrf::math::Vec3<T> outgoing_dir = in_dir + h * (-2.0f * h.dot(in_dir));
  outgoing_dir.clampToZero(0.0001);
  outgoing_dir[0] = clamp(outgoing_dir[0], T(-1.0), T(1.0));
  outgoing_dir[1] = clamp(outgoing_dir[1], T(-1.0), T(1.0));
  outgoing_dir[2] = clamp(outgoing_dir[2], T(-1.0), T(1.0));


  T const dotNH = Math::max(lf.normal().dot(h), T(0.0));
  T const dotHV = Math::max(h.dot(-in_dir), T(0.0));
  //T const dotHL = h.dot( outgoing_dir );

  T const p_h = (exponent + T(1.0)) * Math::INV_2PI * std::pow(dotNH, exponent);

#ifdef MRF_BRDF_DEBUG
  if (p_h <= 0.0)
  {
    std::cout << lf << " h = " << h << " dotNH = " << dotNH << std::endl
              << "  outgoind dir =  " << outgoing_dir
              << " lf.normal.dot( outgoing_dir)  = " << lf.normal().dot(outgoing_dir) << std::endl
              << "exponent = " << exponent << " pow(dotNH,exponent) = " << std::pow(dotNH, exponent) << std::endl;
  }

#endif
  assert(p_h > 0.0);

  proba_out_dir = p_h / (T(4.0) * dotHV);
  return outgoing_dir;
}




}   // namespace materials
}   // namespace mrf
