/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/


#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/math/math.hpp>
#include <mrf_core/color/color.hpp>
#include <mrf_core/materials/umat.hpp>
#include <mrf_core/radiometry/spectrum.hpp>


namespace mrf
{
namespace materials
{
/**
 * Mother class of materials which ONLY EMIT light
 *
 * If you want to create an emitter which also reflects light
 * create your own class which should implement UMat interface
 *
 **/
class MRF_CORE_EXPORT Emittance: public UMat
{
public:
  Emittance(std::string const &name, std::string const &type);
  virtual ~Emittance();

  virtual bool isEmissive() const;

  virtual RADIOSITY_TYPE radiosityEmitted() const = 0;
  virtual RADIOSITY_TYPE radiosityEmitted(mrf::geom::LocalFrame const &) const;
  virtual void           computeInternalValues() = 0;

  // NO IOR FOR LIGHT SOURCES
  virtual mrf::materials::IOR ior() const;


  //-------------------------------------------------------------------------
  // Partial Implementation of UMat's methods
  //-------------------------------------------------------------------------

  //-------------------------------------------------------------------------
  // Reflection methods return false or 0.0
  //-------------------------------------------------------------------------
  virtual float
  brdfValue(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  virtual RADIANCE_TYPE coloredBRDFValue(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f const &     out_dir) const;

  virtual float diffuseAlbedo() const;
  virtual float diffuseAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  diffuseComponent() const;

  virtual float specularAlbedo() const;
  virtual float specularAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  specularComponent() const;

  virtual float transmissionAlbedo() const;
  virtual float transmissionAlbedo(mrf::geom::LocalFrame const &) const;
  virtual bool  transmissionComponent() const;

  virtual float nonDiffuseAlbedo() const;
  virtual bool  nonDiffuseComponent() const;

  virtual float totalAlbedo() const;

  virtual float
  diffPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  specPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  transPDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;
  virtual float
  nonDiffusePDF(mrf::geom::LocalFrame const &lf, mrf::math::Vec3f const &in_dir, mrf::math::Vec3f const &out_dir) const;

  /**
   * Emitters does not reflect any light
   * This methods always return false
   **/
  virtual bool reflectionIsPossible(
      mrf::math::Vec3f const &normal,
      mrf::math::Vec3f const &incident_dir,
      float &                 dot_Normal_Light) const;

  /**
   * Emitters absorb light.
   *
   **/
  virtual ScatterEvent::SCATTER_EVT generateScatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      RADIANCE_TYPE &              incident_radiance,
      mrf::math::Vec3f &           outgoing_dir) const;



  ScatterEvent::SCATTER_EVT scatteringDirection(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      mrf::math::Vec3f &           outgoing_dir,
      float &                      proba_outging_dir,
      float                        e1,
      float                        e2) const;

  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBRDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              brdf_corrected_by_pdf) const;


  virtual ScatterEvent::SCATTER_EVT scatteringDirectionWithBSDF(
      mrf::geom::LocalFrame const &lf,
      mrf::math::Vec3f const &     in_dir,
      float                        e1,
      float                        e2,
      mrf::math::Vec3f &           outgoing_dir,
      RADIANCE_TYPE &              bsdf_corrected_by_pdf,
      mrf::materials::IOR const &  in_ior,
      mrf::materials::IOR const &  out_ior) const;

  inline COLOR            getColor() const;
  inline void             setColor(COLOR const &color);
  inline float            getRadiance() const;
  inline void             setRadiance(float);
  inline mrf::math::Vec3f getDirection() const;
  inline void             setDirection(mrf::math::Vec3f);

protected:
  COLOR            _color;
  float            _radiance;    //IN Watt per square meter per steradian
  mrf::math::Vec3f _direction;   //IN Watt per square meter per steradian
  RADIANCE_TYPE    _cRadiance;
  RADIOSITY_TYPE   _cRadiosity;
};

inline COLOR Emittance::getColor() const
{
  return _color;
}

inline void Emittance::setColor(COLOR const &color)
{
  _color = color;
  //_color.normalize();
  computeInternalValues();
}

inline float Emittance::getRadiance() const
{
  return _radiance;
}

inline void Emittance::setRadiance(float radiance)
{
  _radiance = radiance;
  computeInternalValues();
}


inline mrf::math::Vec3f Emittance::getDirection() const
{
  return _direction;
}

inline void Emittance::setDirection(mrf::math::Vec3f dir)
{
  _direction = dir;
  computeInternalValues();
}




}   // namespace materials
}   // namespace mrf
