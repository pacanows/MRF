/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/fresnel_functions.hpp>

namespace mrf
{
namespace materials
{
class MRF_CORE_EXPORT FresnelConductor
{
private:
  RADIANCE_TYPE _square_eta;
  RADIANCE_TYPE _square_k;

public:
  FresnelConductor() {}

  FresnelConductor(RADIANCE_TYPE const &eta, RADIANCE_TYPE const &k): _square_eta(eta * eta), _square_k(k * k) {}

  RADIANCE_TYPE eta() const { return _square_eta.sqrt(); }

  RADIANCE_TYPE kappa() const { return _square_k.sqrt(); }

  RADIANCE_TYPE sqrEta() const { return _square_eta; }

  RADIANCE_TYPE sqrKappa() const { return _square_k; }

  void setSqrEta(RADIANCE_TYPE const &sqr_eta) { _square_eta = sqr_eta; }

  void setSqrKappa(RADIANCE_TYPE const &sqr_kappa) { _square_k = sqr_kappa; }

  void setEta(RADIANCE_TYPE const &eta) { _square_eta = eta * eta; }

  void setK(RADIANCE_TYPE const &k) { _square_k = k * k; }

  RADIANCE_TYPE evalFromCosThetaDiff(double cos_theta_d) const;

  FresnelConductor fresnelFromColor(COLOR const &f0, COLOR const &g);
};

}   // namespace materials
}   // namespace mrf