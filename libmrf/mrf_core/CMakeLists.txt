
find_package(OpenMP)
find_package(Eigen3)

# ----------------------------------------------------------------------------
# Options
# ----------------------------------------------------------------------------

option(RENDERER_INTERACTIVE "Use interactive mode" ON)
# set(RENDERING_MODE "SPECTRAL" CACHE STRING "MRF Rendering mode: Spectral or RGB")
# set_property(CACHE RENDERING_MODE PROPERTY STRINGS "SPECTRAL" "RGB")

# ----------------------------------------------------------------------------
# Add all modules
# ----------------------------------------------------------------------------

add_subdirectory(color)
add_subdirectory(data_struct)
add_subdirectory(feedback)
add_subdirectory(geometry)
add_subdirectory(image)
add_subdirectory(io)
add_subdirectory(lighting)
add_subdirectory(materials)
add_subdirectory(math)
add_subdirectory(radiometry)
add_subdirectory(rendering)
add_subdirectory(sampling)
add_subdirectory(util)

foreach(RENDERING_MODE "spectral" "rgb")
  # create configuration
  if(RENDERING_MODE MATCHES "spectral")
    # message("-- Spectral Mode")
    set(MRF_RENDERING_MODE_SPECTRAL ON)
    set(MRF_RENDERING_MODE_RGB      OFF)
  else()
    # message("-- RGB Mode")
    set(MRF_RENDERING_MODE_SPECTRAL OFF)
    set(MRF_RENDERING_MODE_RGB      ON)
  endif()
  
  if(EIGEN3_FOUND)
    set(MRF_WITH_EIGEN_SUPPORT ON)
  else()
    set(MRF_WITH_EIGEN_SUPPORT OFF)
  endif()

  # We want to build with and without Eigen when testing MRF lib
  set(mrf_target "mrf_core_${RENDERING_MODE}")
  set(mrf_current_builds "${RENDERING_MODE}")

  if(ENABLE_TEST AND EIGEN3_FOUND)
    set(mrf_current_builds ${mrf_current_builds} "${RENDERING_MODE}_no_eigen")
  endif()

  foreach(build ${mrf_current_builds})
    set(current_target "mrf_core_${build}")
    
    add_library(${current_target} ${SOURCES} ${HEADERS} )
    
    set_target_properties( ${current_target} 
      PROPERTIES     
      ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/$<CONFIG>"
      LIBRARY_OUTPUT_DIRECTORY "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/$<CONFIG>"
      RUNTIME_OUTPUT_DIRECTORY "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/$<CONFIG>")

    target_compile_definitions(${current_target} PRIVATE -DMRF_CORE_BUILD)
    
    if(NOT ENABLE_TEST)
      set_target_properties(${current_target} PROPERTIES OUTPUT_NAME "${current_target}$<$<CONFIG:Debug>:_d>" )
    endif()


    if(build MATCHES "${RENDERING_MODE}_no_eigen")
      set(MRF_WITH_EIGEN_SUPPORT OFF)
    endif()
    
    # if(${CMAKE_BUILD_SHARED_LIBS})
      # generate_export_header(${build}
      # BASE_NAME ${build}
      # EXPORT_MACRO_NAME MRF_CORE_EXPORT
      # EXPORT_FILE_NAME ${PROJECT_SOURCE_DIR}/libmrf/mrf_core/mrf_dll.h
      # # STATIC_DEFINE SHARED_EXPORTS_BUILT_AS_STATIC
      # )
    # endif()

    target_include_directories(${current_target} SYSTEM PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
    target_include_directories(${current_target} SYSTEM PUBLIC ${CMAKE_CURRENT_BINARY_DIR}/mrf_${RENDERING_MODE})
    # set_target_properties(${build} PROPERTIES PUBLIC_HEADER "${HEADERS}")

    # if(MSVC)
      # target_compile_options(${build} PUBLIC /W4)
    # else()
      # target_compile_options(${build} PUBLIC -Wall -Wextra -Wpedantic)
    # endif()

    # Link to external dependencies
    # find_library(mrf_externals_loc mrf_externals)
    # message("externals at: ${mrf_externals_loc}")
    target_link_libraries(${current_target} PUBLIC mrf_externals)

    if((OpenMP_CXX_FOUND) OR (OpenMP_FOUND))
      target_link_libraries(${current_target} PUBLIC OpenMP::OpenMP_CXX)
      target_link_libraries(${current_target} PUBLIC OpenMP::OpenMP_C)
    endif()
  endforeach(build)

  if(EIGEN3_FOUND)
    # Ensure compatibility with some older versions of Ubuntu
    target_compile_definitions(${mrf_target} PUBLIC ${EIGEN3_DEFINITIONS})
    target_link_libraries(${mrf_target} PUBLIC Eigen3::Eigen)
    target_include_directories(${mrf_target} SYSTEM PUBLIC ${EIGEN3_INCLUDE_DIR})
  endif()
    
  configure_file(mrf_config.h.in ${CMAKE_CURRENT_BINARY_DIR}/mrf_${RENDERING_MODE}/mrf_core/mrf_config.h)

  # We install the main target. The potential additional ones are just
  # there when testing
  install(TARGETS ${mrf_target}
    EXPORT ${mrf_target}
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    # This does not keep the hierarchy... fix below
    # PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/mrf_${RENDERING_MODE}
    COMPONENT MRF
  )
  
  # This keeps the include hierarchy
  install(
    DIRECTORY   ${CMAKE_CURRENT_SOURCE_DIR} 
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/mrf_${RENDERING_MODE}
    COMPONENT MRF
    FILES_MATCHING PATTERN "*.hpp" PATTERN "*.h" PATTERN "*.cxx" PATTERN "*.inl" 
  ) 
  
  install(
    FILES       ${CMAKE_CURRENT_BINARY_DIR}/mrf_${RENDERING_MODE}/mrf_core/mrf_config.h
    DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/mrf_${RENDERING_MODE}/mrf_core
    COMPONENT MRF
  )
  
  if(CMAKE_VERSION VERSION_GREATER "3.7")
    source_group(
      TREE ${CMAKE_CURRENT_SOURCE_DIR}
      PREFIX "src"
      FILES ${SOURCES})
    source_group(
      TREE ${CMAKE_CURRENT_SOURCE_DIR}
      PREFIX "headers"
      FILES ${HEADERS})
  endif()

  # Keep track of all builds done
  # set(mrf_build_mode ${mrf_build_mode} ${RENDERING_MODE})
  set(mrf_build_mode ${mrf_build_mode} ${mrf_current_builds})
  set(mrf_libs ${mrf_libs} ${mrf_target})

endforeach(RENDERING_MODE)

# Make the list of target visible to unit_tests
set(mrf_build_mode ${mrf_build_mode} PARENT_SCOPE)
set(mrf_libs ${mrf_libs} PARENT_SCOPE)
