/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/umat.hpp>
#include <mrf_core/geometry/local_frame.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/sampling/random_generator.hpp>
#include <mrf_core/sampling/jittering.hpp>
#include <mrf_core/color/color.hpp>
#include <mrf_core/rendering/background_manager.hpp>
#include <mrf_core/materials/materials.hpp>
#include <mrf_core/math/piecewise_cdf_1d.hpp>

#ifdef MRF_RENDERING_MODE_SPECTRAL
#  include <mrf_core/image/uniform_spectral_image.hpp>
#else
#  include <mrf_core/image/color_image.hpp>
#endif

//STL
#include <vector>
#include <memory>

// TODO:
// This shall be replaced by an abstract function.
// We need implementation for derived classes though for now using only the GPU
// backend (af)
#define MRF_UNIMPLEMENTED_ENVMAP                                                                                       \
  assert(0);                                                                                                           \
  std::cout << __FILE__ << " " << __LINE__ << std::endl;

namespace mrf
{
namespace lighting
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
typedef mrf::image::UniformSpectralImage EnvmapImage;
#else
typedef mrf::image::ColorImage EnvmapImage;
#endif

class MRF_CORE_EXPORT EnvMap: public mrf::rendering::BckgManager
{
protected:
  //
  // This quaternion represents the composition of two rotations
  // used to rotate the environment map.
  // theta = rotation around X axis (1, 0, 0)
  // phi   = rotation around Y axis (0, 1, 0)
  mrf::math::Quatf _rot;

  //Store the rotation angle used by _rot
  //all angles are in radians
  float _rotation_theta;
  float _rotation_phi;
  float _rotation_gamma;

  // The inverse quaternion (representing the inverse rotation)
  // of the previous quaternion
  mrf::math::Quatf _invRot;

  /**
   * file path to the image
   * in case of a spectral image we store the path to the ENVI header (.hdr)
   **/
  std::string _file_path;

  bool _is_loaded;

  /**
   * Envmap image
   **/
  std::unique_ptr<EnvmapImage>                  _envmap;
  mrf::math::PiecewiseCDF1D<float>              _cdf_1d_envmap_rows;
  std::vector<mrf::math::PiecewiseCDF1D<float>> _cdf_1d_envmap_columns;
  /*
  std::vector<int> _cdf_inv_rows;//to choose a row
  std::vector<int> _cdf_inv_2d;//to choose a column knowing a row to sample
  std::vector<float> _pdf_rows;
  std::vector<float> _pdf_2d;
  */

  /**
   *linear power scaling, reduces or increases pixel values
   *apply at loading in scene parser
   *need to be store for IRAY BACKEND
   **/
  float _power_scaling_factor;

public:
  EnvMap(
      float phi_rotation_in_degree   = 0.0f,
      float theta_rotation_in_degree = 0.0f,
      float gamma_rotation_in_degree = 0.0f);
  virtual ~EnvMap();

  mrf::image::IMAGE_LOAD_SAVE_FLAGS load();

  inline EnvmapImage const &image() const;
  inline EnvmapImage &      image();

  /**
   * Width of the image
   **/
  virtual inline size_t width() const;

  /**
   * Height of the image
   **/
  virtual inline size_t height() const;

  /**
   * Size of the image (width*height)
   **/
  virtual inline size_t size() const;

  /**
   * Setter for power scaling
   **/
  virtual inline void setPowerScalingFactor(float f);

  /**
   * Getter for power scaling
   **/
  virtual inline float powerScalingFactor() const;

  /**
   * TODOC
   **/
  void
  updateRotationAngles(float phi_rotation_in_degree, float theta_rotation_in_degree, float gamma_rotation_in_degree);

  /**
   * filepath getter
   **/
  inline std::string filePath() const;

  /**
   * envmap total power
   * \todo support this in spectral
   **/
  virtual inline float totalPower() const;

  /**
   * Compute average luminance emitted over horizon
   * Might take some time has it needs to loop over 360*90 values
   * in the image.
   * \todo This is for now a fixed resolution, use two parameters to choose the resolution in
   * theta and phi.
   * This method is usefull for shadow catchers.
   **/
  virtual mrf::materials::RADIANCE_TYPE computeAverageLuminanceOverHorizon(uint /*nb_samples*/) const
  {
    MRF_UNIMPLEMENTED_ENVMAP;
    return mrf::materials::RADIANCE_TYPE();
  }

  /**
   * TODO
   **/
  virtual void computeCDF2D();

  /**
   * TODO
   **/
  //virtual void computeCDFInverse(uint sqrt_nb_samples);

  /**
   * TODO
   **/
  //virtual void computeCDF2DAndCDFInverse(uint sqrt_nb_samples);

  inline std::vector<float> const &getCdfRows();
  inline std::vector<float> const &getCdfColumns(mrf::uint row);
  inline std::vector<float> const &getPdfRows();
  inline std::vector<float> const &getPdfColumns(mrf::uint row);
  /**
   * envmap max power
   * \todo support this in spectral
   **/
  virtual inline float maxPower() const;

  /**
   * Rotation getter
   **/
  inline mrf::math::Quatf rotation() const;

  /**
   * Rotation getter
   **/
  inline mrf::math::Mat3f rotationMatrix() const;

  /**
   * Access rotation angle
   **/
  inline float rotationTheta() const;
  inline float rotationPhi() const;
  inline float rotationGamma() const;

  /**
   * Returns the uv coordinates for a given direction
   * (u,v) in [0,1]X[0,1]
   *
   *  Bilinear Interpolation must be performed
   **/
  virtual mrf::math::Vec2f uv(mrf::math::Vec3f const &dir) const = 0;

  /**
   * Returns the direction for a given uv coordinates
   * (u,v) in [0,1]X[0,1]
   *
   **/
  virtual mrf::math::Vec3f direction(mrf::math::Vec2f const &uvs) const = 0;


  /**
   * Bilinear interpolation should be perform
   *
   **/
  virtual mrf::materials::RADIANCE_TYPE
  radianceEmitted(mrf::math::Vec3f const &emit_out_dir, mrf::geom::LocalFrame const &lf) const = 0;


  /**
   * Bilinear interpolation should be perform
   *
   **/
  virtual mrf::materials::RADIANCE_TYPE radianceEmitted(mrf::math::Vec3f const &emit_out_dir) const = 0;

  /**
   * Return the RADIANCE value for a given pixel
   * i = column index [0-width]
   * j = row index [0-height]
   * WARNING THis is super slow in SPECTRAL MODE, need to reconstruct a spectrum
   **/
  inline mrf::materials::RADIANCE_TYPE radianceFromPixel(unsigned int i, unsigned int j) const;

  /**
   * Samples uniformly the environment map with the given number of samples
   * Returns a vector that contains a pair of directional light with their associated power
   **/
  template<class VECT_TYPE, class POWER_TYPE>
  void uniformSampling(std::vector<std::pair<VECT_TYPE, POWER_TYPE>> &dir_pow, unsigned int num_of_samples) const;


  // virtual  void uniformSampling( std::vector<std::pair<mrf::math::Vec3f, mrf::rendering::Color3F> > & dir_pow,
  //                                unsigned int num_of_samples) const = 0;


  inline mrf::materials::COLOR colorFromDirection(mrf::math::Vec3f const &dir) const;

private:
  inline void errorMessage() const;
};



inline EnvmapImage const &EnvMap::image() const
{
  return *_envmap.get();
}

inline EnvmapImage &EnvMap::image()
{
  return *_envmap.get();
}

inline size_t EnvMap::width() const
{
  return _envmap->width();
}

inline size_t EnvMap::height() const
{
  return _envmap->height();
}

inline size_t EnvMap::size() const
{
  return width() * height();
}

inline void EnvMap::setPowerScalingFactor(float f)
{
  _power_scaling_factor = f;
}

inline float EnvMap::powerScalingFactor() const
{
  return _power_scaling_factor;
}

inline float EnvMap::totalPower() const
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  /**
   * \todo
   **/
  return 0;
#else
  return _envmap->totalPower();
#endif
}

inline std::vector<float> const &EnvMap::getCdfRows()
{
  return _cdf_1d_envmap_rows.cdf();
}
inline std::vector<float> const &EnvMap::getCdfColumns(mrf::uint row)
{
  return _cdf_1d_envmap_columns[row].cdf();
}

inline std::vector<float> const &EnvMap::getPdfRows()
{
  return _cdf_1d_envmap_rows.pdf();
}

inline std::vector<float> const &EnvMap::getPdfColumns(mrf::uint row)
{
  return _cdf_1d_envmap_columns[row].pdf();
}

inline float EnvMap::maxPower() const
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  /**
   * \todo
   **/
  return 0;
#else
  return _envmap->maxValue();
#endif
}

inline std::string EnvMap::filePath() const
{
  return _file_path;
}

inline mrf::math::Quatf EnvMap::rotation() const
{
  return _rot;
}

inline mrf::math::Mat3f EnvMap::rotationMatrix() const
{
  return _rot.getMatrix().upperLeft();
}

inline float EnvMap::rotationTheta() const
{
  return _rotation_theta;
}

inline float EnvMap::rotationPhi() const
{
  return _rotation_phi;
}

inline float EnvMap::rotationGamma() const
{
  return _rotation_gamma;
}

template<class VECT_TYPE, class POWER_TYPE>
void EnvMap::uniformSampling(std::vector<std::pair<VECT_TYPE, POWER_TYPE>> &dir_pow, unsigned int num_of_samples) const
{
  using namespace std;
  using namespace mrf::materials;
  using namespace mrf::sampling;

  vector<float> epsilon1;
  epsilon1.resize(num_of_samples);
  vector<float> epsilon2;
  epsilon2.resize(num_of_samples);

  VECT_TYPE        direction;
  JitteredSample2D js2d(num_of_samples);

  for (unsigned int i = 0; i < num_of_samples; i++)
  {
    epsilon1[i] = js2d.getRandomFloat(0, i, RandomGenerator::Instance().getFloat());
    epsilon2[i] = js2d.getRandomFloat(1, i, RandomGenerator::Instance().getFloat());

    direction = UMat::randomSphereDirection(epsilon1[i], epsilon2[i]);


    //assert( this != NULL );

    pair<VECT_TYPE, POWER_TYPE> dir_light = make_pair(direction, radianceEmitted(direction));

    dir_pow.push_back(dir_light);
  }
}



inline void EnvMap::errorMessage() const
{
  std::cerr << __FILE__ << " " << __LINE__ << std::endl;
  std::cerr << " WHAT THE HELL ARE YOU DOING ?????????? " << std::endl;
  std::cerr << " YOU ARE USING AN ENVMAP AS BRDF !!!! " << std::endl;

  assert(0);
}

inline mrf::materials::RADIANCE_TYPE EnvMap::radianceFromPixel(unsigned int i, unsigned int j) const
{
#ifdef MRF_RENDERING_MODE_SPECTRAL
  //std::cout << "RADIANCE FROM PIXEL\n";

  mrf::materials::RADIANCE_TYPE spectrum;
  auto const &                  temp_w = _envmap->wavelengths();
  spectrum.wavelengths().resize(temp_w.size());
  spectrum.values().resize(temp_w.size());

  uint num_w = 0;
  for (auto wave : temp_w)
  {
    spectrum.wavelengths()[num_w] = static_cast<float>(wave);

    //accessor for mrf::Array2D and Eigen::Array are (row,column)
    spectrum.values()[num_w] = _envmap->data()[num_w](j, i);

    num_w++;
  }
  return spectrum;

#else
  return _envmap->getPixel(i, j);
#endif
}


inline mrf::materials::COLOR EnvMap::colorFromDirection(mrf::math::Vec3f const &dir) const
{
  // mrf::math::Quatf q_phi;
  // q_phi.fromAxisAngle( mrf::math::Vec3f(0.0f, 1.0f, 0.0f), 90.0f );
  // mrf::math::Quatf q_theta;
  // q_theta.fromAxisAngle( mrf::math::Vec3f(1.0f, 0.0f, 0.0f), 00.0f );
  //
  // mrf::math::Quatf qComp = q_theta * q_phi;
  // mrf::math::Mat4f rot_matrix = qComp.getMatrix();


  return radianceEmitted(dir);
}

}   // namespace lighting
}   // namespace mrf
