/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#include <mrf_core/lighting/envmap.hpp>
#include <mrf_core/image/image_formats.hpp>

#include <iostream>


namespace mrf
{
namespace lighting
{
using namespace std;
using namespace mrf::geom;
using namespace mrf::math;

//-------------------------------------------------------------------------
//-------------------------------------------------------------------------
//-------------------------------------------------------------------------

EnvMap::EnvMap(float phi_rotation_in_degree, float theta_rotation_in_degree, float gamma_rotation_in_degree)
{
  _is_loaded            = false;
  _file_path            = "";
  _power_scaling_factor = 1.f;


  Quatf rot_theta;
  rot_theta.fromAxisAngle(Vec3f(1.0f, 0.0f, 0.0f), mrf::math::Math::toRadian(theta_rotation_in_degree));

  Quatf rot_phi;
  rot_phi.fromAxisAngle(Vec3f(0.0f, 1.0f, 0.0f), mrf::math::Math::toRadian(phi_rotation_in_degree));

  Quatf rot_gamma;
  rot_gamma.fromAxisAngle(Vec3f(0.0f, 0.0f, 1.0f), mrf::math::Math::toRadian(gamma_rotation_in_degree));


  _rot    = rot_gamma * rot_theta * rot_phi;
  _invRot = _rot.inverse();

#ifdef ENVMAP_DEBUG
  std::cout << "  _rot Matrix = " << _rot.getMatrix() << "  _invRot = " << _invRot.getMatrix() << std::endl;
#endif

  _rotation_theta = mrf::math::Math::toRadian(theta_rotation_in_degree);
  _rotation_phi   = mrf::math::Math::toRadian(phi_rotation_in_degree);
  _rotation_gamma = mrf::math::Math::toRadian(gamma_rotation_in_degree);
}

EnvMap::~EnvMap()
{
  _cdf_1d_envmap_columns.clear();
}

mrf::image::IMAGE_LOAD_SAVE_FLAGS EnvMap::load()
{
  try
  {
    _envmap = mrf::image::load(_file_path);
  }
  catch (mrf::image::IMAGE_LOAD_SAVE_FLAGS &error)
  {
    return error;
  }

  _is_loaded = true;

  return mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_NO_ERROR;
}

void EnvMap::updateRotationAngles(
    float phi_rotation_in_degree,
    float theta_rotation_in_degree,
    float gamma_rotation_in_degree)
{
  Quatf rot_theta;
  rot_theta.fromAxisAngle(Vec3f(1.0f, 0.0f, 0.0f), mrf::math::Math::toRadian(theta_rotation_in_degree));

  Quatf rot_phi;
  rot_phi.fromAxisAngle(Vec3f(0.0f, 1.0f, 0.0f), mrf::math::Math::toRadian(phi_rotation_in_degree));

  Quatf rot_gamma;
  rot_gamma.fromAxisAngle(Vec3f(0.0f, 0.0f, 1.0f), mrf::math::Math::toRadian(gamma_rotation_in_degree));

  _rot    = rot_gamma * rot_theta * rot_phi;
  _invRot = _rot.inverse();

  _rotation_theta = mrf::math::Math::toRadian(theta_rotation_in_degree);
  _rotation_phi   = mrf::math::Math::toRadian(phi_rotation_in_degree);
  _rotation_gamma = mrf::math::Math::toRadian(gamma_rotation_in_degree);
}

void EnvMap::computeCDF2D()
{
  std::vector<float> x_values;
  std::vector<float> y_values;
  x_values.resize(height());
  y_values.resize(height());

  for (uint iy = 0; iy < height(); iy++)
  {
    //x absciss for cdf is just image x coordinates, ranges from 0 to 1
    x_values[iy] = float(iy);   // / float(height() - 1);
    y_values[iy] = 0.f;

    //y values is the average of luminance over the row of the image
    for (uint ix = 0; ix < width(); ix++)
    {
      //This is super costly in spectral (both radiance radiance from pixel and luminance function
      //that computes an integral of the spectrum)
      y_values[iy] += radianceFromPixel(ix, iy).luminance();
    }
    y_values[iy] /= float(width());
  }
  _cdf_1d_envmap_rows = mrf::math::PiecewiseCDF1D<float>(x_values, y_values);


  _cdf_1d_envmap_columns.resize(height());
  x_values.resize(width());
  y_values.resize(width());

  for (uint iy = 0; iy < height(); iy++)
  {
    for (uint ix = 0; ix < width(); ix++)
    {
      //x absciss for cdf is just image y coordinates, ranges from 0 to 1
      x_values[ix] = float(ix);   // / float(width() - 1);

      //This is super costly in spectral
      y_values[ix] = radianceFromPixel(ix, iy).luminance();
    }

    _cdf_1d_envmap_columns[iy] = mrf::math::PiecewiseCDF1D<float>(x_values, y_values);
  }
  /*
  std::ofstream file("./test_cdf.csv");
  if (!file.is_open())
    throw std::runtime_error("Could not open file for saving");

  file << "CDF_col" << std::endl;
  for (uint ix = 0; ix<height(); ix++)
  {

    file << ix << "," << _cdf_1d_envmap_rows.cdf()[ix] << ","<< _cdf_1d_envmap_rows.pdf()[ix]<< std::endl;
  }

  file.close();
  */
}

#if 0
void EnvMap::computeCDFInverse(uint sqrt_nb_samples)
{
  _cdf_inv_2d.resize(sqrt_nb_samples*height());
  _cdf_inv_rows.resize(sqrt_nb_samples);
  //_cdf_inv.resize(2 * sqrt_nb_samples*sqrt_nb_samples);
  _pdf_2d.resize(sqrt_nb_samples*height());
  _pdf_rows.resize(sqrt_nb_samples);

  for (uint i = 0; i < sqrt_nb_samples; i++)
  {
    //x sample the cdf_1 at regular interval
    float x_value = float(i) / float(sqrt_nb_samples - 1);
    float pdf_row;
    int row_number = int(_cdf_1d_envmap_rows.inverse(x_value, pdf_row));
    _cdf_inv_rows[i] = row_number;
    _pdf_rows[i] = pdf_row;
  }

  //WHY DO WER NEED PDF NORMALIZATION HERE ???
  /*
  float pdf_rows_sum = 0.f;
  for (uint i = 0; i < _pdf_rows.size(); i++)
  {
    pdf_rows_sum += _pdf_rows[i];
  }
  for (uint i = 0; i < _pdf_rows.size(); i++)
  {
    _pdf_rows[i] /= pdf_rows_sum;
  }
  */

  for (uint i = 0; i < height(); i++)
  {

    int row_number = i;

    for (uint j = 0; j < sqrt_nb_samples; j++)
    {
      float y_value = float(j) / float(sqrt_nb_samples - 1);
      float pdf_col;
      int col_number = int(_cdf_1d_envmap_columns[row_number].inverse(y_value, pdf_col));

      _cdf_inv_2d[sqrt_nb_samples*i+j] = col_number;
      _pdf_2d[sqrt_nb_samples*i + j] = pdf_col;
      
      //_pdf[i*sqrt_nb_samples + j] = pdf_row * pdf_col;
    }


    //WHY DO WER NEED PDF NORMALIZATION HERE ???
    /*
    float pdf_cols_sum = 0.f;
    for (uint j = 0; j < sqrt_nb_samples; j++)
    {
      pdf_cols_sum += _pdf_2d[sqrt_nb_samples*i + j];
    }
    for (uint j = 0; j < sqrt_nb_samples; j++)
    {
       _pdf_2d[sqrt_nb_samples*i + j] /= pdf_cols_sum;
    }
    */
  }

  /*
  for (uint i = 0; i < sqrt_nb_samples; i++)
  {
    //x sample the cdf_1 at regular interval
    float x_value = float(i) / float(sqrt_nb_samples - 1);
    float pdf_row;
    int row_number = int(_cdf_1d_envmap_rows.inverse(x_value, pdf_row));

    for (uint j = 0; j < sqrt_nb_samples; j++)
    {
      float y_value = float(j) / float(sqrt_nb_samples - 1);
      float pdf_col;
      int col_number = int(_cdf_1d_envmap_columns[row_number].inverse(y_value, pdf_col));

      _cdf_inv[2 * (i*sqrt_nb_samples + j)] = row_number;
      _cdf_inv[2 * (i*sqrt_nb_samples + j) + 1] = col_number;
      _pdf[i*sqrt_nb_samples + j] = pdf_row * pdf_col;
    }
  }
  */


void EnvMap::computeCDF2DAndCDFInverse(uint sqrt_nb_samples)
{
  computeCDF2D();
  computeCDFInverse(sqrt_nb_samples);
}
#endif
}   // namespace lighting
}   // namespace mrf
