/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016, 2017
 *
 **/


#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/geometry/aabbox.hpp>
#include <mrf_core/geometry/local_frame.hpp>
#include <mrf_core/math/math.hpp>


namespace mrf
{
namespace lighting
{
/**
 * Interface for Lighting that every Geometry Must implement
 * to be a light.
 *
 **/
class MRF_CORE_EXPORT ILight
{
public:
  virtual ~ILight() {};

  virtual mrf::geom::MRF_AABBox &bbox() = 0;

  virtual mrf::math::Vec3f center() const = 0;

  /**
   * Returns the total surface area of the light surface
   *
   **/
  virtual float surfaceArea() const = 0;

  /**
   * Sample a position on the surface with the two given random
   * variables.
   *
   * The position is put in the given light_local_frame
   *
   *
   * Returns the associated sampling probability
   *
   **/
  virtual float
  surfaceSampling(float random_var_1, float random_var_2, mrf::geom::LocalFrame &light_local_frame) const = 0;


  //          virtual mrf::geom::LocalFrame const & localFrame() const = 0;


  /**
   * Returns the dot product between the light_vector and the
   * light normal for a given light's position
   *
   *
   * Note: light_vector is assumed to be normalized and points
   * toward the light source
   *
   **/
  virtual float lightGeometricFactor(mrf::math::Vec3f const &light_vector) const = 0;


  /**
   * Given the point being shaded and the position of a light
   * sample, this methods returns the light vector. This vector
   * is defined as light_position - point_shaded except for
   * special light like directional light where the light vector
   * is constant.
   *
   * The light_vector returned is normalized.
   *
   * This methods also returs the square distance between the
   * point being shaded and the light position
   *
   **/
  virtual mrf::math::Vec3f lightVector(
      mrf::math::Vec3f const &point_shaded,
      mrf::math::Vec3f const &light_position,
      float &                 distance_point_light,
      float &                 square_distance_point_light) const = 0;
};



}   // namespace lighting

}   // namespace mrf