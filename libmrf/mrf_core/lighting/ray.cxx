template<typename T>
_Ray<T>::_Ray(T origin_x, T origin_y, T origin_z, T dir_x, T dir_y, T dir_z)
  : _origin(origin_x, origin_y, origin_z)
  , _direction(dir_x, dir_y, dir_z)
  , _min_parametric_distance(RAY_EPSILON)
  , _max_parametric_distance(INFINITY)
  , _diff_depth(0)
  , _spec_depth(0)
  , _ior(1.0)
  , _id(ray_id++)
{
  _direction.normalize();
}


template<typename T>
_Ray<T>::_Ray(mrf::math::Vec3f const &origin, mrf::math::Vec3f const &direction)
  : _origin(origin)
  , _direction(direction)
  , _min_parametric_distance(T(RAY_EPSILON))
  , _max_parametric_distance(INFINITY)
  , _diff_depth(0)
  , _spec_depth(0)
  , _ior(1.f)
  , _id(ray_id++)

{
  _direction.normalize();
}

template<typename T>
_Ray<T>::_Ray(mrf::math::Vec3f const &origin, mrf::math::Vec3f const &direction, unsigned int an_id)
  : _origin(origin)
  , _direction(direction)
  , _min_parametric_distance(RAY_EPSILON)
  , _max_parametric_distance(INFINITY)
  , _diff_depth(0)
  , _spec_depth(0)
  , _ior(1.0)
  , _id(an_id)
{
  _direction.normalize();
}




template<typename T>
_Ray<T>::_Ray(mrf::math::Vec3f const &origin, mrf::math::Vec3f const &direction, float tmin, float tmax)
  : _origin(origin)
  , _direction(direction)
  , _min_parametric_distance(tmin)
  , _max_parametric_distance(tmax)
  , _diff_depth(0)
  , _spec_depth(0)
  , _ior(1.0)
  , _id(ray_id++)
{
  _direction.normalize();
}


template<typename T>
_Ray<T>::_Ray(
    mrf::math::Vec3f const &origin,
    mrf::math::Vec3f const &direction,
    unsigned int            diff_depth,
    unsigned int            spec_depth)
  : _origin(origin)
  , _direction(direction)
  , _min_parametric_distance(RAY_EPSILON)
  , _max_parametric_distance(INFINITY)
  , _diff_depth(diff_depth)
  , _spec_depth(spec_depth)
  , _ior(1.0)
  , _id(ray_id++)

{
  _direction.normalize();
}


template<typename T>
_Ray<T>::_Ray(_Ray const &aray)
  : _origin(aray._origin)
  , _direction(aray._direction)
  , _min_parametric_distance(aray.tMin())
  , _max_parametric_distance(aray.tMax())
  , _diff_depth(aray._diff_depth)
  , _spec_depth(aray._spec_depth)
  , _ior(aray._ior)
  , _id(ray_id++)
{
  _direction.normalize();
}


template<typename T>
_Ray<T>::~_Ray()
{
  ray_id--;
}



template<typename T>
_Ray<T> &_Ray<T>::operator=(_Ray<T> const &a_ray)
{
  if ((void *)this == (void *)&a_ray)   //assignment to itself?
  {
    return *this;
  }

  _origin    = a_ray.origin();
  _direction = a_ray.dir();
  _direction.normalize();

  _min_parametric_distance = a_ray.tMin();
  _max_parametric_distance = a_ray.tMax();

  _diff_depth = a_ray.diffDepth();
  _spec_depth = a_ray.specDepth();

  _ior = a_ray.ior();
  _id  = a_ray.id();

  return *this;
}
