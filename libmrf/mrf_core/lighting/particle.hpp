/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/materials/materials.hpp>
#include <mrf_core/radiometry/spectrum.hpp>
#include <mrf_core/color/color.hpp>
#include <mrf_core/math/vec3.hpp>

namespace mrf
{
namespace lighting
{
/**
 * A Particle is the combination of a direction and a power in Watt (W)
 *
 * Direction and power may be stored in a compressed or uncompressed
 * wat
 *
 * Contrary to the Photon Class, the direction
 * is the propagating vector of the particle.
 * (In Photon class the direction is the incomming light direction )
 *
 **/
template<class POWER_TYPE, class POSITION_TYPE, class DIRECTION_TYPE>
class MRF_CORE_EXPORT _Particle
{
public:
  POWER_TYPE     power;
  POSITION_TYPE  position;
  DIRECTION_TYPE direction;

  //This tracks the diffuse and specular bounds of the particle path
  unsigned int diff_depth;
  unsigned int spec_depth;   //This includes transmission

  inline _Particle(POWER_TYPE const &apower, POSITION_TYPE const &aposition, DIRECTION_TYPE const &adirection);

  inline std::string info() const;

  inline void scalePower(float scale_factor);

  inline void update(POSITION_TYPE const &a_new_position, DIRECTION_TYPE const &a_new_direction);

  inline void increaseDiffDepth();
  inline void increaseSpecDepth();
};

template<class POWER_TYPE, class POSITION_TYPE, class DIRECTION_TYPE>
MRF_CORE_EXPORT _Particle<POWER_TYPE, POSITION_TYPE, DIRECTION_TYPE>::_Particle(
    POWER_TYPE const &    apower,
    POSITION_TYPE const & aposition,
    DIRECTION_TYPE const &adirection)
  : power(apower)
  , position(aposition)
  , direction(adirection)
  , diff_depth(0)
  , spec_depth(0)
{}


template<class POWER_TYPE, class POSITION_TYPE, class DIRECTION_TYPE>
MRF_CORE_EXPORT std::string _Particle<POWER_TYPE, POSITION_TYPE, DIRECTION_TYPE>::info() const
{
  std::string info("********************* Particle Info *********************\n");

  info.append("* POWER : \n");
  info.append(power.info());
  info.append(" \n ");

  info.append("*  POSITION : \n");
  info.append(position.info());
  info.append(" \n ");

  info.append("*  DIRECTION : \n");
  info.append(direction.info());
  info.append(" \n ");

  info.append(" ************************************************************\n ");

  return info;
}

template<class POWER_TYPE, class POSITION_TYPE, class DIRECTION_TYPE>
inline void _Particle<POWER_TYPE, POSITION_TYPE, DIRECTION_TYPE>::scalePower(float scale_factor)
{
  power *= scale_factor;
}


template<class POWER_TYPE, class POSITION_TYPE, class DIRECTION_TYPE>
inline void _Particle<POWER_TYPE, POSITION_TYPE, DIRECTION_TYPE>::update(
    POSITION_TYPE const & a_new_position,
    DIRECTION_TYPE const &a_new_direction)
{
  position  = a_new_position;
  direction = a_new_direction;
}


template<class POWER_TYPE, class POSITION_TYPE, class DIRECTION_TYPE>
inline void _Particle<POWER_TYPE, POSITION_TYPE, DIRECTION_TYPE>::increaseDiffDepth()
{
  diff_depth++;
}

template<class POWER_TYPE, class POSITION_TYPE, class DIRECTION_TYPE>
inline void _Particle<POWER_TYPE, POSITION_TYPE, DIRECTION_TYPE>::increaseSpecDepth()
{
  spec_depth++;
}


typedef _Particle<mrf::materials::COLOR, mrf::math::Vec3f, mrf::math::Vec3f> Particle;




}   // namespace lighting
}   // namespace mrf
