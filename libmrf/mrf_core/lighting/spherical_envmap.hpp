/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016
 * Copyright CNRS 2017
 *
 **/

#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/lighting/envmap.hpp>
#include <mrf_core/materials/umat.hpp>
#include <mrf_core/sampling/random_generator.hpp>

#include <vector>
#include <algorithm>
#include <map>

namespace mrf
{
namespace lighting
{
/**
 *
 * This class represents a spherical environment map
 * as defined here : http://ict.debevec.org/~debevec/Probes/
 *
 **/
class MRF_CORE_EXPORT SphericalEnvMap: public EnvMap
{
public:
  SphericalEnvMap(std::string const &, float phi_rotation_in_degree = 0.0f, float theta_rotation_in_degree = 0.0f);
  virtual ~SphericalEnvMap();

  virtual mrf::math::Vec2f uv(mrf::math::Vec3f const &dir) const;
  virtual mrf::math::Vec3f direction(mrf::math::Vec2f const &uvs) const;

  virtual mrf::materials::RADIANCE_TYPE
  radianceEmitted(mrf::math::Vec3f const &emit_out_dir, mrf::geom::LocalFrame const &lf) const;

  virtual mrf::materials::RADIANCE_TYPE radianceEmitted(mrf::math::Vec3f const &emit_out_dir) const;

  /**
   * \todo
   **/
  // virtual mrf::materials::RADIANCE_TYPE computeAverageLuminanceOverHorizon(uint nb_samples) const;
};

}   // namespace lighting
}   // namespace mrf