# Externals SOURCES
file(GLOB_RECURSE SOURCES
  ${CMAKE_CURRENT_SOURCE_DIR}/*.cpp
  ${CMAKE_CURRENT_SOURCE_DIR}/*.c)

# Externals HEADERS
file(GLOB_RECURSE HEADERS
  ${CMAKE_CURRENT_SOURCE_DIR}/*.hpp
  ${CMAKE_CURRENT_SOURCE_DIR}/*.inl
  ${CMAKE_CURRENT_SOURCE_DIR}/*.h)

add_library(mrf_externals STATIC ${SOURCES} ${HEADERS} )


set_target_properties( mrf_externals
  PROPERTIES
  ARCHIVE_OUTPUT_DIRECTORY "${CMAKE_ARCHIVE_OUTPUT_DIRECTORY}/$<CONFIG>"
  LIBRARY_OUTPUT_DIRECTORY "${CMAKE_LIBRARY_OUTPUT_DIRECTORY}/$<CONFIG>"
  RUNTIME_OUTPUT_DIRECTORY "${CMAKE_RUNTIME_OUTPUT_DIRECTORY}/$<CONFIG>")
# if(${CMAKE_BUILD_SHARED_LIBS})
  # generate_export_header(mrf_externals
  # BASE_NAME mrf_externals
  # EXPORT_MACRO_NAME MRF_CORE_EXPORT
  # EXPORT_FILE_NAME ${PROJECT_SOURCE_DIR}/libmrf/mrf_core/mrf_dll.h
  # # STATIC_DEFINE SHARED_EXPORTS_BUILT_AS_STATIC
  # )
# endif()

if(NOT ENABLE_TEST)
  set_target_properties(mrf_externals PROPERTIES OUTPUT_NAME "mrf_externals$<$<CONFIG:Debug>:_d>" )
endif()


target_include_directories(mrf_externals PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/..)
target_include_directories(mrf_externals PUBLIC ${CMAKE_CURRENT_SOURCE_DIR})
target_include_directories(mrf_externals PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/rgbe)
target_include_directories(mrf_externals PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/tinyexr)
target_include_directories(mrf_externals PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/tinyply)
target_include_directories(mrf_externals PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/lodepng)
target_include_directories(mrf_externals PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/tinyxml2)
target_include_directories(mrf_externals PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/ghc)
target_include_directories(mrf_externals PUBLIC ${CMAKE_CURRENT_SOURCE_DIR}/termcolor)

install(TARGETS mrf_externals
    EXPORT mrf_externals
    ARCHIVE DESTINATION ${CMAKE_INSTALL_LIBDIR}
    LIBRARY DESTINATION ${CMAKE_INSTALL_LIBDIR}
    RUNTIME DESTINATION ${CMAKE_INSTALL_BINDIR}
    # This does not keep the hierarchy... fix bellow
    # PUBLIC_HEADER DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/mrf_${RENDERING_MODE}
  )

# This keeps the include hierarchy
install(
  DIRECTORY   ${CMAKE_CURRENT_SOURCE_DIR}
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/mrf_externals
  COMPONENT MRF
  FILES_MATCHING PATTERN "*.hpp" PATTERN "*.h" PATTERN "*.cxx" PATTERN "*.inl"
)