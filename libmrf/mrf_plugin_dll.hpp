/*
 *
 * author : Romain Pacanowski @ institutoptique.fr
 * Copyright CNRS 2016,2017
 *
 **/
#pragma once

#include <mrf_core_dll.hpp>
#ifdef MRF_WITH_OPTIX_SUPPORT
#  include <mrf_optix_dll.hpp>
#endif
#include <mrf_plugins/mrf_plugin_config.h>

// The following is only used when building MRF as DLL
// under WIN32 with Visual C++...
#if defined(_MSC_VER)
#  ifdef MRF_STATIC
#    define MRF_PLUGIN_EXPORT
#  else
#    ifdef MRF_PLUGIN_BUILD
#      define MRF_PLUGIN_EXPORT __declspec(dllexport)
#      pragma warning(disable : 4251)
#      pragma warning(disable : 4250)
#    else
#      define MRF_PLUGIN_EXPORT __declspec(dllimport)
#      pragma warning(disable : 4251)
#      pragma warning(disable : 4217)
#    endif
#  endif
#else
#  define MRF_PLUGIN_EXPORT
#endif
