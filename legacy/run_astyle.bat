::::::::::::::::::::::::::::::::::::::::::::::::
:: Author : Arthur Dufay
::
:: Indents (recursively) all the files of folder given as parameter
::::::::::::::::::::::::::::::::::::::::::::::::

:: @setlocal
@ECHO OFF

if "%1" == "" (
	set folder="."
)else (
	set folder=%1
)
echo Folder path set to %folder%

 if NOT EXIST %folder% (
 	ECHO Error: path does not exist
 	PAUSE
 	GOTO end
)


ECHO.
ECHO ================================================
ECHO Applying AStyle automatic indentation to sources
ECHO ================================================
ECHO.

Astyle --options="astyle.options" "%folder%/*.cpp" 
Astyle --options="astyle.options" "%folder%/*.cxx" 
Astyle --options="astyle.options" "%folder%/*.h"
Astyle --options="astyle.options" "%folder%/*.inl"
Astyle --options="astyle.options" "%folder%/*.hpp"
Astyle --options="astyle.options" "%folder%/*.cu"

ECHO.
ECHO =======================================================
ECHO.

:end
