#---------------------------------------------------------------------------------------------------
# author : Arthur Dufay @ inria.fr
# Copyright INRIA 2017.
# 
#---------------------------------------------------------------------------------------------------

project(black_body_generator)

cmake_minimum_required(VERSION 2.8)

# IF THE TYPE OF BUILD NOT DEFINED 
#
if( NOT CMAKE_BUILD_TYPE )
  set(CMAKE_BUILD_TYPE "RelWithDebInfo")
endif()


if( CMAKE_HOST_APPLE )
  message(" Compiling on Apple MacoS X" )
endif()

message(" System Processor ${CMAKE_SYSTEM_PROCESSOR} " )
message(" Compiler ${CMAKE_CXX_COMPILER_ID}")

#DEFINITIONS REGARDING THE BUILD MODE
message(" Building in " ${CMAKE_BUILD_TYPE} " Mode ")


if( ${CMAKE_BUILD_TYPE} STREQUAL "Debug" )
  add_definitions(-D${PROJECT_NAME}_DEBUG)
endif ()


#COMPILER DEFINITIONS
include(CheckCXXCompilerFlag)

check_cxx_compiler_flag(--std=c++11 SUPPORTS_STD_CXX11)

if(SUPPORTS_STD_CXX11)
    set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} --std=c++11")
    set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} --std=c++11")
endif()



#---------------------------------------------------------------------------------------------------
# SOURCES MANAGEMENT 
#---------------------------------------------------------------------------------------------------

# Include the files from the MRF hierarchy
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/MRF/mrf/)

# External Libraries
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/MRF/externals/)


include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/MRF/externals/ )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/MRF )
include_directories( ${CMAKE_CURRENT_SOURCE_DIR}/MRF/mrf )

#MRF SOURCES
file(GLOB_RECURSE SRCS
	${CMAKE_CURRENT_SOURCE_DIR}/MRF/mrf/*.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/MRF/mrf/*.c)


#MRF HEADERS
file(GLOB_RECURSE HEADERS
	${CMAKE_CURRENT_SOURCE_DIR}/MRF/mrf/*.hpp 
	${CMAKE_CURRENT_SOURCE_DIR}/MRF/mrf/*.inl
	${CMAKE_CURRENT_SOURCE_DIR}/MRF/mrf/*.h)

#Externals SOURCES
file(GLOB_RECURSE EXT_SRCS
	${CMAKE_CURRENT_SOURCE_DIR}/MRF/externals/*.cpp
	${CMAKE_CURRENT_SOURCE_DIR}/MRF/externals/*.c)


#Externals HEADERS
file(GLOB_RECURSE EXT_HEADERS
	${CMAKE_CURRENT_SOURCE_DIR}/MRF/externals/*.hpp 
	${CMAKE_CURRENT_SOURCE_DIR}/MRF/externals/*.inl
	${CMAKE_CURRENT_SOURCE_DIR}/MRF/externals/*.h)


set(SRCS ${SRCS} ${EXT_SRCS} main.cpp)
set(HEADERS ${HEADERS} ${EXT_HEADERS})

message("SRCS is: " ${SRCS})
message("Headers is: " ${HEADERS})

if(CMAKE_VERSION VERSION_GREATER "3.7")
	source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR}  PREFIX "src" FILES ${SRCS})
	source_group(TREE ${CMAKE_CURRENT_SOURCE_DIR}  PREFIX "headers" FILES ${HEADERS})
endif()

#use mrf as static library
add_definitions(-DMRF_STATIC)

add_executable(black_body_generator ${SRCS} ${HEADERS})

