/*
*
* author : Arthur Dufay @ inria.fr
* Copyright INRIA 2017
*
*
**/



//test tiny png
#include <string>
#include <iostream>
#include <chrono>

#include "mrf/image/color_image.hpp"
#include "externals/tinypng/lodepng.h"
#include "mrf/radiometry/d65.hpp"
#include "mrf/image/uniform_spectral_image.hpp"
#include "rendering/optix_renderer.hpp"

#include "externals/tinypng/lodepng.h"
#include <iostream>
#include <fstream>

#include "io/scene_parser.hpp"
#include "io/camera_parser.hpp"
#include "util/command_line.hpp"

using namespace std;
using namespace mrf;
using namespace mrf::image;
using namespace mrf::color;
using namespace mrf::radiometry;
using namespace mrf::rendering;
using mrf::gui::fb::Loger;




void
printUsage()
{
  std::cout << "\
  ******** Help message for ********\
    Usage is : manioc mandatory_options[optional_options]\
    where mandatory_options are :\
    -scene : {string} Scene file name\
      and where[optional_options] are :\
      [--help] : Print this help message"<<
            //[-anim_envmap] : Rotation_Step Nb_Frame. Rotate the environment map around Y = [0, 1, 0] with a Rotation_Step(in degree) angle Nb_Frame time
            "[-cam] : {string} Camera file name.\
      If not specified manioc looks for a.mcf file which name is the same as the scene.\
      If none is found a default Pinhole camera is constructed."<<
            //[-gbuffer] : Enable G - Buffer mecanism.
            "[-h] : Print this help message\
      [-height] : {uint} Height in pixels of the image\
      [-help] : Print this help message"<<
            //[-iss] : {string} Defines the Importance Sampling Strategy.Default is cosine hemisphere sampling.
            //Other options are : Kajiya, BRDF, Bidir, MIS,
            //[-lens] : {uint} Number of samples for the lens.This is only used for none - pinhole Camera.
            "[-logging] : {string} Defines the level of the logging system.\
      Possible values are : none fatal warning info trace debug.\
      [-max_rebound] : Maximum Level of Recursion of a ray\
      [-o] : {string} Name to save the rendered image.\
      If not specified the name of the scene file will be used.\
      Bare in mind that if multiple cameras are specified this name is used as prefix only"<<
            //[-pblock_size] : {uint} Square Root of the number of pixels to be rendered per thread(i.e., Pixel Block Size)
            //[-pixels] : Render only from pixel(i, j) and corresponding width and height
            //-debug_pixels i j w h  :  debuging et trace des pixels concernÚs (sont ils tous != de nan et inf etc)
            //- pixel_zone i j w h : rendus d une sous - partie de l image(important pour le rendu sur cluster notamment)
            "[-samples] : {uint} Number of samples per pixel(SPP)."<<
            /*[-save_bckg] : Save Background Buffer\
            [-save_gbuffer] : Save Geometric Buffers computed from G - Buffer System\
            [-seed] : {uint} Initialises the seed for the random generator.\
            [-shadow] : {uint} The number of shadow rays used for Direct Lighting evaluation.This is used only with compatible IS strategy.\
            [-shrate] : {uint} Number of samples to shade a point when evaluating the rendering equation.\
            [-super] : {uint} Super Sampling Rate.\
            Renders an image at high - resolution and downsamples when saved.ONLY WITH G - Buffer\
            [-thread] : {uint} Defines the number of threads to be used.\
            [-to] : {string} Tone - mapping operator. Possible values are : TO BE IMPLEMENTED .\
            If not specified the output is saved in floating point format.\
            [-variance] : Estimate and save the variance per pixel as well\
            [-verify_brdf] : Verify the reflectivity of the BRDF for nv views computed with nl light samples\*/
            "[-width] : {uint} Width in pixels of the image\
      ***********************************************************"<<std::endl;
}


int main(int argc, char *argv[])
{
  mrf::rendering::OptixRenderer renderer;

  uint max_samples = 100;
  uint max_rebounds = 0;
  bool use_max_rebounds = false;
  uint width = 960;
  uint height = 512;
  std::string output_file;
  std::string scene_file;
  std::string camera_file;
  Loger::LEVEL loger_level;


  mrf::util::CommandLine command_line(argc, argv);
  if (command_line.hasOption("help") || command_line.hasOption("h"))
  {
    printUsage();
    return 0;
  }

  if (command_line.hasOption("scene"))
  {
    if (command_line.option("scene").nbParameter() > 0)
      scene_file = command_line.option("scene").parameter(0);
  }
  else
  {
    std::cout << "No scene file specified !!"<<std::endl;
    printUsage();
    return 0;
  }

  if (command_line.hasOption("cam"))
  {
    if (command_line.option("cam").nbParameter() > 0)
      camera_file = command_line.option("cam").parameter(0);
  }
  else
  {
    mrf::util::StringParsing::getFileNameWithoutExtension(scene_file, camera_file);
    camera_file += ".mcf";
  }

  if (command_line.hasOption("o"))
  {
    if (command_line.option("o").nbParameter() > 0)
      output_file = command_line.option("o").parameter(0);
  }
  else
  {
    mrf::util::StringParsing::getFileNameWithoutExtension(scene_file, output_file);
    output_file += "_" + std::to_string(max_samples) + "spp.png";
  }

  if (command_line.hasOption("height"))
  {
    if (command_line.option("height").nbParameter() > 0)
      height = stoi(command_line.option("height").parameter(0));
  }
  if (command_line.hasOption("width"))
  {
    if (command_line.option("width").nbParameter() > 0)
      width = stoi(command_line.option("width").parameter(0));
  }

  if (command_line.hasOption("max_rebound"))
  {
    if (command_line.option("max_rebound").nbParameter() > 0)
    {
      use_max_rebounds = true;
      max_rebounds = stoi(command_line.option("max_rebound").parameter(0));
    }
  }

  if (command_line.hasOption("samples"))
  {
    if (command_line.option("samples").nbParameter() > 0)
    {
      max_samples = stoi(command_line.option("samples").parameter(0));
    }
  }


  if (command_line.hasOption("logging"))
  {
    if (command_line.option("logging").nbParameter() > 0)
    {
      string log_level = command_line.option("logging").parameter(0);
      //No_Log = 0, Fatal = 1, Warning = 2, Debug = 5, Trace = 4, Info = 3
      if (log_level == "No_log" || log_level == "0")
        loger_level = Loger::LEVEL::No_Log;
      if (log_level == "Fatal" || log_level == "1")
        loger_level = Loger::LEVEL::Fatal;
      if (log_level == "Warning" || log_level == "2")
        loger_level = Loger::LEVEL::Warning;
      if (log_level == "Debug" || log_level == "3")
        loger_level = Loger::LEVEL::Debug;
      if (log_level == "Info" || log_level == "4")
        loger_level = Loger::LEVEL::Info;
      if (log_level == "Trace" || log_level == "5")
        loger_level = Loger::LEVEL::Trace;
    }
  }



  renderer.init();
  std::string envmaps_path = MRF_INSTALL_DIR;
  envmaps_path += "/assets/envmaps";
  //renderer.addResourcePath(envmaps_path);

  mrf::rendering::Scene* scene = nullptr;
  mrf::io::loadScene(scene, scene_file, loger_level);

  std::map<std::string, mrf::rendering::Camera*> cameras;
  mrf::io::CameraParser::loadCameras(camera_file, cameras, loger_level);


  //renderer.importMrfScene(*scene);




  if (cameras.size() > 0)
  {
    auto cam = cameras.begin()->second;
    //renderer.setCamera(*cam);
    //renderer.createRenderTarget(cam->sensor().resolution().x(), cam->sensor().resolution().y());
    max_samples = cam->sensorSampler().spp();
  }
  else
  {
    mrf::rendering::Camera cam(
      mrf::math::Vec3f(0.f, 2.1502713f, 50.f),
      mrf::math::Vec3f(0.f, 2.1502713f, 0.f),
      mrf::math::Vec3f(0.f, 1.f, 0.f),
      50.f, 0.1f, 1000.f, width, height, 1);

    //renderer.setCamera(cam);
    //renderer.createRenderTarget(cam.sensor().resolution().x(), cam.sensor().resolution().y());
  }
  //renderer.createRenderContext();
  //renderer.setRenderingQualityCriterion(false);
  //renderer.setMinSamplesPerUpdate(1);
  //renderer.setMaxSamples(max_samples);
  if (use_max_rebounds)
  {
    //renderer.setMaxPathLength(max_rebounds);
  }
  //renderer.printSceneOptions();
  //renderer.printRenderContextOptions();
  renderer.render();

  //renderer.saveImage(output_file);

#if(WIN32)
  //system("pause");
#else
#endif
}