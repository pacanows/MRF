/*
*
* author : Arthur Dufay @ inria.fr
* Copyright INRIA 2017
*
*
**/


#include <string>

#include "mrf/image/color_image.hpp"

#include <iostream>
#include <fstream>

using namespace std;
using namespace mrf;
using namespace mrf::image;
using namespace mrf::color;


int main(int argc, char *argv[])
{
  if(argc<2)
    return -1;

  ColorImage im;
  std::string file_path = argv[1];
  im.load(file_path);

  std::string file_out = file_path + "_test.pfm";
  im.save(file_out);
  file_out = file_path + "_test.exr";
  im.save(file_out);
  file_out = file_path + "_test.png";
  im.save(file_out);

  return 0;
}