/*
  Array2D red(width, height);
  Array2D green(width, height);
  Array2D blue(width, height);

  float norm = 0.f;

  t1 = std::chrono::high_resolution_clock::now();
  //for each wavelength
  for (uint i = 0; i < _data.size(); i++)
  {
    int wavelength = _wavelengths[i];
    int index_sensitivity_curve = wavelength - spectrum_converter._sensitivity_curve_first_wavelength;
    int index_illuminant = (wavelength - mrf::radiometry::D_65_FIRST_WAVELENGTH) / mrf::radiometry::D_65_PRECISION;

    //if both sensitivity curve and illuminant are defined at this wavelength
    if (index_sensitivity_curve >= 0 && uint(index_sensitivity_curve) < spectrum_converter._sensitivity_curve_size &&
      index_illuminant >= 0 && uint(index_illuminant) < mrf::radiometry::D_65_ARRAY_SIZE)
    {
      red += _data[i] * (spectrum_converter._sensitivity_curve_x[index_sensitivity_curve] * mrf::radiometry::D65_SPD[index_illuminant]);
      green += _data[i] * (spectrum_converter._sensitivity_curve_y[index_sensitivity_curve] * mrf::radiometry::D65_SPD[index_illuminant]);
      blue += _data[i] * (spectrum_converter._sensitivity_curve_z[index_sensitivity_curve] * mrf::radiometry::D65_SPD[index_illuminant]);
      norm += spectrum_converter._sensitivity_curve_y[index_sensitivity_curve] * mrf::radiometry::D65_SPD[index_illuminant];
    }
  }
  red /= norm;
  green /= norm;
  blue /= norm;
  t2 = std::chrono::high_resolution_clock::now();
  auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
  std::cout << "time convert " << nanoseconds;

  t1 = std::chrono::high_resolution_clock::now();
  for (uint i = 0; i < width*height; i++)
  {
    xyz_image(i).r() = red(i);
    xyz_image(i).g() = green(i);
    xyz_image(i).b() = blue(i);
  }
  t2 = std::chrono::high_resolution_clock::now();
  nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
  std::cout << "time copy " << nanoseconds;
  */



//Parallel version
/*
Array2D temp_arrays[3] = { Array2D(width,height), Array2D(width,height), Array2D(width,height) };
float norm = 0.f;

t1 = std::chrono::high_resolution_clock::now();
//main thread computes norm
for (uint i = 0; i < _data.size(); i++)
{
  int wavelength = _wavelengths[i];
  int index_sensitivity_curve = wavelength - spectrum_converter._sensitivity_curve_first_wavelength;
  int index_illuminant = (wavelength - mrf::radiometry::D_65_FIRST_WAVELENGTH) / mrf::radiometry::D_65_PRECISION;

  //if both sensitivity curve and illuminant are defined at this wavelength
  if (index_sensitivity_curve >= 0 && uint(index_sensitivity_curve) < spectrum_converter._sensitivity_curve_size &&
    index_illuminant >= 0 && uint(index_illuminant) < mrf::radiometry::D_65_ARRAY_SIZE)
  {
    norm += spectrum_converter._sensitivity_curve_y[index_sensitivity_curve] * mrf::radiometry::D65_SPD[index_illuminant];
  }
}

// number of threads
uint nthreads = 3;//one thread per channel (red,green,blue)
std::vector<std::thread> threads(nthreads);


//bind lambda expression to threads
for (uint th = 0; th < nthreads; th++)
{
  threads[th] = std::thread(std::bind(
    [&](const int thread_num)
  {

    //for each wavelength
    for (uint i = 0; i < _data.size(); i++)
    {
      int wavelength = _wavelengths[i];
      int index_sensitivity_curve = wavelength - spectrum_converter._sensitivity_curve_first_wavelength;
      int index_illuminant = (wavelength - mrf::radiometry::D_65_FIRST_WAVELENGTH) / mrf::radiometry::D_65_PRECISION;

      //if both sensitivity curve and illuminant are defined at this wavelength
      if (index_sensitivity_curve >= 0 && uint(index_sensitivity_curve) < spectrum_converter._sensitivity_curve_size &&
        index_illuminant >= 0 && uint(index_illuminant) < mrf::radiometry::D_65_ARRAY_SIZE)
      {
        if(thread_num==0)
          temp_arrays[thread_num] += _data[i] * (spectrum_converter._sensitivity_curve_x[index_sensitivity_curve] * mrf::radiometry::D65_SPD[index_illuminant] / norm);
        else if(thread_num == 1)
          temp_arrays[thread_num] += _data[i] * (spectrum_converter._sensitivity_curve_y[index_sensitivity_curve] * mrf::radiometry::D65_SPD[index_illuminant] / norm);
        else if (thread_num == 2)
          temp_arrays[thread_num] += _data[i] * (spectrum_converter._sensitivity_curve_z[index_sensitivity_curve] * mrf::radiometry::D65_SPD[index_illuminant] / norm);

        //norm += spectrum_converter._sensitivity_curve_y[index_sensitivity_curve] * mrf::radiometry::D65_SPD[index_illuminant];
      }
    }
  }, th));
}



//Join the threads with the main thread
std::for_each(threads.begin(), threads.end(), [](std::thread& x) {x.join(); });

t2 = std::chrono::high_resolution_clock::now();
auto nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
std::cout << "time convert " << nanoseconds;

t1 = std::chrono::high_resolution_clock::now();

nthreads = std::thread::hardware_concurrency();
//std::vector<std::thread> threads(nthreads);
threads.clear();
threads.resize(nthreads);
uint nloop = width*height;
for (uint th = 0; th<nthreads; th++)
{
  threads[th] = std::thread(std::bind(
    [&](const int begin_loop, const int end_loop)
  {
    // loop over all items
    for (int i = begin_loop; i < end_loop; i++)
    {
      // inner loop
      {
        xyz_image(i).r() = temp_arrays[0][i];
        xyz_image(i).g() = temp_arrays[1][i];
        xyz_image(i).b() = temp_arrays[2][i];
      }
    }
  }, th*nloop / nthreads, (th + 1) == nthreads ? nloop : (th + 1)*nloop / nthreads));
}
std::for_each(threads.begin(), threads.end(), [](std::thread& x) {x.join(); });

t2 = std::chrono::high_resolution_clock::now();
nanoseconds = std::chrono::duration_cast<std::chrono::nanoseconds>(t2 - t1).count();
std::cout << "time copy " << nanoseconds;
*/
