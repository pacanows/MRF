################################################
## Author # Arthur Dufay
##
## Create an archive to deploy MRF
################################################

#!bin/sh

tar czvf MRF.tar.gz assets/* cmake/* externals/* mdl/* mrf/* tools/* apps/iray_renderer/CMakeLists.txt apps/iray_renderer/main.cpp
