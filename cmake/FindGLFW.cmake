#Copyright (c) 2019 David Murray.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions
# are met:
#  * Redistributions of source code must retain the above copyright
#    notice, this list of conditions and the following disclaimer.
#  * Redistributions in binary form must reproduce the above copyright
#    notice, this list of conditions and the following disclaimer in the
#    documentation and/or other materials provided with the distribution.
#  * Neither the name of NVIDIA CORPORATION nor the names of its
#    contributors may be used to endorse or promote products derived
#    from this software without specific prior written permission.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS ``AS IS'' AND ANY
# EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR
# PURPOSE ARE DISCLAIMED.  IN NO EVENT SHALL THE COPYRIGHT OWNER OR
# CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
# EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
# PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
# PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY
# OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT
# (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
# OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
#


if (WIN32)
	# Find include files
	find_path(
		GLFW_INCLUDE_DIR
		NAMES 
      GLFW/glfw3.h
		PATHS
      $ENV{PROGRAMFILES}/include
      ${GLFW_ROOT_DIR}/include
		DOC "The directory where GLFW/glfw.h resides")

	# Use glfw3.lib for static library
	if (GLFW_USE_STATIC_LIBS)
		set(GLFW_LIBRARY_NAME glfw3)
	else()
		set(GLFW_LIBRARY_NAME glfw3dll)
	endif()

	# Find library files
	find_library(
		GLFW_LIBRARY
		NAMES 
      ${GLFW_LIBRARY_NAME}
		PATHS
      ${GLFW_LOCATION}/lib
      $ENV{GLFW_LOCATION}/lib
      $ENV{PROGRAMFILES}/GLFW/lib
    DOC "The GLFW library")

	unset(GLFW_LIBRARY_NAME)
else()
	# Find include files
	find_path(
		GLFW_INCLUDE_DIR
		NAMES 
      GLFW/glfw3.h
		PATHS
      ${GLFW_LOCATION}/include
      $ENV{GLFW_LOCATION}/include
      /usr/include
      /usr/local/include
      /sw/include
      /opt/local/include
      NO_DEFAULT_PATH
		DOC "The directory where GL/glfw.h resides")
  
	# Find library files
	# Try to use static libraries
	find_library(
		GLFW_LIBRARY
		NAMES 
      glfw3 glfw
		PATHS
      ${GLFW_LOCATION}/lib
      $ENV{GLFW_LOCATION}/lib
      /usr/lib64
      /usr/lib
      /usr/local/lib64
      /usr/local/lib
      /sw/lib
      /opt/local/lib
      /usr/lib/x86_64-linux-gnu
      NO_DEFAULT_PATH
      DOC "The GLFW library")

endif()

if(GLFW_LIBRARY)
  set(GLFW_FOUND TRUE)
  message("GLFW found: " ${GLFW_FOUND})
endif()


# IF (WIN32)
  # SET(GLFW_ROOT_PATH $ENV{GLFW_ROOT_PATH})
# ENDIF (WIN32)

# find_package(OpenGL REQUIRED)

# if(WIN32)
  # # For whatever reason, cmake doesn't detect that a library is 32 or 64 bits,
  # # so we have to selectively look for it in one of two places.
  # if(CMAKE_SIZEOF_VOID_P EQUAL 8)
    # set(dir win64)
  # else() # 32 bit
    # set(dir win32)
  # endif()
  # find_path(GLFW_INCLUDE_DIR GL/glfw3.h
    # PATHS ${CMAKE_CURRENT_SOURCE_DIR}/ext/glfw/include
    # NO_DEFAULT_PATH
    # )
  # if(GLFW_INCLUDE_DIR)
    # # We need to set some of the same variables that FindGLFW.cmake does.
    # set(GLFW_FOUND TRUE)
    # set(sources ${sources} ${GLFW_INCLUDE_DIR}/GLFW/glfw3.h)

  # endif() # All the components were found

  # # Mark the libraries as advanced
  # mark_as_advanced(
    # GLFW_INCLUDE_DIR
    # )
# else() # Now for everyone else
  # find_package(GLFW REQUIRED)
# endif()
