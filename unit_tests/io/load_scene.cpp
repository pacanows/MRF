/*
 *
 * Author :
 *  - David Murray @ inria.fr
 *  Copyright  CNRS 2021
 *
 *
 **/

#include <mrf_core/rendering/scene.hpp>
#include <mrf_core/io/scene_parser.hpp>

#include <mrf_plugins/register_mrf_plugins.hpp>

#include <cstring>
#include <iostream>


//------------------------------------------------------------------------------
//
// Main
//
//------------------------------------------------------------------------------

int main(int argc, char **argv)
{
  //Loger for main.cpp, prints everything
  mrf::gui::fb::Loger *loger = mrf::gui::fb::Loger::getInstance(mrf::gui::fb::Loger::LEVEL::Info);

  loger->message("Testing loading\n");

  if (argc != 2)
  {
    std::cout << "This tests requires an input scene " << std::endl;
    return EXIT_FAILURE;
  }

  try
  {
    std::string scene_file(argv[1]);

    mrf::io::MaterialPlugins matplugins;
    mrf::io::register_all_mat(matplugins);
    mrf::io::SceneParser *sceneLoader = new mrf::io::SceneParser(matplugins);

    sceneLoader->setPath(scene_file);
    mrf::rendering::Scene *mrf_scene = new mrf::rendering::Scene();

    bool loaded = sceneLoader->loadScene(mrf_scene);

    if (!loaded)
    {
      loger->fatal("FAIL TO LOAD SCENE");
      return EXIT_FAILURE;
    }
    else
    {
      loger->info("Success !");
      return EXIT_SUCCESS;
    }
  }
  catch (std::exception &e)
  {
    loger->fatal(" Error while loading scene " + std::string(e.what()));

    return EXIT_FAILURE;
  }


  return EXIT_SUCCESS;
}
