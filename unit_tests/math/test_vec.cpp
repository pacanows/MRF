/**
 * Author: Romain Pacanowski @ institutoptique DOT fr
 * Copyright CNRS  : 2020
 * Copyright INRIA : 2022
 **/


#include <cmath>
#include <cstdlib>
#include <cassert>
#include <iostream>
#include <string>

#include <mrf_core/sampling/random_generator.hpp>
#include <mrf_core/math/math.hpp>

// #include <mrf_core/math/vec2.hpp>
// #include <mrf_core/math/vec3.hpp>
// #include <mrf_core/math/vec4.hpp>




bool equalL1(int a, int b, int epsilon = 0)
{
  return a == b;
}


bool equalL1(unsigned int a, unsigned int b, unsigned int epsilon = 0)
{
  return a == b;
}



template<typename T>
bool equalL1(T a, T b, T epsilon = T(0.00001))
{
  return (mrf::math::_Math<float>::absVal(a - b) < epsilon);
}


template<class VEC_TYPE, typename T>
void checkOrthogonality(VEC_TYPE const &v1, VEC_TYPE const &v2, VEC_TYPE const &v3)
{
  using namespace mrf::math;

  assert(equalL1(v1.dot(v2), T(0.0)));
  assert(equalL1(v1.dot(v3), T(0.0)));
  assert(equalL1(v2.dot(v3), T(0.0)));
}

template<typename T>
void testOrthogonalityForVec3()
{
  using namespace mrf::math;


  Vec3<T> a_x_axis = Vec3<T>::xaxis();
  Vec3<T> a_y_axis = Vec3<T>::yaxis();
  Vec3<T> a_z_axis = Vec3<T>::zaxis();



  Vec3<T> gen_tan;
  Vec3<T> gen_bitan;

  Vec3<T>::generateOrthogonalWithProjection(a_x_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(a_x_axis, gen_tan, gen_bitan);


  Vec3<T>::generateOrthogonalWithUV(a_x_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(a_x_axis, gen_tan, gen_bitan);

  Vec3<T>::generateOrthogonalWithJCGT(a_x_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(a_x_axis, gen_tan, gen_bitan);

  Vec3<T>::generateOrthogonalAsBRDFExplorer(a_x_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(a_x_axis, gen_tan, gen_bitan);


  Vec3<T>::generateOrthogonalWithProjection(a_y_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(a_y_axis, gen_tan, gen_bitan);

  Vec3<T>::generateOrthogonalWithUV(a_y_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(a_y_axis, gen_tan, gen_bitan);

  Vec3<T>::generateOrthogonalWithJCGT(a_y_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(a_y_axis, gen_tan, gen_bitan);

  Vec3<T>::generateOrthogonalAsBRDFExplorer(a_y_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(a_y_axis, gen_tan, gen_bitan);


  Vec3<T>::generateOrthogonalWithUV(-a_x_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(-a_x_axis, gen_tan, gen_bitan);

  Vec3<T>::generateOrthogonalWithUV(-a_y_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(-a_y_axis, gen_tan, gen_bitan);


  Vec3<T>::generateOrthogonalWithJCGT(-a_z_axis, gen_tan, gen_bitan);
  checkOrthogonality<Vec3<T>, T>(-a_z_axis, gen_tan, gen_bitan);


  // Generate Orthogonal does not garantee orthogonality between three vectors
  // but only two
  gen_tan = a_x_axis.generateOrthogonal();
  assert(equalL1(gen_tan.dot(a_x_axis), T(0.0)));

  gen_bitan = gen_tan.generateOrthogonal();
  assert(equalL1(gen_tan.dot(a_x_axis), T(0.0)));

  gen_tan = (a_z_axis).generateOrthogonal();
  assert(equalL1(gen_tan.dot(a_z_axis), T(0.0)));

  Vec3<T> vxy(T(0.0), T(1.0), T(1.0));
  gen_tan = (vxy).generateOrthogonal();
  assert(equalL1(gen_tan.dot(vxy), T(0.0)));



  //Other Tests

  //operator^ which is cross
  Vec3<T> test = a_y_axis ^ a_x_axis;
  assert(test == -a_z_axis);
}

template<typename T>
void test_vec2_ctr()
{
  using namespace mrf::math;
  using namespace mrf::sampling;

  RandomGenerator &rnd_gen = RandomGenerator::Instance();

  T const x1 = static_cast<T>(rnd_gen.getFloat() * 100);
  T const y1 = static_cast<T>(rnd_gen.getFloat() * 100);

  Vec2<T> v1(x1, y1);
  Vec2<T> v2(x1, y1);

  assert(v1 == v2);

  v1.set(T(2.) * x1, T(-10.) * y1);

  v2 = v1;

  assert(v1 == v2);
  assert(v1.x() == T(2.) * x1);
  assert(v1.y() == T(-10.) * y1);


  Vec2<T> v3(v1.x());

  std::cout << " v1 = " << v1 << std::endl;
  std::cout << " v3 = " << v3 << std::endl;

  assert(v3 != v1);
  assert(v3.x() == v1.x());

  const T &v3_x = v3[0];
  const T &v1_x = v1[0];
  assert(v3_x == v1_x);


  Vec2<T> a_x_axis = Vec2<T>::xaxis();
  assert(a_x_axis[0] == T(1));
  assert(a_x_axis[1] == T(0));

  Vec2<T> a_y_axis = Vec2<T>::yaxis();

  assert(Vec2<T>::dot(a_x_axis, a_y_axis) == T(0));



  Vec2i default_v2;
  assert(default_v2[0] == T(0));
  assert(default_v2(1) == T(0));

  Vec2<T> default_v3(default_v2);

  assert(default_v3 == default_v2);

  default_v2.setValues(v1[0], v1[1]);
  assert(default_v2 == v1);

  default_v2(0) = v1.x();
  default_v2(1) = v1.y();
  assert(default_v2 == v1);

  default_v2[0] = v1.x();
  default_v2[1] = v1.y();
  assert(default_v2 == v1);
}

template<typename T>
void test_vec3_ctr()
{
  using namespace mrf::math;
  using namespace mrf::sampling;

  RandomGenerator &rnd_gen = RandomGenerator::Instance();

  T const x1 = static_cast<T>(rnd_gen.getFloat());
  T const y1 = static_cast<T>(rnd_gen.getFloat());
  T const z1 = static_cast<T>(rnd_gen.getFloat());

  Vec3<T> v1(x1, y1, z1);
  Vec3<T> v2(x1, y1, z1);

  assert(v1 == v2);

  // Constructor from 2 points
  Vec3<T> test_ctr_2pts(v1, v2);
  assert(test_ctr_2pts == (v2 - v1));


  // Constructor from another type
  Vec3i   v21(1, 2, 3);
  Vec3<T> v4_from_other_type(v21);
  assert(v4_from_other_type == v21);

  Vec3<T> v1_zaxis = Vec3<T>::zaxis();

  assert(v1_zaxis.x() == T(0));
  assert(v1_zaxis.y() == T(0));
  assert(v1_zaxis.z() == T(1));


  Vec3<T> test_gravity = Vec3<T>::gravity();
  assert(test_gravity.x() == T(0));
  assert(test_gravity.y() == T(0));
  assert(test_gravity.z() == T(-9.8));
}


template<typename T>
void test_vec3_methods()
{
  using namespace mrf::math;

  Vec3<T> a_x_axis = Vec3<T>::xaxis();


  // Set method
  Vec3<T> test_set;
  test_set.set(a_x_axis.x(), a_x_axis.y(), a_x_axis.z());
  assert(test_set == a_x_axis);

  Vec3<T> test_set_values;
  test_set_values.setValues(a_x_axis.x(), a_x_axis.y(), a_x_axis.z());
  assert(test_set_values == a_x_axis);


  test_set_values.setZ(T(4.));
  assert(test_set_values.z() == T(4.));

  std::string test_set_values_str = test_set_values.info();

  std::cout << "test_set_values = " << test_set_values_str << std::endl;
}

template<typename T>
void test_vec4_ctr()
{
  using namespace mrf::math;
  using namespace mrf::sampling;

  RandomGenerator &rnd_gen = RandomGenerator::Instance();

  T const x1 = static_cast<T>(rnd_gen.getFloat());
  T const y1 = static_cast<T>(rnd_gen.getFloat());
  T const z1 = static_cast<T>(rnd_gen.getFloat());
  T const w1 = static_cast<T>(rnd_gen.getFloat());

  Vec4<T> v1(x1, y1, z1, w1);
  Vec4<T> v2(x1, y1, z1, w1);

  assert(v1 == v2);


  const T values_array[4] = {T(1), T(1), T(1), T(1)};
  Vec4<T> v_from_val_array(values_array);

  Vec4<T> one;
  one.setConstant(T(1));

  assert(v_from_val_array == one);


  const T *ptr_values = v_from_val_array.getArray();
  for (size_t i = 0; i < 4; i++)
  {
    assert(ptr_values[i] == values_array[i]);
  }
}

template<typename T>
void test_vec4_methods()
{
  using namespace mrf::math;

  Vec4<T> a_x_axis = Vec4<T>::xaxis();


  // Set method
  Vec4<T> test_set;
  test_set.set(a_x_axis.x(), a_x_axis.y(), a_x_axis.z(), a_x_axis.w());
  assert(test_set == a_x_axis);

  Vec4<T> test_set_values;
  test_set_values.setValues(a_x_axis.x(), a_x_axis.y(), a_x_axis.z(), a_x_axis.w());
  assert(test_set_values == a_x_axis);

  test_set_values.setZ(T(4.));
  assert(test_set_values.z() == T(4.));
}


template<typename T, class VEC_TYPE>
void test_axes_orthonormality()
{
  VEC_TYPE a_x_axis = VEC_TYPE::xaxis();
  VEC_TYPE a_y_axis = VEC_TYPE::yaxis();
  VEC_TYPE a_z_axis = VEC_TYPE::zaxis();

  //----
  assert(equalL1(a_z_axis.z(), T(1)));

  assert(a_z_axis.z() == T(1));
  assert(a_z_axis.x() == a_z_axis.y());
  assert(a_z_axis.x() == T(0));


  //----
  // Check orthogonality
  assert(equalL1(VEC_TYPE::dot(a_x_axis, a_z_axis), T(0)));
  assert(equalL1(VEC_TYPE::dot(a_x_axis, a_y_axis), T(0)));
  assert(equalL1(VEC_TYPE::dot(a_y_axis, a_z_axis), T(0)));
  assert(equalL1(a_x_axis.dot(a_y_axis), T(0)));


  // Check unity
  assert(equalL1(a_x_axis.length(), T(1)));
  assert(equalL1(a_x_axis.norm(), a_x_axis.length()));
  assert(equalL1(a_x_axis.norm2(), a_x_axis.sqrLength()));
  assert(equalL1(a_x_axis.norm2(), T(1)));

  assert(equalL1(a_y_axis.length(), T(1)));
  assert(equalL1(a_z_axis.length(), T(1)));
}

template<typename T, class VEC_TYPE, unsigned int VEC_SIZE>
void test_inf_nan()
{
  // This wont work with int...
  VEC_TYPE test_inf_nan;
  test_inf_nan.setConstant(T(0));
  test_inf_nan(0) = T(NAN);
  test_inf_nan(1) = T(INFINITY);

  //  std::cout << "test_inf_nan = " << test_inf_nan << std::endl;

  assert(test_inf_nan.isInfOrNan());
  assert(!test_inf_nan.isFinite());


  test_inf_nan(1) = T(NAN);
  test_inf_nan(0) = T(INFINITY);

  assert(test_inf_nan.isInfOrNan());
  assert(!test_inf_nan.isFinite());


  for (size_t i = 0; i < VEC_SIZE; i++)
  {
    test_inf_nan(i) = T(NAN);
  }
  assert(test_inf_nan.isInfOrNan());
  assert(!test_inf_nan.isFinite());


  for (size_t i = 0; i < VEC_SIZE; i++)
  {
    test_inf_nan(i) = T(INFINITY);
  }

  assert(test_inf_nan.isInfOrNan());
  assert(!test_inf_nan.isFinite());
}


template<typename T, class VEC_TYPE, unsigned int VEC_SIZE>
void test_vec()
{
  using namespace mrf::math;

  VEC_TYPE v1 = VEC_TYPE::xaxis();
  VEC_TYPE v2 = VEC_TYPE::xaxis();

  assert(v1 == v2);

  v1 = v1;
  assert(v1 == v2);


  T *v1_ptr = v1.getArray();
  T *v2_ptr = v2.getArray();

  assert(v1_ptr[0] == v1.x());
  assert(v1_ptr[1] == v2_ptr[1]);

  const T *v1_ptr_cte = v1.getArray();
  const T *v2_ptr_cte = v2.getArray();

  assert(v1_ptr_cte[1] == v1.y());
  assert(v1_ptr_cte[VEC_SIZE - 1] == v2_ptr[VEC_SIZE - 1]);

  const T *v1_ptr2 = v1.ptr();
  const T *v2_ptr2 = v2.ptr();

  for (size_t i = 0; i < VEC_SIZE; i++)
  {
    assert(v1_ptr2[i] == v2_ptr2[i]);
  }


  // Copy Ctr
  VEC_TYPE v3 = v2;

  assert(v1 == v3);
  assert(v2 == v3);

  VEC_TYPE v4(v2);

  assert(v4 == v3);
  assert(v4 == v2);
  assert(v4 == v1);


  //Constructor from T*
  T *values = new T[VEC_SIZE];
  values[0] = T(1);
  for (size_t i = 1; i < VEC_SIZE; i++)
  {
    values[i] = T(0);
  }

  VEC_TYPE test_ctr(values);
  assert(test_ctr == VEC_TYPE::xaxis());

  // Operator +
  VEC_TYPE sum_v1_v2 = v1 + v2;

  assert(sum_v1_v2.x() == v1.x() + v2.x());
  assert(sum_v1_v2.y() == v1.y() + v2.y());
  assert(sum_v1_v2.z() == v1.z() + v2.z());

  // Operator -
  VEC_TYPE diff_v1_v2 = v1 - v2;

  assert(diff_v1_v2.x() == T(0));
  assert(diff_v1_v2.y() == T(0));
  assert(diff_v1_v2.z() == T(0));


  //----
  VEC_TYPE a_x_axis = VEC_TYPE::xaxis();

  assert(equalL1(a_x_axis.x(), T(1)));

  assert(a_x_axis.x() == T(1));
  assert(a_x_axis.y() == a_x_axis.z());
  assert(a_x_axis.y() == T(0));

  //-----
  VEC_TYPE a_y_axis = VEC_TYPE::yaxis();
  assert(equalL1(a_y_axis.y(), T(1)));

  assert(a_y_axis.y() == T(1));
  assert(a_y_axis.x() == a_y_axis.z());
  assert(a_y_axis.x() == T(0));




  //----
  VEC_TYPE test_normal = a_y_axis.normal();

  assert(equalL1(test_normal.x(), a_y_axis.x()));
  assert(equalL1(test_normal.y(), a_y_axis.y()));
  assert(equalL1(test_normal.z(), a_y_axis.z()));


  // Component per component multiplications
  assert((a_x_axis * a_y_axis) == VEC_TYPE::zero());


  // Scalar Multiplication
  T const  a_scalar        = T(5);
  VEC_TYPE test_mul_scalar = a_x_axis * a_scalar;
  assert(test_mul_scalar.x() == a_scalar);
  assert(test_mul_scalar.y() == T(0));
  assert(test_mul_scalar.z() == T(0));


  VEC_TYPE test_mul_scalar2 = T(5) * a_x_axis;
  assert(test_mul_scalar2 == test_mul_scalar);


  // != operator
  test_mul_scalar2 = a_x_axis;
  assert(test_mul_scalar2 != test_mul_scalar);

  // Set
  test_mul_scalar.setX(test_mul_scalar2.x());
  test_mul_scalar.setY(test_mul_scalar2.y());

  // operator <=
  assert(test_mul_scalar <= test_mul_scalar2);

  // operator >=
  assert(test_mul_scalar >= test_mul_scalar2);

  // operator >
  assert(!(test_mul_scalar > test_mul_scalar2));

  // operator <
  assert(!(test_mul_scalar < test_mul_scalar2));



  // sum method
  assert(equalL1(a_x_axis.sum(), T(1)));


  // More methods on inf and nan
  assert(!(a_x_axis.isInfOrNan()));
  assert(a_x_axis.isFinite());



  // Set constant
  VEC_TYPE one;
  one.setConstant(T(1));
  for (size_t i = 0; i < VEC_SIZE; i++)
  {
    assert(one(i) == T(1));
  }


  // Absolute
  one.setConstant(T(1));
  VEC_TYPE minus_one = one;
  minus_one *= T(-1);
  minus_one.absVal();

  assert(one == minus_one);

  //Square
  one.setConstant(T(3.));

  VEC_TYPE sqr_test;
  sqr_test.setConstant(T(9));
  //(one.x() * one.x(), one.y() * one.y(), one.z() * one.z());

  assert(one.square() == sqr_test);
  one.squareEq();

  assert(one == sqr_test);

  //Sqrt
  assert(one.sqrt() == sqr_test.sqrt());

  // sqrLenght dot norm2 norm check
  assert(one.dot(one) == one.sqrLength());
  assert(one.norm2() == one.sqrLength());
  assert(one.length() == one.norm());



  //Normal Equal
  VEC_TYPE vnormal_eq_test;
  vnormal_eq_test.setConstant(T(4.));

  T const  vnormal_eq_test_length = vnormal_eq_test.length();
  VEC_TYPE vnormal_eq_check       = vnormal_eq_test;
  vnormal_eq_check /= vnormal_eq_test_length;

  vnormal_eq_test.normalEq();
  assert(vnormal_eq_test == vnormal_eq_check);

  //Now scaling vnormal_eq_test by 10
  vnormal_eq_test    = VEC_TYPE::zero();
  vnormal_eq_test(1) = T(1.);

  vnormal_eq_test.normalEq(T(10));

  assert(equalL1(vnormal_eq_test.y(), T(10.0)));


  //Negate Equal
  VEC_TYPE neg_test = vnormal_eq_test;
  neg_test *= T(-1);

  vnormal_eq_test.negateEq();

  assert(neg_test == vnormal_eq_test);

  // Double opposite unary operator -
  neg_test = -(-neg_test);
  assert(neg_test == vnormal_eq_test);


  //Clamp to MaxEq
  vnormal_eq_test.setConstant(T(10));
  one.setConstant(T(1.));

  vnormal_eq_test.clampToMaxEq(T(1));
  assert(vnormal_eq_test == one);

  // second branch
  vnormal_eq_test.clampToMaxEq(T(10));
  assert(vnormal_eq_test == one);


  //RoundTo Epsilon  first branch
  vnormal_eq_test.epsilonRoundZero(T(3));
  assert(vnormal_eq_test == VEC_TYPE::zero());

  //RoundTo Epsilon  second branch
  vnormal_eq_test.epsilonRoundZero(T(-1));
  assert(vnormal_eq_test == VEC_TYPE::zero());



  //Clamp to Zero
  VEC_TYPE clamp_test;
  clamp_test.setConstant(T(2));
  clamp_test.clampToZero(T(10));
  assert(clamp_test == VEC_TYPE::zero());

  // Other Branch
  clamp_test.clampToZero(T(0));
  assert(clamp_test == VEC_TYPE::zero());



  // operator /= between two vectors
  one.setConstant(T(1));

  VEC_TYPE ten_v;
  ten_v.setConstant(T(10));
  ten_v /= ten_v;

  assert(ten_v == one);


  //Mean
  VEC_TYPE mean_v1;
  mean_v1.setConstant(T(12.0));
  assert(mean_v1.mean() == mean_v1.x());


  //Pow
  mean_v1.setConstant(T(3));

  //VEC_TYPE tmp_pow = mean_v1.pow( T(3.) );
  mean_v1.pow(T(3.));

  //assert( tmp_pow == mean_v1 );

  assert(mean_v1.x() == std::pow(T(3.), T(3.)));

  // allPosOrNull
  assert(mean_v1.allPosOrNull(T(0.2)));

  // other branch
  mean_v1.setConstant(T(-10));
  assert(!mean_v1.allPosOrNull(T(0)));

  // other branch
  mean_v1(1) = T(0);
  assert(!mean_v1.allPosOrNull(T(0)));


  //  operator+( scalar_value)
  VEC_TYPE v1_scalar_value;
  v1_scalar_value.setConstant(-T(2.));

  VEC_TYPE v1_scalar_value_test = v1_scalar_value + T(2.);

  assert(v1_scalar_value_test == VEC_TYPE::zero());

  v1_scalar_value.setConstant(-T(10.));

  // operator  / by scalar
  VEC_TYPE v1_divide_v_test = v1_scalar_value / v1_scalar_value.x();
  one.setConstant(T(1));
  assert(v1_divide_v_test == one);

  // operator += scalar
  VEC_TYPE v1_plus_scalar;
  v1_plus_scalar.setConstant(T(123));
  v1_plus_scalar += -T(123);

  assert(v1_plus_scalar == VEC_TYPE::zero());

  // operator -= scalar
  v1_plus_scalar.setConstant(T(123));
  v1_plus_scalar -= T(123);
  assert(v1_plus_scalar == VEC_TYPE::zero());

  // operator - scalar
  v1_plus_scalar = v1_plus_scalar + T(123);
  v1_plus_scalar = v1_plus_scalar - T(123);
  assert(v1_plus_scalar == VEC_TYPE::zero());

  // operator / by vec
  v1_plus_scalar.setConstant(T(123));
  one.setConstant(T(1));
  assert(one == (v1_plus_scalar / v1_plus_scalar));

  // operator *= by vec
  VEC_TYPE check_op = v1_plus_scalar;
  v1_plus_scalar *= one;
  assert(check_op == v1_plus_scalar);


  // operator -= vector
  v1_plus_scalar.setConstant(T(123));
  VEC_TYPE v1_test_plus_vec = v1_plus_scalar;
  v1_test_plus_vec -= v1_plus_scalar;
  assert(v1_test_plus_vec == VEC_TYPE::zero());


  // Minimize
  VEC_TYPE m1;
  m1.setConstant(T(10));

  VEC_TYPE m2;
  m2.setConstant(T(-2));

  m1.minimize(m2);
  assert(m1 == m2);

  // Maximize
  m1.setConstant(T(10));
  m2.maximize(m1);
  assert(m1 == m2);

  // Other branches for maximize and minimize
  m2.maximize(m2);
  assert(m1 == m2);

  m2.minimize(m2);
  assert(m1 == m2);
}

template<typename T, class VEC_TYPE>
inline void testInterpolation()
{
  using namespace mrf::math;

  //Interpolate test
  VEC_TYPE v1_interpolate;
  v1_interpolate.setConstant(T(3.0));

  VEC_TYPE v2_interpolate;
  v2_interpolate.setConstant(T(15.0));

  VEC_TYPE v_interpolated = VEC_TYPE::interpolate(v1_interpolate, v2_interpolate, T(0.0));
  assert(v_interpolated == v1_interpolate);

  v_interpolated = VEC_TYPE::interpolate(v1_interpolate, v2_interpolate, T(1.0));
  assert(v_interpolated == v2_interpolate);

  v_interpolated = VEC_TYPE::interpolate(v1_interpolate, v2_interpolate, T(0.5));

  VEC_TYPE mean_manual = ((v1_interpolate + v2_interpolate) / T(2.));
  //std::cout << "mean_manual = " << mean_manual << std::endl;


  assert(v_interpolated == mean_manual);
}



int main(int /*argc*/, char ** /*argv*/)
{
  // Constructor for Vec3
  test_vec3_ctr<float>();
  test_vec3_ctr<double>();
  test_vec3_ctr<int>();


  test_vec<float, mrf::math::Vec3f, 3>();
  test_vec<double, mrf::math::Vec3d, 3>();
  test_vec<int, mrf::math::Vec3i, 3>();


  testOrthogonalityForVec3<float>();
  testOrthogonalityForVec3<double>();

  //Interpolation
  testInterpolation<float, mrf::math::Vec3<float>>();
  testInterpolation<double, mrf::math::Vec3<double>>();

  testInterpolation<float, mrf::math::Vec4<float>>();
  testInterpolation<double, mrf::math::Vec4<double>>();

  testInterpolation<float, mrf::math::Vec2<float>>();
  testInterpolation<double, mrf::math::Vec2<double>>();


  test_vec3_methods<float>();
  test_vec3_methods<double>();

  test_axes_orthonormality<float, mrf::math::Vec3<float>>();
  test_axes_orthonormality<double, mrf::math::Vec3<double>>();


  // On Vec4
  test_vec<float, mrf::math::Vec4f, 4>();
  test_vec<double, mrf::math::Vec4d, 4>();
  test_vec<int, mrf::math::Vec4i, 4>();

  test_axes_orthonormality<float, mrf::math::Vec4<float>>();
  test_axes_orthonormality<double, mrf::math::Vec4<double>>();

  // Vec4 constructors
  test_vec4_ctr<float>();
  test_vec4_ctr<double>();
  test_vec4_ctr<int>();

  // Vec4 methods
  test_vec4_methods<float>();
  test_vec4_methods<double>();
  test_vec4_methods<int>();

  // Vec2 constructors
  test_vec2_ctr<float>();
  test_vec2_ctr<double>();
  test_vec2_ctr<int>();


  // On Vec2 operations
  test_vec<float, mrf::math::Vec2f, 2>();
  test_vec<double, mrf::math::Vec2d, 2>();

  // Vec2,3,4 floating point only
  test_inf_nan<float, mrf::math::Vec2f, 2>();
  test_inf_nan<double, mrf::math::Vec2d, 2>();

  test_inf_nan<float, mrf::math::Vec3f, 3>();
  test_inf_nan<double, mrf::math::Vec3d, 3>();

  test_inf_nan<float, mrf::math::Vec4f, 4>();
  test_inf_nan<double, mrf::math::Vec4d, 4>();



  return EXIT_SUCCESS;
}