
/**
 * Author: Romain Pacanowski @ inria DOT fr
 * Copyright INRIA : 2022
 **/

#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/exr_spectral_image.hpp>
#include <mrf_core/image/image_diff.hpp>

#include <cassert>
#include <cstdlib>
#include <string>


float const EPSILON_TEST = 1e-3;


void myprogress_function_test(int progress)
{
  std::cout << "[TEST] Progress is : " << progress << " ";
}

int main(int argc, char **argv)
{
  using namespace std;
  using namespace mrf::image;

  assert(isSpectralExtension("exr"));



  if (argc != 2)
  {
    std::cout << "This tests requires an input image " << std::endl;
    return EXIT_FAILURE;
  }

  try
  {
    std::string const filename(argv[1]);
    std::cout << "[INFO] filename = " << filename << std::endl;

    EXRSpectralImage img1(filename);

    std::string const TEST_FILENAME_1 = "test_save_spectral_exr.exr";
    img1.save(TEST_FILENAME_1);


    EXRSpectralImage img2(TEST_FILENAME_1, &myprogress_function_test);

    assert(img1 == img2);

    // These images (reflective or emissive) are not supposed to be Polarized,
    assert(img1.nStokesComponents() != 4);


    bool is_spectral = EXRSpectralImage::isSpectral(filename);
    assert(is_spectral);


    // The cast is necessary unfortunately
    // TODO:  remove the cast by fixing the image diff functions in image_diff.hpp
    bool const rmse_diff1_img_12
        = rmse_diff(static_cast<UniformSpectralImage &>(img1), static_cast<UniformSpectralImage &>(img2));

    assert(rmse_diff1_img_12 < EPSILON_TEST);

    EXRSpectralImage diff_img12 = img1;
    bool const       rmse_diff2_img_12
        = rmse_diff(static_cast<UniformSpectralImage &>(img1), static_cast<UniformSpectralImage &>(img2), diff_img12);

    assert(mrf::math::_Math<float>::absVal(rmse_diff2_img_12 - rmse_diff2_img_12) < EPSILON_TEST);


    return EXIT_SUCCESS;
  }
  catch (const mrf::image::IMAGE_LOAD_SAVE_FLAGS &f)
  {
    std::cerr << "Error code: " << f << std::endl;
    return EXIT_FAILURE;
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << '\n';
    return EXIT_FAILURE;
  }

  std::cout << __FILE__ << " " << __LINE__ << std::endl;


  return EXIT_SUCCESS;
}