compile_named_test(
  ARTRaw_load_and_save
  ${CMAKE_CURRENT_SOURCE_DIR}/test_load_and_save_ART.cpp)

compile_named_test(
  ENVI_load_and_save
  ${CMAKE_CURRENT_SOURCE_DIR}/test_load_and_save_ENVI.cpp)

compile_named_test(
  LDR_load_and_save
  ${CMAKE_CURRENT_SOURCE_DIR}/test_load_and_save_LDR.cpp)

compile_named_test(
  EXR_load_and_save
  ${CMAKE_CURRENT_SOURCE_DIR}/test_load_and_save_HDR.cpp)

compile_named_test(
  SPECTRAL_EXR
  ${CMAKE_CURRENT_SOURCE_DIR}/test_spectral_EXR.cpp)

compile_named_test(
  POLARIZED_SPECTRAL_EXR
  ${CMAKE_CURRENT_SOURCE_DIR}/test_polarized_spectral_EXR.cpp)

compile_named_test(
  DIFF_IMAGE
  ${CMAKE_CURRENT_SOURCE_DIR}/test_image_diff.cpp
  )
# Will be added later
compile_named_test(
  SPECTRAL_IMAGE
  ${CMAKE_CURRENT_SOURCE_DIR}/test_spectral_image.cpp)

compile_named_test(
  COLOR_IMAGE
  ${CMAKE_CURRENT_SOURCE_DIR}/test_color_image.cpp)

compile_named_test(
  BISPECTRAL_EXR
  ${CMAKE_CURRENT_SOURCE_DIR}/test_bispectral_EXR.cpp
  )

# ART Raw

mrf_add_test(
  ARTRaw_load_and_save
  artraw_1
  ${TEST_DATA}/image_format/artraw/CornellBox_10x10_11.artraw
)

mrf_add_test(
  ARTRaw_load_and_save
  artraw_2
  ${TEST_DATA}/image_format/artraw/CornellBox_10x10_18.artraw
)

mrf_add_test(
  ARTRaw_load_and_save
  artraw_3
  ${TEST_DATA}/image_format/artraw/CornellBox_10x10_46.artraw
)

# ENVI

mrf_add_test(
  ENVI_load_and_save
  envi_1
  ${TEST_DATA}/image_format/envy/test_area_light_20x20_8.hdr
)

# EXR

mrf_add_test(
  EXR_load_and_save
  exr_1
  ${TEST_DATA}/image_format/exr/all_materials_rgb.exr
)

# LDR

mrf_add_test(
  LDR_load_and_save
  ldr_1
  ${TEST_DATA}/image_format/png/bm3.png
  ${TEST_DATA}/image_format/pfm/area_light.pfm
)

# SPECTRAL EXR

mrf_add_test(
  SPECTRAL_EXR
  spectral_unpolarised_exr_1
  ${TEST_DATA}/image_format/spectral-exr/emissive_unpolarised.exr
)

mrf_add_test(
  SPECTRAL_EXR
  spectral_reflective_exr_1
  ${TEST_DATA}/image_format/spectral-exr/reflective_macbeth.exr
)

# SPECTRAL AND POLARIZED EXR
mrf_add_test(
  POLARIZED_SPECTRAL_EXR
  spectral_polarised_exr_1
  ${TEST_DATA}/image_format/spectral-exr/emissive_polarised.exr
)

mrf_add_test(
  DIFF_IMAGE
  diff_image_all
  ${TEST_DATA}/image_format/png/bm3.png
  ${TEST_DATA}/image_format/pfm/area_light.pfm
  ${TEST_DATA}/image_format/exr/all_materials_rgb.exr
)

# Will be added later
# SPECTRAL IMAGE
mrf_add_test(
  SPECTRAL_IMAGE
  spectral_image_1
  ${TEST_DATA}/image_format/spectral-folder/fake_and_real_lemon_slices_r25_ms_01.png
  ${TEST_DATA}/image_format/spectral-folder/fake_and_real_lemon_slices_r25_ms_02.png
  ${TEST_DATA}/image_format/spectral-folder/fake_and_real_lemon_slices_r25_ms_03.png
  ${TEST_DATA}/image_format/spectral-folder/fake_and_real_lemon_slices_r25_ms_04.png
)

mrf_add_test(
  COLOR_IMAGE
  color_image
)

mrf_add_test(
  BISPECTRAL_EXR
  bispectral_exr_1
  ${TEST_DATA}/image_format/spectral-exr/bispectral.exr
  )