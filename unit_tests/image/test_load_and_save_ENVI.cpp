
/**
 * Author: Romain Pacanowski @ institutoptique DOT fr
 * Copyright CNRS : 2020
 **/

#include <mrf_core/image/uniform_spectral_image.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/envi_spectral_image.hpp>
#include <mrf_core/image/image_diff.hpp>

#include <cassert>
#include <cstdlib>
#include <string>


int main(int argc, char **argv)
{
  using namespace std;
  using namespace mrf::image;

  assert(isSpectralExtension("hdr"));
  assert(isSpectralExtension("raw"));


  if (argc != 2)
  {
    std::cout << "This tests requires an input image " << std::endl;
    return EXIT_FAILURE;
  }

  try
  {
    std::string const filename(argv[1]);
    std::cout << "[INFO] filename = " << filename << std::endl;
    /*int progress*/;

    //-------------------------------------------------------------------------
    // New ENVI handling mode
    //-------------------------------------------------------------------------

    // Loading an ENVI
    EnviSpectralImage img1(filename);
    std::cout << "img1.width height nbWaves " << img1.width() << " " << img1.height() << " "
              << img1.wavelengths().size() << std::endl;

    mrf::image::IMAGE_LOAD_SAVE_FLAGS ret = img1.save("test_save_envi.hdr");
    assert(ret == MRF_NO_ERROR);

    // We try to reload the previously saved image
    EnviSpectralImage img2("test_save_envi.hdr");
    assert(img1 == img2);

    // Try the copy operator
    EnviSpectralImage img3(img2);
    assert(img3 == img2);

    // More tests to be done
    //Checking if meta data are read correctly
    auto metadata     = img2.metadata();
    auto key_value_it = metadata.find("Interleaved mode:");

    assert(key_value_it != metadata.end());
    assert(key_value_it->second == "bsq");
  }
  catch (const mrf::image::IMAGE_LOAD_SAVE_FLAGS &f)
  {
    std::cerr << "Error code: " << f << std::endl;
    return EXIT_FAILURE;
  }
  catch (const std::exception &e)
  {
    std::cerr << e.what() << '\n';
    return EXIT_FAILURE;
  }

  std::cout << __FILE__ << " " << __LINE__ << std::endl;

  return EXIT_SUCCESS;
}
