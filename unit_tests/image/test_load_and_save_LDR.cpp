/**
 * Author: Romain Pacanowski @ institutoptique DOT fr
 * Copyright CNRS : 2020
 **/


#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/color_image.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/image_diff.hpp>

#include <cassert>
#include <cstdlib>
#include <string>
#include <memory>
#include <iostream>

void my_progress_function(int progress)
{
  std::cout << " Progress = " << progress << std::endl;
}

int main(int argc, char **argv)
{
  using namespace std;
  using namespace mrf::image;

  if (argc > 3)
  {
    std::cout << " This tests requires two input images " << std::endl;
    return EXIT_FAILURE;
  }

  //---------------------------------------------------------------------------
  // PNG
  //---------------------------------------------------------------------------
  std::string                 png_filename(argv[1]);
  std::unique_ptr<ColorImage> c_img1;

  try
  {
    c_img1 = loadColorImage(png_filename);
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &f)
  {
    assert(f == MRF_NO_ERROR);
  }


  assert(save(*c_img1, "test_save.png") == MRF_NO_ERROR);

  c_img1 = loadColorImage(png_filename);

  //Dont have any other choince to get a PNGColorImage in order to test the other function
  // with linear = true
  PNGColorImage *       img_png = reinterpret_cast<PNGColorImage *>(c_img1.get());
  IMAGE_LOAD_SAVE_FLAGS res     = img_png->save("test_save2.png", true, "foo", my_progress_function);

  assert(res == MRF_NO_ERROR);


  PNGColorImage c_img2;

  try
  {
    c_img2 = PNGColorImage("test_save.png");
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &f)
  {
    assert(f == MRF_NO_ERROR);
  }

  // Compare c_img1 and c_img2. They should be almost equal:
  // the sRGB -> linearRGB -> sRGB introduces some discrepancies
  assert(mse_diff<ColorImage>(*c_img1, c_img2) < 1e-3f);

  assert(save(*c_img1, "test_save.pfm") == MRF_NO_ERROR);

  PFMColorImage c_img3;

  try
  {
    c_img3 = PFMColorImage("test_save.pfm");
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &f)
  {
    assert(f == MRF_NO_ERROR);
  }

  assert(save(c_img3, "test_save.exr") == MRF_NO_ERROR);
  assert(save(c_img3, "test_save.hdr") == MRF_NO_ERROR);

  std::unique_ptr<ColorImage> c_img4;

  try
  {
    c_img4 = loadColorImage("test_save.hdr");
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &f)
  {
    assert(f == MRF_NO_ERROR);
  }

  assert(mse_diff<ColorImage>(c_img3, *c_img4) < 1e-3f);

  std::unique_ptr<ColorImage> c_img5;

  try
  {
    c_img5 = loadColorImage("test_save.exr");
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &f)
  {
    assert(f == MRF_NO_ERROR);
  }

  assert(c_img3 == *c_img5);


  //---------------------------------------------------------------------------
  // Load the argv[2] argument which is a pfm image
  //---------------------------------------------------------------------------
  try
  {
    std::unique_ptr<ColorImage> a_pfm_image = loadColorImage(argv[2]);
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &f)
  {
    assert(f == MRF_NO_ERROR);
  }


  //---------------------------------------------------------------------------
  // Now testing more Functions
  //---------------------------------------------------------------------------
  assert(!mrf::image::isRadianceHDR("test_save.png"));
  assert(!mrf::image::isRadianceHDR("test_save.pfm"));
  assert(!mrf::image::isRadianceHDR("test_save.exr"));
  assert(mrf::image::isRadianceHDR("test_save.hdr"));

  assert(mrf::image::isLDRExtension("png"));
  assert(!mrf::image::isLDRExtension("pfm"));
  assert(!mrf::image::isLDRExtension("exr"));
  assert(!mrf::image::isLDRExtension("hdr"));


  //---------------------------------------------------------------------------
  // Testing failure cases
  //---------------------------------------------------------------------------

  //---
  // Loading failure case
  //---
  try
  {
    PNGColorImage *ptr_img1 = new PNGColorImage("./foo.png");

    assert(false);
  }
  catch (...)
  {
    assert(true);
  }

  //---
  // Saving failure case
  //---
  try
  {
    //Loading original image
    c_img1 = loadColorImage(png_filename);

#ifdef _WIN32
    //Should failed due to write access
    IMAGE_LOAD_SAVE_FLAGS const is_save_ok = c_img1->save(string("P:\\foo.png"), string("failure_case"));
    std::cout << " is_save_ok = " << is_save_ok << std::endl;
#else
    //Should failed due to write access
    IMAGE_LOAD_SAVE_FLAGS const is_save_ok = c_img1->save(string("/foo.png"), string("failure_case"));
    std::cout << " is_save_ok = " << is_save_ok << std::endl;
#endif

    assert(is_save_ok != MRF_NO_ERROR);
  }
  catch (...)
  {
    std::cout
        << "Saving of image was unexpectedly successfull despite attempting to save to a non-existing location\n"
        << "This test is designed to check correct write error detection.\n\n"
        << "If the test was launched with a Windows OS possessing a drive mounted on P:, please either:\n"
        << "-ignore the failure and use another device without P: (or the GitLab CI).\n"
        << "-unmount your P: and check again, the test should not fail if you do not have a P: drive\n\n"
        << "If the test was launched with a UNIX OS, you likely launched the test with root privilege.\n"
        << "Please check again without it, or using the GitLab CI.\n"
        << "If you are not in ANY of the false positive cases above, you probably broke something in the image saving function."
        << std::endl;
    assert(false);
  }




  return EXIT_SUCCESS;
}
