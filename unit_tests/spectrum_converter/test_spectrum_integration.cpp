/**
 * Author: Alban Fichet @ institutoptique DOT fr
 * Copyright CNRS : 2020
 **/

#include <cassert>
#include <cstdlib>
#include <vector>

#include <mrf_core/color/spectrum_converter.hpp>

// ----------------------------------------------------------------------------
// Testing B&W energy integration
// ----------------------------------------------------------------------------

int main(int /*argc*/, char ** /*argv*/)
{
  using namespace mrf::color;

  std::vector<mrf::uint>    wavelengths;
  std::vector<float>        values;
  mrf::radiometry::Spectrum spectrum(wavelengths, values);

  float r1, r2;

  // --------------------------------------------------------------------------
  // Case empty vectors
  // --------------------------------------------------------------------------

  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values);
  assert(r1 == 0.f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum);
  assert(r1 == 0.f);

  // --------------------------------------------------------------------------
  // Case single value
  // --------------------------------------------------------------------------

  wavelengths.push_back(1);
  values.push_back(1.F);
  spectrum = mrf::radiometry::Spectrum(wavelengths, values);

  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values);
  assert(r1 == 1.f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum);
  assert(r1 == 1.f);

  // --------------------------------------------------------------------------
  // Case known integral
  // --------------------------------------------------------------------------

  values.resize(11);
  wavelengths.resize(11);

  for (size_t i = 0; i < values.size(); i++)
  {
    values[i]      = float(i);
    wavelengths[i] = mrf::uint(i);
  }

  spectrum = mrf::radiometry::Spectrum(wavelengths, values);

  // \int_{0}^{10} x dx = 50
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values);
  assert(r1 == 50.f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum);
  assert(r1 == 50.f);

  // out of bounds 1
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 0, 1000);
  assert(r1 == 50.f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 0, 1000);
  assert(r1 == 50.f);

  // \int_{2}^{5} x dx = 10.5
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 2, 5);
  assert(r1 == 10.5f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 2, 5);
  assert(r1 == 10.5f);

  // --------------------------------------------------------------------------
  // Irregular grid
  // --------------------------------------------------------------------------

  values.resize(4);
  wavelengths.resize(4);

  wavelengths[0] = 0;
  wavelengths[1] = 4;
  wavelengths[2] = 5;
  wavelengths[3] = 10;

  for (size_t i = 0; i < values.size(); i++)
  {
    values[i] = float(wavelengths[i]);
  }

  spectrum = mrf::radiometry::Spectrum(wavelengths, values);

  // \int_{0}^{10} x dx = 50
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values);
  assert(r1 == 50.f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum);
  assert(r1 == 50.f);

  // out of bounds 1
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 0, 1000);
  assert(r1 == 50.f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 0, 1000);
  assert(r1 == 50.f);

  // \int_{2}^{5} x dx = 10.5
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 2, 5);
  assert(r1 == 10.5f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 2, 5);
  assert(r1 == 10.5f);

  // --------------------------------------------------------------------------
  // Use roughther sampling
  // --------------------------------------------------------------------------

  values.resize(6);
  wavelengths.resize(6);

  for (size_t i = 0; i < values.size(); i++)
  {
    values[i]      = float(2 * i);
    wavelengths[i] = mrf::uint(2 * i);
  }

  spectrum = mrf::radiometry::Spectrum(wavelengths, values);

  // \int_{0}^{10} x dx = 50
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values);
  assert(r1 == 50.f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum);
  assert(r1 == 50.f);

  // \int_{2}^{5} x dx = 10.5
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 2, 5);
  assert(r1 == 10.5f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 2, 5);
  assert(r1 == 10.5f);

  // \int_{1}^{5} x dx = 12
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 1, 5);
  assert(r1 == 12.f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 1, 5);
  assert(r1 == 12.f);

  // \int_{1}^{2} x dx = 1.5
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 1, 2);
  assert(r1 == 1.5f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 1, 2);
  assert(r1 == 1.5f);

  // --------------------------------------------------------------------------
  // Use even roughther sampling
  // --------------------------------------------------------------------------

  values.resize(3);
  wavelengths.resize(3);

  for (size_t i = 0; i < values.size(); i++)
  {
    values[i]      = float(3 * i);
    wavelengths[i] = mrf::uint(3 * i);
  }

  spectrum = mrf::radiometry::Spectrum(wavelengths, values);

  // \int_{1}^{2} x dx = 1.5
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 1, 2);
  assert(r1 == 1.5f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 1, 2);
  assert(r1 == 1.5f);

  // --------------------------------------------------------------------------
  // Out of bounds -> starts at 3nm
  // --------------------------------------------------------------------------

  for (size_t i = 0; i < values.size(); i++)
  {
    values[i]      = float(3 * i + 3);
    wavelengths[i] = mrf::uint(3 * i + 3);
  }

  spectrum = mrf::radiometry::Spectrum(wavelengths, values);

  // Way larger than function definition shall equal to integral on whole function
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values);
  r2 = SpectrumConverter::spectrumIntegral(wavelengths, values, 0, 1000);
  assert(r1 == r2);
  r1 = SpectrumConverter::spectrumIntegral(spectrum);
  r2 = SpectrumConverter::spectrumIntegral(spectrum, 0, 1000);
  assert(r1 == r2);

  // Bounds out
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 0, 2);
  assert(r1 == 0.f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 0, 2);
  assert(r1 == 0.f);

  // Single value
  // \int_{3}^{4} x dx = 3.5
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 0, 4);
  assert(r1 == 3.5f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 0, 4);
  assert(r1 == 3.5f);

  // in the middle of two
  // \int_{4}^{5} x dx = 4.5
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 4, 5);
  assert(r1 == 4.5f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 4, 5);
  assert(r1 == 4.5f);

  // Single value
  // \int_{8}^{9} x dx = 8.5
  r1 = SpectrumConverter::spectrumIntegral(wavelengths, values, 8, 12);
  assert(r1 == 8.5f);
  r1 = SpectrumConverter::spectrumIntegral(spectrum, 8, 12);
  assert(r1 == 8.5f);

  return 0;
}
