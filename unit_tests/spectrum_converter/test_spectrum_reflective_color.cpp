/**
 * Author: Alban Fichet @ institutoptique DOT fr
 * Copyright CNRS : 2020
 **/

#include <cassert>
#include <cstdlib>
#include <string>
#include <vector>

#include <iostream>

#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>
#include <mrf_core/math/vec3.hpp>
#include <mrf_core/radiometry/d65.hpp>
#include <mrf_core/radiometry/spectrum.hpp>

int main(int argc, char **argv)
{
  if (argc < 5)
  {
    return EXIT_FAILURE;
  }

  const std::string spectrum_filename(argv[1]);
  const float       r = std::stof(argv[2]);
  const float       g = std::stof(argv[3]);
  const float       b = std::stof(argv[4]);

  const mrf::color::Color reference(r, g, b);
  mrf::color::Color       result;
  mrf::color::Color       difference;

  std::cout << "Loading: " << spectrum_filename << std::endl;
  mrf::radiometry::Spectrum ref_spectrum;
  assert(ref_spectrum.loadSPDFile(spectrum_filename));

  mrf::color::SpectrumConverter spectrum_converter(mrf::color::SensitivityCurveName::CIE_1931_2DEG);

  // 1st method
  result = spectrum_converter.reflectiveSpectrumToXYZ(
      ref_spectrum,
      mrf::radiometry::D_65_SPD,
      mrf::radiometry::D_65_FIRST_WAVELENGTH,
      mrf::radiometry::D_65_PRECISION,
      mrf::radiometry::D_65_ARRAY_SIZE);

  result = result.XYZtoLinearRGB();
  result = result.clamped();
  result = mrf::color::getGammaFunction()(result);

  difference = result - reference;

  std::cout << "Reference is:                              " << reference << std::endl;
  std::cout << "Conversion gave:                           " << result << std::endl;
  std::cout << "Signed difference (result - reference) is: " << difference << std::endl;

  assert(mrf::math::_Math<float>::absVal(difference.r()) < 0.005);
  assert(mrf::math::_Math<float>::absVal(difference.g()) < 0.005);
  assert(mrf::math::_Math<float>::absVal(difference.b()) < 0.005);

  // 2nd method
  result = spectrum_converter.reflectiveSpectrumToXYZ(
      ref_spectrum.wavelengths(),
      ref_spectrum.values(),
      mrf::radiometry::D_65_SPD,
      mrf::radiometry::D_65_FIRST_WAVELENGTH,
      mrf::radiometry::D_65_PRECISION,
      mrf::radiometry::D_65_ARRAY_SIZE);

  result = result.XYZtoLinearRGB();
  result = result.clamped();
  result = mrf::color::getGammaFunction()(result);

  difference = result - reference;

  std::cout << "Reference is:                              " << reference << std::endl;
  std::cout << "Conversion gave:                           " << result << std::endl;
  std::cout << "Signed difference (result - reference) is: " << difference << std::endl;

  assert(mrf::math::_Math<float>::absVal(difference.r()) < 0.005);
  assert(mrf::math::_Math<float>::absVal(difference.g()) < 0.005);
  assert(mrf::math::_Math<float>::absVal(difference.b()) < 0.005);

  return EXIT_SUCCESS;
}
