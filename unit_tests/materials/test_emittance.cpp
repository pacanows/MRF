/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_plugins/register_mrf_plugins.hpp>

int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::DiffuseEmittance a_mat("a_default_diffEmit");
  assert(a_mat.getType() == mrf::materials::EmittanceTypes::DIFFUSE);
  assert(a_mat.name() == "a_default_diffEmit");

  //Test setter/getter
  assert(a_mat.getRadiance() == 1.f);
  assert(a_mat.getColor() == mrf::materials::COLOR(1.0f));

  float radiance = 2.f;
  a_mat.setRadiance(radiance);
  assert(a_mat.getRadiance() == radiance);

  mrf::materials::COLOR color(0.5f);
  a_mat.setColor(color);
  assert(a_mat.getColor() == color);

  //TODO: add assertion for CPU backend function when functionnal


  return EXIT_SUCCESS;
}
