/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_plugins/register_mrf_plugins.hpp>

int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::LPhysicalPhong a_mat("a_default_phong");
  assert(a_mat.getType() == mrf::materials::BRDFTypes::PHONG_NORMALIZED);
  assert(a_mat.name() == "a_default_phong");

  //Test setter/getter
  assert(a_mat.getDiffuseColor() == mrf::materials::COLOR(0.f));
  assert(a_mat.getSpecularColor() == mrf::materials::COLOR(0.5f));
  assert(a_mat.diffuseAlbedo() == 0.1f);
  assert(a_mat.specularAlbedo() == 0.7f);
  assert(a_mat.getExponent() == 10);


  mrf::materials::COLOR diffuse(0.6f);
  a_mat.setDiffuseColor(diffuse);
  assert(a_mat.getDiffuseColor() == diffuse);

  mrf::materials::COLOR specular(0.3f);
  a_mat.setSpecularColor(specular);
  assert(a_mat.getSpecularColor() == specular);

  float rho_d = 0.8f;
  a_mat.setDiffuseReflectivity(rho_d);
  assert(a_mat.diffuseAlbedo() == rho_d);

  float rho_s = 0.1f;
  a_mat.setSpecularReflectivity(rho_s);
  assert(a_mat.specularAlbedo() == rho_s);

  unsigned int exponent = 5;
  a_mat.setExponent(exponent);
  assert(a_mat.getExponent() == exponent);


  //Tests other constructors
  mrf::materials::LPhysicalPhong a_mat2("a_default_phong", diffuse, rho_d, specular, rho_s, exponent);
  assert(a_mat2.getType() == mrf::materials::BRDFTypes::PHONG_NORMALIZED);
  assert(a_mat2.name() == "a_default_phong");
  assert(a_mat2.getDiffuseColor() == diffuse);
  assert(a_mat2.getSpecularColor() == specular);
  assert(a_mat2.diffuseAlbedo() == rho_d);
  assert(a_mat2.specularAlbedo() == rho_s);
  assert(a_mat2.getExponent() == exponent);


  //TODO: add assertion for CPU backend function when functionnal


  return EXIT_SUCCESS;
}
