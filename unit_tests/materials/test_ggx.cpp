/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_plugins/register_mrf_plugins.hpp>

int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::GGX a_mat("a_default_GGX");
  assert(a_mat.getType() == mrf::materials::BRDFTypes::GGX);
  assert(a_mat.name() == "a_default_GGX");

  //Test setter/getter
  assert(a_mat.ior().eta() == mrf::materials::COLOR(1.f));
  assert(a_mat.ior().kappa() == mrf::materials::COLOR(0.f));
  assert(a_mat.eta() == mrf::materials::COLOR(1.f));
  assert(a_mat.k() == mrf::materials::COLOR(0.f));
  assert(a_mat.alpha_g() == 0.f);

  mrf::materials::COLOR eta(1.5f);
  mrf::materials::COLOR kappa(3.5f);
  a_mat.setFresnel(eta, kappa);
  assert(a_mat.ior().eta() == eta);
  assert(a_mat.ior().kappa() == kappa);
  assert(a_mat.eta() == eta);
  assert(a_mat.k() == kappa);

  float alpha_g = 0.5;
  a_mat.setAlpha(alpha_g);
  assert(a_mat.alpha_g() == alpha_g);


  //Test other constructors
  mrf::materials::GGX a_mat2("a_default_ggx", alpha_g);
  assert(a_mat2.getType() == mrf::materials::BRDFTypes::GGX);
  assert(a_mat2.name() == "a_default_ggx");
  assert(a_mat2.ior().eta() == mrf::materials::COLOR(1.f));
  assert(a_mat2.ior().kappa() == mrf::materials::COLOR(0.f));
  assert(a_mat2.eta() == mrf::materials::COLOR(1.f));
  assert(a_mat2.k() == mrf::materials::COLOR(0.f));
  assert(a_mat2.alpha_g() == alpha_g);

  mrf::materials::GGX a_mat3("a_default_ggx", alpha_g, eta, kappa);
  assert(a_mat3.getType() == mrf::materials::BRDFTypes::GGX);
  assert(a_mat3.name() == "a_default_ggx");
  assert(a_mat3.ior().eta() == eta);
  assert(a_mat3.ior().kappa() == kappa);
  assert(a_mat3.eta() == eta);
  assert(a_mat3.k() == kappa);
  assert(a_mat3.alpha_g() == alpha_g);

  //TODO: add assertion for CPU backend function when functionnal


  return EXIT_SUCCESS;
}
