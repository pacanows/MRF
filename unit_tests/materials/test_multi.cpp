/**
 * Author: David Murray @ institutoptique DOT fr
 * Copyright CNRS : 2022
 **/

#include <cassert>
#include <cstdlib>
#include <string>

#include <mrf_core/materials/umat.hpp>
#include <mrf_core/materials/multi_material.hpp>
#include <mrf_plugins/register_mrf_plugins.hpp>

int main(int argc, char **argv)
{
  mrf::io::MaterialPlugins matplugins;
  mrf::io::register_all_mat(matplugins);

  using namespace std;

  //Test basic constructor
  mrf::materials::MultiMaterial a_mat("a_multi_mat");
  assert(a_mat.name() == "a_multi_mat");

  //Test setter/getter
  assert(a_mat.getAllMRFId().size() == 0);
  assert(a_mat.getAllLocalId().size() == 0);
  assert(a_mat.getAllNames().size() == 0);

  a_mat.addEntry(0, "dummy", 1);
  assert(a_mat.getAllMRFId().size() == 1);
  assert(a_mat.getAllNames().size() == 1);
  assert(a_mat.getAllLocalId().size() == 1);
  assert(a_mat.getAllMRFId()[0] == 0);
  assert(a_mat.getAllNames()[0] == "dummy");
  assert(a_mat.getAllLocalId()[0] == 1);

  assert(a_mat.hasTexture() == false);

  std::string dummy_path = "dummy/path";
  assert(a_mat.loadIndexTexture(dummy_path) == mrf::image::IMAGE_LOAD_SAVE_FLAGS::MRF_ERROR_WRONG_EXTENSION);
  //TODO, add a check in load image to check for valid path to return WRONG_FILE_PATH when necessary.

  assert(a_mat.hasTexture() == false);   //Should still be false.

  assert(a_mat.getTexturePath() == dummy_path);

  //TODO: add assertion for CPU backend function when functionnal


  return EXIT_SUCCESS;
}
