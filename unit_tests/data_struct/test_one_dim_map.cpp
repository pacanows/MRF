/**
 * Author: Romain Pacanowski @ INRIA DOT fr
 * Copyright INRIA : 2021
 **/

#include <cassert>
#include <cstdlib>

#include <mrf_core/data_struct/one_dim_map.hpp>
#include <mrf_core/math/line2d.hpp>

int main(int /*argc*/, char ** /*argv*/)
{
  using namespace mrf::util;

  _OneDimMap<float, double>  amapFD;
  _OneDimMap<double, double> amapDD;

  _OneDimMap<float, float> amapFF;

  //Testing an analytical monotonic function x^3
  for (size_t i = 0; i < 10; i++)
  {
    amapFF.insert(static_cast<float>(i), static_cast<float>(i * i * i));
  }

  assert(amapFF[9] == 9 * 9 * 9);

  assert(amapFF[2] == amapFF(2));
  assert(amapFF[2] != amapFF(3));

  assert(amapFF.nearestKey(0.01f) == 0.0f);
  assert(amapFF.nearestValue(0.01f) == 0.0f);

  auto an_it_on_map = amapFF.begin();
  amapFF.nearest(0.01f, an_it_on_map);
  // We found something
  assert(an_it_on_map != amapFF.end());
  // The value pointed by the iterator is correct
  assert(an_it_on_map->second == 0.0f);

  // Another cases
  // < min_key
  amapFF.nearest(-0.01f, an_it_on_map);
  assert(an_it_on_map == amapFF.begin());
  // > max_key
  amapFF.nearest(10.f, an_it_on_map);
  auto last_element = --amapFF.end();
  assert(an_it_on_map == last_element);


  // Testing Method inf
  assert(amapFF.inf(3.0f) == 3 * 3 * 3);
  assert(amapFF.inf(3.1f) == amapFF.inf(3.0f));

  // Linear Keys
  float low_key, upper_key, weight;
  amapFF.linearKeys(2.3f, low_key, upper_key, weight);

  assert(low_key == 2.0f);
  assert(upper_key == 3.0f);
  assert(mrf::math::equals<float>(weight, 0.7f));



  // Keys and Values
  float low_key2, upper_key2;
  float value_inf, value_sup;
  amapFF.keysAndValues(2.3f, low_key2, upper_key2, value_inf, value_sup);
  assert(low_key2 == low_key);
  assert(upper_key2 == upper_key);
  assert(value_inf == amapFF[low_key2]);
  assert(value_sup == amapFF[upper_key2]);

  //Linear
  float expected_linear_interpolated_value = value_inf * weight + (1.f - weight) * value_sup;
  assert(mrf::math::equals<float>(amapFF.linear(2.3f), expected_linear_interpolated_value));

  //Other Linear Cases
  // < min
  float const inside_min_value   = -1.0f;
  float       extrapolated_value = amapFF.linear(inside_min_value);
  auto        first_element      = amapFF.begin();
  auto        second_element     = first_element;
  second_element++;
  float x1 = first_element->first;
  float y1 = first_element->second;

  float x2 = second_element->first;
  float y2 = second_element->second;

  mrf::math::_Line2D<float, float> line1(x1, y1, x2, y2);
  assert(mrf::math::equals<float>(line1.valueAt(inside_min_value), extrapolated_value));

  // > max
  float const outside_max_value = 10.0f;
  // amapFF.keysAndValues(outside_max_value, low_key2, upper_key2, value_inf, value_sup);
  // expected_linear_interpolated_value = value_inf*weight + (1.f-weight)*value_sup;
  extrapolated_value = amapFF.linear(outside_max_value);

  last_element = amapFF.end();
  last_element--;
  auto plast_element = last_element;
  plast_element--;
  x1 = plast_element->first;
  y1 = plast_element->second;

  x2 = last_element->first;
  y2 = last_element->second;

  mrf::math::_Line2D<float, float> line2(x1, y1, x2, y2);

  assert(mrf::math::equals<float>(line2.valueAt(outside_max_value), extrapolated_value));


  //Clear method Tests
  amapFF.clear();

  assert(amapFF.size() == 0);
  assert(amapFF.begin() == amapFF.end());

  // Now we test the special case of OneDimMap with only one element
  //AND linear method is called
  amapFF.insert(4.0, 15.0f);

  float const extrapolated_value1 = amapFF.linear(2.0f);
  float const extrapolated_value2 = amapFF.linear(5.0f);

  auto element = amapFF.begin();
  assert(extrapolated_value1 == element->second);
  assert(extrapolated_value2 == element->second);

  // Case requested key is below the first one
  amapFF.linearKeys(3.0, low_key, upper_key, weight);
  element = amapFF.begin();
  assert(low_key == element->first);
  assert(low_key == upper_key);
  assert(weight == 0.5);

  // Case requested key is after the last one
  amapFF.insert(7.0f, 11.f);
  amapFF.linearKeys(10.0f, low_key, upper_key, weight);
  element = --amapFF.end();
  assert(low_key == element->first);
  assert(low_key == upper_key);
  assert(weight == 0.5);


  return EXIT_SUCCESS;
}