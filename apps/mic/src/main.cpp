/*
 *
 * author : Arthur Dufay @ inria.fr
 * Copyright INRIA 2017
 *
 *
 **/

#include <string>
#include <iostream>
#include <chrono>
#include <fstream>
#include <memory>

#include <mrf_core/data_struct/array2d.hpp>
#include <mrf_core/color/spectrum_converter.hpp>
#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/image/color_image.hpp>
#include <mrf_core/radiometry/illuminants.hpp>
#include <mrf_core/util/command_line.hpp>

#include <lodepng/lodepng.h>

using namespace std;
using namespace mrf;
using namespace mrf::image;
using namespace mrf::color;
using namespace mrf::radiometry;

void printUsage()
{
  std::cout << "Usage: mic -in <input file> [optional options]" << std::endl
            << std::endl
            << "Mandatory_options is:" << std::endl
            << "  -in <input file>" << std::endl
            << std::endl
            << "Optionnal options are:" << std::endl
            << "  -out output_file" << std::endl
            << "  -output_xyz          Will output the data as xyz instead of srgbd65." << std::endl
            << "  -input_xyz           Will consider the input data as xyz instead of RGB." << std::endl
            << "  -exposition <value>  Multiplication value to apply for each color in RGB output (default 1)"
            << std::endl
            //<< "  -gamma <value>       Gamma value to apply in RGB output (only for LDR, default 2.2)" << std::endl
            << std::endl
            << "If any one of the Reinhard tonemaping option are found, the Reinhard tonemapper will be used."
            << std::endl
            << "Reinhardt tonemapping is only used for LDR files" << std::endl
            << "Its options are:" << std::endl
            << "  -chromatic_adaptation <value>" << std::endl
            << "  -light_adaptation <value>" << std::endl
            << "  -intensity <value>" << std::endl
            << "  -contrast <value>" << std::endl
            << std::endl
            << "Supported ouput format are: exr, pfm and png." << std::endl
            << "If no output file is specified, your file will be save as <input file>.exr" << std::endl;
}


int main(int argc, char *argv[])
{
  mrf::util::CommandLine command_line(argc, argv);
  if (command_line.hasOption("help") || command_line.hasOption("h"))
  {
    printUsage();
    return 0;
  }

  // -------------------------------------------------------------------------
  // Input and output file parsing
  // -------------------------------------------------------------------------

  std::string input_file;
  std::string output_file;

  std::string input_extension;
  std::string output_extension;

  // Input
  // -----

  if (command_line.hasOption("in") && command_line.option("in").nbParameter() > 0)
  {
    input_file = command_line.option("in").parameter(0);
    mrf::util::StringParsing::getFileExtension(input_file.c_str(), input_extension);
  }

  if (input_file.size() == 0)
  {
    std::cerr << "No input file specified" << std::endl;
    return -1;
  }

  if (!mrf::image::isSupportedFile(input_file))
  {
    std::cerr << "Input file format is not supported: " << input_file << std::endl;
    return -1;
  }

  // Output
  // ------

  mrf::util::StringParsing::getFileNameWithoutExtension(input_file.c_str(), output_file);

  // We default to EXR format
  output_extension = "exr";
  output_file      = output_file + "." + output_extension;

  if (command_line.hasOption("out") && command_line.option("out").nbParameter() > 0)
  {
    output_file = command_line.option("out").parameter(0);
    mrf::util::StringParsing::getFileExtension(output_file.c_str(), output_extension);
  }

  if (!mrf::image::isSupportedFile(output_file))
  {
    std::cerr << "Output file format is not supported: " << output_file << std::endl;
    return -1;
  }

  // -------------------------------------------------------------------------
  // Tonemapping parameters parsing
  // -------------------------------------------------------------------------

  float exposition = 1.f;
  // float gamma                 = 2.2f;
  bool  reinhardt_tonemapping = false;
  bool  output_xyz            = command_line.hasOption("output_xyz");
  bool  input_xyz             = command_line.hasOption("input_xyz");
  float chromatic_adaptation  = 0.5f;
  float light_adaptation      = 0.f;
  float contrast              = 0.5f;
  float intensity             = 1.0;

  if (command_line.hasOption("exposition") && command_line.option("exposition").nbParameter() > 0)
  {
    exposition = std::stof(command_line.option("exposition").parameter(0));
  }

  // if (command_line.hasOption("gamma") && command_line.option("gamma").nbParameter() > 0)
  // {
  //   gamma = std::stof(command_line.option("gamma").parameter(0));
  // }

  if (command_line.hasOption("chromatic_adaptation") && command_line.option("chromatic_adaptation").nbParameter() > 0)
  {
    chromatic_adaptation  = std::stof(command_line.option("chromatic_adaptation").parameter(0));
    reinhardt_tonemapping = true;
  }

  if (command_line.hasOption("light_adaptation") && command_line.option("light_adaptation").nbParameter() > 0)
  {
    light_adaptation      = std::stof(command_line.option("light_adaptation").parameter(0));
    reinhardt_tonemapping = true;
  }

  if (command_line.hasOption("contrast") && command_line.option("contrast").nbParameter() > 0)
  {
    contrast              = std::stof(command_line.option("contrast").parameter(0));
    reinhardt_tonemapping = true;
  }

  if (command_line.hasOption("intensity") && command_line.option("intensity").nbParameter() > 0)
  {
    intensity             = std::stof(command_line.option("intensity").parameter(0));
    reinhardt_tonemapping = true;
  }


  bool const is_Radiance_file = mrf::image::isRadianceHDR(input_file);
  if (is_Radiance_file)
  {
    std::cout << "[INFO] This is a radiance File ! " << std::endl;
  }

  const bool is_input_spectral  = mrf::image::isSpectralImage(input_file);
  bool       is_output_spectral = false;

  std::unique_ptr<mrf::image::UniformSpectralImage> spectral_image(nullptr);
  std::unique_ptr<mrf::image::ColorImage>           coloured_image(nullptr);


  // -------------------------------------------------------------------------
  // Image loading
  // -------------------------------------------------------------------------

  try
  {
    // Handling spectral image input
    if (is_input_spectral)
    {
      std::cout << "[INFO] Loading spectral file: " << input_file << std::endl;

      spectral_image     = mrf::image::loadUniformSpectralImage(input_file);
      is_output_spectral = mrf::image::isSpectralExtension(output_extension.c_str());

      // If the output is not a spectral image, we convert it to RGB or XYZ
      if (!is_output_spectral)
      {
        coloured_image = mrf::image::getColorImageForSave(output_file);

        mrf::color::SpectrumConverter _converter(mrf::color::SensitivityCurveName::CIE_1931_2DEG);

        if (output_xyz)
          _converter.emissiveSpectralImageToXYZImage(*spectral_image, *coloured_image);
        else
          _converter.emissiveSpectralImageToRGBImage(*spectral_image, *coloured_image);
      }
    }
    // Handling coloured image input
    else
    {
      std::cout << "[INFO] Loading colored file: " << input_file << std::endl;

      // We do not support spectral uplifting for now
      if (!mrf::image::isColorExtension(output_extension.c_str()))
      {
        std::cerr << "Cannot save from color image to spectral image" << std::endl;
        return -1;
      }

      coloured_image = mrf::image::loadColorImage(input_file);

      if (!output_xyz && input_xyz)
      {
        #pragma omp parallel for schedule(static)
        for (int i = 0; i < static_cast<int>(coloured_image->size()); i++)
        {
          Color c              = (*coloured_image)[i];
          (*coloured_image)[i] = c.XYZtoLinearRGB();
        }
      }
    }
  }
  catch (IMAGE_LOAD_SAVE_FLAGS &e)
  {
    std::cerr << "Cannot open file: " << input_file << std::endl << "Error: " << e << std::endl;

    return EXIT_FAILURE;
  }


  // -------------------------------------------------------------------------
  // Image saving
  // -------------------------------------------------------------------------

  mrf::image::IMAGE_LOAD_SAVE_FLAGS status;

  if (is_output_spectral)
  {
    assert(spectral_image != nullptr);

    std::cout << "[INFO] Saving spectral file: " << output_file << std::endl;

    status = mrf::image::save(*spectral_image, output_file);
  }
  else
  {
    std::cout << "[INFO] Applying Tonemaping..." << std::endl;

    if (isLDRExtension(output_extension.c_str()))
    {
      if (reinhardt_tonemapping)
      {
        //Same code as in spectral viewer
        intensity = exp(-intensity);

        std::cout << "Using Reinhard tonemapper with values:" << std::endl;
        std::cout << "    light adaptation:     " << light_adaptation << std::endl;
        std::cout << "    intensity:            " << intensity << std::endl;
        std::cout << "    contrast:             " << contrast << std::endl;
        std::cout << "    chromatic adaptation: " << chromatic_adaptation << std::endl;
      }
      else
      {
        std::cout << "Using Basic tonemapper with values: " << std::endl;
        std::cout << "    exposition: " << exposition << std::endl;
        // std::cout << "    gamma:     " << gamma << std::endl;
      }

      #pragma omp parallel for schedule(static)
      for (int i = 0; i < int(coloured_image->size()); i++)
      {
        Color c = (*coloured_image)[i];

        if (reinhardt_tonemapping)
        {
          c = c.reinhard05Tonemapping(
              chromatic_adaptation,
              Color(
                  coloured_image->redStatistics()[2],
                  coloured_image->greenStatistics()[2],
                  coloured_image->blueStatistics()[2]),
              coloured_image->lumStatistics()[2],
              light_adaptation,
              contrast,
              intensity);
        }
        else
        {
          c = exposition * c;
          // c.pow(1.f / gamma);
        }

        coloured_image->setPixel(i, c);
      }
    }
    else
    {
      if (reinhardt_tonemapping)
      {
        std::cout << "[WARN] We do not apply Reinhardt tonemapping on HDR output" << std::endl;
      }
      else if (exposition != 1.f)
      {
        #pragma omp parallel for schedule(static)
        for (int i = 0; i < int(coloured_image->size()); i++)
        {
          Color c = (*coloured_image)[i];

          c = exposition * c;
          coloured_image->setPixel(i, c);
        }
      }
    }

    std::cout << "[INFO] Saving color file: " << output_file << std::endl;

    status = mrf::image::save(*coloured_image, output_file);
  }

  if (status != MRF_NO_ERROR)
  {
    std::cerr << "Cannot save output file: " << output_file << std::endl << "Error: " << status << std::endl;

    return EXIT_FAILURE;
  }

  return EXIT_SUCCESS;
}
