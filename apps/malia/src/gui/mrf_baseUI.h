#pragma once

#include <mrf_core_dll.hpp>
#include <mrf_core/mrf_types.hpp>

#include <mrf_core/rendering/renderer.hpp>

#include <mrf_core/color/spectrum_converter.hpp>
#include <mrf_core/radiometry/illuminants.hpp>
#include <mrf_core/radiometry/d50.hpp>
#include <mrf_core/radiometry/d65.hpp>

#include "Arcball.h"
#include "shader.h"
#include "texture.h"

#define IMGUI_IMPL_OPENGL_LOADER_GLEW
#include <GLFW/glfw3.h>

namespace mrf
{
namespace gui
{
class BaseUI
{
public:   //METHODS
  /*!
   *  Constructor.
   */
  BaseUI();
  virtual ~BaseUI();

  /*!
   *  Initializes the GUI, its context and its parameters depending on the chosen backend.
   *  \param renderer : the rendering backend. Default will use tge MRF CPU backend (TBA).
   *  \param v_sync : enable vertical synchronization (true).
   */
  virtual void initializeUI(rendering::Renderer *renderer = nullptr, bool v_sync = true);

  /*!
   *  Starts UI specific module if necessary and the rendering loop (see mainLoop()).
   */
  virtual void run();

  /*!
   *  Starts the OpenGL rendering loop.
   */
  virtual void mainLoop();

  /*!
   *  Setter to allow the UI to control the rendering backend.
   *  \param renderer : the rendering backend. Default will use the MRF CPU backend (TBA).
   */
  virtual void setRenderer(rendering::Renderer *renderer);

  /*!
   *  Displays all usable keyboard shortcuts in the terminal.
   */
  virtual void printInteractiveCommandHelp();

  /*!
   * UI's width setter.
   * \param width.
   */
  virtual void setWidth(int width) { _frame_width = width; }
  /*!
   * UI's height setter.
   * \param height.
   */
  virtual void setHeight(int height) { _frame_height = height; }
  /*!
   * Simple getter.
   * \return UI's width.
   */
  virtual int width() const { return (int)_frame_width; }
  /*!
   * Simple getter.
   * \return UI's height.
   */
  virtual int height() const { return (int)_frame_height; }

  //TODO DOC ME
  virtual float getSPF() const { return _samples_per_second; }
  virtual float getTotalTime() const { return _rendering_time; }

  /*!
   * Handles and propagates resize events to the backend.
   * \param w : the new width.
   * \param h : the new height.
   */
  virtual void resize(int w, int h, bool force = false);

  /*!
   * Handles and propagates minimization events (going to task bar).
   * \param iconified : the iconified state (1 = iconified).
   */
  virtual void minimizeEvent(int iconified);

  /*!
   * Handles and propagates mouse button events to the backend.
   * \param button : the GLFW button identifier that toggled the event.
   * \param action : the GLFW action identifier that toggled the event.
   * \param mods : the GLFW mods identifier. Unused but necessary for GLFW compatibility.
   */
  virtual void mousePress(int button, int action, int mods);

  /*!
   * Handles and propagates mouse movement events to the backend. Mouse movements done after a mouse click modify the camera position (left click) or the camera target (right click).
   * \param x : the new x position of the cursor.
   * \param y : the new y position of the cursor.
   */
  virtual void mouseMotion(double x, double y);

  /*!
   * Handles and propagates mouse scroll events to the backend. Horizontal scrolling is currently not supported. Vertical scrolling is used to control zoom parameters.
   * \param xoffset : x scrolling value. Unused but necessary for GLFW compatibility.
   * \param yoffset : y scrolling value.
   */
  virtual void mouseScroll(double xoffset, double yoffset);

  /*!
   * Handles and propagates keyboard characters events to the backend. The full list is presented by printInteractiveCommandHelp().
   * \param codepoint : the GLFW character identifier that toggled the event.
   */
  virtual void charPress(unsigned int codepoint);

  /*!
   * Handles and propagates keyboard non-character (e.g. escape or space bar) events to the backend. The full list is presented by printInteractiveCommandHelp().
   * \param key : the GLFW key identifier that toggled the event.
   * \param scancode : the GLFW character identifier. Unused but necessary for GLFW compatibility.
   * \param action : the GLFW character identifier that toggled the event.
   * \param mods : the GLFW mods identifier. Unused but necessary for GLFW compatibility.
   */
  virtual void keyboardPress(int key, int scancode, int action, int mods);

  /*!
   * Handles and propagates file drop events to the backend. Currently, only .msf files are handled. If multiple files are dropped, only the first valid one is processed.
   * \param count : the number of file dropped.
   * \param paths : the paths of the dropped files.
   */
  virtual void dropEvent(int count, const char **paths);

  /*!
   * Simple setter to make the scene's name known to the UI.
   * \param filename.
   */
  void setSceneInput(std::string filename) { _scene_file = filename; }

  /*!
   * Simple setter to make the output image's name known to the UI.
   * \param filename.
   */
  void setImageOutput(std::string filename) { _image_output_file = filename; }

  /*!
   * Simple setter to make the scene's camera name known to the UI.
   * \param filename.
   */
  void setCameraOutput(std::string filename) { _camera_output_file = filename; }

  /*!
   * Simple getter.
   * \return UI's window.
   */
  GLFWwindow *getWindow() { return _window; }

protected:   //METHODS
  /*!
   * Toggles the rendering of a frame as follow: the backend generate the result for the current frame then the rendering texture is updated accordingly.
   */
  virtual void render();

  /*!
   * Apply pre-display operators. This is done using the OpenGL pipeline along with shaders to apply pre-processes.
   */
  virtual void process();

  /*!
   * Displays the resulting frame of the current backend. This is done using the OpenGL pipeline along with shaders to apply post-processes.
   */
  virtual void display();

  /*!
   * Resizes the GLFW window.
   * \param w : the new width.
   * \param h : the new height.
   * \param resize_frame : boolean to force a frame resize, true by default.
   */
  void resizeAppWindow(int w, int h, bool resize_frame = true);

  /*!
   * Create the OpenGL context and window.
   * \param w : the rendering window's width.
   * \param h : the rendering window's height.
   * \param v_sync : enable vertical synchronization (true).
   */
  virtual void createContext(int w, int h, bool v_sync);

  /*!
   * Initializes OpenGL shaders and textures needed to display the backend results.
   */
  virtual void initializeUIRenderingResources(bool v_sync);

  /*!
   * If a camera update is necessary, propagates to the backend which owns the camera. Also, it reset the rendering to account for the new view point.
   */
  void updateCamera();

  /*!
   * Propagates a camera order to the backend which owns the camera.
   */
  virtual void resetCamera();
  /*!
   * Propagates a camera order to the backend which owns the camera.
   */
  void makeHeadStraight();
  /*!
   * Propagates a camera order to the backend which owns the camera.
   */
  virtual void previousCamera();
  /*!
   * Propagates a camera order to the backend which owns the camera.
   */
  virtual void nextCamera();
  /*!
   * Propagates a camera order to the backend which owns the camera.
   */
  bool saveCameraToFile();

  /*!
   * Propagates a closing order to the backend.
   */
  void closeFunction();

  /*!
   * Sends the OpenGL shader uniform variables.
   */
  void setAllParameters(Shader *prog);

  /*!
   * Sends the OpenGL shader uniform variables.
   */
  void updateFrameParameters(Shader *prog);

  /*!
   * Initializes the OpenGl buffer objects required to build a quad covering the render area.
   */
  void initQuad();

  /*!
   * Binds the OpenGl buffer objects required to render the quad.
   */
  void displayQuad(Shader *prog);

  /*!
   * Checks if a path is a valid .msf file and if so, loads the associated scene.
   * /param scene : the (full) path to the scene.
   */
  virtual bool checkScene(std::string scene);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  /*!
   * Initializes the spectral sensitivity buffer for spectral to RGB conversion.
   */
  void initSensitivityBuffer();

  /*!
   * Initializes the spectral illuminant buffer for spectral to RGB conversion.
   */
  // void initIlluminantBuffer();

  /*!
   * Initializes the wavelength, the one used for rendering by the backend, buffer for spectral to RGB conversion.
   */
  void initWavelengthBuffer();

  /*!
   * Compute normalizatioon factor of the sensitivity: the integral on the Y component of the sensitivity curve.
   */
  // void computeSensitivityNorm();
#endif

  void retrieveScreenProperties();

protected:
  enum ILLUMINANT
  {
    D50,
    D65,
    BLACK_BODY
  };

  enum SPACE
  {
    SRGB,
    ARGB
  };

  enum TEXTURE
  {
    OUTPUT_BUFFER,
    PROCESS_BUFFER,
#ifdef MRF_RENDERING_MODE_SPECTRAL
    SENSITIVITY_BUFFER,
    ILLUMINANT_BUFFER,
    WAVELENGTH_BUFFER,
#endif
    ALL_TEX   //Convenience enum to get total number of texture
  };

  enum PROGRAM
  {
    PROCESS,
    DISPLAY
  };

  //MEMBERS
  GLFWwindow *_window;

  rendering::Renderer *_renderer;


  std::map<int, Texture> _texture_list;

  int _current_illuminant;
  int _current_sensitivity;
  int _current_space;

  std::map<int, Shader *> _shader_prog;
  std::map<int, bool>     _update_parameters;

  rendering::Arcball _arcball;

  size_t       _frame_width;
  size_t       _frame_height;
  int          _decoration_width;
  int          _decoration_height;
  int          _decoration_left;
  int          _decoration_right;
  int          _decoration_top;
  int          _decoration_bottom;
  size_t       _max_screen_width;
  size_t       _max_screen_height;
  unsigned int _spectrum_size;   //Set to 1 for RGB rendering, the total number of wavelengths otherwise.


  math::Vec2f _mouse_prev_pos;
  int         _mouse_button;
  bool        _mouse_pressed;
  bool        _render_on_pause;
  float       _rendering_time;
  float       _total_rendering_time;
  float       _samples_per_second;
  double      _sample_count;
  int         _spatial_factor;
  int         _max_spatial_factor;
  int         _target_FPS;
  bool        _optimize_FPS;

  bool  _camera_changed;
  float _straf_speed;

  std::string _image_output_file;
  std::string _camera_output_file;
  std::string _scene_file;

  math::Vec4f _clearColor;

private:
  GLuint _vao;
  GLuint _vbo;
  bool   _quad_ready;
};
}   // namespace gui
}   // namespace mrf
