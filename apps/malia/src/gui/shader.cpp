#include "shader.h"

#include "gl_helper.h"

#include <iostream>
#include <string>
#include <fstream>
#include <assert.h>

namespace mrf
{
namespace gui
{
std::string loadSourceFromFile(const std::string &filename)
{
  std::string source = "";

  std::ifstream in(filename.c_str(), std::ios::in);
  if (!in)
  {
    std::cerr << "File not found " << filename << std::endl;
    return source;
  }

  const int maxBuffersize = 2048;
  char      buffer[maxBuffersize];
  while (in.getline(buffer, maxBuffersize))
  {
    source += std::string(buffer) + "\n";
  }

  return source;
}

bool Shader::loadFromFile(ShaderObject obj, const std::string &obj_file)
{
  std::string obj_src = loadSourceFromFile(obj_file);
  if (_obj_additional_header[obj].size() > 1)
  {
    std::string version("#version 450 compatibility");
    std::size_t end_pos = obj_src.find(version) + version.size();
    obj_src.insert(end_pos, "\n" + _obj_additional_header[obj] + "\n");
  }
  bool allOK = createShaderObject(obj, obj_src);
  if (allOK) _obj_file[obj] = obj_file;
  return allOK;
}

bool Shader::loadFromFiles(const std::string &vtx_file, const std::string &frag_file)
{
  //  _fragment_file       = frag_file;
  //  _vertex_file         = vtx_file;
  //  std::string vtx_src  = loadSourceFromFile(vtx_file);
  //  std::string frag_src = loadSourceFromFile(frag_file);
  //  return loadSources(vtx_src, frag_src);

  std::string vtx_src = loadSourceFromFile(vtx_file);
  bool        allOK   = createShaderObject(VERTEX, vtx_src);
  if (allOK) _obj_file[VERTEX] = vtx_file;
  std::string frag_src = loadSourceFromFile(frag_file);
  allOK &= createShaderObject(FRAGMENT, frag_src);
  if (allOK) _obj_file[FRAGMENT] = frag_file;
  allOK &= createShaderProgram();
  return allOK;
}

bool Shader::createShaderObject(ShaderObject obj, const std::string &obj_src)
{
  GLuint shaderID(GL_INVALID_INDEX);

  switch (obj)
  {
    case VERTEX:
    {
      shaderID = glCreateShader(GL_VERTEX_SHADER);
      break;
    }
    case FRAGMENT:
    {
      shaderID = glCreateShader(GL_FRAGMENT_SHADER);
      break;
    }
    case COMPUTE:
    {
      shaderID = glCreateShader(GL_COMPUTE_SHADER);
      break;
    }
    default:
    {
      return false;
      break;
    }
  }

  const GLchar *arbSource = obj_src.c_str();

  glShaderSource(shaderID, 1, (const GLchar **)&arbSource, 0);
  glCompileShader(shaderID);

  int compiled;
  glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compiled);
  printShaderInfoLog(shaderID);

  checkErrorGL();

  if (compiled) _obj_ID[obj] = shaderID;

  return compiled;
}

void Shader::setAdditionalHeader(ShaderObject obj, std::string header)
{
  _obj_additional_header[obj] = header;
}



bool Shader::createShaderProgram()
{
  bool allIsOk = true;

  //if (_programID == GL_INVALID_INDEX)
  _programID = glCreateProgram();

  for (std::pair<int, GLuint> shader_obj : _obj_ID)
  {
    glAttachShader(_programID, shader_obj.second);
  }

  glLinkProgram(_programID);

  int isLinked;
  glGetProgramiv(_programID, GL_LINK_STATUS, &isLinked);
  allIsOk   = allIsOk && isLinked;
  _is_valid = (isLinked == (int)GL_TRUE);
  if (!allIsOk) printProgramInfoLog(_programID);

  checkErrorGL();

  return allIsOk;
}

bool Shader::loadSources(const std::string &vtx_src, const std::string &frag_src)
{
  bool allIsOk = true;

  _programID = glCreateProgram();

  // vertex shader
  {
    GLuint shaderID = glCreateShader(GL_VERTEX_SHADER);

    const GLchar *arbSource = vtx_src.c_str();

    glShaderSource(shaderID, 1, (const GLchar **)&arbSource, 0);
    glCompileShader(shaderID);

    int compiled;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compiled);
    allIsOk = allIsOk && compiled;
    printShaderInfoLog(shaderID);

    if (compiled) glAttachShader(_programID, shaderID);
  }

  // fragment shader
  {
    GLuint shaderID = glCreateShader(GL_FRAGMENT_SHADER);

    const GLchar *arbSource = frag_src.c_str();

    glShaderSource(shaderID, 1, (const GLchar **)&arbSource, 0);
    glCompileShader(shaderID);
    int compiled;
    glGetShaderiv(shaderID, GL_COMPILE_STATUS, &compiled);

    allIsOk = allIsOk && compiled;
    printShaderInfoLog(shaderID);

    if (compiled) glAttachShader(_programID, shaderID);
  }

  glLinkProgram(_programID);

  int isLinked;
  glGetProgramiv(_programID, GL_LINK_STATUS, &isLinked);
  allIsOk   = allIsOk && isLinked;
  _is_valid = (isLinked == (int)GL_TRUE);
  if (allIsOk) printProgramInfoLog(_programID);

  checkErrorGL();

  return allIsOk;
}

bool Shader::reload()
{
  //std::string vtx_src  = loadSourceFromFile(_vertex_file);
  //std::string frag_src = loadSourceFromFile(_fragment_file);
  //return loadSources(vtx_src, frag_src);
  bool allOK = true;
  for (std::pair<int, GLuint> shader_obj : _obj_ID)
  {
    ShaderObject obj = ShaderObject(shader_obj.first);
    allOK &= loadFromFile(obj, _obj_file[obj]);
  }
  allOK = createShaderProgram();
  return allOK;
}

void Shader::activate(void) const
{
  checkErrorGL();
  assert(_is_valid);
  glUseProgram(_programID);
}

void Shader::deactivate(void) const
{
  glUseProgram(0);
}

int Shader::getUniformLocation(const char *name) const
{
  assert(_is_valid);
  return glGetUniformLocation(_programID, name);
}

int Shader::getAttribLocation(const char *name) const
{
  assert(_is_valid);
  return glGetAttribLocation(_programID, name);
}

void Shader::printProgramInfoLog(GLuint programID)
{
  int     infologLength = 0, charsWritten = 0;
  GLchar *infoLog;
  glGetProgramiv(programID, GL_INFO_LOG_LENGTH, &infologLength);
  if (infologLength > 0)
  {
    infoLog = new GLchar[infologLength];
    glGetProgramInfoLog(programID, infologLength, &charsWritten, infoLog);
    if (charsWritten > 0)
    {
      std::cerr << "Program info : \n" << infoLog << std::endl;
    }
    delete[] infoLog;
  }
}

void Shader::printShaderInfoLog(GLuint objectID)
{
  int     infologLength = 0, charsWritten = 0;
  GLchar *infoLog;
  glGetShaderiv(objectID, GL_INFO_LOG_LENGTH, &infologLength);
  if (infologLength > 0)
  {
    infoLog = new GLchar[infologLength];
    glGetShaderInfoLog(objectID, infologLength, &charsWritten, infoLog);
    if (charsWritten > 0) std::cerr << "Shader info : \n" << infoLog << std::endl;
    delete[] infoLog;
  }
}
}   // namespace gui
}   // namespace mrf