#include "mrf_baseUI.h"
#include "gl_helper.h"

#include <mrf_core/util/precision_timer.hpp>
#include <mrf_core/util/path.hpp>

#include <mrf_optix/optix_renderer.hpp>

#include <tinyxml2/tinyxml2.h>

namespace mrf
{
namespace gui
{
BaseUI::BaseUI()
  : _renderer(nullptr)
  , _current_illuminant(ILLUMINANT::D65)
  , _current_sensitivity(color::CIE_1931_2DEG)
  , _current_space(SPACE::SRGB)
  , _arcball(math::Vec2f(0.5f), 1.0f)
  , _frame_width(512)
  , _max_screen_width(0)
  , _frame_height(512)
  , _max_screen_height(0)
  , _mouse_prev_pos(math::Vec2f(0.f, 0.f))
  , _mouse_pressed(false)
  , _render_on_pause(false)
  , _rendering_time(0.f)
  , _total_rendering_time(0.f)
  , _samples_per_second(0.f)
  , _sample_count(0.)
  , _spatial_factor(1)
  , _max_spatial_factor(5)
  , _target_FPS(40)
  , _optimize_FPS(false)
  , _camera_changed(true)
  , _straf_speed(10.f)
  , _clearColor(1.f)
  , _quad_ready(false)
{}

BaseUI::~BaseUI()
{
  _shader_prog.clear();
  _texture_list.clear();
}

void BaseUI::setRenderer(rendering::Renderer *renderer)
{
  _renderer       = renderer;
  _camera_changed = true;
}

void BaseUI::initializeUI(rendering::Renderer *renderer, bool v_sync)
{
  //TODO later: if nullptr, create a full initialized renderer ? Or add a check to disable most events as long as no scene has been imported.
  if (renderer == nullptr)
    exit(-1);
  else
    _renderer = renderer;

  _frame_width = (_renderer->hasScene()) ? _renderer->getWidth() : 0;

  _frame_height = (_renderer->hasScene()) ? _renderer->getHeight() : 0;
#ifdef MRF_RENDERING_MODE_SPECTRAL
  _spectrum_size = static_cast<int>(_renderer->getSpectralWavelengths().size());
#else
  _spectrum_size = 1;
#endif
  initializeUIRenderingResources(v_sync);
}

void BaseUI::initializeUIRenderingResources(bool v_sync)
{
  createContext(width(), height(), v_sync);
#ifdef MRF_RENDERING_MODE_SPECTRAL
  //_texture_list.resize(ALL_TEX);
  _texture_list[OUTPUT_BUFFER] = Texture(_frame_width, _frame_height, _spectrum_size, GL_R32F, "output_buffer", true);
  _renderer->createBufferFromGLBuffer(
      _texture_list[TEXTURE::OUTPUT_BUFFER].getInternalBufferID(),
      _texture_list[TEXTURE::OUTPUT_BUFFER].getName(),
      _frame_width,
      _frame_height,
      1);
  initSensitivityBuffer();
  // initIlluminantBuffer();
  initWavelengthBuffer();
// computeSensitivityNorm();
#else
  //_texture_list.resize(ALL_TEX);
  _texture_list[OUTPUT_BUFFER]
      = Texture(_frame_width, _frame_height, _spectrum_size, GL_RGB32F_ARB, "output_buffer", true);
  _renderer->createBufferFromGLBuffer(
      _texture_list[TEXTURE::OUTPUT_BUFFER].getInternalBufferID(),
      _texture_list[TEXTURE::OUTPUT_BUFFER].getName(),
      _frame_width,
      _frame_height,
      3);
#endif

  _texture_list[PROCESS_BUFFER] = Texture(_frame_width, _frame_height, GL_RGBA32F_ARB, "display_buffer", false);

  mrf::util::Path current_path  = mrf::util::Path::getExecutionPath();
  std::string     pathToShaders = current_path.name() + std::string("shaders/");

  _shader_prog[DISPLAY] = new Shader;
  _shader_prog[DISPLAY]->loadFromFile(ShaderObject::VERTEX, pathToShaders + "display.vert");
  _shader_prog[DISPLAY]->loadFromFile(ShaderObject::FRAGMENT, pathToShaders + "display.frag");
  _shader_prog[DISPLAY]->createShaderProgram();
  _update_parameters[DISPLAY] = true;

  _shader_prog[PROCESS] = new Shader;
  _shader_prog[PROCESS]->setAdditionalHeader(
      ShaderObject::COMPUTE,
    "layout(binding = " + std::to_string(_texture_list[PROCESS_BUFFER].getTexUnit()) +", rgba32f) uniform image2D img_input;\n"
    "layout(local_size_x = 16, local_size_y = 16) in;\n ");
#ifdef MRF_RENDERING_MODE_SPECTRAL
  //_shader_prog[DISPLAY]->loadFromFile(ShaderObject::FRAGMENT, pathToShaders + "display_spectral.frag");
  _shader_prog[PROCESS]->loadFromFile(ShaderObject::COMPUTE, pathToShaders + "process_spectral.comp");
#else
  //_shader_prog[DISPLAY]->loadFromFile(ShaderObject::FRAGMENT, pathToShaders + "display_rgb.frag");
  _shader_prog[PROCESS]->loadFromFile(ShaderObject::COMPUTE, pathToShaders + "process_rgb.comp");
#endif
  _shader_prog[PROCESS]->createShaderProgram();
  _update_parameters[PROCESS] = true;
}

void BaseUI::run()
{
  mainLoop();

  glfwDestroyWindow(_window);
  glfwTerminate();
}

void BaseUI::printInteractiveCommandHelp()
{
  std::cout << "COMMANDS:" << std::endl;
  std::cout << "Tonemapping:" << std::endl;
  std::cout << "  i: increase exposure" << std::endl;
  std::cout << "  o: decrease exposure" << std::endl;
  std::cout << "  k: increase gammma" << std::endl;
  std::cout << "  l: decrease gamma" << std::endl;
  std::cout << "Camera:" << std::endl;
  std::cout << "  space_bar: Reset camera" << std::endl;
  std::cout << "  u: make head straight (aligned with (0,1,0))" << std::endl;
  std::cout << "  z: Rotate camera upward" << std::endl;
  std::cout << "  q: Rotate camera to the left" << std::endl;
  std::cout << "  s: Rotate camera downward" << std::endl;
  std::cout << "  d: Rotate camera to the right" << std::endl;
  std::cout << "  e: increase FOV" << std::endl;
  std::cout << "  r: decrease FOV" << std::endl;
  std::cout << "  left arrow: Go to previous camera" << std::endl;
  std::cout << "  right arrow: Go to next camera" << std::endl;
  std::cout << "  w: Save current camera" << std::endl;
  if (_renderer->hasEnvmap())
  {
    std::cout << "Envmap:" << std::endl;
    std::cout << "  c: Increase rotation theta" << std::endl;
    std::cout << "  v: Decrease rotation theta" << std::endl;
    std::cout << "  b: Increase rotation phi" << std::endl;
    std::cout << "  n: Decrease rotation phi" << std::endl;
  }
  std::cout << "Others:" << std::endl;
  std::cout << "  p: pause/unpause rendering" << std::endl;
  std::cout << "  h: print this help" << std::endl;
  std::cout << "  esc: quit" << std::endl;
}

void BaseUI::createContext(int w, int h, bool v_sync)
{
  if (!glfwInit()) exit(EXIT_FAILURE);

  //glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 4);
  //glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 5);
  //glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
  //glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GLFW_TRUE);
  glfwWindowHint(GLFW_RESIZABLE, GLFW_TRUE);

  //First create windows size using the low size
  _window = glfwCreateWindow(640, 480, "Malia Renderer", NULL, NULL);
  if (!_window)
  {
    glfwTerminate();
    exit(EXIT_FAILURE);
  }

  //Get decoration size, only useful when explicitly setting the window size.
  int window_width, window_height;
  glfwGetWindowSize(_window, &window_width, &window_height);
  glfwGetWindowFrameSize(_window, &_decoration_left, &_decoration_top, &_decoration_right, &_decoration_bottom);
  _decoration_width  = 0;
  _decoration_height = _decoration_top - _decoration_bottom;

  //Retrieve screen property to resize to the requested frame size.
  retrieveScreenProperties();
  resizeAppWindow(w, h, false);

  glfwMakeContextCurrent(_window);

#ifndef __APPLE__
  if (glewInit() != GLEW_OK)
  {
    fprintf(stderr, "Failed to initialize GLEW\n");
    exit(EXIT_FAILURE);
  }
#endif
  //v_sync should be int but as we do not need values other 0 (no v-sync) or 1, a bool is enough.
  glfwSwapInterval(v_sync);
  //glfwSetWindowPos(_window, 100, 100);

  printInteractiveCommandHelp();
}

void BaseUI::resizeAppWindow(int w, int h, bool resize_frame)
{
  bool update_cam = false;
  if (w > _max_screen_width - _decoration_width)
  {
    if (w == _frame_width)
    {
      _frame_width = _max_screen_width - _decoration_width;
      update_cam   = true;
    }
    else if (w > _frame_width)
    {
      size_t offset = w - _frame_width;
      _frame_width  = _max_screen_width - _decoration_width - offset;
      update_cam    = true;
    }
  }

  if (h > _max_screen_height - _decoration_height)
  {
    if (h == _frame_height)
    {
      _frame_height = _max_screen_height - _decoration_height;
      update_cam    = true;
    }
    else if (h > _frame_height)
    {
      size_t offset = h - _frame_height;
      _frame_height = _max_screen_height - _decoration_height - offset;
      update_cam    = true;
    }
  }

  if (update_cam) _renderer->getMainCamera()->sensor().setResolution(_frame_width, _frame_height);
  int w2 = width() + (update_cam ? _decoration_width : 0);
  int h2 = height() + (update_cam ? _decoration_height : 0);
  glfwSetWindowSize(_window, w2, h2);
  //glfwSetWindowSize(_window, w, h);
  if (resize_frame) resize(width(), height(), true);
}

void BaseUI::mainLoop()
{
  // Initialize GL state
  glMatrixMode(GL_PROJECTION);
  glLoadIdentity();
  glOrtho(0, 1, 0, 1, -1, 1);

  glMatrixMode(GL_MODELVIEW);
  glLoadIdentity();

  glViewport(0, 0, GLsizei(_frame_width), GLsizei(_frame_height));

  glClearColor(_clearColor.x(), _clearColor.y(), _clearColor.z(), _clearColor.w());

  // Start loop
  while (!glfwWindowShouldClose(_window))
  {
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    render();
    process();
    display();

    glfwSwapBuffers(_window);
    glfwPollEvents();
  }
}

void BaseUI::render()
{
  if (_render_on_pause == false && _renderer->hasScene())
  {
    updateCamera();

    float frame_time = _renderer->renderFrame();
    _rendering_time += frame_time;
    _total_rendering_time += frame_time;
    _samples_per_second = (float)_renderer->getNumFrame() / _total_rendering_time;
    _sample_count += (1.f / (_renderer->getSpatialFactor() * _renderer->getSpatialFactor()));


    if (_optimize_FPS)
    {
      int num_frame       = _renderer->getNumFrame();
      int spatial_factor  = _renderer->getSpatialFactor();
      int frame_frequence = 20;

      bool should_check_FPS = (num_frame > 0 && num_frame % frame_frequence == 0);   //Check interval.

      bool is_frame_slow = (frame_time > 5.f / _target_FPS);
      //True if frame_time is more than 5 time slower the (inverse) target FPS.

      bool is_FPS_below = (frame_frequence / _rendering_time < _target_FPS);
      //True if FPS over interval is below target.

      bool check_for_increase
          = spatial_factor < _max_spatial_factor && (is_frame_slow || (should_check_FPS && is_FPS_below));

      bool is_frame_fast = (frame_time < 0.2f / _target_FPS);
      bool is_FPS_above  = (frame_frequence / _rendering_time > 2.f * _target_FPS);
      //True if FPS over interval is above target.

      bool check_for_decrease = spatial_factor > 1 && (is_frame_fast || (should_check_FPS && is_FPS_above));

      if (check_for_increase)
      {
        //HARDCODED: Do not go below one every 16 pixels //TODO: argument for this ?
        mrf::gui::fb::Loger::getInstance()->info(
            "FPS(" + std::to_string(frame_frequence / _rendering_time)
            + ") is below target, increase subdivision factor");
        _renderer->setSpatialFactor(++spatial_factor);
        _update_parameters[PROCESS] = true;
      }
      else if (check_for_decrease)
      {
        mrf::gui::fb::Loger::getInstance()->info(
            "FPS(" + std::to_string(frame_frequence / _rendering_time)
            + ") is above the target, decrease factor if possible.");
        _renderer->setSpatialFactor(--spatial_factor);
        _update_parameters[PROCESS] = true;
      }
      if (should_check_FPS) _rendering_time = 0.f;
    }
    else
    {
      if (_renderer->getSpatialFactor() > 1)
      {
        _renderer->setSpatialFactor(1);
      }
    }

    _texture_list[TEXTURE::OUTPUT_BUFFER].updateTextureFromInternalBuffer();
  }
}

void BaseUI::process()
{
  _shader_prog[PROCESS]->activate();
  _texture_list[TEXTURE::OUTPUT_BUFFER].activate(_shader_prog[PROCESS]);
  _texture_list[TEXTURE::PROCESS_BUFFER].activateImage();

#ifdef MRF_RENDERING_MODE_SPECTRAL
  _texture_list[TEXTURE::SENSITIVITY_BUFFER].activate(_shader_prog[PROCESS]);
  _texture_list[TEXTURE::WAVELENGTH_BUFFER].activate(_shader_prog[PROCESS]);
#endif   // MRF_RENDERING_MODE_SPECTRAL

  if (_update_parameters[PROCESS])
  {
    setAllParameters(_shader_prog[PROCESS]);
    _update_parameters[PROCESS] = false;
  }
  else
    updateFrameParameters(_shader_prog[PROCESS]);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  //First convert to RGB
  glUniform1i(_shader_prog[PROCESS]->getUniformLocation("pass"), 0);
  glDispatchCompute(
      static_cast<int>(ceil(_renderer->getWidth() / 16.f + 0.5f)),
      static_cast<int>(ceil(_renderer->getHeight() / 16.f + 0.5f)),
      1);
  glUniform1i(_shader_prog[PROCESS]->getUniformLocation("pass"), 1);
  glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

#endif

  int group_x = static_cast<int>(ceil(_renderer->getWidth() / (16.f * _spatial_factor) + 0.5f));
  int group_y = static_cast<int>(ceil(_renderer->getHeight() / (16.f * _spatial_factor) + 0.5f));
  glDispatchCompute(group_x, group_y, 1);
  glMemoryBarrier(GL_SHADER_IMAGE_ACCESS_BARRIER_BIT);

  _texture_list[TEXTURE::OUTPUT_BUFFER].deactivate();
  _texture_list[TEXTURE::PROCESS_BUFFER].deactivateImage();
#ifdef MRF_RENDERING_MODE_SPECTRAL
  _texture_list[TEXTURE::SENSITIVITY_BUFFER].deactivate();
  _texture_list[TEXTURE::WAVELENGTH_BUFFER].deactivate();
#endif   // MRF_RENDERING_MODE_SPECTRAL

  _shader_prog[PROCESS]->deactivate();
}

void BaseUI::display()
{
  _shader_prog[DISPLAY]->activate();

  _texture_list[TEXTURE::PROCESS_BUFFER].activate(_shader_prog[DISPLAY]);

  if (_update_parameters[DISPLAY])
  {
    setAllParameters(_shader_prog[DISPLAY]);
    _update_parameters[DISPLAY] = false;
  }
  else
    updateFrameParameters(_shader_prog[DISPLAY]);

  displayQuad(_shader_prog[DISPLAY]);

  _texture_list[TEXTURE::PROCESS_BUFFER].deactivate();

  _shader_prog[DISPLAY]->deactivate();
}

void BaseUI::displayQuad(Shader *prog)
{
  if (!_quad_ready)
  {
    initQuad();
  }

  glBindVertexArray(_vao);

  glBindBuffer(GL_ARRAY_BUFFER, _vbo);

  int vertex_loc = prog->getAttribLocation("vtx_position");
  if (vertex_loc >= 0)
  {
    glVertexAttribPointer(vertex_loc, 4, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void *)0);
    glEnableVertexAttribArray(vertex_loc);
  }

  glDrawArrays(GL_TRIANGLES, 0, 6);

  if (vertex_loc >= 0) glDisableVertexAttribArray(vertex_loc);

  glBindVertexArray(0);

  checkErrorGL();
}

void BaseUI::initQuad()
{
  static const GLfloat vertex_buffer_data[]
      = {-1.0f, -1.0f, 0.f, 1.0f, -1.0f, 0.f, -1.0f, 1.0f, 0.f, 1.0f, -1.0f, 0.1f, 1.0f, 1.0f, 0.1f, -1.0f, 1.0f, 0.1f};

  glGenVertexArrays(1, &_vao);
  glGenBuffers(1, &_vbo);

  glBindVertexArray(_vao);

  glBindBuffer(GL_ARRAY_BUFFER, _vbo);
  glBufferData(GL_ARRAY_BUFFER, 2 * 18 * sizeof(GLfloat), vertex_buffer_data, GL_STATIC_DRAW);

  glBindVertexArray(0);

  _quad_ready = true;

  checkErrorGL();
}


void BaseUI::setAllParameters(Shader *prog)
{
  updateFrameParameters(prog);
  glUniform2f(
      prog->getUniformLocation("viewport"),
      static_cast<float>(_frame_width),
      static_cast<float>(_frame_height));
  glUniform1f(prog->getUniformLocation("gamma"), _renderer->getGamma());
  glUniform1f(prog->getUniformLocation("exposure"), _renderer->getExposure());

  if (_current_space == SRGB)
    glUniform1i(prog->getUniformLocation("is_srgb"), 1);
  else
    glUniform1i(prog->getUniformLocation("is_srgb"), 0);

#ifdef MRF_RENDERING_MODE_SPECTRAL
  // glUniform1i(_shader_prog->getUniformLocation("full_spectrum"), _full_spectrum_integration);

  if (_current_space == SRGB && _current_illuminant == D50)
    glUniformMatrix4fv(prog->getUniformLocation("xyzToRGB"), 1, false, mrf::color::XYZ_TO_SRGB_D50_MATRIX.ptr());
  else if (_current_space == SRGB && _current_illuminant == D65)
    glUniformMatrix4fv(prog->getUniformLocation("xyzToRGB"), 1, false, mrf::color::XYZ_TO_SRGB_D65_MATRIX.ptr());
  else if (_current_space == ARGB && _current_illuminant == D50)
    glUniformMatrix4fv(
        prog->getUniformLocation("xyzToRGB"),
        1,
        false,
        mrf::color::XYZ_TO_ADOBE_RGB_1998_D50_MATRIX.ptr());
  else if (_current_space == ARGB && _current_illuminant == D65)
    glUniformMatrix4fv(
        prog->getUniformLocation("xyzToRGB"),
        1,
        false,
        mrf::color::XYZ_TO_ADOBE_RGB_1998_D65_MATRIX.ptr());

  mrf::color::SpectrumConverter spectrum_converter((color::SensitivityCurveName)_current_sensitivity);
  glUniform1i(prog->getUniformLocation("curve_first_wavelength"), spectrum_converter.sensCurveFirstWavelength());
  glUniform1i(prog->getUniformLocation("curve_size"), static_cast<int>(spectrum_converter.sensCurveSize()));

  glUniform1i(
      prog->getUniformLocation("wavelength_size"),
      static_cast<int>(_renderer->getSpectralWavelengths().size()));
  glUniform1f(prog->getUniformLocation("inv_wavelength_size"), 1.f / _renderer->getSpectralWavelengths().size());

  int first_wave = _renderer->getSpectralWavelengths()[0];
  glUniform1i(prog->getUniformLocation("first_wave"), first_wave);

  int last_wave = _renderer->getSpectralWavelengths().back();
  glUniform1i(prog->getUniformLocation("last_wave"), last_wave);

  // glUniform1f(_shader_prog->getUniformLocation("curve_norm"), _curve_norm);
  // glUniform1f(_shader_prog->getUniformLocation("inv_curve_norm"), (_curve_norm > 0.f)? 1.f/_curve_norm : 1.f);
#endif
  checkErrorGL();
}


void BaseUI::updateFrameParameters(Shader *prog)
{
  int frame = _renderer->getNumFrame();
  //TODO use attribute instead?
  int spatial_factor = _renderer->getSpatialFactor();
  _spatial_factor    = spatial_factor;
  while (frame > 25 * (5 - _spatial_factor) * spatial_factor * spatial_factor && _spatial_factor > 1)
  {
    _spatial_factor -= 1;
    //_target_FPS = 20;
  }
  glUniform1i(prog->getUniformLocation("spatial_factor"), _spatial_factor);
  glUniform1f(prog->getUniformLocation("spp"), (float)(frame));
}

#ifdef MRF_RENDERING_MODE_SPECTRAL
void BaseUI::initSensitivityBuffer()
{
  mrf::color::SpectrumConverter spectrum_converter((color::SensitivityCurveName)_current_sensitivity);
  const size_t                  sensitivity_curve_size = spectrum_converter.sensCurveSize();

  float *curve = new float[sensitivity_curve_size * 4];
  for (size_t i = 0; i < sensitivity_curve_size; ++i)
  {
    curve[i * 4]     = spectrum_converter.xSensitivityAtIndex(i);
    curve[i * 4 + 1] = spectrum_converter.ySensitivityAtIndex(i);
    curve[i * 4 + 2] = spectrum_converter.zSensitivityAtIndex(i);
    curve[i * 4 + 3] = 0.f;
  }

  _texture_list[SENSITIVITY_BUFFER] = Texture(sensitivity_curve_size, GL_RGBA32F, "sensitivity_buffer", false, curve);
}

// void BaseUI::initIlluminantBuffer()
// {
//   int illuminant_size = (_current_illuminant == ILLUMINANT::D50)? mrf::radiometry::D_50_ARRAY_SIZE : mrf::radiometry::D_65_ARRAY_SIZE;

//   float* illuminant = new float[illuminant_size];
//   for (int i = 0; i < illuminant_size; ++i)
//     illuminant[i] = (_current_illuminant == ILLUMINANT::D50) ? mrf::radiometry::D_50_SPD[i] : mrf::radiometry::D_65_SPD[i];

//   _texture_list[ILLUMINANT_BUFFER] = Texture(illuminant_size, GL_R32F, "illuminant_buffer", false, illuminant);
// }

void BaseUI::initWavelengthBuffer()
{
  int size = static_cast<int>(_renderer->getSpectralWavelengths().size());

  float *wavelengths = (size > 0) ? new float[size] : nullptr;
  for (int i = 0; i < size; ++i)
    wavelengths[i] = static_cast<float>(_renderer->getSpectralWavelengths()[i]);

  _texture_list[WAVELENGTH_BUFFER] = Texture(size, GL_R32F, "wavelength_buffer", false, wavelengths);
}

// void BaseUI::computeSensitivityNorm()
// {
//   mrf::color::SpectrumConverter spectrum_converter((color::SensitivityCurveName)_current_sensitivity);
//   mrf::uint sensitivity_curve_size = spectrum_converter.sensCurveSize();

//   _curve_norm = 0.f;

//   for (int i = 0; i < sensitivity_curve_size; ++i)
//   {
//     _curve_norm += spectrum_converter.ySensitivityAtIndex(i);
//   }
// }
#endif

void BaseUI::resize(int w, int h, bool force)
{
  if (w == (int)width() && h == (int)height() && !force) return;

  _camera_changed = true;

  _frame_width  = w;
  _frame_height = h;

  mrf::gui::fb::Loger::getInstance()->debug("W: " + std::to_string(_frame_width) + " | " + std::to_string(w));
  mrf::gui::fb::Loger::getInstance()->debug("H: " + std::to_string(_frame_height) + " | " + std::to_string(h));

  _renderer->getMainCamera()->aspectRatio() = static_cast<float>(_frame_width) / static_cast<float>(_frame_height);
  _renderer->getMainCamera()->sensor().setResolution(_frame_width, _frame_height);

  _frame_width  = std::max<size_t>(_frame_width, 1u);
  _frame_height = std::max<size_t>(_frame_height, 1u);

  _renderer->readyBufferForUpdate(
      _texture_list[TEXTURE::OUTPUT_BUFFER].getInternalBufferID(),
      _texture_list[TEXTURE::OUTPUT_BUFFER].getName());
  _texture_list[TEXTURE::OUTPUT_BUFFER].resize(_frame_width, _frame_height, _spectrum_size);
  _renderer->resizeRenderTarget(_frame_width, _frame_height);
  _renderer->readyBufferForRendering(
      _texture_list[TEXTURE::OUTPUT_BUFFER].getInternalBufferID(),
      _texture_list[TEXTURE::OUTPUT_BUFFER].getName());

  _texture_list[TEXTURE::PROCESS_BUFFER].resize(_frame_width, _frame_height, _spectrum_size);

  glViewport(0, 0, GLsizei(_frame_width), GLsizei(_frame_height));
  _update_parameters[DISPLAY] = true;
  _update_parameters[PROCESS] = true;
}

void BaseUI::minimizeEvent(int iconified)
{
  //TODO CODE ME
  if (iconified)
  {
  }
  else
  {}
}

void BaseUI::mousePress(int button, int action, int /*mods*/)
{
  if (action == GLFW_PRESS)
  {
    double x, y;
    glfwGetCursorPos(_window, &x, &y);

    _mouse_button   = button;
    _mouse_pressed  = true;
    _mouse_prev_pos = math::Vec2f(static_cast<float>(x), static_cast<float>(y));
  }
  else if (action == GLFW_RELEASE)
  {
    _mouse_pressed = false;
  }
}

void BaseUI::mouseMotion(double x, double y)
{
  if (_mouse_pressed)
  {
    if (_mouse_button == GLFW_MOUSE_BUTTON_MIDDLE)
    {
      const float dx    = static_cast<float>(x - _mouse_prev_pos.x()) / static_cast<float>(_frame_width);
      const float dy    = static_cast<float>(y - _mouse_prev_pos.y()) / static_cast<float>(_frame_height);
      const float dmax  = fabsf(dx) > fabs(dy) ? dx : dy;
      const float scale = std::min<float>(dmax, 0.9f);
      _renderer->getMainCamera()->position(
          _renderer->getMainCamera()->position()
          + (_renderer->getMainCamera()->lookAt() - _renderer->getMainCamera()->position()) * scale);
      _camera_changed = true;
    }
    else if (_mouse_button == GLFW_MOUSE_BUTTON_LEFT)
    {
      const math::Vec2f from = {static_cast<float>(_mouse_prev_pos.x()), static_cast<float>(_mouse_prev_pos.y())};
      const math::Vec2f to   = {static_cast<float>(x), static_cast<float>(y)};

      const float dx = static_cast<float>(x - _mouse_prev_pos.x()) / static_cast<float>(_frame_width);
      const float dy = static_cast<float>(y - _mouse_prev_pos.y()) / static_cast<float>(_frame_height);


      const math::Vec2f a = {0.5f, 0.5f};
      const math::Vec2f b = {0.5f + dx, 0.5f + dy};

      _renderer->getMainCamera()->rotMat(_arcball.rotate(b, a).upperLeft());
      _camera_changed = true;
    }
    else if (_mouse_button == GLFW_MOUSE_BUTTON_RIGHT)
    {
      const float dx = -static_cast<float>(x - _mouse_prev_pos.x()) / static_cast<float>(_frame_width);
      const float dy = static_cast<float>(y - _mouse_prev_pos.y()) / static_cast<float>(_frame_height);

      auto camera_right = (_renderer->getMainCamera()->lookAt() - _renderer->getMainCamera()->position())
                              .normalize()
                              .cross(_renderer->getMainCamera()->up());

      auto straf = camera_right * dx * _straf_speed + _renderer->getMainCamera()->up() * dy * _straf_speed;

      _renderer->getMainCamera()->position(_renderer->getMainCamera()->position() + straf);
      _renderer->getMainCamera()->lookAt(_renderer->getMainCamera()->lookAt() + straf);
      _camera_changed = true;
    }

    _mouse_prev_pos = math::Vec2f(static_cast<float>(x), static_cast<float>(y));
  }
}

void BaseUI::mouseScroll(double /*xoffset*/, double yoffset)
{
  const float scale = (yoffset > 0) ? 0.05f : -0.05f;
  _renderer->getMainCamera()->position(
      _renderer->getMainCamera()->position()
      + (_renderer->getMainCamera()->lookAt() - _renderer->getMainCamera()->position()) * scale);
  _camera_changed = true;
}

void BaseUI::charPress(unsigned int codepoint)
{
  mrf::rendering::Camera *   main_camera      = _renderer->getMainCamera();
  mrf::rendering::CameraType main_camera_type = main_camera->getType();
  switch (codepoint)
  {
    case ('h'):
    {
      printInteractiveCommandHelp();
      break;
    }
    case ('d'):
    {
      const math::Vec2f a = {0.5f, 0.5f};
      const math::Vec2f b = {0.5f + 0.005f, 0.5f};

      main_camera->rotMat(_arcball.rotate(b, a).upperLeft());
      _camera_changed = true;
      break;
    }
    case ('q'):
    {
      const math::Vec2f a = {0.5f, 0.5f};
      const math::Vec2f b = {0.5f - 0.005f, 0.5f};

      main_camera->rotMat(_arcball.rotate(b, a).upperLeft());
      _camera_changed = true;
      break;
    }
    case ('s'):
    {
      const math::Vec2f a = {0.5f, 0.5f};
      const math::Vec2f b = {0.5f, 0.5f + 0.005f};

      main_camera->rotMat(_arcball.rotate(b, a).upperLeft());
      _camera_changed = true;
      break;
    }
    case ('z'):
    {
      const math::Vec2f a = {0.5f, 0.5f};
      const math::Vec2f b = {0.5f, 0.5f - 0.005f};

      main_camera->rotMat(_arcball.rotate(b, a).upperLeft());
      _camera_changed = true;
      break;
    }
    case ('p'):
    {
      _render_on_pause = !_render_on_pause;
      break;
    }
    case ('u'):   //make head straight
    {
      makeHeadStraight();
      break;
    }
    case ('e'):
    {
      if (main_camera_type == mrf::rendering::CameraType::Pinhole)
      {
        mrf::rendering::Pinhole *cam = dynamic_cast<mrf::rendering::Pinhole *>(main_camera);
        cam->fovy(cam->fovy() + 1.0f);
        _camera_changed = true;
        mrf::gui::fb::Loger::getInstance()->info("FOV = ", cam->fovy());
      }
      else if (main_camera_type == mrf::rendering::CameraType::ThinLens)
      {
        mrf::rendering::ThinLens *cam = dynamic_cast<mrf::rendering::ThinLens *>(main_camera);
        cam->fovy(cam->fovy() + 1.0f);
        _camera_changed = true;
        mrf::gui::fb::Loger::getInstance()->info("FOV = ", cam->fovy());
      }


      break;
    }
    case ('r'):
    {
      if (main_camera_type == mrf::rendering::CameraType::Pinhole)
      {
        mrf::rendering::Pinhole *cam = dynamic_cast<mrf::rendering::Pinhole *>(main_camera);
        cam->fovy(cam->fovy() - 1.0f);
        _camera_changed = true;
        mrf::gui::fb::Loger::getInstance()->info("FOV = ", cam->fovy());
      }
      else if (main_camera_type == mrf::rendering::CameraType::ThinLens)
      {
        mrf::rendering::ThinLens *cam = dynamic_cast<mrf::rendering::ThinLens *>(main_camera);
        cam->fovy(cam->fovy() - 1.0f);
        _camera_changed = true;
        mrf::gui::fb::Loger::getInstance()->info("FOV = ", cam->fovy());
      }
    }
    case ('i'):
    {
      _renderer->setExposure(_renderer->getExposure() + 0.1f);
      std::cout << "exposure = " << _renderer->getExposure() << std::endl;

      break;
    }
    case ('o'):
    {
      _renderer->setExposure(_renderer->getExposure() - 0.1f);
      std::cout << "exposure = " << _renderer->getExposure() << std::endl;

      break;
    }
    case ('k'):
    {
      _renderer->setGamma(_renderer->getGamma() + 0.1f);
      std::cout << "Gamma = " << _renderer->getGamma() << std::endl;

      break;
    }
    case ('l'):
    {
      _renderer->setGamma(_renderer->getGamma() - 0.1f);
      std::cout << "Gamma = " << _renderer->getGamma() << std::endl;

      break;
    }
    case ('c'):
    {
      mrf::gui::fb::Loger::getInstance()->info("envmap rot theta = ", _renderer->increaseRotationTheta(5.f));
      _camera_changed = true;
      break;
    }
    case ('v'):
    {
      mrf::gui::fb::Loger::getInstance()->info("envmap rot theta = ", _renderer->increaseRotationTheta(-5.f));
      _camera_changed = true;
      break;
    }
    case ('b'):
    {
      mrf::gui::fb::Loger::getInstance()->info("envmap rot phi = ", _renderer->increaseRotationPhi(5.f));
      _camera_changed = true;
      break;
    }
    case ('n'):
    {
      mrf::gui::fb::Loger::getInstance()->info("envmap rot phi = ", _renderer->increaseRotationPhi(-5.f));
      _camera_changed = true;
      break;
    }
    case ('w'):   //save camera to file
    {
      saveCameraToFile();
      break;
    }
    default:
    {
      //std::cout << "pressed key=" << k << std::endl;
      break;
    }
  }
}

void BaseUI::keyboardPress(int key, int /*scancode*/, int action, int /*mods*/)
{
  if (action == GLFW_PRESS)
  {
    switch (key)
    {
      case (GLFW_KEY_ESCAPE):   //ESC
      {
        closeFunction();
        exit(0);
      }
      case (GLFW_KEY_SPACE):   //SPACEBAR
      {
        resetCamera();
        break;
      }
      default:
      {
        break;
      }
    }
  }
}

void BaseUI::updateCamera()
{
  if (_camera_changed)   // reset accumulation
  {
    _sample_count         = 0;
    _rendering_time       = 0.f;
    _total_rendering_time = 0.f;
    _renderer->updateCameraParameters();
    //_target_FPS     = 40;
    _spatial_factor = _renderer->getSpatialFactor();

    _renderer->resetRendering(true);
    //_renderer->setMaxPathLength(10);
    //if (_spatial_factor > 1)
    //{
    //  if (_mouse_pressed)
    //  {
    //    _renderer->setMaxPathLength(1);
    //  }
    //}
  }
  _camera_changed = false;
}

void BaseUI::resetCamera()
{
  _renderer->resetCamera();
  _frame_width    = _renderer->getWidth();
  _frame_height   = _renderer->getHeight();
  _camera_changed = true;
}

void BaseUI::makeHeadStraight()
{
  _renderer->makeHeadStraight();
  _camera_changed = true;
}

void BaseUI::previousCamera()
{
  _renderer->previousCamera();
  _frame_width    = _renderer->getWidth();
  _frame_height   = _renderer->getHeight();
  _camera_changed = true;
}

void BaseUI::nextCamera()
{
  _renderer->nextCamera();
  _frame_width    = _renderer->getWidth();
  _frame_height   = _renderer->getHeight();
  _camera_changed = true;
}

bool BaseUI::saveCameraToFile()
{
  //fov, camera->aspectRatio(), camera_eye, camera_lookat, camera_up, camera_rotate
  tinyxml2::XMLDocument xmlDoc;

  tinyxml2::XMLNode *pRoot = xmlDoc.NewElement("cameras");
  xmlDoc.InsertFirstChild(pRoot);

  mrf::rendering::Camera *   main_camera      = _renderer->getMainCamera();
  mrf::rendering::CameraType main_camera_type = main_camera->getType();

  auto cam_elem = xmlDoc.NewElement("camera");
  cam_elem->SetAttribute("id", "test");
  cam_elem->SetAttribute("type", mrf::rendering::CameraType2string(main_camera_type).c_str());

  auto pos_elem = xmlDoc.NewElement("position");
  pos_elem->SetAttribute("x", main_camera->position().x());
  pos_elem->SetAttribute("y", main_camera->position().y());
  pos_elem->SetAttribute("z", main_camera->position().z());

  auto lookat_elem = xmlDoc.NewElement("lookat");
  lookat_elem->SetAttribute("x", main_camera->lookAt().x());
  lookat_elem->SetAttribute("y", main_camera->lookAt().y());
  lookat_elem->SetAttribute("z", main_camera->lookAt().z());

  auto up_elem = xmlDoc.NewElement("up");
  up_elem->SetAttribute("x", main_camera->up().x());
  up_elem->SetAttribute("y", main_camera->up().y());
  up_elem->SetAttribute("z", main_camera->up().z());

  cam_elem->InsertEndChild(pos_elem);
  cam_elem->InsertEndChild(lookat_elem);
  cam_elem->InsertEndChild(up_elem);

  switch (main_camera_type)
  {
    case mrf::rendering::CameraType::Pinhole:
    {
      mrf::rendering::Pinhole *cam      = dynamic_cast<mrf::rendering::Pinhole *>(main_camera);
      auto                     fov_elem = xmlDoc.NewElement("fovy");
      fov_elem->SetAttribute("value", cam->fovy());
      cam_elem->InsertEndChild(fov_elem);
      break;
    }
    case mrf::rendering::CameraType::ThinLens:
    {
      mrf::rendering::ThinLens *cam      = dynamic_cast<mrf::rendering::ThinLens *>(main_camera);
      auto                      fov_elem = xmlDoc.NewElement("fovy");
      fov_elem->SetAttribute("value", cam->fovy());

      auto aperture_elem = xmlDoc.NewElement("aperture");
      aperture_elem->SetAttribute("value", cam->aperture());

      auto focus_distance_elem = xmlDoc.NewElement("focus_distance");
      focus_distance_elem->SetAttribute("value", cam->focalPlaneDistance());

      cam_elem->InsertEndChild(fov_elem);
      cam_elem->InsertEndChild(aperture_elem);
      cam_elem->InsertEndChild(focus_distance_elem);
      break;
    }
    case mrf::rendering::CameraType::Orthographic:
    {
      mrf::rendering::Orthographic *cam = dynamic_cast<mrf::rendering::Orthographic *>(main_camera);

      break;
    }
    case mrf::rendering::CameraType::Gonio:
    {
      break;
    }
    case mrf::rendering::CameraType::Environment:
    {
      mrf::rendering::Environment *cam = dynamic_cast<mrf::rendering::Environment *>(main_camera);

      auto theta_elem = xmlDoc.NewElement("theta");
      theta_elem->SetAttribute("value", cam->theta());

      auto phi_elem = xmlDoc.NewElement("phi");
      phi_elem->SetAttribute("value", cam->phi());

      cam_elem->InsertEndChild(theta_elem);
      cam_elem->InsertEndChild(phi_elem);
      break;
    }
    default:
      break;
  }


  auto sensor = xmlDoc.NewElement("sensor");
  sensor->SetAttribute("spp", main_camera->sensorSampler().spp());

  auto resolution = xmlDoc.NewElement("resolution");
  resolution->SetAttribute("height", int(_frame_height));
  resolution->SetAttribute("width", int(_frame_width));

  auto sensor_size = xmlDoc.NewElement("size");
  sensor_size->SetAttribute("height", main_camera->sensor().size().y());
  sensor_size->SetAttribute("width", main_camera->sensor().size().x());
  auto distance = xmlDoc.NewElement("distance");
  distance->SetAttribute("value", 0.1590);

  sensor->InsertEndChild(resolution);
  sensor->InsertEndChild(sensor_size);
  sensor->InsertEndChild(distance);

  cam_elem->InsertEndChild(sensor);
  pRoot->InsertEndChild(cam_elem);

  tinyxml2::XMLError eResult = xmlDoc.SaveFile(_camera_output_file.c_str());

  return (eResult == tinyxml2::XML_SUCCESS) ? true : false;
}

void BaseUI::dropEvent(int count, const char **paths)
{
  std::string scene;
  if (count > 1)
    mrf::gui::fb::Loger::getInstance()->warn("Trying to load multiple scene. Only the first valid one will be loaded.");
  for (int i = 0; i < count; i++)
  {
    scene = std::string(paths[i]);

    mrf::gui::fb::Loger::getInstance()->info("Attempting to load:");
    mrf::gui::fb::Loger::getInstance()->info(scene);

    if (checkScene(scene))
    {
      resizeAppWindow(width(), height());
      break;
    }
  }
}

void BaseUI::closeFunction()
{
  _renderer->shutdown();
  delete _renderer;
  _renderer = nullptr;
}

bool BaseUI::checkScene(std::string scene)
{
  std::string ext;
  util::StringParsing::getFileExtension(scene, ext);
  if (ext == "msf")
  {
    std::ifstream f(scene.c_str());
    if (f.good())
    {
      if (_scene_file == scene)
      {
        mrf::gui::fb::Loger::getInstance()->info("Requested scene is already loaded.");
        return true;
      }
      _scene_file = scene;
      _renderer->freeSceneResources();
      std::string cam_file, dir;
      util::StringParsing::fileNameFromAbsPathWithoutExtension(_scene_file.c_str(), cam_file);
      util::StringParsing::getDirectory(_scene_file.c_str(), dir);
      cam_file = dir + cam_file + ".mcf";

      _renderer->importMrfScene(_scene_file, cam_file);

      if (!_renderer->hasScene()) return false;
      _frame_width  = _renderer->getWidth();
      _frame_height = _renderer->getHeight();
#ifdef MRF_RENDERING_MODE_SPECTRAL
      _texture_list[OUTPUT_BUFFER]
          = Texture(_frame_width, _frame_height, _spectrum_size, GL_R32F, "output_buffer", true);
      _renderer->createBufferFromGLBuffer(
          _texture_list[TEXTURE::OUTPUT_BUFFER].getInternalBufferID(),
          _texture_list[TEXTURE::OUTPUT_BUFFER].getName(),
          _frame_width,
          _frame_height,
          1);
#else
      _texture_list[OUTPUT_BUFFER]
          = Texture(_frame_width, _frame_height, _spectrum_size, GL_RGB32F_ARB, "output_buffer", true);
      _renderer->createBufferFromGLBuffer(
          _texture_list[TEXTURE::OUTPUT_BUFFER].getInternalBufferID(),
          _texture_list[TEXTURE::OUTPUT_BUFFER].getName(),
          _frame_width,
          _frame_height,
          3);
#endif
      _renderer->updateCameraParameters();
      _camera_changed             = true;
      _update_parameters[DISPLAY] = true;
      _update_parameters[PROCESS] = true;
      return true;
    }
  }
  else
  {
    mrf::gui::fb::Loger::getInstance()->info(scene + " is not a msf file.");
  }

  return false;
}

void BaseUI::retrieveScreenProperties()
{
  //Retrieve monitors.
  int           count;
  GLFWmonitor **monitors = glfwGetMonitors(&count);

  //Retrieve the work area of every one.
  for (int i = 0; i < count; ++i)
  {
    //const GLFWvidmode *mode = glfwGetVideoMode(monitors[i]);
    int xpos, ypos, width, height;
    glfwGetMonitorWorkarea(monitors[i], &xpos, &ypos, &width, &height);

    _max_screen_height = std::max(_max_screen_height, (size_t)(height));
    _max_screen_width += (size_t)(width);
  }

  mrf::gui::fb::Loger::getInstance()->debug("Max screen height: " + std::to_string(_max_screen_height));
  mrf::gui::fb::Loger::getInstance()->debug("Max screen width: " + std::to_string(_max_screen_width));
}

}   // namespace gui
}   // namespace mrf
