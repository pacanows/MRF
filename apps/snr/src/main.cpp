/**
 * Simple SNR tool
 * ---------------
 * Compute the SNR (Signal to Noise Ratio) from a reference with a target.
 * Uses the formula from: http://bigwww.epfl.ch/sage/soft/snr/
 *
 * Author: Alban Fichet <alban.fichet@gmx.fr>
 * Year: 2021
 */

#include <iostream>
#include <string>

#include <mrf_core/image/image_formats.hpp>
#include <mrf_core/util/prog_options.hpp>

#define PRGM_NAME "snr"

#define CMD_FLAG_REF "ref"
#define CMD_FLAG_CMP "cmp"

struct CommandLine
{
  std::string filename_reference;
  std::string filename_compare;
};

bool process_command_line_options(int argc, char **argv, CommandLine &cmd)
{
  mrf::util::ProgOptions snr_options(PRGM_NAME);

  snr_options.addOption(CMD_FLAG_REF, mrf::util::ProgArg(true, "{string} image to use as a reference", 1));
  snr_options.addOption(CMD_FLAG_CMP, mrf::util::ProgArg(true, "{string} image to use for snr computation", 1));

  if (!mrf::util::validateProgramOptions(argc, argv, snr_options))
  {
    throw std::runtime_error("command line options : bad options error");
  }

  if (!snr_options.hasOption(CMD_FLAG_REF) || !snr_options.hasOption(CMD_FLAG_CMP)
      || !snr_options.option(CMD_FLAG_REF).wasProvided() || !snr_options.option(CMD_FLAG_CMP).wasProvided())
  {
    throw std::runtime_error("command line options : missing mandatory options");
  }

  snr_options.option(CMD_FLAG_REF).paramValue(0, cmd.filename_reference);
  snr_options.option(CMD_FLAG_CMP).paramValue(0, cmd.filename_compare);

  return true;
}

int main(int argc, char *argv[])
{
  CommandLine cmd;

  try
  {
    if (process_command_line_options(argc, argv, cmd))
    {
      // Sanity check: spectral and spectral
      if (mrf::image::isSpectralImage(cmd.filename_reference) && !mrf::image::isSpectralImage(cmd.filename_compare))
      {
        throw std::runtime_error("Trying to compare spectral and not spectral image");
      }

      // Now we do not support colored images
      if (!mrf::image::isSpectralImage(cmd.filename_reference))
      {
        throw std::runtime_error("Currently only supporting spectral images");
      }

      // We can load two spectral images from the provided filenames
      std::unique_ptr<mrf::image::UniformSpectralImage> reference = mrf::image::load(cmd.filename_reference);
      std::unique_ptr<mrf::image::UniformSpectralImage> compare   = mrf::image::load(cmd.filename_compare);

      // Sanity check: does the dimensions match?
      if (reference->wavelengths().size() != compare->wavelengths().size() || reference->width() != compare->width()
          || reference->height() != compare->height())
      {
        throw std::runtime_error("Images dimensions mismatch");
      }

      // Sanity check: does the wavelengths match?
      for (size_t i = 0; i < reference->wavelengths().size(); i++)
      {
        if (reference->wavelengths()[i] != compare->wavelengths()[i])
        {
          throw std::runtime_error("Wavelengths mismatch");
        }
      }

      // Now that the images have the same dimensions and same wavelengths,
      // we can compute the SNR.
      // http://bigwww.epfl.ch/sage/soft/snr/
      double sumRefSquared  = 0;
      double sumDiffSquared = 0;

      for (size_t wl = 0; wl < reference->wavelengths().size(); wl++)
      {
        const mrf::data_struct::Array2D &curr_ref_wl = reference->data()[wl];
        const mrf::data_struct::Array2D &curr_cmp_wl = compare->data()[wl];

        for (size_t y = 0; y < reference->height(); y++)
        {
          for (size_t x = 0; x < reference->width(); x++)
          {
            const double ref  = curr_ref_wl(y, x);
            const double cmp  = curr_cmp_wl(y, x);
            const double diff = ref - cmp;

            sumRefSquared += ref * ref;
            sumDiffSquared += diff * diff;
          }
        }
      }

      // Now display SNR in dB
      const double snr    = sumRefSquared / sumDiffSquared;
      const double snr_dB = 10. * std::log10(snr);

      std::cout << "SNR = " << snr_dB << " dB" << std::endl;
    }
  }
  catch (std::runtime_error &e)
  {
    std::cerr << e.what() << std::endl;
  }


  return 0;
}