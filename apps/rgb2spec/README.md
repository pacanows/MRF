

# RGB2SPEC application


rgb2spec is a small utility that convert RGB color to spectral
distribution.

## Input/Output data

 Input can be either a
 - triple (r,g,b) passed on the command line
 - an HDR image 
 - an LDR image

 When passing a single color on the commande line rgb2spec will
save a text file that contains the spectral distribution

 When passing an image, the output will be a spectral image (.hdr/.raw
 format), which can be directly open with the SpectralViewer.


## Command line Options 

 * --reflectivity : the color is supposed to represent a value
                    associated with BRDF or reflectivy. Therefore, the spectral curve
                    values should be in the range of [0,1]

 * -icolorspace COLORSPACE_NAME : input color space. This forces
    rgb2spec to interpret rgb colors to belong to the specified
    COLORSPACE_NAME.

     By default, the input (r,g,b) color is supposed to be in the
     CIE-RGB linear space.

 * -illuminant ILLUMINANT_NAME : This option only available when
    --reflectivity is present. This option specifies a specific
     illuminant spectral distribution under which the color is supposed to
     be observed. By default the nentral (E illuminant) is assumed.



 
