/**
 * @file rgb2spec.hpp
 * @author Romain Pacanowski
 * @brief
 * @version 0.1
 * @date 2018-10-03
 *
 * @copyright Copyright CNRS (c) 2019, 2018
 *
 */

#pragma once

//Quadprog is always use for BRDF Reflectivity curve

//#define USE_QUADPROG_CPP_FOR_ILL_SPECTRUM
#ifndef USE_QUADPROG_CPP_FOR_ILL_SPECTRUM
#  define USE_LEAST_SQUARE_FOR_ILL_SPECTRUM
#else
#  undef USE_LEAST_SQUARE_FOR_ILL_SPECTRUM
#endif

#define RGB2SPEC_ENABLE_CHECK
//#define RGB2SPEC_TRACE


#include "rgb2spec_brdf.hpp"

//STL
#include <iterator>
#include <numeric>
#include <exception>

// MRF
#include <mrf_core/radiometry/spectrum.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>
#include <mrf_core/color/sensitivity_curve_cie_1931_2deg.hpp>
#include <mrf_core/util/cpu_memory.hpp>
#include <mrf_core/util/precision_timer.hpp>

using mrf::uint;

// lOCAL
#include <QuadProgPP.h>

//EXTERMALS
#include <Eigen/Eigen>
#include "nnls.h"

/**
 * This version of the function assumes a 1 nanometer resolution
 */
// inline void sensitivity_curves_to_matrix(Eigen::Ref<Eigen::MatrixXd> sense_matrix,
//                                          mrf::color::SpectrumConverter const &sc )
// {
//   using namespace mrf::color;

//   mrf::math::Vec3f current_xyz_value;
//   float luminance_normalization_cte = 0.0f;

//   float accu_x = 0.0f;
//   float accu_z = 0.0f;
//   for (size_t i = 0; i < sc.nbWavelengths(); i++)
//   {
//     //    std::cout << " i = " << std::endl;
//     current_xyz_value = sc.xyzValuesAtWavelengthIndex(i);


//     //First Row
//     sense_matrix(0, i) = current_xyz_value(0);
//     //Second Row
//     sense_matrix(1, i) = current_xyz_value(1);
//     //Third Row
//     sense_matrix(2, i) = current_xyz_value(2);

//     luminance_normalization_cte += sense_matrix(1, i);
//     accu_x += sense_matrix(0, i);
//     accu_z += sense_matrix(2, i);
//   }

//   luminance_normalization_cte = 1.0 / luminance_normalization_cte;

//   std::cout << "[INFO] Case BRDF?  luminance_normalization_cte: " << luminance_normalization_cte << std::endl;
//   std::cout << " Max possible value for X = " << accu_x * luminance_normalization_cte <<  "  max value for Z = " << accu_z * luminance_normalization_cte << std::endl;

//   sense_matrix *= luminance_normalization_cte;

// }

inline void sensitivity_curves_to_matrix(
    Eigen::Ref<Eigen::MatrixXd>          sense_matrix,
    mrf::color::SpectrumConverter const &sc,
    std::vector<uint> const &            requested_wavelengths)
{
  using namespace std;
  using namespace mrf::color;


  mrf::math::Vec3f current_xyz_value;

  float const integration_cte = (requested_wavelengths[requested_wavelengths.size() - 1] - requested_wavelengths[0])
                                / static_cast<float>(requested_wavelengths.size() - 1);

  std::cout << "[INFO] Number of Requested Wavelengths is : " << requested_wavelengths.size() << std::endl
            << " [INFO] first wavelength, last wavelength = " << requested_wavelengths[0] << " "
            << requested_wavelengths[requested_wavelengths.size() - 1] << endl;

#ifdef DEBUG
  std::cout << "[DEBUG] Integration constant = " << integration_cte;
#endif

  double luminance_normalization_cte = 0.0;

  double accu_x = 0.0;
  double accu_z = 0.0;

  for (size_t i = 0; i < requested_wavelengths.size(); i++)
  {
    uint const requested_wav = requested_wavelengths[i];

    current_xyz_value = sc.xyzValuesAtWavelength(requested_wav);

    //First Row
    sense_matrix(0, i) = current_xyz_value(0);
    //Second Row
    sense_matrix(1, i) = current_xyz_value(1);
    //Third Row
    sense_matrix(2, i) = current_xyz_value(2);

    luminance_normalization_cte += sense_matrix(1, i);
    accu_x += sense_matrix(0, i);
    accu_z += sense_matrix(2, i);
  }

  luminance_normalization_cte = 1.0 / luminance_normalization_cte;

  std::cout << "[INFO] Case Emissivity  luminance_normalization_cte: " << luminance_normalization_cte << std::endl;
  //std::cout << " Max possible value for X = " << integration_cte* accu_x * luminance_normalization_cte <<  "  max value for Z = " << integration_cte*accu_z *  luminance_normalization_cte << std::endl;

  sense_matrix *= integration_cte * luminance_normalization_cte;
}

inline void prepare_quadratic_prog(
    mrf::color::SpectrumConverter const &sc,
    std::vector<uint> const &            requested_wavelengths,
    Eigen::MatrixXd &                    G,
    Eigen::VectorXd &                    g0,
    Eigen::MatrixXd &                    CE,
    Eigen::MatrixXd &                    CI_matrix,
    Eigen::VectorXd &                    ci0,
    Eigen::VectorXd &                    pre_solution)
{
  //Forcing to solve Spectrum at 1nm resolution
  // between 380 and 830
  //size_t const SIZE_SENSITIVITY_CIE_CURVES = sc.nbWavelengths();
  size_t const SIZE_SENSITIVITY_CIE_CURVES = requested_wavelengths.size();

  G = Eigen::MatrixXd::Identity(SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES);


  g0 = Eigen::VectorXd::Zero(SIZE_SENSITIVITY_CIE_CURVES);

  //Constraints Equality
  CE = Eigen::MatrixXd::Zero(3, SIZE_SENSITIVITY_CIE_CURVES);
  sensitivity_curves_to_matrix(CE, sc, requested_wavelengths);
  CE.transposeInPlace();

  // Enforce A positive Solution
  //CI^T*X + ci0 >= 0
  CI_matrix = Eigen::MatrixXd::Identity(SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES);

  //ci0 = Eigen::VectorXd::Zero(SIZE_SENSITIVITY_CIE_CURVES);
  ci0 = Eigen::VectorXd::Constant(SIZE_SENSITIVITY_CIE_CURVES, 0.000001);   // Minimal value, epsilon

  pre_solution = Eigen::VectorXd::Zero(SIZE_SENSITIVITY_CIE_CURVES);
}


/**
 * @brief Converts a RGB triple considered as illumination value to a Spectrum
 *
 * Remark : For illuminant there is no normalization term
 * X = \int x(\lambda) E(\lambda) d\lambda
 * Y = \int y(\lambda) E(\lambda) d\lambda
 * Z = \int z(\lambda) E(\lambda) d\lambda
 *
 *  where
 *  - x(\lambda), y(\lambda), z(\lambda) are sensitiviy curves
 *  - E(\lamda) the emissiion spectrum
 *  - [X,Y,Z] or the values of the color in CIE-XYZ color space
 *
 */
inline Eigen::VectorXd rgb2spec_for_ill(mrf::color::Color const &a_color, mrf::color::SpectrumConverter const &sc)
{
  using namespace std;
  using namespace mrf::color;
  using namespace mrf::math;

  mrf::radiometry::Spectrum result;

  // Convert from RGB to XYZ
  // Step 1: Assume  CIE-RGB (Neutral Illuminant) and convert to CIE-XYZ?
  //Vec3f const xyz_color =  a_color;

  std::cout << "[INFO] Solving with Quadratic Programming " << std::endl;
  std::cout << "[INFO] Input Color in RGB Space " << a_color << std::endl;

  mrf::math::Mat4f const &rgb_to_xyz = mrf::color::getRGBtoXYZMat(mrf::color::CIE_RGB, mrf::color::E);
  mrf::math::Vec4f const  xyz_color  = rgb_to_xyz * a_color;

  std::cout << "[INFO] Input Color in XYZ Space " << xyz_color << std::endl;

  // Target Equality Constraints
  Eigen::VectorXd ce0(3);
  ce0(0) = xyz_color(0);
  ce0(1) = xyz_color(1);
  ce0(2) = xyz_color(2);


  //Vector for the solution
  Eigen::VectorXd solution;

  //Constraing Equality
  Eigen::MatrixXd CE;

  mrf::util::PrecisionTimer ptimer;

  std::vector<uint> requested_wavelengths = sc.wavelengths();

#ifdef USE_QUADPROG_CPP_FOR_ILL_SPECTRUM
  Eigen::MatrixXd sense_matrix;
  Eigen::MatrixXd G;
  Eigen::VectorXd g0;

  Eigen::MatrixXd CI_matrix;
  Eigen::VectorXd ci0;

  ptimer.start();
  prepare_quadratic_prog(sc, requested_wavelengths, sense_matrix, G, g0, CE, CI_matrix, ci0, solution);


  //QuadProg Sover call
  double const cost_returned = QuadProgPP::solve_quadprog(G, g0, CE, -ce0, CI_matrix, -ci0, solution);
  ptimer.stop();
  ptimer.reset();
  cout << "[INFO] Inverse problem solved in  " << ptimer.elapsed() * 1000 << "ms"
       << " with returned cost : " << cost_returned << endl;


#  ifdef RGB2SPEC_ENABLE_CHECK
  if (std::isinf(cost_returned))
  {
    //cout << " SOLVER DID NOT FIND ANY SOLUTION " << endl;
    throw runtime_error("COULD NOT SOLVE PROBLEM WITH QUADRATIC PROGRAMMING");
  }
#  endif
  //cout <<" SOLUTION FOUND with cost: " << cost_returned << endl;
#elif defined(USE_LEAST_SQUARE_FOR_ILL_SPECTRUM)

  std::cout << "[INFO] Solving  With Least Square " << std::endl;
  ptimer.start();
  CE = Eigen::MatrixXd::Zero(3, sc.nbWavelengths());


  sensitivity_curves_to_matrix(CE, sc, requested_wavelengths);

  Eigen::MatrixXd const transpose_CE     = CE.transpose();
  Eigen::Matrix3d const inverse_CE_tCE   = (CE * transpose_CE).inverse();
  Eigen::MatrixXd const trans_inv_CE_tCE = transpose_CE * inverse_CE_tCE;

#  ifdef RGB2SPEC_ENABLE_CHECK
  std::cout << "  To debug inverse_CE_tCE_" << std::endl;
  std::cout << inverse_CE_tCE << std::endl;
#  endif

  solution = trans_inv_CE_tCE * ce0;

  ptimer.stop();
  cout << "[INFO] Inverse problem solved with LEast Square in  " << ptimer.elapsed() * 1000 << "ms" << endl;

  return solution;

#else
// USE non-negative least-square ?
#  ifdef USE_NNLS_FOR_ILL_SPECTRUM

  CE = Eigen::MatrixXd::Zero(3, sc.nbWavelengths());
  sensitivity_curves_to_matrix(CE, sc, requested_wavelengths);

  Eigen::NNLS<Eigen::MatrixXd> nonneg_lss_prob(CE);
  ptimer.start();
  bool const solution_found = Eigen::NNLS<Eigen::MatrixXd>::solve(CE, ce0, solution);
  // bool const solution_found_nnls = nonneg_lss_prob.solve(-ce0);
  ptimer.stop();
  cout << "[INFO] Inverse problem solved With Non-negative LEast Square in  " << ptimer.elapsed() * 1000 << "ms"
       << endl;
#  endif
#endif


  return solution;
}

/**
 * @brief Converts an RGB color to an illumination spectrum
 *
 * @param a_color :  a RGB color
 * @param result : The Spectrum found
 * @param curve_specs : The type CIE XYZ sensitivy curves (see mrf::color::SensitivityCurveName)
 *
 * @throw : throws a runtime error if the problem was not solved
 */
void convert_rgb_to_ill_spectrum(
    mrf::color::Color const &        a_color,
    mrf::radiometry::Spectrum &      result,
    mrf::color::SensitivityCurveName curve_specs = mrf::color::CIE_1931_2DEG)
{
  // Allocate the proper SpectrumConverter
  mrf::color::SpectrumConverter sc(curve_specs);

  // 1: Try to solve
  Eigen::VectorXd eigen_result = rgb2spec_for_ill(a_color, sc);

#ifdef RGB2SPEC_TRACE
  std::cout << " Solution looks like: " << std::endl;
#endif

  for (size_t i = 0; i < sc.nbWavelengths(); i++)
  {
#ifdef RGB2SPEC_TRACE
    std::cout << " [ " << i << " ] = " << eigen_result(i) << std::endl;
#endif

    result.add(sc.sensCurveFirstWavelength() + (mrf::uint)i, static_cast<float>(eigen_result(i)));
  }


  // 2 : Verify accuracy of the result by projecting the obtaind Spectrum
  mrf::color::Color const obtained_color = sc.emissiveSpectrumToXYZ(result);
  mrf::math::Mat4f const &rgb_to_xyz     = mrf::color::getRGBtoXYZMat(mrf::color::CIE_RGB, mrf::color::E);
  mrf::math::Vec4f const  xyz_color      = rgb_to_xyz * a_color;

  std::cout << "[INFO] Error   on X = " << (xyz_color(0) - obtained_color[0])
            << "  Y = " << (xyz_color(1) - obtained_color[1]) << "  Z = " << (xyz_color(2) - obtained_color[2])
            << std::endl;

  // FIX ME :
  //   mrf::color::Color const obtained_color_rgb  = mrf::color::Color::XYZ_TO_CIE_RGB_E_MATRIX * obtained_color ;

  // std::cout << "[INFO] Error   on R = " << (obtained_color_rgb(0) - a_color[0])
  //             << "  G = " << (obtained_color_rgb(1) - a_color[1])
  //             << "  B = " << (obtained_color_rgb(2) - a_color[2]) << std::endl;
}


// This assigns a spectrum represented as Eigen::VectorXd to the pixel
/**
 * @brief Set the spectrum to the pixel (i,j) of the given UniformSpectralImage
 *
 * Note that negative spectrum will be clamped to zero
 *
 * @param img_result : The UniformSpectralImage that will be modified at pixel(i,j)
 * @param i  : pixel coordinate
 * @param j  : pixel coordinate
 * @param eigen_result : VectorXd representing the spectrum
 */
inline void set_spectrum(
    mrf::image::UniformSpectralImage &img_result,
    mrf::uint                         column_index,
    mrf::uint                         row_index,
    Eigen::Ref<Eigen::VectorXd>       eigen_result)
{
  using namespace mrf;
  std::vector<mrf::data_struct::Array2D> &spectral_data = img_result.data();

  for (uint k = 0; k < img_result.wavelengths().size(); k++)
  {
    mrf::data_struct::Array2D &slice = spectral_data[k];
    slice(row_index, column_index)   = (eigen_result(k) < 0.0) ? 0.f : static_cast<float>(eigen_result(k));
  }
}

inline void set_spectrum_debug(
    mrf::image::UniformSpectralImage &img_result,
    mrf::uint                         column_index,
    mrf::uint                         row_index,
    mrf::math::Vec3f const &          a_color)
{
  using namespace mrf;
  std::vector<mrf::data_struct::Array2D> &spectral_data = img_result.data();

  for (uint k = 0; k < img_result.wavelengths().size(); k++)
  {
    mrf::data_struct::Array2D &slice = spectral_data[k];
    slice(row_index, column_index)   = (a_color.x());
  }
}

void convert_image_to_ill_SpectralImage(
    mrf::image::ColorImage const &    img,
    mrf::image::UniformSpectralImage &img_result,
    mrf::color::SensitivityCurveName  curve_specs = mrf::color::CIE_1931_2DEG)
{
  unsigned long long int total_size_in_bytes = sizeof(mrf::color::Color) * img.size();
  std::cout << "[INFO] Total Size in Bytes of Color Image: " << total_size_in_bytes << " bytes "
            << ". Eq in MBytes = " << total_size_in_bytes / (1024.0 * 1024.0)
            << ". Eq in GBytes = " << total_size_in_bytes / (1024.0 * 1024.0 * 1024.0) << std::endl;

  mrf::color::SpectrumConverter sc(curve_specs);

  unsigned long long int requested_size_in_bytes_for_spectral_img
      = img.size() * img_result.wavelengths().size() * sizeof(float);

  std::cout << "[INFO] Size of input image width,height = " << img.width() << ", " << img.height() << std::endl;
  std::cout << "[INFO] Number of Wavelength for Image = " << img_result.wavelengths().size() << std::endl;

  std::cout << "[INFO] Total Size in Bytes for Spectral Image: " << requested_size_in_bytes_for_spectral_img
            << " bytes "
            << ". Eq in MBytes = " << requested_size_in_bytes_for_spectral_img / (1024.0 * 1024.0)
            << ". Eq in GBytes = " << requested_size_in_bytes_for_spectral_img / (1024.0 * 1024.0 * 1024.0)
            << std::endl;


  std::cout << "[INFO] Available memory in MBytes " << mrf::util::CPUMemory::availableMemory() << std::endl;


  Eigen::MatrixXd sense_matrix;
  Eigen::MatrixXd G;
  Eigen::VectorXd g0;
  Eigen::MatrixXd CE;
  Eigen::MatrixXd CI_matrix;
  Eigen::VectorXd ci0;

  //Vector for the solution
  Eigen::VectorXd pre_solution;

// DESACTIVATED BY DEFALT BECAUSE TOO SLOW FOR AN ENVIRONMENT MAP
#ifdef USE_QUADPROG_CPP_FOR_ILL_SPECTRUM
  prepare_quadratic_prog(sc, sense_matrix, G, g0, CE, CI_matrix, ci0, pre_solution);
#else
  //CE = Eigen::MatrixXd::Zero(3, sc.nbWavelengths());
  CE = Eigen::MatrixXd::Zero(3, img_result.wavelengths().size());
  sensitivity_curves_to_matrix(CE, sc, img_result.wavelengths());
#endif

#ifdef USE_LEAST_SQUARE_FOR_ILL_SPECTRUM
  Eigen::MatrixXd const transpose_CE     = CE.transpose();
  Eigen::Matrix3d const inverse_CE_tCE   = (CE * transpose_CE).inverse();
  Eigen::MatrixXd const trans_inv_CE_tCE = transpose_CE * inverse_CE_tCE;

  std::cout << __FILE__ << "  " << __LINE__ << std::endl;
  std::cout << "  To debug " << std::endl;
  std::cout << inverse_CE_tCE << std::endl;
#endif

  mrf::util::PrecisionTimer a_timer;
  a_timer.start();

#if defined(_OPENMP)
  std::cout << "[INFO] Num of cores available: " << omp_get_num_procs() << std::endl;
  omp_set_num_threads(omp_get_num_procs() - 1);
#endif

#if defined(__GNUC__) && (__GNUC__ <= 8) && !defined(__llvm__) && !defined(__INTEL_COMPILER)
#pragma omp parallel for schedule(static) default(none) shared(img_result, img,sc,G,g0,CE,CI_matrix,ci0,pre_solution)
#endif
#if defined(__GNUC__) && (__GNUC__ > 8) && !defined(__llvm__) && !defined(__INTEL_COMPILER)
#pragma omp parallel for schedule(static) default(none) shared(img_result, img, sc, G, g0, CE, CI_matrix, ci0, pre_solution, trans_inv_CE_tCE)
#endif
#if defined(__clang__)
#pragma omp parallel for schedule(static) default(none) shared(img_result, img, sc, G, g0, CE, CI_matrix, ci0, pre_solution, trans_inv_CE_tCE)
#endif
  for (int i = 0; i < int(img.width()); i++)
  {
    for (size_t j = 0; j < img.height(); j++)
    {
      mrf::color::Color a_color(img(i, j).r(), img(i, j).g(), img(i, j).b());

#ifdef USE_QUADPROG_CPP_FOR_ILL_SPECTRUM
      Eigen::VectorXd ce0(3);
      ce0(0) = -a_color(0);
      ce0(1) = -a_color(1);
      ce0(2) = -a_color(2);

      Eigen::VectorXd solution = pre_solution;
      QuadProgPP::solve_quadprog(G, g0, CE, ce0, CI_matrix, ci0, solution);
      //TODO Copy VectorXd to UniformSpectralIMage
      //set_spectrum(img_result, i, j, solution);
      //set_spectrum_debug( img_result, i, j, a_color);
#endif

#ifdef USE_LEAST_SQUARE_FOR_ILL_SPECTRUM
      //mrf::math::Vec3f linear_color = a_color;
      //linear_color.pow(1.0f);

      // Hard-to Choose. Probably give the user the CHOICE ?
      mrf::math::Mat4f const &rgb_to_xyz = mrf::color::getRGBtoXYZMat(mrf::color::CIE_RGB, mrf::color::E);
      mrf::math::Vec4f const  xyz_color  = rgb_to_xyz * a_color;

      Eigen::VectorXd ce0(3);
      ce0(0)                   = xyz_color(0);
      ce0(1)                   = xyz_color(1);
      ce0(2)                   = xyz_color(2);
      Eigen::VectorXd solution = trans_inv_CE_tCE * ce0;
#else
      //Non negative Least Square
      mrf::math::Vec3f const xyz_color = mrf::color::Color::CIE_RGB_E_XYZ_MATRIX * a_color;
      Eigen::VectorXd        ce0(3);
      ce0(0)                   = xyz_color(0);
      ce0(1)                   = xyz_color(1);
      ce0(2)                   = xyz_color(2);
      Eigen::VectorXd solution = Eigen::VectorXd::Zero(sc.nbWavelengths());
      Eigen::NNLS<Eigen::MatrixXd>::solve(CE, ce0, solution);
#endif

      //Negative Values wil be clamped by set_spectrum function
      set_spectrum(img_result, mrf::uint(i), mrf::uint(j), solution);
    }
    //std::cout << " Row : " << i << " completed" << std::endl;
  }
  a_timer.stop();
  std::cout << "[INFO] Conversion of Image to Spectral Image. " << a_timer << std::endl;
}

inline void export_color_image_to_spectral(
    mrf::image::ColorImage const &    img,
    mrf::image::UniformSpectralImage &img_result,
    mrf::color::SensitivityCurveName  curve_specs     = mrf::color::CIE_1931_2DEG,
    std::string                       output_filename = "output")
{
  convert_image_to_ill_SpectralImage(img, img_result, curve_specs);

  //img_result.spectralDownsampling(10);


  std::string comments = " Generated by rgb2spec Application";

  img_result.save(output_filename);
}
