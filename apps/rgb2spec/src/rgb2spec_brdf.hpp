/**
 * @file rgb2spec_brdf.hpp
 * @author Romain Pacanowski (romain.pacanowski@institutoptique.fr)
 * @brief
 * @version 0.1
 * @date 2018-11-15
 *
 * @copyright CNRS Copyright (c) 2018
 *
 */

#pragma once

//STL
#include <iterator>
#include <numeric>
#include <exception>

// MRF
#include <mrf_core/radiometry/spectrum.hpp>
#include <mrf_core/math/math.hpp>
#include <mrf_core/color/color.hpp>
#include <mrf_core/color/spectrum_converter.hpp>
#include <mrf_core/color/sensitivity_curve_cie_1931_2deg.hpp>
#include <mrf_core/util/cpu_memory.hpp>
#include <mrf_core/util/precision_timer.hpp>

#define USE_QUADPROG_CPP_FOR_REFLECTIVITY
#ifdef USE_QUADPROG_CPP_FOR_REFLECTIVITY
#  include <QuadProgPP.h>
#endif


inline void sensitivity_ill_curves_to_matrix(
    Eigen::MatrixXd &                    sense_ill_matrix,
    mrf::color::SpectrumConverter const &sc,
    mrf::radiometry::Spectrum const &    ill_spectrum,
    std::vector<mrf::uint> const &       requested_wavelengths)
{
  using mrf::uint;

  using namespace mrf::color;
  using namespace Eigen;

  mrf::math::Vec3f current_xyz_value;


  double luminance_normalization_cte = 0.0f;

  for (size_t i = 0; i < requested_wavelengths.size(); i++)
  {
    uint const requested_wav = requested_wavelengths[i];

    current_xyz_value = sc.xyzValuesAtWavelength(requested_wav);

    float const curr_ill_value_at_wav = ill_spectrum.findLinearyInterpolatedValue(requested_wav);

    //First Row
    sense_ill_matrix(0, i) = curr_ill_value_at_wav * current_xyz_value(0);
    //Second Row
    sense_ill_matrix(1, i) = curr_ill_value_at_wav * current_xyz_value(1);
    //Third Row
    sense_ill_matrix(2, i) = curr_ill_value_at_wav * current_xyz_value(2);

    luminance_normalization_cte += sense_ill_matrix(1, i);
  }

  luminance_normalization_cte = 1.0 / luminance_normalization_cte;

  const double integration_cte = static_cast<double>(requested_wavelengths.back() - requested_wavelengths.front())
                                 / static_cast<double>(requested_wavelengths.size() - 1);

  sense_ill_matrix *= integration_cte * luminance_normalization_cte;


  std::cout << __FILE__ << " " << __LINE__ << " DEBUGING HERE " << std::endl;
  Eigen::VectorXd const x_test = sense_ill_matrix * Eigen::VectorXd::Constant(requested_wavelengths.size(), 1);
  std::cout << x_test(0) << " " << x_test(1) << " " << x_test(2) << std::endl;
  std::cout << __FILE__ << " " << __LINE__ << " DEBUGING HERE " << std::endl;
}

void displayEigenMatrix(Eigen::MatrixXd const &a_matrix, size_t max_rows = 10, size_t max_columns = 10)
{
  std::cout << "  Matrix = " << std::endl;
  for (size_t i = 0; i < max_rows; i++)
  {
    for (size_t j = 0; j < max_columns; j++)
    {
      std::cout << a_matrix(i, j) << " ";
    }
    std::cout << std::endl;
  }
}

inline void prepare_quadratic_prog_for_reflectivity(
    mrf::color::SpectrumConverter const &   sc,
    std::vector<mrf::uint> const &          requested_wavelengths,
    mrf::radiometry::MRF_ILLUMINANTS const &illuminant_type,
    Eigen::MatrixXd &                       G,
    Eigen::VectorXd &                       g0,
    Eigen::MatrixXd &                       CE,
    Eigen::MatrixXd &                       CI_matrix,
    Eigen::VectorXd &                       ci0,
    Eigen::VectorXd &                       pre_solution)
{
  //Forcing to solve Spectrum at 1nm resolution
  // between 380 and 830
  //size_t const SIZE_SENSITIVITY_CIE_CURVES = sc.nbWavelengths();
  size_t const SIZE_SENSITIVITY_CIE_CURVES = requested_wavelengths.size();

  // Minimize ||x||^2
  G = Eigen::MatrixXd::Identity(SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES);

  // MInimize || Laplacian(x) ||^2
  G              = -2.0f * Eigen::MatrixXd::Identity(SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES);
  G.diagonal(+1) = Eigen::VectorXd::Constant(SIZE_SENSITIVITY_CIE_CURVES - 1, 1.0f);
  G.diagonal(-1) = Eigen::VectorXd::Constant(SIZE_SENSITIVITY_CIE_CURVES - 1, 1.0f);
  G *= 4.0f;
  //displayEigenMatrix(G);

  g0 = Eigen::VectorXd::Zero(SIZE_SENSITIVITY_CIE_CURVES);

  //Constraints Equality
  CE                                     = Eigen::MatrixXd::Zero(3, SIZE_SENSITIVITY_CIE_CURVES);
  mrf::radiometry::Spectrum ill_spectrum = mrf::radiometry::illuminantSpectrum(illuminant_type);

  sensitivity_ill_curves_to_matrix(CE, sc, ill_spectrum, requested_wavelengths);
  CE.transposeInPlace();

  //displayEigenMatrix(CE);

  // Enforce A positive Solution
  //CI^T*X + ci0 >= 0
  bool reflectivity_constraint = false;
  if (reflectivity_constraint)   //also enforce a solution bounded by 1
  {
    //TODO: FIX ME BELOW !
    CI_matrix = Eigen::MatrixXd::Zero(SIZE_SENSITIVITY_CIE_CURVES, 2 * SIZE_SENSITIVITY_CIE_CURVES);

    CI_matrix.block(0, 0, SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES)
        = Eigen::MatrixXd::Identity(SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES);
    CI_matrix.block(0, SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES)
        = -1.0f * Eigen::MatrixXd::Identity(SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES);

    ci0 = Eigen::VectorXd::Constant(2 * SIZE_SENSITIVITY_CIE_CURVES, 0.00000f);   // Minimal value, epsilon

    ci0.segment(SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES)
        = Eigen::VectorXd::Constant(SIZE_SENSITIVITY_CIE_CURVES, 1.0);

    displayEigenMatrix(CI_matrix, 2, 600);
  }
  else
  {
    CI_matrix = Eigen::MatrixXd::Identity(SIZE_SENSITIVITY_CIE_CURVES, SIZE_SENSITIVITY_CIE_CURVES);


    //ci0 = Eigen::VectorXd::Zero(SIZE_SENSITIVITY_CIE_CURVES);
    ci0 = Eigen::VectorXd::Constant(SIZE_SENSITIVITY_CIE_CURVES, 0.00001);   // Minimal value, epsilon
  }




  pre_solution = Eigen::VectorXd::Constant(SIZE_SENSITIVITY_CIE_CURVES, 1.0f);
}




//---------------------------------------------------------------------------------------------
//
// REFLECTIVITY PART
//
//---------------------------------------------------------------------------------------------
Eigen::VectorXd rgb2spec_for_brdf(
    mrf::color::Color const &               a_color,
    mrf::color::SpectrumConverter const &   sc,
    mrf::radiometry::MRF_ILLUMINANTS const &illuminant_type)
{
  using mrf::uint;

  using namespace std;
  using namespace mrf::color;

  mrf::radiometry::Spectrum result;

  mrf::math::Vec4f linear_rgb = a_color.pow(2.2f);
  std::cout << " linear_rgb= = " << linear_rgb << std::endl;

  mrf::math::Mat4f const &rgb_to_xyz = mrf::color::getRGBtoXYZMat(mrf::color::CIE_RGB, mrf::color::E);
  mrf::math::Vec4f const  xyz_color  = rgb_to_xyz * a_color;
  std::cout << " Original Color = " << a_color << "  XYZ Color " << xyz_color << std::endl;
  float const total_xyz = xyz_color(0) + xyz_color(1) + xyz_color(2);
  std::cout << " xyY = " << xyz_color(0) / total_xyz << " " << xyz_color(2) / total_xyz << " " << xyz_color(1)
            << std::endl;

  mrf::math::Vec4f xyz_color_from_linear = rgb_to_xyz * linear_rgb;
  std::cout << " XYZ Color from linear " << xyz_color_from_linear << std::endl;

  // Target Equality Constraints
  Eigen::VectorXd ce0(3);
  ce0(0) = xyz_color(0);
  ce0(1) = xyz_color(1);
  ce0(2) = xyz_color(2);

  //Vector for the solution
  Eigen::VectorXd solution;

  //Equality Constraints
  Eigen::MatrixXd CE;

  mrf::util::PrecisionTimer ptimer;

  Eigen::MatrixXd se;
  Eigen::MatrixXd G;
  Eigen::VectorXd g0;

  Eigen::MatrixXd CI_matrix;
  Eigen::VectorXd ci0;

  std::vector<uint> requested_wavelengths = sc.wavelengths();

  ptimer.start();
  prepare_quadratic_prog_for_reflectivity(
      sc,
      requested_wavelengths,
      illuminant_type,
      G,
      g0,
      CE,
      CI_matrix,
      ci0,
      solution);

  //QuadProg Sover call
  double const cost_returned = QuadProgPP::solve_quadprog(G, g0, CE, -ce0, CI_matrix, -ci0, solution);
  ptimer.stop();
  ptimer.reset();
  cout << "[INFO] Inverse problem done in  " << ptimer.elapsed() * 1000 << "ms with cost returned: " << cost_returned
       << endl;

  if (std::isinf(cost_returned))
  {
    cout << " SOLVER DID NOT FIND ANY SOLUTION " << endl;
    throw runtime_error("COULD NOT SOLVE PROBLEM WITH QUADRATIC PROGRAMMING");
  }

  //Check that the Spectrum is between 0 and 1
  std::cout << "[INFO] Checking Spectrum " << std::endl;
  for (size_t i = 0; i < sc.nbWavelengths(); i++)
  {
    // std::cout << " solution( " << i << " ) = " << solution(i) << std::endl;

    if (solution(i) > 1.0)
    {
#ifdef RGB2SPEC_TRACE
      cout << "[Warning] Reflectivity > 1 at " << sc.wavelengthValue(i) << " nm" << endl;
#endif
      solution(i) = 1.0f;
    }
    else if (solution(i) < 0.0)
    {
#ifdef RGB2SPEC_TRACE
      cout << "[Warning] Reflectivity < 0  at " << sc.wavelengthValue(i) << endl;
#endif
      solution(i) = 0.0f;
    }
  }

  return solution;
}

void convert_rgb_to_brdf_spectrum(
    mrf::math::Vec3f const &                a_color,
    mrf::radiometry::Spectrum &             result,
    mrf::radiometry::MRF_ILLUMINANTS const &illuminant_type = mrf::radiometry::NONE,
    mrf::color::SensitivityCurveName        curve_specs     = mrf::color::CIE_1931_2DEG)
{
  std::cout << "[INFO] Solving for BRDF Spectrum " << illuminant_type << std::endl;


  // Allocate the proper SpectrumConverter
  mrf::color::SpectrumConverter sc(curve_specs);

  // 1: Try to solve
  Eigen::VectorXd eigen_result = rgb2spec_for_brdf(a_color, sc, illuminant_type);

  // #ifdef RGB2SPEC_TRACE
  //   std::cout << " Solution looks like: " << std::endl;
  // #endif

  for (size_t i = 0; i < sc.nbWavelengths(); i++)
  {
    // #ifdef RGB2SPEC_TRACE
    //     std::cout << " [ " << i << " ] = " << eigen_result(i) << std::endl;
    // #endif
    result.add(sc.sensCurveFirstWavelength() + (mrf::uint)i, static_cast<float>(eigen_result(i)));
  }

  // 2 : Verify accuracy of the result by projecting the obtaind Spectrum
  //  mrf::color::Color obtained_color = sc.emissiveSpectrumToXYZ(result);
  // TODO : compute the reprojection error


  std::cout << "[INFO] DONE " << std::endl;
}
